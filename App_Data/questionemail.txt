﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns:fb="https://www.facebook.com/2008/fbml" xmlns:og="https://opengraph.org/schema/"><head>
        
<meta property="og:title" content="Thank you for signing up with %CompanyName% Let’s get started!">
<meta property="fb:page_id" content="43929265776">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">        
  <meta https-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Welcome to %CompanyName% !</title>
  
<style type="text/css">
    #outlook a{
      padding:0;
    }
    body{
      width:100% !important;
      -webkit-text-size-adjust:100%;
      -ms-text-size-adjust:100%;
      margin:0;
      padding:0;
      -webkit-font-smoothing:antialiased;
    }
    .ExternalClass{
      width:100%;
    }
    .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{
      line-height:100%;
    }
    .backgroundTable{
      margin:0;
      padding:0;
      width:100% !important;
      line-height:100% !important;
    }
    img{
      outline:none;
      text-decoration:none;
      border:none;
      -ms-interpolation-mode:bicubic;
    }
    a img{
      border:none;
    }
    .image_fix{
      display:block;
    }
    p{
      margin:0px 0px !important;
    }
    table td{
      border-collapse:collapse;
    }
    table{
      border-collapse:collapse;
      mso-table-lspace:0pt;
      mso-table-rspace:0pt;
    }
    a{
      color:#33b9ff;
      text-decoration:none;
    }
    table[class=full]{
      width:100%;
      clear:both;
    }
  @media only screen and (max-width: 640px){
    a[href^=tel],a[href^=sms]{
      text-decoration:none;
      color:#0a8cce;
      cursor:default;
    }

} @media only screen and (max-width: 640px){
    .mobile_link a[href^=tel],.mobile_link a[href^=sms]{
      text-decoration:default;
      color:#0a8cce !important;
      pointer-events:auto;
      cursor:default;
    }

} @media only screen and (max-width: 640px){
		table[id=templatePreheader]{
			display:none;
		}

} @media only screen and (max-width: 640px){
    table[class=devicewidth]{
      width:440px !important;
      text-align:left !important;
    }

} @media only screen and (max-width: 640px){
    table[class=devicewidthmob]{
      width:420px !important;
      text-align:left !important;
    }

} @media only screen and (max-width: 640px){
    table[class=devicewidthinner]{
      width:420px !important;
      text-align:left !important;
    }

} @media only screen and (max-width: 640px){
    table[class=col2header]{
      width:210px !important;
      text-align:left !important;
    }

} @media only screen and (max-width: 640px){
    img[class=col2img]{
      width:440px !important;
      height:330px !important;
    }

} @media only screen and (max-width: 640px){
    table[class=cols3inner]{
      width:100px !important;
    }

} @media only screen and (max-width: 640px){
    table[class=col3img]{
      width:131px !important;
    }

} @media only screen and (max-width: 640px){
    img[class=col3img]{
      width:131px !important;
      height:82px !important;
    }

} @media only screen and (max-width: 640px){
    table[class=removeMobile]{
      width:10px !important;
    }

} @media only screen and (max-width: 640px){
    img[class=blog]{
      width:420px !important;
      height:162px !important;
    }

} @media only screen and (max-width: 640px){
    table[class=cta-bottom]{
      width:100% !important;
    }

} @media only screen and (max-width: 640px){
    img[class=banner]{
      width:440px !important;
      height:121px !important;
    }

} @media only screen and (max-width: 640px){
    table[class=product]{
      width:420px !important;
    }

} @media only screen and (max-width: 640px){
    table[class=productimg]{
      width:120px !important;
    }

} @media only screen and (max-width: 640px){
    table[class=productdesc]{
      width:270px !important;
    }

} @media only screen and (max-width: 480px){
    a[href^=tel],a[href^=sms]{
      text-decoration:none;
      color:#0a8cce;
      cursor:default;
    }

} @media only screen and (max-width: 480px){
    .mobile_link a[href^=tel],.mobile_link a[href^=sms]{
      text-decoration:default;
      color:#0a8cce !important;
      pointer-events:auto;
      cursor:default;
    }

} @media only screen and (max-width: 480px){
    table[class=devicewidth]{
      width:280px !important;
      text-align:left !important;
    }

} @media only screen and (max-width: 480px){
    table[class=devicewidthmob]{
      width:260px !important;
      text-align:left !important;
    }

} @media only screen and (max-width: 480px){
    table[class=devicewidthinner]{
      width:260px !important;
      text-align:left !important;
    }

} @media only screen and (max-width: 480px){
    table[class=col2header]{
      width:100% !important;
      text-align:center !important;
    }

} @media only screen and (max-width: 480px){
    img[class=col2img]{
      width:280px !important;
      height:210px !important;
    }

} @media only screen and (max-width: 480px){
    table[class=cols3inner]{
      width:260px !important;
    }

} @media only screen and (max-width: 480px){
    img[class=col3img]{
      width:280px !important;
      height:175px !important;
    }

} @media only screen and (max-width: 480px){
    table[class=col3img]{
      width:280px !important;
    }

} @media only screen and (max-width: 480px){
    img[class=blog]{
      width:260px !important;
      height:100px !important;
    }

} @media only screen and (max-width: 480px){
    td[class=padding-top-right15]{
      padding:15px 15px 0 0 !important;
      border-bottom:1px solid #dbe1e5;
    }

} @media only screen and (max-width: 480px){
    td[class=padding-right15]{
      padding-right:15px !important;
    }

} @media only screen and (max-width: 480px){
    a[class=headerbtn]{
      float:none !important;
      width:100% !important;
      margin-top:10px !important;
      border-right:0 !important;
      border-left:0 !important;
    }

} @media only screen and (max-width: 480px){
    span[class=switcher]{
      display:block;
      background-image:url(https://gallery.mailchimp.com/1afb11ec7c60fd3bc744d3f2e/images/7b5fd90c-ea20-43f3-9089-18c555cdcea8.jpg) !important;
      background-repeat:no-repeat !important;
      background-position:center !important;
      background-size:280px 162px;
      width:280px !important;
      height:162px !important;
    }

} @media only screen and (max-width: 480px){
    img[class=banner]{
      display:none !important;
    }

} @media only screen and (max-width: 480px){
    td[class=socialicons]{
      text-align:left !important;
      padding-top:10px;
    }

} @media only screen and (max-width: 480px){
    td[class=footermenu]{
      text-align:left !important;
    }

} @media only screen and (max-width: 480px){
    table[class=product]{
      width:100% !important;
    }

} @media only screen and (max-width: 480px){
    table[class=productimg]{
      display:none;
      !important;:table[class="productdesc"] {width:260px !important;
    }

}</style>        <script type="text/javascript">
            var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "https://www.");
            document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
            </script><script src="Welcome%20to%20CallFire%21_files/ga.js" type="text/javascript"></script>
            <script type="text/javascript">
            try {
                var _gaq = _gaq || [];
                _gaq.push(["_setAccount", "UA-329148-88"]);
                _gaq.push(["_setDomainName", ".campaign-archive.com"]);
                _gaq.push(["_trackPageview"]);
                _gaq.push(["_setAllowLinker", true]);
            } catch(err) {console.log(err);}</script>
                    <script src="Welcome%20to%20CallFire%21_files/jquery.js"></script> <script src="Welcome%20to%20CallFire%21_files/fancyzoom.js"></script>  <script type="text/javascript">
    function incrementFacebookLikeCount() {
        var current = parseInt($('#campaign-fb-like-btn span').html());
        $('#campaign-fb-like-btn span').fadeOut().html(++current).fadeIn();
    }

    function getUrlParams(str) {
        var vars = {}, hash;
        if (!str) return vars;
        var hashes = str.slice(str.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
    
    function setupSocialSharingStuffs() {
        var numSocialElems = $('a[rel=socialproxy]').length;
        var numSocialInitialized = 0;
        var urlParams = getUrlParams(window.document.location.href);
        var paramsToCopy = {'e':true, 'eo':true};
        $('a[rel=socialproxy]').each(function() {
            var href = $(this).attr('href');
            var newHref = decodeURIComponent(href.match(/socialproxy=(.*)/)[1]);
            // for facebook insanity to work well, it needs to all be run against just campaign-archive
            newHref = newHref.replace(/campaign-archive(\d)/gi, 'campaign-archive');
            var newHrefParams = getUrlParams(newHref);
            for(var param in urlParams) {
                if ((param in paramsToCopy) && !(param in newHrefParams)) {
                    newHref += '&' + param + '=' + urlParams[param];
                }
            }
            $(this).attr('href', newHref);
            if (href.indexOf('facebook-comment') !== -1) {
                $(this).fancyZoom({"zoom_id": "social-proxy", "width":620, "height":450, "iframe_height": 450});
            } else {
                $(this).fancyZoom({"zoom_id": "social-proxy", "width":500, "height":200, "iframe_height": 500});
            }
            numSocialInitialized++;
                    });
    }
	if (window.top!=window.self){
        $(function() {
          var iframeOffset = $("#archive", window.parent.document).offset();
          $("a").each(function () {
              var link = $(this);
              var href = link.attr("href");
              if (href && href[0] == "#") {
                  var name = href.substring(1);
                  $(this).click(function () {
                      var nameElement = $("[name='" + name + "']");
                      var idElement = $("#" + name);
                      var element = null;
                      if (nameElement.length > 0) {
                          element = nameElement;
                      } else if (idElement.length > 0) {
                          element = idElement;
                      }
         
                      if (element) {
                          var offset = element.offset();
                          var height = element.height();
                          //3 is totally arbitrary, but seems to work best.
                          window.parent.scrollTo(offset.left, (offset.top + iframeOffset.top - (height*3)) );
                      }
         
                      return false;
                  });
              }
          });
        });
    }
</script>  <script type="text/javascript">
            $(document).ready(function() {
                setupSocialSharingStuffs();
            });
        </script> <style type="text/css">
            /* Facebook/Google+ Modals */
            #social-proxy { background:#fff; -webkit-box-shadow: 4px 4px 8px 2px rgba(0,0,0,.2); box-shadow: 4px 4px 8px 2px rgba(0,0,0,.2); padding-bottom:35px; z-index:1000; }
            #social-proxy_close { display:block; position:absolute; top:0; right:0; height:30px; width:32px; background:transparent url(https://cdn-images.mailchimp.com/awesomebar-sprite.png) 0 -200px; text-indent:-9999px; outline:none; font-size:1px; }
            #social-proxy_close:hover { background-position:0 -240px; }
            body { padding-bottom:50px !important; }
        </style> </head>
    
    <body style="background-color: #dbe1e5;" bgcolor="#dbe1e5">
<div style="display:none; white-space:nowrap; font:15px courier; color:#ffffff; line-height:0; width:600px !important; min-width:600px !important; max-width:600px !important;">&nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
  <!-- Start of main-banner -->
 
  <!-- End of main-banner -->
  <!-- heading -->
  <table class="backgroundTable" style="font-family: Helvetica, Arial, sans-serif;margin-top:20px;" bgcolor="#dbe1e5" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
      <tr>
        <td>
          <table class="devicewidth" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
            <tbody>
              <tr>
                <td width="100%">
                  <table class="devicewidth" align="center" bgcolor="#002c47" border="0" cellpadding="0" cellspacing="0" width="600">
                    <tbody>
                      <!-- Spacing -->
                      <tr>
                       <td style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;" height="10">&nbsp;</td>
                     </tr>
                     <!-- Spacing -->
                     <tr>
                      <td>
                        <table class="devicewidthinner" align="center" border="0" cellpadding="0" cellspacing="0" width="560">
                          <tbody>
                            <tr>
                              <td>
                                <!-- col 1 -->
                                <table class="col2header" align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                 <tbody>
                                  <!-- image 2 -->
                                  <tr>
                                   <td style="font-family: Helvetica, Arial, sans-serif; font-size: 20px; color: #fff; line-height: 24px;">
                                   Field Technicians Application and Questionaire
                                  </td>
                                </tr>
                                <!-- end of image2 -->
                              </tbody>
                            </table>
                            <!-- col 2 -->
                            <table class="col2header" align="right" border="0" cellpadding="0" cellspacing="0" width="270">
                             <tbody>
                              <!-- image 2 -->
                              <tr>
                               <td>
                                
                              </td>
                            </tr>
                            <!-- end of image2 -->
                          </tbody>
                        </table>
                        <!-- end of col 2 -->
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <!-- Spacing -->
            <tr>
             <td style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;" height="10">&nbsp;</td>
           </tr>
           <!-- Spacing -->
         </tbody>
       </table>
     </td>
   </tr>
 </tbody>
</table>
</td>
</tr>
</tbody>
</table>
<!-- end of heading -->

<!-- fulltext -->
<table class="backgroundTable" st-sortable="left-image" style=" font-family: Helvetica, Arial, sans-serif;" bgcolor="#dbe1e5" border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td>
        <table class="devicewidth" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
          <tbody>
            <tr>
              <td width="100%">
                <table class="devicewidth" align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="600">
                  <tbody>
                    <!-- Spacing -->
                    <tr>
                      <td>
					<table class="devicewidthinner" align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="560">
                         <tbody>
                          <tr>
                            <td style="font-size:14px;  font-family: Helvetica, Arial, sans-serif; background-color: #fff; color: #252525;" bgcolor="#FFFFFF">
                   
				   <h2>Personal Information</h2>
				   <hr/>
                        </td>
						
                      </tr>
                <tr>
                 <td style="font-size:1px; line-height:1px; mso-line-height-rule: exactly; background-color: #fff;" bgcolor="#FFFFFF" height="20">&nbsp;</td>
               </tr>
               <!-- Spacing -->
             </tbody>
           </table>
    </td>
                    </tr>
                    <!-- Spacing -->
                    <tr>
                      <td>
                        <table class="devicewidthinner" align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="560">
                         <tbody>
                          <tr>
                            <td style="font-size:14px; font-family: Helvetica, Arial, sans-serif; background-color: #fff; color: #252525;" bgcolor="#FFFFFF">
                   
							<p style="margin: 0; padding: 0 0 10px;"><b>First name :</b> @fname</p>
                            
                        </td>
						 <td style="font-size:14px; font-family: Helvetica, Arial, sans-serif; background-color: #fff; color: #252525;" bgcolor="#FFFFFF">
                   
							<p style="margin: 0; padding: 0 0 10px;"><b>Last name :</b> @lname</p>
                            
                        </td>
                      </tr>
					  <tr>
                            <td style="font-size:14px; font-family: Helvetica, Arial, sans-serif; background-color: #fff; color: #252525;" bgcolor="#FFFFFF">
                   
						<p style="margin: 0; padding: 0 0 10px;"><b>Company :</b> @company</p>	
                            
                        </td>
						 <td style="font-size:14px; font-family: Helvetica, Arial, sans-serif; background-color: #fff; color: #252525;" bgcolor="#FFFFFF">
                   
							<p style="margin: 0; padding: 0 0 10px;"><b>Email :</b> @email</p>
                            
                        </td>
                      </tr>
</tr>
					  <tr>
                            <td style="font-size:14px; font-family: Helvetica, Arial, sans-serif; background-color: #fff; color: #252525;" bgcolor="#FFFFFF">
                   <p style="margin: 0; padding: 0 0 10px;"><b>Cell phone number :</b> @Cell</p>
                            
                        </td>
						 <td style="font-size:14px; font-family: Helvetica, Arial, sans-serif; background-color: #fff; color: #252525;" bgcolor="#FFFFFF">
                   
							<p style="margin: 0; padding: 0 0 10px;"><b>Alternative phone :</b> @alternatrivephone</p>
                            
                        </td>
                      </tr>
					  <tr>
						 <td colspan="2" style="font-size:14px; font-family: Helvetica, Arial, sans-serif; background-color: #fff; color: #252525;" bgcolor="#FFFFFF">
                   
							<p style="margin-top: 50px; padding: 0 0 10px;"></p>
                            
                        </td>
                      </tr>
                      </tr>
               
               <!-- Spacing -->
             </tbody>
           </table>
         </td>
       </tr>
     </tbody>
   </table>
 </td>
</tr>
</tbody>
</table>
<!-- end of fulltext -->
<!-- Start of footer -->
 
<!-- End of footer -->

    

</body></html>