﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace hrm
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "main", action = "index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "client",
                url: "client",
                defaults: new { controller = "client", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "company",
               url: "company",
               defaults: new { controller = "company", action = "Index", id = UrlParameter.Optional }
           );
        }
    }
}
