﻿using hrm.Database;
using hrm.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace hrm.Areas.form.Controllers
{
    public class indexController : Controller
    {
        // GET: form/index
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult done(int test = 1, int type = 1)
        {
            if (test == 0)
            {
                Session["end"] = "test";
            }
            if (Session["end"] == null)
            {
                return RedirectToAction("index");
            }
            Session["end"] = null;
            string view = type == 1 ? "done" : "done1";
            return View(view);
        }



        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");
            if (model != null)
                ViewData.Model = model;
            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString().Replace("<br>", "<br/>");
            }
        }
        public string getquestionEmail()
        {
            string empty = string.Empty;
            string str = string.Empty;
            try
            {
                string str1 = "~/App_Data/questionemail.txt";
                str = System.IO.File.ReadAllText(Server.MapPath(str1));
            }
            catch (Exception exception1)
            {
            }
            return str;
        }

        public void sendemail(string Certification = "", string alternatrivephone = "", string company = "", string to = "3056umesh@gmail.com", string fname = "", string lastname = "", string email = "", string telephone = "", string address = "", string city = "", string state = "", string zipcode = "", string satisfaction = "", string authorized = "", string convicted = "", string convicted_text = "", string Network = "", string Wi_Fi = "", string data = "", string Audio = "", string Personal = "", string Digital = "", string Structured = "", string Fiber = "", string Telephone_chk = "", string Distributed = "", string Levels = "", string Security = "")
        {
            string body = getquestionEmail();
            body = body.Replace("@Certification", Certification).Replace("@company", company).Replace("@alternatrivephone", alternatrivephone).Replace("@Security", Security).Replace("@Electrical", Levels).Replace("@Distributed", Distributed).Replace("@Telephone_chk", Telephone_chk).Replace("@Fiber", Fiber).Replace("@Structured", Structured).Replace("@Digital", Digital).Replace("@Personal", Personal).Replace("@Audio", Audio).Replace("@Data", data).Replace("@Wi_Fi", Wi_Fi).Replace("@Network", Network).Replace("@explain", convicted_text).Replace("@convicted", convicted).Replace("@authorized", authorized).Replace("@citizen", satisfaction).Replace("@zip", zipcode).Replace("@state", state).Replace("@city", city).Replace("@address", address).Replace("@Cell", telephone).Replace("@email", email).Replace("@lname", lastname).Replace("@fname", fname);
            long ticks = DateTime.Now.Ticks;
            string str = fname + "_" + lastname + "-application.pdf";
            string strbody = this.RenderPartialViewToString("~/Views/offer/_viewconvo.cshtml", null);
            strbody = strbody.Replace("$Certification", Certification).Replace("$company", company).Replace("$alternatrivephone", alternatrivephone).Replace("$Security", Security).Replace("$Electrical", Levels).Replace("$Distributed", Distributed).Replace("$Telephone_chk", Telephone_chk).Replace("$Fiber", Fiber).Replace("$Structured", Structured).Replace("$Digital", Digital).Replace("$Personal", Personal).Replace("$Audio", Audio).Replace("$Data", data).Replace("$Wi_Fi", Wi_Fi).Replace("$Network", Network).Replace("$explain", convicted_text).Replace("$convicted", convicted).Replace("$authorized", authorized).Replace("$citizen", satisfaction).Replace("$zip", zipcode).Replace("$state", state).Replace("$city", city).Replace("$address", address).Replace("$Cell", telephone).Replace("$email", email).Replace("$lname", lastname).Replace("$fname", fname);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                StringReader stringReader = new StringReader(strbody);
                Document document = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                PdfWriter instance = PdfWriter.GetInstance(document, new FileStream(Server.MapPath(string.Concat("~/pdffiles/", str)), FileMode.Create));
                document.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(instance, document, stringReader);
                document.Close();
            }
            Crypto _crypt = new Crypto();
            Session["end"] = "1";
            int i = 0;
            string memberid = new memberservices().getLatestMemberId();
            if (!Int32.TryParse(memberid, out i))
            {
                memberid = new helper().get5RandomDidit();
            }
            else
            {
                memberid = (i + 1).ToString();
            }
            membermodal _mdl = new membermodal();
            _mdl.FirstName = fname;
            _mdl.lastname = lastname;
            _mdl.member_id = memberid;
            _mdl.email = email;
            _mdl.membertype = "M";
            _mdl.password = new helper().GetRandomalphanumeric(8);
            _mdl.hourlyrate = "$0.00";
            _mdl.tax = "$0.00";
            _mdl.status = 1007;
            _mdl.roleid = 1013;
            _mdl.gender = "M";
            _mdl.depid = 0;
            _mdl.desid = 0;
            _mdl.background = "Pending";
            _mdl.drugtested = "Pending";
            _mdl.email = email;
            _mdl.phone = telephone;
            _mdl.alertnative_phone = alternatrivephone;
            _mdl.state_id = 0;
            _mdl.city_id = 0;
            _mdl.zip = zipcode;
            _mdl.address = address;
            var mser = new memberservices();
            int userId = Convert.ToInt16(mser.add_new_membergetid(_mdl, 0));
            mser.add_new_doc("Application Form", userId, "/pdffiles/" + str);
            mser.add_new_doc("W9 Form", userId, "/App_Data/W9_Form.pdf");
            mser.add_new_doc("Direct Deposit Payment Form", userId, "/App_Data/Direct_Deposit_Payment_Form.pdf");
            mser.add_new_doc("Subcontractor Agreeement", userId, "/App_Data/Subcontractor_Agreeement.pdf");
            var template = new templateservices().get_template(3);
            List<int> _data = new List<int>();
            _data.Add(userId);
            new jobofferservices().sendoffers2(_data, 3, true, true, template.sms, template.email, "Field Technician Account", false);
            // Sendemailtech(email, bdy, fname + " Field technician application");

            Sendemailtech(to, body, fname.ToTitleCase() + " Field Technician Account");
            //Server.MapPath(string.Concat("~", str))
            // new emailHelper().sendField_Technicians_Application_and_Questionaire(body, Server.MapPath(string.Concat("~/pdffiles/", str)), to);
            //
        }


        public int getcityidbystate(int stateid, string city)
        {
            if (string.IsNullOrEmpty(city))
            {
                return 0;
            }
            else
            {
                return new defaultservices().savegetCityId(stateid, city);
            }

        }

        public ActionResult sendemail_v2(string message = "", string primary = "", string capctha = "", string Certification = "", string alternatrivephone = "", string company = "", string to = "3056umesh@gmail.com", string fname = "", string lastname = "", string email = "", string telephone = "", string address = "", string city = "", string state = "", string zipcode = "", string satisfaction = "", string authorized = "", string convicted = "", string convicted_text = "", string Network = "", string Wi_Fi = "", string data = "", string Audio = "", string Personal = "", string Digital = "", string Structured = "", string Fiber = "", string Telephone_chk = "", string Distributed = "", string Levels = "", string Security = "", int type = 1, int state_id = 0, string zip = "", string Traveldistance = "")
        {
            bool issend = true;
            capctha = Request.Params["g-recaptcha-response"] != null ? Request.Params["g-recaptcha-response"].ToString() : capctha;
            if (string.IsNullOrEmpty(capctha))
            {
                issend = false;
            }
            else
            {
                var res = statichelper.Validate(capctha) == "true" ? true : false;
                if (!res)
                {
                    issend = false;
                }
            }
            if (issend)
            {
                string body = "";
                if (type == 1)
                {
                    if (!string.IsNullOrEmpty(zip) && state_id != 0 && !string.IsNullOrEmpty(city) && !string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(telephone) && !string.IsNullOrEmpty(fname) && !string.IsNullOrEmpty(lastname))
                    {
                        body = "First name : @fname <br/>Last name : @lname <br/>Cell phone number : @Cell <br/>Email : @email <br/>City : @city <br/>State : @state <br/>Zipcode : @zipcode ";
                        Crypto _crypt = new Crypto();
                        Session["end"] = "1";
                        int i = 0;
                        string memberid = new memberservices().getLatestMemberId();
                        if (!Int32.TryParse(memberid, out i))
                        {
                            memberid = new helper().get5RandomDidit();
                        }
                        else
                        {
                            memberid = (i + 1).ToString();
                        }
                        membermodal _mdl = new membermodal();
                        _mdl.FirstName = fname.ToTitleCase();
                        _mdl.lastname = lastname.ToTitleCase();
                        _mdl.member_id = memberid;
                        _mdl.email = email;
                        _mdl.membertype = "T";
                        _mdl.company = "";
                        _mdl.password = new helper().GetRandomalphanumeric(8);
                        _mdl.hourlyrate = "$0.00";
                        _mdl.tax = "$0.00";
                        _mdl.status = 1007;
                        _mdl.roleid = 1013;
                        _mdl.gender = "M";
                        _mdl.depid = 0;
                        _mdl.desid = 0;
                        _mdl.background = "Pending";
                        _mdl.drugtested = "Pending";
                        _mdl.email = email;
                        _mdl.phone = telephone;
                        _mdl.alertnative_phone = "";
                        _mdl.state_id = state_id;
                        _mdl.city_id = getcityidbystate(state_id, city);
                        _mdl.zip = zip;
                        _mdl.address = "";
                        _mdl.drivingdistance = string.IsNullOrEmpty(Traveldistance) ? "50 mile" : Traveldistance;
                        var mser = new memberservices();
                        int userId = Convert.ToInt16(mser.add_new_membergetid(_mdl, 0, true));
                        mser.addmembersignupnotification(userId);
                        var template = new templateservices().get_template(3);
                        List<int> _data = new List<int>();
                        _data.Add(userId);
                        new jobofferservices().sendoffers2(_data, 3, true, true, template.sms, template.email, "Field Technician Account", false);
                        body = body.Replace("@Cell", telephone).Replace("@email", email).Replace("@lname", lastname).Replace("@fname", fname).Replace("@state", state).Replace("@city", city).Replace("@zipcode", zip);
                        Sendemailtech("contact@convotechnology.com", body, fname.ToTitleCase() + " Field Technician Account");
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(telephone) && !string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(company) && !string.IsNullOrEmpty(primary))
                    {
                        body = "Company name : @company <br/>Primary contact : @Primary <br/>Cell phone number : @Cell <br/>Email address : @email <br/>Message : @message";
                        body = body.Replace("@company", company).Replace("@message", message).Replace("@Cell", telephone).Replace("@email", email).Replace("@Primary", primary);
                        Sendemailtech("contact@convotechnology.com", body, fname.ToTitleCase() + " Client Account");
                        var hsmodel = new HubSpotCOmpanyModel()
                        {
                            Name = company.ToTitleCase(),
                            Description = "Company created by Signup form from Taskayak",
                            City = "",
                            state = "",
                            Emailaddress = email,
                            Phonenumber = telephone,
                            message = message,
                            Primarycontact = primary.ToTitleCase()
                        };
                        var HubSpotId = new hubspot().CreateHubSpotCompany(hsmodel);
                        var mser1 = new clientservices();
                        clientmodal_item client = new clientmodal_item();
                        client.name = company.ToTitleCase();
                        client.primarycontactname = primary.ToTitleCase();
                        client.rate = "$0.00";
                        client.address = "";
                        client.city_id = 0;
                        client.state_id = 0;
                        client.zipcode = "";
                        client.email = email;
                        client.phone = telephone;
                        client.HubSpotId = HubSpotId;
                        int id = mser1.add_new_client(client);
                        mser1.addclientsignupnotification(id);
                       
                    }
                }
            }
            var loc = "https://taskayak.com/form/index/done?type=" + type.ToString();
            return Redirect(loc);
        }


        public string Sendemailtech(string _to, string _text, string subject)
        {
            string str = "email sent";
            try
            {
                string smtpEndpoint = "email-smtp.us-east-1.amazonaws.com";
                int port = 587;
                string senderName = "Convo Technologies";
                string senderAddress = "contact@convotechnology.com";
                string toAddress = _to;
                string smtpUsername = "AKIAX27HMUUW47OFXK6R";
                string smtpPassword = "BMViwJZMzP93r6QFU7/fECr/vOeJCNF5lNCfUuQweyL+";
                AlternateView htmlBody = AlternateView.
                            CreateAlternateViewFromString(_text, null, "text/html");
                MailMessage message = new MailMessage();
                message.From = new MailAddress(senderAddress, senderName);
                message.To.Add(new MailAddress(toAddress));
                message.Subject = subject;
                message.AlternateViews.Add(htmlBody);
                using (var client = new System.Net.Mail.SmtpClient(smtpEndpoint, port))
                {
                    // Create a Credentials object for connecting to the SMTP server
                    client.Credentials =
                        new NetworkCredential(smtpUsername, smtpPassword);
                    client.EnableSsl = true;
                    try
                    {

                        client.Send(message);

                    }
                    // Show an error message if the message can't be sent
                    catch (Exception ex)
                    {
                        //     new Comman().LogWrite(Enums.LogType.Error, ex.Message.ToString(), "Utilis", "Sendmail", Enums.Priority.Critical, ex.StackTrace);
                    }
                }
            }
            catch (SmtpException smtpException)
            {
                str = string.Concat("Error : ", smtpException.Message);
            }
            catch (WebException webException1)
            {
                WebException webException = webException1;
                using (WebResponse response = webException.Response)
                {
                    HttpWebResponse httpWebResponse = (HttpWebResponse)response;
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(responseStream))
                        {
                            str = string.Concat("Error : ", streamReader.ReadToEnd());
                        }
                    }
                }
            }
            return str;
        }
    }
}