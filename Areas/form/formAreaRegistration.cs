﻿using System.Web.Mvc;

namespace hrm.Areas.form
{
    public class formAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "form";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "form_default",
                "form/{controller}/{action}/{id}",
                new {Controller="index" ,action = "index", id = UrlParameter.Optional }
            );
        }
    }
}