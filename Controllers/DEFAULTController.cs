﻿using hrm.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    public class defaultController : Controller
    {
        // GET: DEFAULT
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string get_all_active_memberbycompanyid(int id = 0)
        {
            string ddl = "<option value='0'>Manager</option>";
            var deptser = new defaultservices();
            var list = deptser.get_all_active_memberbycompanyid(id);
            foreach (var item in list)
            {
                if (id == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }


        [HttpPost]
        public string get_all_memberid(int id = 0,int cid=0)
        {
            string ddl = "<option value='0'>User Id</option>";
            var deptser = new defaultservices();
            var list = deptser.get_all_active_memberid(cid);
            foreach (var item in list)
            {
                    if (id == item.Key)
                    {
                        ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                    }
                    else
                    {
                        ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                    }
               
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }


        [HttpPost]
        public string get_all_mebertypeprojectmanager(int id = 0)
        {
            string ddl = "<option value='0'>Manager</option>";
            var deptser = new defaultservices();
            var list = deptser.get_all_membertype_projectmanager();
            foreach (var item in list)
            {
                if (id == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }

            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public string get_all_manager_by_clientid(int id = 0, int clientid = 0, int ismulti = 0)
        {
            string ddl = "<option value='0'>Manager</option>";
            if (ismulti == 1)
            {
                ddl = "";
            }
            var deptser = new defaultservices();
            var list = deptser.get_all_manager_byid(id);
            foreach (var item in list)
            {
                if (clientid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }


        [HttpPost]
        public string get_all_manager_by_clientiduserid(int Managerid = 0, int userid = 0, int ismulti = 0)
        {
            string ddl = "<option value='0'>Manager</option>";
            if (ismulti == 1)
            {
                ddl = "";
            }
            var deptser = new defaultservices();
            var list = deptser.get_all_manager_byuserid(userid);
            foreach (var item in list)
            {
                if (Managerid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public string get_all_jobs(int jobid = 0, int ismulti = 0)
        {
            string ddl = "<option value='0'>Job</option>";
            if (ismulti == 1)
            {
                ddl = "";
            }
            var deptser = new defaultservices();
            var list = deptser.get_all_job();
            foreach (var item in list)
            {
                if (jobid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public string get_assigned_jobs(int jobid = 0, int memberid = 0, int ismulti = 0, bool forpay = false)
        {
            string ddl = "<option value='0'>Job</option>";
            if (ismulti == 1)
            {
                ddl = "";
            }
            var deptser = new defaultservices();
            var list = deptser.get_assigned_jobs(memberid, forpay);
            foreach (var item in list)
            {
                if (jobid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public string get_all_clients(int clientid = 0, int ismulti = 0)
        {
            string ddl = "<option value='0'>Client</option>";
            if (ismulti == 1)
            {
                ddl = "";
            }
            var deptser = new defaultservices();
            var list = deptser.get_all_client();
            foreach (var item in list)
            {
                if (clientid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }


        [HttpPost]
        public string get_all_city_by_stateid(int id, int cityid = 0, int ismulti = 0,string city="")
        {
            string ddl = "<option value='0'>City</option>";
            if (ismulti == 1)
            {
                ddl = "";
            }
            if (id != 0)
            {
                var deptser = new defaultservices();
                var list = deptser.get_all_city_by_stateid(id);
                foreach (var item in list)
                {
                    if (!string.IsNullOrEmpty(city))
                    {
                        int[] nums = Array.ConvertAll(city.Split(','), int.Parse);
                        if (nums.Contains(item.Key))
                        {
                            ddl = ddl + "<option selected='selected' value='" + item.Key + "'>" + item.Value + "</option>";

                        }
                        else
                        {
                            ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                        }
                    }
                    else
                    {
                        if (cityid == item.Key)
                        {
                            ddl = ddl + "<option selected='selected' value='" + item.Key + "'>" + item.Value + "</option>";

                        }
                        else
                        {
                            ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                        }
                    }


                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public string get_all_state(int ismulti = 0, int stateid = 0)
        {
            string ddl = "<option value='0'>State</option>";
            if (ismulti == 1)
            {
                ddl = "";
            }
            var deptser = new defaultservices();
            var list = deptser.get_all_satets();
            foreach (var item in list)
            {
                if (stateid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public string get_all_member(int memberid = 0, int ismulti = 0, string optvalue = "Member",string type="M",int currentid=0,bool issubcontractor=false)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            string ddl = "<option value='0'>" + optvalue + "</option>";
            if (ismulti == 1)
            {
                ddl = "";
            }
            var deptser = new defaultservices();
            var list = deptser.get_all_active_member(type, issubcontractor, userid);
            foreach (var item in list)
            {
                if (memberid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }


     

        [HttpPost]
        public string get_selcted_member(int memberid = 0, string optvalue = "Member",string type="T")
        {
            string ddl = "";
            var deptser = new defaultservices();
            var list = deptser.get_all_active_member_byid(memberid, type);
            foreach (var item in list)
            {
                if (memberid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";
                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }


        [HttpPost]
        public string get_selcted_managers(int userid = 0, string type = "T",int managerid=0)
        {
            string ddl = "<option  value='0'> Manager</option>";
            var deptser = new defaultservices();
            var list = deptser.get_all_active_manager_byid(userid, type);
            foreach (var item in list)
            {
                if (managerid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";
                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public string get_selcted_member_bycity(int cityid = 0, string optvalue = "Member")
        {
            string ddl = "<option value='0'>"+ optvalue + "</option>";
            var deptser = new defaultservices();
            var list = deptser.get_all_active_member_bycityid(cityid);
            foreach (var item in list)
            {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
             
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }


        [HttpPost]
        public string get_selcted_member_bystate(int stateid = 0, string optvalue = "Member")
        {
            string ddl = "<option value='0'>" + optvalue + "</option>";
            var deptser = new defaultservices();
            var list = deptser.get_all_active_member_bystateid(stateid);
            foreach (var item in list)
            {
                ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";

            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public string get_all_skills(int ismulti = 0)
        {
            string ddl = "<option value='0'>Skills</option>";
            if (ismulti == 1)
            {
                ddl = "";
            }
            var deptser = new defaultservices();
            var list = deptser.get_all_skills();
            foreach (var item in list)
            {
                ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public string get_selected_template(int templateid=0)
        {
            string ddl = "<option  value='0'>Template</option>";
            var deptser = new defaultservices();
            int defaultid = 0;
            var list = deptser.get_all_template(ref defaultid);
            foreach (var item in list)
            {
                if (defaultid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }


        [HttpPost]
        public string get_job_OfferByState(int stateId = 0)
        {
            string ddl = "<option  value='0'>Offer</option>";
            var deptser = new defaultservices();
            int defaultid = 0;
            var list = deptser.get_all_job_OffersByStateId(stateId);
            foreach (var item in list)
            {
                if (defaultid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }
            [HttpPost]
        public string get_job_OfferBycity(string city = "")
        {
            string ddl = "<option  value='0'>Offer</option>";
            var deptser = new defaultservices();
            var list = deptser.get_all_job_OffersBycity(city);
            foreach (var item in list)
            {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }
    }
}