﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class HomeController : Controller
    {
        public List<int> _permission;
        public List<int> _Paypermission;
        public  int roleid;
        public HomeController()
        {
             roleid = System.Web.HttpContext.Current.Request.Cookies["_pid"] == null ? 0 : Convert.ToInt16(System.Web.HttpContext.Current.Request.Cookies["_pid"].Value.ToString());
            roleservices _ser = new roleservices();
            _permission = _ser.get_all_active_permissions_bytypeid(10, roleid);
            
        }

        public ActionResult reset_password()
        {
            resetmodal _model = new resetmodal();
            return View(_model);
        }

        [HttpPost]
        public ActionResult reset_password(resetmodal _model)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var _crypt = new Crypto();
            var newpassword = _crypt.EncryptStringAES(_model.password);
            var message = new accountservices().update_password(userid, newpassword);
            TempData["error"] = message;
            bool _status = true;
            _status = Convert.ToBoolean(Request.Cookies["_status"].Value.ToString());
            if (!_status)
            {
                return RedirectToAction("edit", "account");
            }else
            {
                return RedirectToAction("Index", "home");
            }
        }


        public ActionResult Index( int? id)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            dashboardmodal _model = new dashboardmodal();
            dashboardservices _ser = new dashboardservices();
            string message = TempData["error"] != null ? TempData["error"].ToString() : "";
            if(roleid== 1013)
            {
                return RedirectToAction("edit","account");
            }
            if (_permission.Contains(54))
            {
                _model = _ser.get_all_active_dashboard();
                _model = new helper().GenerateError<dashboardmodal>(_model, message);
                return View("index", _model);
            }
            else
            {
                noticemodel _model1 = new noticemodel();
                noticeboardServices _sers = new noticeboardServices();
                _model1 = _sers.get_all_active_notice(id,1);
                _model1 = new helper().GenerateError<noticemodel>(_model1, message);
                return View("dashboard", _model1);
            }
        }


        public void readgeneral()
        {
            int memberid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            new companyservices().updatnotification(memberid);
        }
        public PartialViewResult getprofile(bool isnotificationpermission=true)
        {
            int memberid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            profilemodal _model = new profilemodal();
            memberservices _ser = new memberservices();
            roleservices _rser = new roleservices();
            bool isadmin = _rser.get_all_active_permissions_bypermissionId(1055, roleid); //_permission.Contains();
            _model = _ser.get_member_profile(memberid, isadmin);
            ViewBag.isnotificationpermission = isnotificationpermission;
            return PartialView("profile", _model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}