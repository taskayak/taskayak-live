﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class RolesController : Controller
    {
        public List<int> _permission;
        public RolesController()
        {
            int roleid = System.Web.HttpContext.Current.Request.Cookies["_pid"] == null ? 0 : Convert.ToInt16(System.Web.HttpContext.Current.Request.Cookies["_pid"].Value.ToString());
            roleservices _ser = new roleservices();
            _permission = _ser.get_all_active_permissions_bytypeid(9, roleid);
        }
        public ActionResult Index(int? id)
        {
            if (!_permission.Contains(52))
            {
                return View("unauth");
            }
            rolemodal _model = new rolemodal();
            var dept = new roleservices();
            string message = "";
            if (id.HasValue)
            {
                message = dept.remove_role(id.Value);
            }
            _model = dept.get_all_active_roleswithstatus();
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            ViewBag.permissions = _permission;
            _model = new helper().GenerateError<rolemodal>(_model, message);
            return View(_model);
        }


        public ActionResult set_roles(int id)
        {
            if (!_permission.Contains(60))
            {
                return View("unauth");
            }
            ViewBag.permissions = _permission;
            permission _model = new permission();
            var dept = new roleservices();
            string name = "";
            string message = "";
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            _model = new helper().GenerateError<permission>(_model, message);
            _model._permissions = dept.get_all_active_permissions_with_type();
            _model._Userpermissions = dept.get_all_active_permissions_byroleid(id, ref name);
            _model.roleid = id;
            _model.name = name;
            return View(_model);
        }

        [HttpPost]
        public ActionResult set_roles(int roleid, int[] perms = null)
        {
            ViewBag.permissions = _permission;
            permission _model = new permission();
            var dept = new roleservices();
            string message = "";
            if (perms != null)
            {
                message = dept.update_role_permissions(perms, roleid);
            }
            else
            {
                message = dept.delete_role_permissions(roleid);
            }
            _model = new helper().GenerateError<permission>(_model, message);
            string name = "";
            _model._permissions = dept.get_all_active_permissions_with_type();
            _model._Userpermissions = dept.get_all_active_permissions_byroleid(roleid, ref name);
            _model.roleid = roleid;
            _model.name = name;
            return View(_model);
        }


        [HttpPost]
        public ActionResult add(rolemodal _model)
        {
            var dept = new roleservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            string message = dept.add_new_role(_model, userid);
            if (!message.ToLower().Contains("error"))
            {
                ModelState.Clear();
            }
            _model = dept.get_all_active_roleswithstatus();
            _model = new helper().GenerateError<rolemodal>(_model, message);
            return View("index", _model);
        }

      
        public ActionResult add()
        {
            if (!_permission.Contains(35))
            {
                return View("unauth");
            }
            rolemodal _model = new rolemodal();
            var dept = new roleservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            string message ="";
            if (!message.ToLower().Contains("error"))
            {
                ModelState.Clear();
            }
            _model = dept.get_all_active_roleswithstatus();
            _model = new helper().GenerateError<rolemodal>(_model, message);
            return View("index", _model);
        }


        [HttpPost]
        public ActionResult edit(rolemodal _model)
        {
            var dept = new roleservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            string message = dept.update_role(_model, userid);
            if (!message.ToLower().Contains("error"))
            {
                ModelState.Clear();
            }
            _model = dept.get_all_active_roleswithstatus();
            _model = new helper().GenerateError<rolemodal>(_model, message);
            return View("index", _model);
        }

       
        public ActionResult edit()
        {
            if (!_permission.Contains(37))
            {
                return View("unauth");
            }
            rolemodal _model = new rolemodal();
            var dept = new roleservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            string message = dept.update_role(_model, userid);
            if (!message.ToLower().Contains("error"))
            {
                ModelState.Clear();
            }
            _model = dept.get_all_active_roleswithstatus();
            _model = new helper().GenerateError<rolemodal>(_model, message);
            return View("index", _model);
        }

    }
}