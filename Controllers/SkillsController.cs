﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class SkillsController : Controller
    {
        public List<int> _permission;
        public SkillsController()
        {
            int roleid = System.Web.HttpContext.Current.Request.Cookies["_pid"] == null ? 0 : Convert.ToInt16(System.Web.HttpContext.Current.Request.Cookies["_pid"].Value.ToString());
            roleservices _ser = new roleservices();
            _permission = _ser.get_all_active_permissions_bytypeid(8, roleid);
        }

        public ActionResult Index()
        {
            if (!_permission.Contains(5))
            {
                return View("unauth");
            }
            var dept = new skillservices();
            var _model = dept.get_all_active_skills();
            string message = "";
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            ViewBag.permissions = _permission;
            _model = new helper().GenerateError<skill>(_model, message);
            return View(_model);
        }

        public ActionResult update()
        {
            ViewBag.permissions = _permission;
            var dept = new skillservices();
            var _model = dept.get_all_active_skills();
            _model = new helper().GenerateError<skill>(_model, _model.error_text);
            return View("index", _model);
        }

        [HttpPost]
        public ActionResult Index(skill _model)
        {
            var dept = new skillservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            var error_text = dept.add_new_skill(_model.skill_name, userid);
            if (!error_text.ToLower().Contains("error"))
            {
                ModelState.Clear();
            }
            _model = dept.get_all_active_skills();
            _model = new helper().GenerateError<skill>(_model, error_text);
            return View(_model);
        }


        [HttpPost]
        public ActionResult update(string skill, int skillid)
        {
            ViewBag.permissions = _permission;
            var dept = new skillservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var error_text = dept.update_skill(skill, skillid, userid);
            skill _model = new skill();
            _model = dept.get_all_active_skills();
            _model = new helper().GenerateError<skill>(_model, error_text);
            return View("index", _model);
        }


        public ActionResult delete(int skillid)
        {
            if (!_permission.Contains(28))
            {
                return View("unauth");
            }
            ViewBag.permissions = _permission;
            var dept = new skillservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var error_text = dept.delete_skill(skillid, userid);
            skill _model = new skill();
            _model = dept.get_all_active_skills();
            _model = new helper().GenerateError<skill>(_model, error_text);
            return View("index", _model);
        }
    }
}