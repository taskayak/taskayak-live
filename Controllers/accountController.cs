﻿using hrm.Database;
using hrm.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    public class accountController : Controller
    {
        // GET: account

        public ActionResult Login(string returnurl = "none")
        {
            loginmodal _model = new loginmodal();
            _model.returnUrl = returnurl;
            _model.rememberme = false;
            return View(_model);
        }

        public ActionResult forgot_password()
        {
            loginmodal _model = new loginmodal();
            return View(_model);
        }

        [HttpPost]
        public ActionResult forgot_password(loginmodal _model)
        {
            accountservices _accs = new accountservices();
            helper _helpre = new helper();
            var crypt = new Crypto();
            var temppass = _helpre.GetRandomalphanumeric(8);
            string name = "";
            string email = "";
            var e_pass = crypt.EncryptStringAES(temppass);
            string message = _accs.reset_password(_model.username, e_pass, ref name, ref email);
            if (!message.Contains("Error"))
            {
                ModelState.Clear();
                string body = getforgetPasswordEmail(temppass, name);
                _helpre.SendemailAWS(email, body, "Password Reset Request", name, "Noreply");
            }
            _model = _helpre.GenerateError<loginmodal>(_model, message);
            return View(_model);
        }


        private string getforgetPasswordEmail(string password, string user)
        {
            string empty = string.Empty;
            string str = string.Empty;
            try
            {
                string str1 = "~/App_Data/forgetpassword.txt";
                str = System.IO.File.ReadAllText(Server.MapPath(str1));
                str = str.Replace("%user%", user).Replace("%passowrd%", password).Replace("%websiteLogoUrl%", "https://convo.crewtok.com/account/login").Replace("%CompanyName%", "Convo").Replace("%websiteLogoUrl1%", "https://www.textingpro.com/images/logo.png").Replace("%welcomeimagepath%", "#");
            }
            catch (Exception exception1)
            {
            }
            return str;
        }

        public ActionResult logout(string returnurl = "none")
        {
            loginmodal _model = new loginmodal();
            _model.returnUrl = returnurl;
            _model.rememberme = false;
            createCookie<int>(0, "_xid", -24);
            createCookie<int>(0, "_pid", -24);
            createCookie<int>(0, "_status", -48);
            return View("login", _model);
        }

        public string getptrn(string ptrn)
        {
            Crypto _crpt = new Crypto();
            return _crpt.DecryptStringAES(ptrn);
        }

        [AuthorizeVerifiedloggedin]
        public ActionResult password()
        {
            changepassword _mdl = new changepassword();
            return View("passowrd", _mdl);
        }
        public List<doctype_item> get_all_doctypes()
        {
            var dept = new documentservices();
            return dept.get_all_active_types();
        }
       

        [HttpPost]
        public ActionResult password_update(string curpass, string newpass)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            accountservices _accs = new accountservices();
            var message = _accs.validatepassword(curpass, newpass, userid);
            TempData["error"] = message;
            return RedirectToAction("edit", "account", new { tab = 1 });
        }
        public int getcityidbystate(int stateid, string city)
        {
            if (string.IsNullOrEmpty(city))
            {
                return 0;
            }
            else
            {
                return new defaultservices().savegetCityId(stateid, city);
            }

        }
        [HttpPost]
        public ActionResult addcompany(companymodal_item _item, int cuserid)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            compnymodal _model = new compnymodal();
            companyservices _services = new companyservices();
            int cityId = getcityidbystate(_item.state_id,_item.city);
            _item.city_id = cityId;
            var message = _services.add_new_companywithmember(_item, cuserid);
            var template = new templateservices().get_template(11);
            List<int> _data = new List<int>();
            _data.Add(userid);
            new jobofferservices().sendoffers2(_data, 11, true, true, template.sms, template.email, "New company welcome", false);

            TempData["error"] = message;
            return RedirectToAction("edit", "account", new { tab = 1 });
        }
        public void accept()
        {
            int id = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            memberservices _m = new memberservices();
            _m.acceptterms(id);
        }
        public void declineterms()
        {
            int id = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            memberservices _m = new memberservices();
            _m.declineterms(id);
        }

        [AuthorizeVerifiedloggedin]
        public ActionResult edit(int tab = 1, int test = 0)
        {
            int id = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.tab = tab;
            ViewBag.isuncomplete = test > 0 ? false : Convert.ToBoolean(Request.Cookies["_status"].Value.ToString());
            membermodal _mdl = new membermodal();
            memberservices _m = new memberservices();
            _mdl = _m.get_all_active_members_byid(id);
            string mes = "";
            if (TempData["error"] != null)
            {
                mes = TempData["error"].ToString();
                _mdl = new helper().GenerateError<membermodal>(_mdl, mes);
            }
            var type = new memberservices().getmembertype(id);
           ViewBag.istech = (type =="T") ? true : false;
            _mdl._docs = _m.get_all_doc_bymemberid(id);
            _mdl._skills = get_all_skills();
            _mdl._tools = get_all_tools();
            _mdl._dctype = get_all_doctypes();
            return View(_mdl);
        }
        public List<skill_items> get_all_tools()
        {
            var dept = new skillservices();
            return dept.get_all_active_tools()._skill;
        }

        [HttpPost]
        public ActionResult edit_image(HttpPostedFileBase file)
        {
            Random rnd = new Random(0);
            var rnditem = rnd.Next(0, 10);
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            memberservices _m = new memberservices();
            string path = Server.MapPath("~/image/picture-" + userid);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            ViewBag.tab = 4;
            string filenames = userid.ToString();
            var extension = Path.GetExtension(file.FileName);
            var imagepath = path + "/" + filenames + extension;
            string tempfilename = "~/image/picture-" + userid + "/" + filenames + extension + "?v=" + rnditem;
            if (System.IO.File.Exists(imagepath))
            {
                System.IO.File.Delete(imagepath);
            }
            file.SaveAs(imagepath);
            //GenerateThumbnails(imagepath, 0.2, imagepath);
            var mes = _m.add_new_member_image(tempfilename, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "account", new { tab = 1 });
        }

        public PartialViewResult getmailing()
        {
            int id = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            memberservices _mser = new memberservices();
            var _mdl = _mser.getmailingdetails(id);
            return PartialView("getmailing", _mdl);
        }


        public PartialViewResult rtw()
        {
            int id = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            memberservices _mser = new memberservices();
            var _mdl = _mser.getrtw(id);
            return PartialView("getrtw", _mdl);
        }



        public PartialViewResult w9f()
        {
            int id = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            memberservices _mser = new memberservices();
            var _mdl = _mser.getw9(id);
            return PartialView("w9f", _mdl);
        }

        public PartialViewResult dd()
        {
            int id = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            memberservices _mser = new memberservices();
            var _mdl = _mser.getdd(id);
            return PartialView("dd", _mdl);
        }

        public PartialViewResult pp()
        {
            int id = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            memberservices _mser = new memberservices();
            var _mdl = _mser.getpp(id);
            return PartialView("pp", _mdl);
        }

        public void updatemailing(getmailing mdl)
        {
            int id = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            memberservices _mser = new memberservices();
            _mser.updatemailinginfo(id, mdl);
        }

        public string getw9formforpdf(w9 mdl)
        {
            string strbody = "";
            int type = mdl.entity;
            switch (type)
            {
                case 1:
                    strbody = this.RenderPartialViewToString("~/Views/account/_w9_1.cshtml", null);
                    strbody = strbody.Replace("$entity", "Individual/Sole Proprietor/Single Member").Replace("$tllc", "").Replace("$EIN", mdl.EIN1).Replace("$SSN1", mdl.SSN1).Replace("$Business", mdl.Business1).Replace("$add1", mdl.Address1).Replace("$add2", mdl.Address2).Replace("$city", mdl.city).Replace("$state", mdl.state).Replace("$zip", mdl.zip);
                    break;
                case 2:
                    strbody = this.RenderPartialViewToString("~/Views/account/_w9_2.cshtml", null);
                    strbody = strbody.Replace("$entity", "C Corporation").Replace("$tllc", "").Replace("$EIN", mdl.EIN1).Replace("$SSN1", mdl.SSN2).Replace("$Business", mdl.Business2).Replace("$add1", mdl.Address2).Replace("$add2", mdl.Address2).Replace("$city", mdl.city).Replace("$state", mdl.state).Replace("$zip", mdl.zip);

                    break;
                case 3:
                    strbody = this.RenderPartialViewToString("~/Views/account/_w9_2.cshtml", null);
                    strbody = strbody.Replace("$entity", "S Corporation").Replace("$tllc", "").Replace("$EIN", mdl.EIN3).Replace("$SSN1", mdl.SSN3).Replace("$Business", mdl.Business3).Replace("$add1", mdl.Address2).Replace("$add2", mdl.Address2).Replace("$city", mdl.city).Replace("$state", mdl.state).Replace("$zip", mdl.zip);

                    break;
                case 4:
                    strbody = this.RenderPartialViewToString("~/Views/account/_w9_2.cshtml", null);
                    strbody = strbody.Replace("$entity", "Limited Liability Company").Replace("$tllc", mdl.SSN5).Replace("$EIN", mdl.EIN4).Replace("$SSN1", mdl.SSN4).Replace("$Business", mdl.Business4).Replace("$add1", mdl.Address1).Replace("$add2", mdl.Address2).Replace("$city", mdl.city).Replace("$state", mdl.state).Replace("$zip", mdl.zip);
                    break;
            }

            return strbody;
        }


        public void updatw9info(w9 mdl)
        {
            int id = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            memberservices _mser = new memberservices();
            _mser.updatw9info(id, mdl);
            string str = id.ToString() + "w9-application.pdf";
            string strbody = getw9formforpdf(mdl);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                StringReader stringReader = new StringReader(strbody);
                Document document = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                PdfWriter instance = PdfWriter.GetInstance(document, new FileStream(Server.MapPath(string.Concat("~/pdffiles/", str)), FileMode.Create));
                document.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(instance, document, stringReader);
                document.Close();
                instance.Close();
            }
            _mser.add_new_doc("W9 Form", id, string.Concat("/pdffiles/", str), 0, true);
        }
        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");
            if (model != null)
                ViewData.Model = model;
            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString().Replace("<br>", "<br/>");
            }
        }

        public void updatddinfo(dd mdl)
        {
            int id = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            memberservices _mser = new memberservices();
            string tecjhid = _mser.updatddinfo(id, mdl);
            long ticks = DateTime.Now.Ticks;
            string str = mdl.account_name.Replace(" ", "") + "DDP-application.pdf";
            string strbody = this.RenderPartialViewToString("~/Views/account/_dd.cshtml", null);
            strbody = strbody.Replace("$tcid", tecjhid).Replace("$acty", mdl.Typeofaccount).Replace("$abnb", mdl.bankname).Replace("$acs", mdl.Accountcitystate).Replace("$arn", mdl.routingnumber).Replace("$acn", mdl.account_number);
            try
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    StringReader stringReader = new StringReader(strbody);
                    Document document = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                    PdfWriter instance = PdfWriter.GetInstance(document, new FileStream(Server.MapPath(string.Concat("~/pdffiles/", str)), FileMode.Create));
                    document.Open();
                    XMLWorkerHelper.GetInstance().ParseXHtml(instance, document, stringReader);
                    document.Close();
                    instance.Close();
                }
            }
            catch (Exception ex)
            {

            }
            _mser.add_new_doc("Direct Deposit Payment Form", id, string.Concat("/pdffiles/", str), 0, true);
        }

        
       [HttpPost]
        public ActionResult add_bck(HttpPostedFileBase file)
        {
            Random rnd = new Random(0);
            var rnditem = rnd.Next(0, 10);
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            memberservices _m = new memberservices();
            string path = Server.MapPath("/pdffiles/bckcheck-"+ rnditem.ToString() + userid);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filenames = userid.ToString();
            var extension = Path.GetExtension(file.FileName);
            var imagepath = path + "/" + filenames + extension;
            string tempfilename = "/pdffiles/bckcheck-" + rnditem.ToString() + userid + "/" + filenames + extension + "?v=" + rnditem;
            if (System.IO.File.Exists(imagepath))
            {
                System.IO.File.Delete(imagepath);
            }
            file.SaveAs(imagepath);
            TempData["error"] = "Background check doc uploded successfully";
            _m.add_new_doc("Background check doc", userid, tempfilename, 2, false);
            return RedirectToAction("edit", "account", new { tab = 1 });
        }


        [HttpPost]
        public void uploadimage(HttpPostedFileBase file)
        {
            Random rnd = new Random(0);
            var rnditem = rnd.Next(0, 10);
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            memberservices _m = new memberservices();
            string path = Server.MapPath("~/image/picture-" + userid);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filenames = userid.ToString();
            var extension = Path.GetExtension(file.FileName);
            var imagepath = path + "/" + filenames + extension;
            string tempfilename = "~/image/picture-" + userid + "/" + filenames + extension + "?v=" + rnditem;
            if (System.IO.File.Exists(imagepath))
            {
                System.IO.File.Delete(imagepath);
            }
            file.SaveAs(imagepath);
            //GenerateThumbnails(imagepath, 0.2, imagepath);
            var mes = _m.add_new_member_image(tempfilename, userid);

        }

        public void rtwsubmit(rtw model)
        {
            int id = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            memberservices _mser = new memberservices();
            _mser.updatrtwinfo(id, model);
        }

        public PartialViewResult complete()
        {
            int id = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            memberservices _mser = new memberservices();
           var _mdl = _mser.update_complete(id);
            createCookie<bool>(true, "_status", 48);
            ViewBag.mes = _mdl.message;
            return PartialView("complete", _mdl);
        }

        [HttpPost]
        public ActionResult edit(membermodal _mdl)
        {
            ViewBag.tab = 1;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            memberservices _m = new memberservices();
            var mes = _m.update_member_by_member(_mdl, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "account", new { tab = 1 });
        }

        [HttpPost]
        public ActionResult edit_skills(int[] skills)
        {
            ViewBag.tab = 1;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            memberservices _m = new memberservices();
            var mes = _m.update_skills_by_member(skills, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "account", new { tab = 2 });
        }

        [HttpPost]
        public ActionResult edit_tools(int[] tools)
        {
            ViewBag.tab = 1;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            memberservices _m = new memberservices();
            var mes = _m.update_tools_by_member(tools, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "account", new { tab = 3 });
        }





        public List<skill_items> get_all_skills()
        {
            var dept = new skillservices();
            return dept.get_all_active_skills()._skill;
        }

        //[HttpPost]
        //public ActionResult password_update(string curpass, string newpass)
        //{
        //    int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
        //    accountservices _accs = new accountservices();
        //    var message = _accs.validatepassword(curpass, newpass, userid);
        //    TempData["error"] = message;
        //    return RedirectToAction("edit", "test", new { tab = 1 });
        //}

        [HttpPost]
        public ActionResult password(changepassword _mdl)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            accountservices _accs = new accountservices();
            var message = _accs.validatepassword(_mdl.currentpassword, _mdl.password, userid);
            ModelState.Clear();
            _mdl = new helper().GenerateError<changepassword>(_mdl, message);
            return View("passowrd", _mdl);
        }

        [HttpPost]
        public ActionResult Login(loginmodal _model)
        {
            accountservices _accs = new accountservices();
            string message = "";
            int roleid = 0;
            bool isrest = false;
            bool _status = true;
            bool accepttermsrqud = true;
            var _xid = _accs.validateLogin(_model, ref message, ref roleid, ref isrest, ref _status,ref accepttermsrqud);
            _model = new helper().GenerateError<loginmodal>(_model, message);
            if (_xid != 0)
            {
                if (_model.rememberme)
                {
                    createCookie<int>(_xid, "_xid", 24);
                    createCookie<int>(roleid, "_pid", 24);
                    createCookie<bool>(_status, "_status", 48);

                }
                else
                {
                    createCookie<int>(_xid, "_xid", 24);
                    createCookie<int>(roleid, "_pid", 24);
                    createCookie<bool>(_status, "_status", 48);
                }
                if (isrest)
                {
                    return RedirectToAction("reset_password", "home");
                }
                if (accepttermsrqud)
                {
                    return RedirectToAction("edit", "account");
                }
                else if (!_status)
                {
                    return RedirectToAction("edit", "account");
                }
                else if (_model.returnUrl == "none")
                {
                    return RedirectToAction("index", "home");
                }
                else
                {
                    return Redirect(_model.returnUrl);
                }
            }
            return View(_model);
        }

        [HttpPost]
        public ActionResult edit_doc(HttpPostedFileBase file, int userid, string doc, int _dctype = 0)
        {
            //ViewBag.permissions = _permission;
            //int userid = 0;// will be added later
            memberservices _m = new memberservices();
            string path = Server.MapPath("~/image/doc-" + userid);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            ViewBag.tab = 1;
            string filenames = new helper().GetRandomalphanumeric(4);
            var extension = Path.GetExtension(file.FileName);
            var imagepath = path + "/" + filenames + extension;
            string tempfilename = "/image/doc-" + userid + "/" + filenames + extension;
            if (System.IO.File.Exists(imagepath))
            {
                System.IO.File.Delete(imagepath);
            }
            file.SaveAs(imagepath);
            //GenerateThumbnails(imagepath, 0.2, imagepath);
            //GenerateThumbnails(imagepath, 0.2, imagepath);
            var mes = _m.add_new_doc(doc, userid, tempfilename, _dctype);
            TempData["error"] = mes;
            return RedirectToAction("edit", "account", new { tab = 1 });
        }
        public ActionResult delete_bank(int memberid, int id)
        {
            ViewBag.tab = 1;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            // ViewBag.permissions = _permission;
            memberservices _m = new memberservices();
            var mes = _m.delete_bank(id);
            TempData["error"] = mes;
            return RedirectToAction("edit", "account", new { tab = 2 });
        }

        [HttpPost]
        public ActionResult add_tools(string tooltype1, string toolname1)
        {
            var mes = "";
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            mes = new toolservices().add_new_toolbytypename(toolname1, tooltype1, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "account", new { tab = 3 });
        }

        public ActionResult delete_doc(int memberid, string id)
        {
            Crypto _crypt = new Crypto();
            int _id = Convert.ToInt32(_crypt.DecryptStringAES(id));
            ViewBag.tab = 1;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            //  ViewBag.permissions = _permission;
            memberservices _m = new memberservices();
            var mes = _m.delete_doc(_id);
            TempData["error"] = mes;
            return RedirectToAction("edit", "account", new { tab = 1 });
        }

        [HttpPost]
        public ActionResult edit_bank(int memberid, string bank_name, string branch_name, string account_name, string account_number, string ifsc_code, string pan_number)
        {
            ViewBag.tab = 1;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            //  ViewBag.permissions = _permission;
            memberservices _m = new memberservices();
            var mes = _m.update_member_bank(memberid, bank_name, branch_name, account_name, account_number, ifsc_code, pan_number, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "account", new { tab = 2 });
        }

        [HttpPost]
        public ActionResult bank_update(int memberid, int bankid, string bank_name, string branch_name, string account_name, string account_number, string ifsc_code, string pan_number)
        {
            ViewBag.tab = 1;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            //  ViewBag.permissions = _permission;
            memberservices _m = new memberservices();
            var mes = _m.update_bank(bankid, bank_name, branch_name, account_name, account_number, ifsc_code, pan_number, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "account", new { tab = 2 });
        }


        ///------------------------Helper method-----------------------------------///
        public void createCookie<T>(T _encyrptedkey, string name, int time = 5)
        {

            HttpCookie httpCookie = new HttpCookie(name, _encyrptedkey.ToString())
            {
                Expires = DateTime.Now.AddHours(time)
            };
            Response.Cookies.Add(httpCookie);
        }

    }
}