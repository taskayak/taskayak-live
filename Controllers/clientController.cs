﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class clientController : Controller
    {
        public List<int> _permission;
        public clientController()
        {
            int roleid = System.Web.HttpContext.Current.Request.Cookies["_pid"] == null ? 0 : Convert.ToInt16(System.Web.HttpContext.Current.Request.Cookies["_pid"].Value.ToString());
            roleservices _ser = new roleservices();
            _permission = _ser.get_all_active_permissions_bytypeid(3, roleid);
        }
        public string get_client_rate(int id)
        {
            var client = new clientservices();
            return client.get_rate(id);
        }

        [Route("client/Index")]
        public ActionResult Index()
        {
            if (!_permission.Contains(2))
            {
                return View("unauth");
            }
            ViewBag.permissions = _permission;
            clientmodal _model = new clientmodal();
            if (TempData["error"] != null)
            {
                var message = TempData["error"].ToString();
                _model = new helper().GenerateError<clientmodal>(_model, message);
            }
            return View(_model);
        }
        public JsonResult Fetchclients(int start = 0, int length = 25, int draw = 1)
        {
            string search = Request.Params["search[value]"] == null ? "" : Request.Params["search[value]"];
            start = (start == 0 ? 1 : start + 1);
            length = (start == 1 ? length : (start - 1) + length);
            clientservices _services = new clientservices();
            datatableAjax<clientmodal_item> tableDate = new datatableAjax<clientmodal_item>();
            int total = 0;
            var _model = _services.get_all_active_clients_bypaging(ref total, start, length, search);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = (_model.Count() > 0 ? total : 0);
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Fetchmanagers(int start = 0, int length = 25, int draw = 1, int clientid = 0)
        {
            string search = Request.Params["search[value]"] == null ? "" : Request.Params["search[value]"];
            start = (start == 0 ? 1 : start + 1);
            length = (start == 1 ? length : (start - 1) + length);
            prjmanagerservices _services = new prjmanagerservices();
            datatableAjax<prjmngr_item> tableDate = new datatableAjax<prjmngr_item>();
            int total = 0;
            var _model = _services.get_all_active_prj_byclient_byindex(ref total, clientid, start, length, search);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = (_model.Count() > 0 ? total : 0);
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }
        public ActionResult add()
        {
            if (!_permission.Contains(11))
            {
                return View("unauth");
            }
            clientmodal_item _item = new clientmodal_item();
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            return View(_item);
        }
        public int getcityidbystate(int stateid, string city)
        {
            if (string.IsNullOrEmpty(city))
            {
                return 0;
            }
            else
            {
                return new defaultservices().savegetCityId(stateid, city);
            }

        }
        [HttpPost]
        public ActionResult add(clientmodal_item _item)
        {
            ViewBag.permissions = _permission;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            clientmodal _model = new clientmodal();
            clientservices _services = new clientservices();
            _item.city_id = getcityidbystate(_item.state_id, _item.city);
            var message = _services.add_new_client(_item, userid);
            //_model = _services.get_all_active_clients();
            _model = new helper().GenerateError<clientmodal>(_model, message);
            return View("Index", _model);
        }
        public ActionResult delete(string token)
        {
            if (!_permission.Contains(14))
            {
                return View("unauth");
            }
            int id = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            ViewBag.permissions = _permission;
            clientmodal _model = new clientmodal();
            clientservices _services = new clientservices();
            var message = _services.delete_client(id);
            //_model = _services.get_all_active_clients();
            _model = new helper().GenerateError<clientmodal>(_model, message);
            return View("Index", _model);
        }
        public ActionResult edit(string token)
        {
            if (!_permission.Contains(12))
            {
                return View("unauth");
            }
            int id = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            clientservices _services = new clientservices();
            clientmodal_item _item = new clientmodal_item();
            _item = _services.get_all_active_clients_byid(id);

            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            return View(_item);
        }
        public ActionResult view(string token)
        {
            if (!_permission.Contains(13))
            {
                return View("unauth");
            }
            int id = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            ViewBag.permissions = _permission;
            clientviewmodal _model = new clientviewmodal();
            clientservices _services = new clientservices();
            prjmanagerservices _ser = new prjmanagerservices();
            if (TempData["error"] != null)
            {
                var message = TempData["error"].ToString();
                _model = new helper().GenerateError<clientviewmodal>(_model, message);
            }
            _model.clientid = id;
            _model.clients = _services.get_all_active_clients_byid(id);
            // _model._prjlist = _ser.get_all_active_prj_byclient(id);
            return View(_model);
        }
        [HttpPost]
        public ActionResult add_manager(prjmngr_item prjectmgr)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            clientviewmodal _model = new clientviewmodal();
            clientservices _services = new clientservices();
            prjmanagerservices _ser = new prjmanagerservices();
            // clientmodal_item _item = new clientmodal_item();
            _model.clientid = prjectmgr.clientid;
            Session["clinetid"] = prjectmgr.clientid;
            var message = _ser.add_new_manager(prjectmgr, userid);
            _model.clients = _services.get_all_active_clients_byid(prjectmgr.clientid);
            //  _model._prjlist = _ser.get_all_active_prj_byclient(prjectmgr.clientid);
            _model = new helper().GenerateError<clientviewmodal>(_model, message);
            return View("view", _model);
        }
        public ActionResult add_manager()
        {
            if (!_permission.Contains(18))
            {
                return View("unauth");
            }
            prjmngr_item prjectmgr = new prjmngr_item();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            clientviewmodal _model = new clientviewmodal();
            clientservices _services = new clientservices();
            prjmanagerservices _ser = new prjmanagerservices();
            prjectmgr.clientid = Session["clinetid"] == null ? 0 : Convert.ToInt32(Session["clinetid"]);
            if (Session["clinetid"] == null)
            {
                return RedirectToAction("Index");
            }
            _model.clientid = prjectmgr.clientid;
            var message = "";
            _model.clients = _services.get_all_active_clients_byid(prjectmgr.clientid);
            // _model._prjlist = _ser.get_all_active_prj_byclient(prjectmgr.clientid);
            _model = new helper().GenerateError<clientviewmodal>(_model, message);
            return View("view", _model);
        }
        [HttpPost]
        public ActionResult update_manager(prjmngr_item prjectmgr)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            clientviewmodal _model = new clientviewmodal();
            clientservices _services = new clientservices();
            prjmanagerservices _ser = new prjmanagerservices();
            // clientmodal_item _item = new clientmodal_item();
            _model.clientid = prjectmgr.clientid;
            Session["clinetid"] = prjectmgr.clientid;
            var message = _ser.update_manager(prjectmgr);
            _model.clients = _services.get_all_active_clients_byid(prjectmgr.clientid);
            // _model._prjlist = _ser.get_all_active_prj_byclient(prjectmgr.clientid);
            _model = new helper().GenerateError<clientviewmodal>(_model, message);
            return View("view", _model);
        }
        public ActionResult update_manager()
        {
            prjmngr_item prjectmgr = new prjmngr_item();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            clientviewmodal _model = new clientviewmodal();
            clientservices _services = new clientservices();
            prjmanagerservices _ser = new prjmanagerservices();
            _model.clientid = Session["clinetid"] == null ? 0 : Convert.ToInt32(Session["clinetid"]);
            if (Session["clinetid"] == null)
            {
                return RedirectToAction("Index");
            }
            var message = "";
            _model.clients = _services.get_all_active_clients_byid(prjectmgr.clientid);
            // _model._prjlist = _ser.get_all_active_prj_byclient(prjectmgr.clientid);
            _model = new helper().GenerateError<clientviewmodal>(_model, message);
            return View("view", _model);
        }
        public ActionResult delete_manager(string clienttoken, string token)
        {
            if (!_permission.Contains(17))
            {
                return View("unauth");
            }
            Crypto _crypt = new Crypto();
            int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
            int clientid = Convert.ToInt32(_crypt.DecryptStringAES(clienttoken));
            clientviewmodal _model = new clientviewmodal();
            clientservices _services = new clientservices();
            prjmanagerservices _ser = new prjmanagerservices();
            _model.clientid = clientid;
            ViewBag.permissions = _permission;
            var message = _ser.delete_manager(id);
            _model.clients = _services.get_all_active_clients_byid(clientid);
            _model = new helper().GenerateError<clientviewmodal>(_model, message);
            return View("view", _model);
        }
        [HttpPost]
        public ActionResult update(clientmodal_item _item)
        {
            ViewBag.permissions = _permission;
            clientmodal _model = new clientmodal();
            clientservices _services = new clientservices();
            _item.city_id = getcityidbystate(_item.state_id, _item.city);
            var message = _services.update_client(_item);
            // _model = _services.get_all_active_clients();
            //  Session["editid"] = id;
            _model = new helper().GenerateError<clientmodal>(_model, message);
            return View("Index", _model);
        }
        public ActionResult update()
        {
            ViewBag.permissions = _permission;
            clientmodal _model = new clientmodal();
            clientservices _services = new clientservices();
            var message = "";
            // _model = _services.get_all_active_clients();
            //  Session["editid"] = id;
            _model = new helper().GenerateError<clientmodal>(_model, message);
            return View("Index", _model);
        }

    }
}