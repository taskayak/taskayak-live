﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class companyController : Controller
    {
        public List<int> _permission;
        public companyController()
        {
            int roleid = System.Web.HttpContext.Current.Request.Cookies["_pid"] == null ? 0 : Convert.ToInt16(System.Web.HttpContext.Current.Request.Cookies["_pid"].Value.ToString());
            roleservices _ser = new roleservices();
            _permission = _ser.get_all_active_permissions_bytypeid(15, roleid);
        }
        public string get_client_rate(int id)
        {
            var client = new clientservices();
            return client.get_rate(id);
        }
        public ActionResult Index()
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            //
            //if (!_permission.Contains(2))
            //{
            //    return View("unauth");
            //}
            var type = new memberservices().getmembertype(userid);
            if (type.ToLower() == "s")
            {
                int id = new memberservices().getcompanyid(userid);
                var _token = new Crypto().EncryptStringAES(id.ToString());
                return RedirectToAction("view", new { token = _token, issubcontractor = true });
            }
            ViewBag.permissions = _permission;
            compnymodal _model = new compnymodal();
            if (TempData["error"] != null)
            {
                var message = TempData["error"].ToString();
                _model = new helper().GenerateError<compnymodal>(_model, message);
            }
            return View(_model);
        }
        public JsonResult Fetchclients(int start = 0, int length = 25, int draw = 1)
        {
            string search = Request.Params["search[value]"] == null ? "" : Request.Params["search[value]"];
            start = (start == 0 ? 1 : start + 1);
            length = (start == 1 ? length : (start - 1) + length);
            companyservices _services = new companyservices();
            datatableAjax<companymodal_item> tableDate = new datatableAjax<companymodal_item>();
            int total = 0;
            var _model = _services.get_all_active_company_bypaging(ref total, start, length, search);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = (_model.Count() > 0 ? total : 0);
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Fetchmanagers(int start = 0, int length = 25, int draw = 1, int clientid = 0)
        {
            string search = Request.Params["search[value]"] == null ? "" : Request.Params["search[value]"];
            start = (start == 0 ? 1 : start + 1);
            length = (start == 1 ? length : (start - 1) + length);
            prjmanagerservices _services = new prjmanagerservices();
            datatableAjax<companyprjmngr_item> tableDate = new datatableAjax<companyprjmngr_item>();
            int total = 0;
            var _model = _services.get_all_active_company_byclient_byindex(ref total, clientid, start, length, search);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = (_model.Count() > 0 ? total : 0);
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }
        public ActionResult add()
        {
            //if (!_permission.Contains(11))
            //{
            //    return View("unauth");
            //}
            companymodal_item _item = new companymodal_item();
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            return View(_item);
        }
        [HttpPost]
        public ActionResult add(companymodal_item _item)
        {
            ViewBag.permissions = _permission;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            compnymodal _model = new compnymodal();
            companyservices _services = new companyservices();
            _item.city_id = getcityidbystate(_item.state_id, _item.city);
            var message = _services.add_new_company(_item, userid);
           
           // _model = _services.get_all_active_clients();
            _model = new helper().GenerateError<compnymodal>(_model, message);
            return View("Index", _model);
        }

        [HttpPost]
        public ActionResult add_member(int state_id, int member_id, string membertype, string fname, int clientid, string lname, string email, string phone, string address, string zip, string Position, int manager_id, string city = "")
        {
            ViewBag.permissions = _permission;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            string _token = new Crypto().EncryptStringAES(clientid.ToString());
            memberservices _services = new memberservices();
            var template = new templateservices().get_template(3);
            int cityid = getcityidbystate(state_id, city);
            int rolid = membertype == "ST" ? 1012 : membertype == "LT" ? 1009 : 1015;
            if (member_id != 0)
            {
                var message = _services.update_member_by_member(member_id, membertype, fname, clientid, lname, email, phone, address, zip, Position, manager_id, userid, cityid, state_id, rolid);
                List<int> _data = new List<int>();
                _data.Add(member_id);
                new jobofferservices().sendoffers2(_data, 3, true, true, template.sms, template.email, "Field Technician Account", false);
                TempData["error"] = message;
            }
            else
            {
                Crypto _crypt = new Crypto();
                Session["end"] = "1";
                int i = 0;
                string memberid = new memberservices().getLatestMemberId();
                if (!Int32.TryParse(memberid, out i))
                {
                    memberid = new helper().get5RandomDidit();
                }
                else
                {
                    memberid = (i + 1).ToString();
                }
                string companyname = "";
                membermodal _mdl = new membermodal();
                _mdl.FirstName = fname;
                _mdl.lastname = lname;
                _mdl.member_id = memberid;
                _mdl.email = email;
                _mdl.membertype = membertype;
                _mdl.position = Position;
                _mdl.password = new helper().GetRandomalphanumeric(8);
                _mdl.hourlyrate = "$0.00";
                _mdl.tax = "$0.00";
                _mdl.status = 1007;
                _mdl.roleid = rolid;
                _mdl.gender = "M";
                _mdl.depid = 0;
                _mdl.desid = 0;
                _mdl.background = "Pending";
                _mdl.drugtested = "Pending";
                _mdl.email = email;
                _mdl.phone = phone;
                _mdl.Managerid = manager_id;
                _mdl.alertnative_phone = new companyservices().getalterntivephone(clientid, ref companyname);
                _mdl.company = companyname;
                _mdl.state_id = state_id;
                _mdl.companyid = clientid;
                _mdl.city_id = cityid;
                _mdl.zip = zip;
                _mdl.address = address;
                var mser = new memberservices();
                int userId = Convert.ToInt16(mser.add_new_membergetid(_mdl, 0, false,false,true));

                List<int> _data = new List<int>();
                _data.Add(userId);
                new jobofferservices().sendoffers2(_data, 3, true, true, template.sms, template.email, "Field Technician Account", false);
                new jobofferservices().addmembersignupnotification(userId);
            }
            var type = new memberservices().getmembertype(userid);
            bool issubcontractor = type.ToLower() == "s" ? true : false;
            return RedirectToAction("view", new { token = _token, issubcontractor = issubcontractor });
        }
        public int getcityidbystate(int stateid, string city)
        {
            if (string.IsNullOrEmpty(city))
            {
                return 0;
            }
            else
            {
                return new defaultservices().savegetCityId(stateid, city);
            }

        }

        [HttpPost]
        public ActionResult update_member(int estate_id, int euserid, int emember_id, string emembertype, string efname, int eclientid, string elname, string eemail, string ephone, string eaddress, string ezip, string ePosition, int emanager_id, string ecity = "")
        {
            ViewBag.permissions = _permission;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            string _token = new Crypto().EncryptStringAES(eclientid.ToString());
            memberservices _services = new memberservices();
            int cityid = getcityidbystate(estate_id, ecity);
            int rolid = emembertype == "ST" ? 1012 : emembertype == "LT" ? 1009 : 1015;
            var message = _services.update_member_by_member(euserid, emembertype, efname, eclientid, elname, eemail, ephone, eaddress, ezip, ePosition, emanager_id, userid, cityid, estate_id, rolid);
            TempData["error"] = message;
            var type = new memberservices().getmembertype(userid);
            bool issubcontractor = type.ToLower() == "s" ? true : false;
            return RedirectToAction("view", new { token = _token, issubcontractor = issubcontractor });
        }


        public JsonResult Getmeberbyid(int id)
        {
            var mdl = new memberservices().get_members_detail_byid(id);
            return Json(mdl, JsonRequestBehavior.AllowGet);
        }

        public ActionResult delete(string token)
        {
            //{
            //    if (!_permission.Contains(14))
            //    {
            //        return View("unauth");
            //    }
            int id = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            ViewBag.permissions = _permission;
            compnymodal _model = new compnymodal();
            companyservices _services = new companyservices();
            var message = _services.delete_company(id);
            //_model = _services.get_all_active_clients();
            _model = new helper().GenerateError<compnymodal>(_model, message);
            return View("Index", _model);
        }
        public ActionResult edit(string token)
        {
            //if (!_permission.Contains(12))
            //{
            //    return View("unauth");
            //}
            int id = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            companyservices _services = new companyservices();
            companymodal_item _item = new companymodal_item();
            _item = _services.get_all_active_company_byid(id);
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            return View(_item);
        }

        public ActionResult view(string token, bool issubcontractor = false)
        {
            //if (!_permission.Contains(13))
            //{
            //    return View("unauth");
            //}
            int id = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            ViewBag.permissions = _permission;
            ViewBag.issubcontractor = issubcontractor;
            companyviewmodal _model = new companyviewmodal();
            companyservices _services = new companyservices();
            if (TempData["error"] != null)
            {
                var message = TempData["error"].ToString();
                _model = new helper().GenerateError<companyviewmodal>(_model, message);
            }
            _model.clientid = id;
            _model.clients = _services.get_all_active_company_byid(id);
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            return View(_model);
        }

        [HttpPost]
        public ActionResult add_manager(prjmngr_item prjectmgr)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            clientviewmodal _model = new clientviewmodal();
            clientservices _services = new clientservices();
            prjmanagerservices _ser = new prjmanagerservices();
            // clientmodal_item _item = new clientmodal_item();
            _model.clientid = prjectmgr.clientid;
            Session["clinetid"] = prjectmgr.clientid;
            var message = _ser.add_new_manager(prjectmgr, userid);
            _model.clients = _services.get_all_active_clients_byid(prjectmgr.clientid);
            //  _model._prjlist = _ser.get_all_active_prj_byclient(prjectmgr.clientid);
            _model = new helper().GenerateError<clientviewmodal>(_model, message);
            return View("view", _model);
        }
        public ActionResult add_manager()
        {
            //if (!_permission.Contains(18))
            //{
            //    return View("unauth");
            //}
            prjmngr_item prjectmgr = new prjmngr_item();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            clientviewmodal _model = new clientviewmodal();
            clientservices _services = new clientservices();
            prjmanagerservices _ser = new prjmanagerservices();
            prjectmgr.clientid = Session["clinetid"] == null ? 0 : Convert.ToInt32(Session["clinetid"]);
            if (Session["clinetid"] == null)
            {
                return RedirectToAction("Index");
            }
            _model.clientid = prjectmgr.clientid;
            var message = "";
            _model.clients = _services.get_all_active_clients_byid(prjectmgr.clientid);
            // _model._prjlist = _ser.get_all_active_prj_byclient(prjectmgr.clientid);
            _model = new helper().GenerateError<clientviewmodal>(_model, message);
            return View("view", _model);
        }
        //[HttpPost]
        //public ActionResult update_manager(prjmngr_item prjectmgr)
        //{
        //    int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
        //    ViewBag.permissions = _permission;
        //    clientviewmodal _model = new clientviewmodal();
        //    clientservices _services = new clientservices();
        //    prjmanagerservices _ser = new prjmanagerservices();
        //    // clientmodal_item _item = new clientmodal_item();
        //    _model.clientid = prjectmgr.clientid;
        //    Session["clinetid"] = prjectmgr.clientid;
        //    var message = _ser.update_manager(prjectmgr);
        //    _model.clients = _services.get_all_active_clients_byid(prjectmgr.clientid);
        //    // _model._prjlist = _ser.get_all_active_prj_byclient(prjectmgr.clientid);
        //    _model = new helper().GenerateError<clientviewmodal>(_model, message);
        //    return View("view", _model);
        //}
        //public ActionResult update_manager()
        //{
        //    prjmngr_item prjectmgr = new prjmngr_item();
        //    int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
        //    ViewBag.permissions = _permission;
        //    clientviewmodal _model = new clientviewmodal();
        //    clientservices _services = new clientservices();
        //    prjmanagerservices _ser = new prjmanagerservices();
        //    _model.clientid = Session["clinetid"] == null ? 0 : Convert.ToInt32(Session["clinetid"]);
        //    if (Session["clinetid"] == null)
        //    {
        //        return RedirectToAction("Index");
        //    }
        //    var message = "";
        //    _model.clients = _services.get_all_active_clients_byid(prjectmgr.clientid);
        //    // _model._prjlist = _ser.get_all_active_prj_byclient(prjectmgr.clientid);
        //    _model = new helper().GenerateError<clientviewmodal>(_model, message);
        //    return View("view", _model);
        //}
        public ActionResult delete_member(string clienttoken, string token)
        {
            //if (!_permission.Contains(17))
            //{
            //    return View("unauth");
            //}
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            Crypto _crypt = new Crypto();
            int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
            int clientid = Convert.ToInt32(_crypt.DecryptStringAES(clienttoken));
            clientviewmodal _model = new clientviewmodal();
            clientservices _services = new clientservices();
            companyservices _ser = new companyservices();
            _model.clientid = clientid;
            ViewBag.permissions = _permission;
            var message = _ser.delete_member(id);
            TempData["error"] = message;
            var type = new memberservices().getmembertype(userid);
            bool issubcontractor = type.ToLower() == "s" ? true : false;
            return RedirectToAction("view", new { token = clienttoken, issubcontractor = issubcontractor });
            //_model = new helper().GenerateError<clientviewmodal>(_model, message);
            //return View("view", _model);
        }
        [HttpPost]
        public ActionResult update(companymodal_item _item)
        {
            ViewBag.permissions = _permission;
            compnymodal _model = new compnymodal();
            companyservices _services = new companyservices();
            _item.city_id = getcityidbystate(_item.state_id, _item.city);
            var message = _services.update_company(_item);
            // _model = _services.get_all_active_clients();
            //  Session["editid"] = id;
            _model = new helper().GenerateError<compnymodal>(_model, message);
            return View("Index", _model);
        }

        [HttpPost]
        public ActionResult updatecompany(companymodal_item _item)
        {
            ViewBag.permissions = _permission;
            compnymodal _model = new compnymodal();
            companyservices _services = new companyservices();
            int cityId = getcityidbystate(_item.state_id, _item.city);
            _item.city_id = cityId;
            var message = _services.update_company(_item);
            TempData["error"] = message;
            _model = new helper().GenerateError<compnymodal>(_model, message);
            var _token = new Crypto().EncryptStringAES(_item.clientid.ToString());
            return RedirectToAction("view", new { token = _token, issubcontractor = true });
        }
        public ActionResult update()
        {
            ViewBag.permissions = _permission;
            compnymodal _model = new compnymodal();
            companyservices _services = new companyservices();
            var message = "";
            // _model = _services.get_all_active_clients();
            //  Session["editid"] = id;
            _model = new helper().GenerateError<compnymodal>(_model, message);
            return View("Index", _model);
        }

    }
}