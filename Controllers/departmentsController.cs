﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class departmentsController : Controller
    {
        public List<int> _permission;
        public departmentsController()
        {
            int roleid = System.Web.HttpContext.Current.Request.Cookies["_pid"] == null ? 0 : Convert.ToInt16(System.Web.HttpContext.Current.Request.Cookies["_pid"].Value.ToString());
            roleservices _ser = new roleservices();
            _permission = _ser.get_all_active_permissions_bytypeid(6, roleid);
        }
        // GET: departments
        public ActionResult Index()
        {
            if (!_permission.Contains(3))
            {
                return View("unauth");
            }
            var dept = new departmentservices();
            var _model = dept.get_all_active_departments();
            string message = "";
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            ViewBag.permissions = _permission;
            _model = new helper().GenerateError<department>(_model, message);
            return View(_model);
        }

        public ActionResult update()
        {
            var dept = new departmentservices();
            var _model = dept.get_all_active_departments();
            ViewBag.permissions = _permission;
            _model = new helper().GenerateError<department>(_model, _model.error_text);
            return View("index", _model);
        }

        [HttpPost]
        public ActionResult Index(department _model)
        {
            var dept = new departmentservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            var error_text = dept.add_new_dept(_model.dep_name, userid);
            if (!error_text.ToLower().Contains("error"))
            {
                ModelState.Clear();
            }
            _model = dept.get_all_active_departments();
            _model = new helper().GenerateError<department>(_model, error_text);
            return View(_model);
        }


        [HttpPost]
        public ActionResult update(string department, int deptid)
        {
            var dept = new departmentservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            var error_text = dept.update_dept(department, deptid, userid);
            department _model = new department();
            _model = dept.get_all_active_departments();
            _model = new helper().GenerateError<department>(_model, error_text);
            return View("index", _model);
        }


        public ActionResult delete(int deptid)
        {
            if (!_permission.Contains(20))
            {
                return View("unauth");
            }
            var dept = new departmentservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            var error_text = dept.delete_dept(deptid, userid);
            department _model = new department();
            _model = dept.get_all_active_departments();
            _model = new helper().GenerateError<department>(_model, error_text);
            return View("index", _model);
        }
    }
}