﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class designationsController : Controller
    {
        public List<int> _permission;
        public designationsController()
        {
            int roleid = System.Web.HttpContext.Current.Request.Cookies["_pid"] == null ? 0 : Convert.ToInt16(System.Web.HttpContext.Current.Request.Cookies["_pid"].Value.ToString());
            roleservices _ser = new roleservices();
            _permission = _ser.get_all_active_permissions_bytypeid(7, roleid);
        }
        public ActionResult Index()
        {
            if (!_permission.Contains(4))
            {
                return View("unauth");
            }
            ViewBag.permissions = _permission;
            var dept = new designationservices();
            var _model = dept.get_all_active_designations();
            string message = "";
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            _model._depts = GetDepartments();
            _model = new helper().GenerateError<designation>(_model, message);
            return View(_model);
        }

        public ActionResult update()
        {
            ViewBag.permissions = _permission;
            var dept = new designationservices();
            var _model = dept.get_all_active_designations();
            _model._depts = GetDepartments();
            _model = new helper().GenerateError<designation>(_model, _model.error_text);
            return View("index", _model);
        }

        [HttpPost]
        public ActionResult Index(designation _model)
        {
            var dept = new designationservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            int deptid = _model.deptid;
            var error_text = dept.add_new_designations(_model.desgnation_name, userid,_model.deptid);
            if (!error_text.ToLower().Contains("error"))
            {
                ModelState.Clear();
            }
            _model = dept.get_all_active_designations();
            _model._depts = GetDepartments();
            _model.deptid = deptid;
            _model = new helper().GenerateError<designation>(_model, error_text);
            return View(_model);
        }


        [HttpPost]
        public ActionResult update(string designation,int desgid, int depid)
        {
            var dept = new designationservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            var error_text = dept.update_designations(designation, desgid, depid, userid);
            designation _model = new designation();
            _model = dept.get_all_active_designations();
            _model._depts = GetDepartments();
            _model = new helper().GenerateError<designation>(_model, error_text);
            return View("index", _model);
        }


        public ActionResult delete(int desgid)
        {
            if (!_permission.Contains(24))
            {
                return View("unauth");
            }
            var dept = new designationservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            var error_text = dept.delete_designations(desgid, userid);
            designation _model = new designation();
            _model = dept.get_all_active_designations();
            _model._depts = GetDepartments();
            _model = new helper().GenerateError<designation>(_model, error_text);
            return View("index", _model);
        }


        public List<department_items> GetDepartments()
        {
            var dept = new departmentservices();
            var _model = dept.get_all_active_departments();
            return _model._dept;
        }
    }
}