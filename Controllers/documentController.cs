﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    public class documentController : Controller
    {
        public List<int> _permission;
        public documentController()
        {
            int roleid = System.Web.HttpContext.Current.Request.Cookies["_pid"] == null ? 0 : Convert.ToInt16(System.Web.HttpContext.Current.Request.Cookies["_pid"].Value.ToString());
            roleservices _ser = new roleservices();
            _permission = _ser.get_all_active_permissions_bytypeid(6, roleid);
        }
        public ActionResult Index()
        {
            //if (!_permission.Contains(3))
            //{
            //    return View("unauth");
            //}
           doctype _model = new doctype();
            var dept = new documentservices();
            _model.doc_type = dept.get_all_active_types();
            _model.mainorder = dept.getmaxorder();
            string message = "";
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            ViewBag.permissions = _permission;
            _model = new helper().GenerateError<doctype>(_model, message);
            return View(_model);
        }

        

        [HttpPost]
        public ActionResult Index(doctype _model)
        {
            var dept = new documentservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            var error_text = dept.add_new_type(_model.name, userid,_model.mainorder);
            if (!error_text.ToLower().Contains("error"))
            {
                ModelState.Clear();
            }
            doctype _model1 = new doctype();
            _model1.doc_type = dept.get_all_active_types();
            _model1.mainorder = dept.getmaxorder();
            _model1 = new helper().GenerateError<doctype>(_model1, error_text);
            return View(_model1);
        }

        [HttpPost]
        public ActionResult update(string name, int id,int mainorder)
        {
            var dept = new documentservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            var error_text = dept.update_type(name, id, userid, mainorder);
            doctype _model = new doctype();
            _model.doc_type = dept.get_all_active_types();
            _model.mainorder = dept.getmaxorder();
            _model = new helper().GenerateError<doctype>(_model, error_text);
            return View("index", _model);
        }
        public ActionResult delete(int id)
        {
            //if (!_permission.Contains(20))
            //{
            //    return View("unauth");
            //}
            var dept = new documentservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            var error_text = dept.delete_type(id, userid);
            doctype _model = new doctype();
            _model.doc_type = dept.get_all_active_types();

            _model.mainorder = dept.getmaxorder();
            _model = new helper().GenerateError<doctype>(_model, error_text);
            return View("index", _model);
        }

    }
}