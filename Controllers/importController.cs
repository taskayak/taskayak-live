﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class importController : Controller
    {
        helper _helper = new helper();
        public string conctionstring = ConfigurationManager.ConnectionStrings["con"].ToString();

        private String sendError(string msg)
        {
            String funcNum = System.Web.HttpContext.Current.Request["CKEditorFuncNum"];
            return "<scr" + "ipt type='text/javascript'> window.parent.CKEDITOR.tools.callFunction(" + funcNum + ", '', '" + msg + "')</scr" + "ipt>";

        }
        [HttpPost]
        public String Upload(string baseData)
        {
            //String basePath = "C:\\TestFiles\\";
            String basePath = Server.MapPath("~/CkEditorFiles"); //"D:\\TestFiles\\";

            String baseUrl = "https://www.taskayak.com/CkEditorFiles/";
            //String baseUrl = "/ckfinder/userfiles/";
            // Optional: instance name (might be used to adjust the server folders for example)
            String CKEditor = System.Web.HttpContext.Current.Request["CKEditor"];

            // Required: Function number as indicated by CKEditor.
            String funcNum = System.Web.HttpContext.Current.Request["CKEditorFuncNum"];

            // Optional: To provide localized messages
            String langCode = System.Web.HttpContext.Current.Request["langCode"];

            int total;
            try
            {
                total = System.Web.HttpContext.Current.Request.Files.Count;
            }
            catch (Exception e)
            {
                return sendError("Error uploading the file");
            }
            if (total == 0)
                return sendError("No file has been sent");

            if (!System.IO.Directory.Exists(basePath))
                return sendError("basePath folder doesn't exists");

            //Grab the file name from its fully qualified path at client
            HttpPostedFile theFile = System.Web.HttpContext.Current.Request.Files[0];

            String strFileName = theFile.FileName;
            if (strFileName == "")
                return sendError("File name is empty");

            String sFileName = System.IO.Path.GetFileName(strFileName);

            String name = System.IO.Path.Combine(basePath, sFileName);
            theFile.SaveAs(name);

            String url = baseUrl + sFileName.Replace("'", "\'");

            // ------------------------
            // Write output
            // ------------------------

            //return "<scr" + "ipt type='text/javascript'> window.parent.CKEDITOR.tools.callFunction(" + funcNum + ", '" + url + "', '')</scr" + "ipt>";

            return "<scr" + "ipt type='text/javascript'> window.parent.CKEDITOR.tools.callFunction(" + funcNum + ", '" + url + "', '')</scr" + "ipt>";
        }
        public ActionResult uploadPartial(int CKEditorFuncNum = 1)
        {
            var appData = Server.MapPath("~/CkEditorFiles");
            var images = Directory.GetFiles(appData).Select(x => new imagesviewmodel
            {
                Url = "https://www.taskayak.com/CkEditorFiles/" + Path.GetFileName(x)
            });
            ViewBag.CKEditorFuncNum = CKEditorFuncNum;
            return View(images);
        }
        [HttpPost]
        public JsonResult uploadimage(HttpPostedFileBase myfile)
        {
            string newfilename = "i" + DateTime.Now.Ticks.ToString() + myfile.FileName;
            string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
            myfile.SaveAs(filepath);
            var rpath = "https://www.taskayak.com/csvfiles/" + newfilename;
            return Json(rpath, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult client(HttpPostedFileBase file)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());

            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                string newfilename = "cl_" + DateTime.Now.Ticks.ToString() + file.FileName;
                string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                file.SaveAs(filepath);
                DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 7, true);
                dataTable = GetClientForBulkInsert(dataTable, userid);
                var objBulk = new BulkUploadToSql()
                {
                    InternalStore_dt = dataTable,
                    TableName = "tbl_clients",
                    CommitBatchSize = 200,
                    ConnectionString = conctionstring
                };
                objBulk.Commit_datatable();
                TempData["error"] = dataTable.Rows.Count + " Clients Imported succesfully";
                return Redirect("https://taskayak.com/client/index/");
                //RedirectToAction("index", "client");
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                return Redirect("https://taskayak.com/client/index/");
                // return RedirectToAction("index", "client");
            }
        }
        public DataTable GetClientForBulkInsert(DataTable gv, int createdby)
        {
            DataTable dataTable = new DataTable("DummyTable");
            DataRow row = null;
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("Clientname", typeof(string));
            dataTable.Columns.Add("rate", typeof(string));
            dataTable.Columns.Add("Address", typeof(string));
            dataTable.Columns.Add("Cityid", typeof(int));
            dataTable.Columns.Add("Stateid", typeof(int));
            dataTable.Columns.Add("Pincode", typeof(string));
            dataTable.Columns.Add("CreatedDate", typeof(DateTime));
            dataTable.Columns.Add("createdby", typeof(int));
            dataTable.Columns.Add("Email", typeof(string));
            dataTable.Columns.Add("Phone", typeof(string));
            dataTable.Columns.Add("primarycontact", typeof(string));
            try
            {
                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = gv.Rows[i][0].ToString();
                    row[2] = gv.Rows[i][3].ToString();
                    row[3] = gv.Rows[i][4].ToString();
                    row[4] = 0;
                    row[5] = 0;
                    row[6] = gv.Rows[i][5].ToString();
                    row[7] = DateTime.Now;
                    row[8] = createdby;
                    row[9] = gv.Rows[i][1].ToString();
                    row[10] = gv.Rows[i][2].ToString();
                    row[11] = gv.Rows[i][6].ToString();
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception exception)
            {
            }
            return dataTable;
        }
        [HttpPost]
        public ActionResult manager(HttpPostedFileBase file, int clientid)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            Crypto _crypt = new Crypto();
            string token = _crypt.EncryptStringAES(clientid.ToString());
            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                string newfilename = "mgr_" + DateTime.Now.Ticks.ToString() + file.FileName;
                string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                file.SaveAs(filepath);
                DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 3, true);
                dataTable = GetmanagerForBulkInsert(dataTable, userid, clientid);
                var objBulk = new BulkUploadToSql()
                {
                    InternalStore_dt = dataTable,
                    TableName = "tbl_projectmanager",
                    CommitBatchSize = 200,
                    ConnectionString = conctionstring
                };
                objBulk.Commit_datatable();
                TempData["error"] = dataTable.Rows.Count + " Project Manager Imported succesfully";
                return RedirectToAction("view", "client", new { token = token });
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                return RedirectToAction("view", "client", new { token = token });
            }
        }

        [HttpPost]
        public ActionResult cmember(HttpPostedFileBase file, int clientid)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var type = new memberservices().getmembertype(userid);
            bool issubcontractor = type.ToLower() == "s" ? true : false;
            Crypto _crypt = new Crypto();
            string token = _crypt.EncryptStringAES(clientid.ToString());
            string ext = Path.GetExtension(file.FileName);
            List<string> members = new List<string>();
            if (ext.ToLower() == ".csv")
            {
                Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                string newfilename = "cmem_" + DateTime.Now.Ticks.ToString() + file.FileName;
                string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                file.SaveAs(filepath);
                DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 9, true);
                dataTable = GetcmemberForBulkInsert(dataTable, userid, clientid, ref members);
                var objBulk = new BulkUploadToSql()
                {
                    InternalStore_dt = dataTable,
                    TableName = "tbl_member",
                    CommitBatchSize = 200,
                    ConnectionString = conctionstring
                };
                objBulk.Commit_datatable();
                var template = new templateservices().get_template(3);
                new jobofferservices().sendoffers4(members, 3, true, true, template.sms, template.email, "Field Technician Account", true);
                TempData["error"] = dataTable.Rows.Count + " Users imported succesfully";
                return RedirectToAction("view", "Company", new { token = token, issubcontractor = issubcontractor });
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                return RedirectToAction("view", "Company", new { token = token, issubcontractor = issubcontractor });
            }
        }



        public DataTable GetmanagerForBulkInsert(DataTable gv, int createdby, int clientid)
        {
            DataTable dataTable = new DataTable("DummyTable");
            DataRow row = null;
            dataTable.Columns.Add("id", typeof(int));
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("ClientId", typeof(int));
            dataTable.Columns.Add("Createddate", typeof(DateTime));
            dataTable.Columns.Add("CreatedBy", typeof(int));
            dataTable.Columns.Add("Phonenumber", typeof(string));
            dataTable.Columns.Add("Email", typeof(string));
            try
            {
                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = gv.Rows[i][0].ToString();
                    row[2] = clientid;
                    row[3] = DateTime.Now;
                    row[4] = createdby;
                    row[5] = gv.Rows[i][2].ToString();
                    row[6] = gv.Rows[i][1].ToString();
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception exception)
            {
            }
            return dataTable;
        }
        [HttpPost]
        public ActionResult department(HttpPostedFileBase file)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());

            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                string newfilename = "dep_" + DateTime.Now.Ticks.ToString() + file.FileName;
                string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                file.SaveAs(filepath);
                DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 1, true);
                dataTable = GetDepartmentForBulkInsert(dataTable, userid);
                var objBulk = new BulkUploadToSql()
                {
                    InternalStore_dt = dataTable,
                    TableName = "tbl_department",
                    CommitBatchSize = 200,
                    ConnectionString = conctionstring
                };
                objBulk.Commit_datatable();
                TempData["error"] = dataTable.Rows.Count + " Departments Imported succesfully";
               // return RedirectToAction("index", "departments");
                return Redirect("https://taskayak.com/departments/index/");
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                return Redirect("https://taskayak.com/departments/index/");
                //return RedirectToAction("index", "departments");
            }
        }
        public DataTable GetDepartmentForBulkInsert(DataTable gv, int createdby)
        {
            DataTable dataTable = new DataTable("DummyTable");
            DataRow row = null;
            dataTable.Columns.Add("id", typeof(int));
            dataTable.Columns.Add("name", typeof(string));
            dataTable.Columns.Add("createddate", typeof(DateTime));
            dataTable.Columns.Add("createdby", typeof(int));
            dataTable.Columns.Add("Isactive", typeof(bool));
            dataTable.Columns.Add("updatedBy", typeof(int));
            dataTable.Columns.Add("updateddate", typeof(DateTime));
            try
            {
                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = gv.Rows[i][0].ToString();
                    row[2] = DateTime.Now;
                    row[3] = createdby;
                    row[4] = true;
                    row[5] = createdby;
                    row[6] = DateTime.Now;
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception exception)
            {
            }
            return dataTable;
        }
        [HttpPost]
        public ActionResult designation(HttpPostedFileBase file, int department)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());

            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                string newfilename = "des_" + DateTime.Now.Ticks.ToString() + file.FileName;
                string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                file.SaveAs(filepath);
                DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 1, true);
                dataTable = GetDesignationForBulkInsert(dataTable, userid, department);
                var objBulk = new BulkUploadToSql()
                {
                    InternalStore_dt = dataTable,
                    TableName = "tbl_designation",
                    CommitBatchSize = 200,
                    ConnectionString = conctionstring
                };
                objBulk.Commit_datatable();
                TempData["error"] = dataTable.Rows.Count + " Designations Imported succesfully";
               // return RedirectToAction("index", "designations");
                return Redirect("https://taskayak.com/designations/index/");
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                return Redirect("https://taskayak.com/designations/index/");
                //return RedirectToAction("index", "designations");
            }
        }
        public DataTable GetDesignationForBulkInsert(DataTable gv, int createdby, int depid)
        {
            DataTable dataTable = new DataTable("DummyTable");
            DataRow row = null;
            dataTable.Columns.Add("id", typeof(int));
            dataTable.Columns.Add("name", typeof(string));
            dataTable.Columns.Add("createddate", typeof(DateTime));
            dataTable.Columns.Add("createdby", typeof(int));
            dataTable.Columns.Add("isactive", typeof(bool));
            dataTable.Columns.Add("deptid", typeof(int));
            dataTable.Columns.Add("updateby", typeof(int));
            dataTable.Columns.Add("updateddate", typeof(DateTime));
            try
            {
                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = gv.Rows[i][0].ToString();
                    row[2] = DateTime.Now;
                    row[3] = createdby;
                    row[4] = true;
                    row[5] = depid;
                    row[6] = createdby;
                    row[7] = DateTime.Now;
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception exception)
            {
            }
            return dataTable;
        }

        [HttpPost]
        public ActionResult skill(HttpPostedFileBase file)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());

            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                string newfilename = "skl_" + DateTime.Now.Ticks.ToString() + file.FileName;
                string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                file.SaveAs(filepath);
                DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 1, true);
                dataTable = GetSkillForBulkInsert(dataTable, userid);
                var objBulk = new BulkUploadToSql()
                {
                    InternalStore_dt = dataTable,
                    TableName = "tbl_skill_master_import",
                    CommitBatchSize = 200,
                    ConnectionString = conctionstring
                };
                objBulk.Commit_datatable();
                TempData["error"] = dataTable.Rows.Count + " Skills Imported succesfully";
                return Redirect("https://taskayak.com/Skills/index/");
                //return RedirectToAction("index", "Skills");
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                return Redirect("https://taskayak.com/Skills/index/");
                // return RedirectToAction("index", "Skills");
            }
        }
        public DataTable GetSkillForBulkInsert(DataTable gv, int createdby)
        {
            DataTable dataTable = new DataTable("DummyTable");
            DataRow row = null;
            dataTable.Columns.Add("skill_Id", typeof(int));
            dataTable.Columns.Add("name", typeof(string));
            dataTable.Columns.Add("CReatedDate", typeof(DateTime));
            dataTable.Columns.Add("Createdby", typeof(int));
            dataTable.Columns.Add("UpdatedBy", typeof(int));
            dataTable.Columns.Add("UpdatedDate", typeof(DateTime));
            dataTable.Columns.Add("IsActive", typeof(bool));
            try
            {
                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = gv.Rows[i][0].ToString();
                    row[2] = DateTime.Now;
                    row[3] = createdby;
                    row[4] = createdby;
                    row[5] = DateTime.Now;
                    row[6] = true;
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception exception)
            {
            }
            return dataTable;
        }
        public int getcityidbystate(int stateid, string city)
        {
            if (string.IsNullOrEmpty(city))
            {
                return 0;
            }
            else
            {
                return new defaultservices().savegetCityId(stateid, city);
            }

        }

        //leads
        [HttpPost]
        public ActionResult members(HttpPostedFileBase file, int state_modal_id = 0, string city_modal_id = "")
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());

            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                string newfilename = "led_" + DateTime.Now.Ticks.ToString() + file.FileName;
                string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                file.SaveAs(filepath);
                DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 9, true);
                int cityid = getcityidbystate(state_modal_id, city_modal_id);
                dataTable = GetLeadsForBulkInsert(dataTable, userid, cityid, state_modal_id);
                var objBulk = new BulkUploadToSql()
                {
                    InternalStore_dt = dataTable,
                    TableName = "tbl_lead",
                    CommitBatchSize = 200,
                    ConnectionString = conctionstring
                };
                objBulk.Commit_datatable();
                TempData["error"] = dataTable.Rows.Count + " Members Imported succesfully";
               // return RedirectToAction("index", "members");
                return Redirect("https://taskayak.com/members/index/");
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                return Redirect("https://taskayak.com/members/index/");
                // return RedirectToAction("index", "members");
            }
        }

        [HttpPost]
        public ActionResult membersbymanual(HttpPostedFileBase file)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());

            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                string newfilename = "led_" + DateTime.Now.Ticks.ToString() + file.FileName;
                string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                file.SaveAs(filepath);
                DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 21, true);
                //int cityid = getcityidbystate(0, 0);
                dataTable = GetLeadsmanualForBulkInsert(dataTable);
                var objBulk = new BulkUploadToSql()
                {
                    InternalStore_dt = dataTable,
                    TableName = "tbl_lead",
                    CommitBatchSize = 200,
                    ConnectionString = conctionstring
                };
                objBulk.Commit_datatable();
                TempData["error"] = dataTable.Rows.Count + " Members Imported succesfully";
                //return RedirectToAction("index", "members");
                return Redirect("https://taskayak.com/members/index/");
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                return Redirect("https://taskayak.com/members/index/");
                //return RedirectToAction("index", "members");
            }
        }

        public int getSourceid(string source)
        {
            int srcid = 0;
            switch (source.ToLower())
            {
                case "employee applicant":
                    srcid = 1;
                    break;
                case "technician":
                    srcid = 2;
                    break;
                case "customer":
                    srcid = 3;
                    break;
            }

            return srcid;
        }
        public DataTable GetLeadsForBulkInsert(DataTable gv, int createdby, int cityid, int stateid)
        {
            DataTable dataTable = new DataTable("DummyTable");
            DataRow row = null;
            dataTable.Columns.Add("leadid", typeof(int));
            dataTable.Columns.Add("Firstname", typeof(string));
            dataTable.Columns.Add("Lastname", typeof(string));
            dataTable.Columns.Add("Cityid", typeof(int));
            dataTable.Columns.Add("Stateid", typeof(int));
            dataTable.Columns.Add("phonernumber", typeof(string));
            dataTable.Columns.Add("Email", typeof(string));
            dataTable.Columns.Add("rate", typeof(string));
            dataTable.Columns.Add("Sourcid", typeof(int));
            dataTable.Columns.Add("Statusid", typeof(int));
            dataTable.Columns.Add("createdby", typeof(int));
            dataTable.Columns.Add("createddate", typeof(DateTime));
            dataTable.Columns.Add("convertedby", typeof(int));
            dataTable.Columns.Add("converteddate", typeof(DateTime));
            dataTable.Columns.Add("convertedmemberid", typeof(int));
            dataTable.Columns.Add("skills", typeof(string));
            dataTable.Columns.Add("tbldatafetchid", typeof(int));
            dataTable.Columns.Add("company_Name", typeof(string));
            dataTable.Columns.Add("Company", typeof(string));
            dataTable.Columns.Add("Website", typeof(string));
            dataTable.Columns.Add("LinkedIn", typeof(string));
            try
            {
                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = gv.Rows[i][0].ToString();
                    row[2] = gv.Rows[i][1].ToString();
                    row[3] = cityid;
                    row[4] = stateid;
                    row[5] = gv.Rows[i][2].ToString();
                    row[6] = gv.Rows[i][3].ToString();
                    row[7] = gv.Rows[i][4].ToString();
                    row[8] = getSourceid(gv.Rows[i][5].ToString());
                    row[9] = 1;
                    row[10] = createdby;
                    row[11] = DateTime.Now;
                    row[12] = 0;
                    row[13] = DateTime.Now;
                    row[14] = 0;
                    row[15] = "";
                    row[16] = 0;
                    row[17] = "";
                    row[18] = gv.Rows[i][6].ToString();
                    row[19] = gv.Rows[i][7].ToString();
                    row[20] = gv.Rows[i][8].ToString();
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception exception)
            {
            }
            return dataTable;
        }


        public DataTable GetLeadsmanualForBulkInsert(DataTable gv)
        {
            DataTable dataTable = new DataTable("DummyTable");
            DataRow row = null;
            dataTable.Columns.Add("leadid", typeof(int));
            dataTable.Columns.Add("Firstname", typeof(string));
            dataTable.Columns.Add("Lastname", typeof(string));
            dataTable.Columns.Add("Cityid", typeof(int));
            dataTable.Columns.Add("Stateid", typeof(int));
            dataTable.Columns.Add("phonernumber", typeof(string));
            dataTable.Columns.Add("Email", typeof(string));
            dataTable.Columns.Add("rate", typeof(string));
            dataTable.Columns.Add("Sourcid", typeof(int));
            dataTable.Columns.Add("Statusid", typeof(int));
            dataTable.Columns.Add("createdby", typeof(int));
            dataTable.Columns.Add("createddate", typeof(DateTime));
            dataTable.Columns.Add("convertedby", typeof(int));
            dataTable.Columns.Add("converteddate", typeof(DateTime));
            dataTable.Columns.Add("convertedmemberid", typeof(int));
            dataTable.Columns.Add("skills", typeof(string));
            dataTable.Columns.Add("tbldatafetchid", typeof(int));
            dataTable.Columns.Add("company_Name", typeof(string));
            dataTable.Columns.Add("Company", typeof(string));
            dataTable.Columns.Add("Website", typeof(string));
            dataTable.Columns.Add("LinkedIn", typeof(string));
            try
            {
                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = gv.Rows[i][1].ToString();
                    row[2] = gv.Rows[i][2].ToString();
                    row[3] = Convert.ToInt32(gv.Rows[i][3].ToString());
                    row[4] = Convert.ToInt32(gv.Rows[i][4].ToString());
                    row[5] = gv.Rows[i][5].ToString();
                    row[6] = gv.Rows[i][6].ToString();
                    row[7] = gv.Rows[i][7].ToString();
                    row[8] = Convert.ToInt32(gv.Rows[i][8].ToString());
                    row[9] = Convert.ToInt32(gv.Rows[i][9].ToString());
                    row[10] = Convert.ToInt32(gv.Rows[i][10].ToString()); 
                    row[11] = DateTime.Now;
                    row[12] =0; 
                    row[13] = DateTime.Now;
                    row[14] = 0; 
                    row[15] = "";
                    row[16] = 0 ;
                    row[17] = "";
                    row[18] = gv.Rows[i][18].ToString();
                    row[19] = gv.Rows[i][19].ToString();
                    row[20] = gv.Rows[i][20].ToString();
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception exception)
            {
            }
            return dataTable;
        }

        //jobs
        [HttpPost]
        public ActionResult offer(HttpPostedFileBase file, int type = 1, int uploadProject_manager = 0, int uplodclient = 0, string upclientRate = "$0.00",int uploadProject_manager1=0)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                string newfilename = "joboffer_" + DateTime.Now.Ticks.ToString() + file.FileName;
                string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                file.SaveAs(filepath);
                DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 9, true);
                dataTable = GetjoboffersForBulkInsert(dataTable, userid, uplodclient, uploadProject_manager, upclientRate, uploadProject_manager1);
                var objBulk = new BulkUploadToSql()
                {
                    InternalStore_dt = dataTable,
                    TableName = "tbl_job_offer_temp",
                    CommitBatchSize = 200,
                    ConnectionString = conctionstring
                };
                objBulk.Commit_datatable("tbl_job_offer_insert_from_temp");
                TempData["error"] = dataTable.Rows.Count + " offers imported succesfully";
               // return RedirectToAction("index", "offer", new { type = type });
                return Redirect("https://taskayak.com/offer/index?type="+ type);
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                return Redirect("https://taskayak.com/offer/index?type=" + type);
                //return RedirectToAction("index", "offer", new { type = type });
            }
        }
        public DataTable GetjoboffersForBulkInsert(DataTable gv, int createdby, int client, int projectmanager, string clientRate,int uploadProject_manager1)
        {
            DataTable dataTable = new DataTable("DummyTable");
            DataRow row = null;
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("JobId", typeof(string));
            dataTable.Columns.Add("Ttile", typeof(string));
            dataTable.Columns.Add("Client", typeof(int));
            dataTable.Columns.Add("Technician", typeof(int));
            dataTable.Columns.Add("Dispatcher", typeof(int));
            dataTable.Columns.Add("Project_manager", typeof(int));
            dataTable.Columns.Add("startdate", typeof(DateTime));
            dataTable.Columns.Add("enddate", typeof(DateTime));
            dataTable.Columns.Add("Client_rate", typeof(string));
            dataTable.Columns.Add("Pay_rate", typeof(string));
            dataTable.Columns.Add("hours", typeof(double));
            dataTable.Columns.Add("est_Hours", typeof(double));
            dataTable.Columns.Add("Status_id", typeof(int));
            dataTable.Columns.Add("CreatedBy", typeof(int));
            dataTable.Columns.Add("CreatedDate", typeof(DateTime));
            dataTable.Columns.Add("City_id", typeof(int));
            dataTable.Columns.Add("State_id", typeof(int));
            dataTable.Columns.Add("Description", typeof(string));
            dataTable.Columns.Add("Expnese", typeof(string));
            dataTable.Columns.Add("street", typeof(string));
            dataTable.Columns.Add("zipcode", typeof(string));
            dataTable.Columns.Add("isview", typeof(bool));
            dataTable.Columns.Add("isaccepted", typeof(bool));
            dataTable.Columns.Add("projectmangerid2", typeof(int));
            try
            {

                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    string _sdate = gv.Rows[i][2].ToString() + " " + gv.Rows[i][3].ToString();
                    string _edate = gv.Rows[i][2].ToString() + " " + gv.Rows[i][4].ToString();
                    TimeSpan span = (Convert.ToDateTime(_edate) - Convert.ToDateTime(_sdate));
                    var estimhour = String.Format("{0}.{1}", span.Hours, span.Minutes > 0 ? (int)Math.Round(Convert.ToDouble(span.Minutes) * 1.66) : span.Minutes);
                    row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = gv.Rows[i][0].ToString();
                    row[2] = gv.Rows[i][1].ToString();
                    row[3] = client;
                    row[4] = 0;
                    row[5] = 0;
                    row[6] = projectmanager;
                    row[7] = _sdate;
                    row[8] = _edate;
                    row[9] = gv.Rows[i][7].ToString() == "$0.00" ? clientRate : gv.Rows[i][7].ToString();
                    row[10] = gv.Rows[i][8].ToString();
                    row[11] = estimhour;
                    row[12] = estimhour;
                    row[13] = 1;
                    row[14] = createdby;
                    row[15] = DateTime.Now;
                    row[16] = 0;
                    row[17] = 0;
                    row[18] = gv.Rows[i][6].ToString();
                    row[19] = "$0.00";
                    row[20] = gv.Rows[i][5].ToString();
                    row[21] ="";
                    row[22] = false;
                    row[23] = false;
                    row[24] = uploadProject_manager1;
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception exception)
            {
            }
            return dataTable;
        }


        [HttpPost]
        public ActionResult job(HttpPostedFileBase file, int type = 1, int uploadProject_manager = 0,int uploadProject_manager1=0, int uplodclient = 0, string upclientRate = "$0.00")
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());

            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                string newfilename = "job_" + DateTime.Now.Ticks.ToString() + file.FileName;
                string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                file.SaveAs(filepath);
                DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 8, true);
                dataTable = GetjobsForBulkInsert(dataTable, userid, uplodclient, uploadProject_manager, upclientRate, uploadProject_manager1);
                var objBulk = new BulkUploadToSql()
                {
                    InternalStore_dt = dataTable,
                    TableName = "tbl_job_temp",
                    CommitBatchSize = 200,
                    ConnectionString = conctionstring
                };
                objBulk.Commit_datatable("tbl_job_insert_from_temp");
                TempData["error"] = dataTable.Rows.Count + " jobs imported succesfully";
                //return RedirectToAction("index", "jobs", new { type = type });
                return Redirect("https://taskayak.com/jobs/index?type=" + type);
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                //return RedirectToAction("index", "jobs", new { type = type });
                return Redirect("https://taskayak.com/jobs/index?type=" + type);
            }
        }

        public DataTable GetjobsForBulkInsert(DataTable gv, int createdby, int client, int projectmanager, string clientRate,int uploadProject_manager1)
        {
            DataTable dataTable = new DataTable("DummyTable");
            DataRow row = null;
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("JobId", typeof(string));
            dataTable.Columns.Add("Ttile", typeof(string));
            dataTable.Columns.Add("Client", typeof(int));
            dataTable.Columns.Add("Technician", typeof(int));
            dataTable.Columns.Add("Dispatcher", typeof(int));
            dataTable.Columns.Add("Project_manager", typeof(int));
            dataTable.Columns.Add("startdate", typeof(DateTime));
            dataTable.Columns.Add("enddate", typeof(DateTime));
            dataTable.Columns.Add("Client_rate", typeof(string));
            dataTable.Columns.Add("Pay_rate", typeof(string));
            dataTable.Columns.Add("hours", typeof(double));
            dataTable.Columns.Add("est_Hours", typeof(double));
            dataTable.Columns.Add("Status_id", typeof(int));
            dataTable.Columns.Add("CreatedBy", typeof(int));
            dataTable.Columns.Add("CreatedDate", typeof(DateTime));
            dataTable.Columns.Add("City_id", typeof(int));
            dataTable.Columns.Add("State_id", typeof(int));
            dataTable.Columns.Add("Description", typeof(string));
            dataTable.Columns.Add("Expnese", typeof(string));
            dataTable.Columns.Add("street", typeof(string));
            dataTable.Columns.Add("zipcode", typeof(string));
            dataTable.Columns.Add("projectmangerid2", typeof(int));
            try
            {

                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    string _sdate = gv.Rows[i][2].ToString() + " " + gv.Rows[i][3].ToString();
                    string _edate = gv.Rows[i][2].ToString() + " " + gv.Rows[i][4].ToString();
                    TimeSpan span = (Convert.ToDateTime(_edate) - Convert.ToDateTime(_sdate));
                    var estimhour = String.Format("{0}.{1}", span.Hours, span.Minutes > 0 ? (int)Math.Round(Convert.ToDouble(span.Minutes) * 1.66) : span.Minutes);
                    row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = gv.Rows[i][0].ToString();
                    row[2] = gv.Rows[i][1].ToString();
                    row[3] = client;
                    row[4] = 0;
                    row[5] = 0;
                    row[6] = projectmanager;
                    row[7] = _sdate;
                    row[8] = _edate;
                    row[9] = gv.Rows[i][7].ToString() == "$0.00" ? clientRate : gv.Rows[i][7].ToString();
                    row[10] = "$0.00";
                    row[11] = estimhour;
                    row[12] = estimhour;
                    row[13] = 1;
                    row[14] = createdby;
                    row[15] = DateTime.Now;
                    row[16] = 0;
                    row[17] = 0;
                    row[18] = gv.Rows[i][6].ToString();
                    row[19] = gv.Rows[i][5].ToString();
                    row[20] = "";
                    row[21] = "";
                    row[22] = uploadProject_manager1;
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception exception)
            {
            }
            return dataTable;
        }


        [HttpPost]
        public ActionResult users(HttpPostedFileBase file)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());

            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                string newfilename = "mbr_" + DateTime.Now.Ticks.ToString() + file.FileName;
                string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                file.SaveAs(filepath);
                DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 15, true);
                dataTable = GetmemberForBulkInsert(dataTable, userid);
                var objBulk = new BulkUploadToSql()
                {
                    InternalStore_dt = dataTable,
                    TableName = "tbl_member_temp",
                    CommitBatchSize = 200,
                    ConnectionString = conctionstring
                };
                objBulk.Commit_datatable("tbl_member_insert_from_temp");
                TempData["error"] = dataTable.Rows.Count + " users imported succesfully";
                return Redirect("https://taskayak.com/users/index");
                //return RedirectToAction("index", "users");
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                return Redirect("https://taskayak.com/users/index");
                // return RedirectToAction("index", "users");
            }
        }
        public DataTable GetmemberForBulkInsert(DataTable gv, int createdby)
        {
            Crypto _cr = new Crypto();
            DataTable dataTable = new DataTable("DummyTable");
            DataRow row = null;
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("member_id", typeof(string));
            dataTable.Columns.Add("f_name", typeof(string));
            dataTable.Columns.Add("l_name", typeof(string));
            dataTable.Columns.Add("address", typeof(string));
            dataTable.Columns.Add("city_id", typeof(int));
            dataTable.Columns.Add("state_id", typeof(int));
            dataTable.Columns.Add("Zip_code", typeof(string));
            dataTable.Columns.Add("Phone", typeof(string));
            dataTable.Columns.Add("EMail", typeof(string));
            dataTable.Columns.Add("Alternative_phone", typeof(string));
            dataTable.Columns.Add("RateId", typeof(int));
            dataTable.Columns.Add("Background", typeof(string));
            dataTable.Columns.Add("drug_tested", typeof(string));
            dataTable.Columns.Add("dob", typeof(DateTime));
            dataTable.Columns.Add("Gender", typeof(string));
            dataTable.Columns.Add("RoleId", typeof(int));
            dataTable.Columns.Add("Department_Id", typeof(int));
            dataTable.Columns.Add("Designation_id", typeof(int));
            dataTable.Columns.Add("ManagerId", typeof(int));
            dataTable.Columns.Add("Username", typeof(string));
            dataTable.Columns.Add("Password", typeof(string));
            dataTable.Columns.Add("Tax", typeof(string));
            dataTable.Columns.Add("member_status_id", typeof(int));
            dataTable.Columns.Add("CreatedDate", typeof(DateTime));
            dataTable.Columns.Add("CreatedBy", typeof(int));
            dataTable.Columns.Add("Converted_lead", typeof(bool));
            dataTable.Columns.Add("issuper_Admin", typeof(bool));
            dataTable.Columns.Add("Imageurl", typeof(string));
            dataTable.Columns.Add("Skills", typeof(string));
            try
            {
                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = gv.Rows[i][2].ToString();
                    row[2] = gv.Rows[i][0].ToString().ToTitleCase();
                    row[3] = gv.Rows[i][1].ToString().ToTitleCase();
                    row[4] = gv.Rows[i][14].ToString();
                    row[5] = 0;
                    row[6] = 0;
                    row[7] = gv.Rows[i][13].ToString();
                    row[8] = gv.Rows[i][11].ToString().PhoneNumber();
                    row[9] = gv.Rows[i][10].ToString();
                    row[10] = gv.Rows[i][12].ToString();
                    row[11] = 0;
                    row[12] = gv.Rows[i][7].ToString();
                    row[13] = gv.Rows[i][8].ToString();
                    row[14] = gv.Rows[i][6].ToString();
                    row[15] = gv.Rows[i][9].ToString();
                    row[16] = 0;
                    row[17] = 0;
                    row[18] = 0;
                    row[19] = 0;
                    row[20] = gv.Rows[i][3].ToString();
                    row[21] = _cr.EncryptStringAES(gv.Rows[i][4].ToString());
                    row[22] = gv.Rows[i][5].ToString();
                    row[23] = 1;
                    row[24] = DateTime.Now;
                    row[25] = createdby;
                    row[26] = false;
                    row[27] = false;
                    row[28] = "default";
                    row[29] = "";
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception exception)
            {
            }
            return dataTable;
        }

        public string getmembertype(string typename)
        {
            string membertype = "M";
            switch (typename.ToLower())
            {
                case "employee":
                    membertype = "E";
                    break;
                case "dispatcher":
                    membertype = "D";
                    break;
                case "project manager":
                    membertype = "PM";
                    break;
                case "technician":
                    membertype = "ST";
                    break;
                case "subcontractor":
                    membertype = "S";
                    break;
                case "subtechnician":
                    membertype = "ST";
                    break;
                case "lead technician":
                    membertype = "LT";
                    break;
            }
            return membertype;
        }
        public int getmemberMainidBymemberid(string memberid)
        {
            int m = 0;


            return m;
        }
        public DataTable GetcmemberForBulkInsert(DataTable gv, int createdby, int companyid, ref List<string> lst)
        {
            Crypto _cr = new Crypto();
            DataTable dataTable = new DataTable("DummyTable");
            DataRow row = null;
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("member_id", typeof(string));
            dataTable.Columns.Add("f_name", typeof(string));
            dataTable.Columns.Add("l_name", typeof(string));
            dataTable.Columns.Add("address", typeof(string));
            dataTable.Columns.Add("city_id", typeof(int));
            dataTable.Columns.Add("state_id", typeof(int));
            dataTable.Columns.Add("Zip_code", typeof(string));
            dataTable.Columns.Add("Phone", typeof(string));
            dataTable.Columns.Add("EMail", typeof(string));
            dataTable.Columns.Add("Alternative_phone", typeof(string));
            dataTable.Columns.Add("RateId", typeof(int));
            dataTable.Columns.Add("Background", typeof(string));
            dataTable.Columns.Add("drug_tested", typeof(string));
            dataTable.Columns.Add("dob", typeof(DateTime));
            dataTable.Columns.Add("Gender", typeof(string));
            dataTable.Columns.Add("RoleId", typeof(int));
            dataTable.Columns.Add("Department_Id", typeof(int));
            dataTable.Columns.Add("Designation_id", typeof(int));
            dataTable.Columns.Add("ManagerId", typeof(int));
            dataTable.Columns.Add("Username", typeof(string));
            dataTable.Columns.Add("Password", typeof(string));
            dataTable.Columns.Add("Tax", typeof(string));
            dataTable.Columns.Add("member_status_id", typeof(int));
            dataTable.Columns.Add("CreatedDate", typeof(DateTime));
            dataTable.Columns.Add("CreatedBy", typeof(int));
            dataTable.Columns.Add("Converted_lead", typeof(bool));
            dataTable.Columns.Add("issuper_Admin", typeof(bool));
            dataTable.Columns.Add("Imageurl", typeof(string));
            dataTable.Columns.Add("Skills", typeof(string));
            dataTable.Columns.Add("resetneeded", typeof(bool));
            dataTable.Columns.Add("Membertype", typeof(string));
            dataTable.Columns.Add("Traveldistance", typeof(string));
            dataTable.Columns.Add("profilecomplete", typeof(bool));
            dataTable.Columns.Add("profilestep", typeof(bool));
            dataTable.Columns.Add("citizencheck", typeof(bool));
            dataTable.Columns.Add("authrizework", typeof(bool));
            dataTable.Columns.Add("company", typeof(string));
            dataTable.Columns.Add("isprofilecompleted", typeof(bool));
            dataTable.Columns.Add("ismailcompleted", typeof(bool));
            dataTable.Columns.Add("citizen", typeof(bool));
            dataTable.Columns.Add("age", typeof(bool));
            dataTable.Columns.Add("auth", typeof(bool));
            dataTable.Columns.Add("fealony", typeof(bool));
            dataTable.Columns.Add("isrtwcomplete", typeof(bool));
            dataTable.Columns.Add("isw9complete", typeof(bool));
            dataTable.Columns.Add("isbankcomplete", typeof(bool));
            dataTable.Columns.Add("explainfelony", typeof(string));
            dataTable.Columns.Add("completeddate", typeof(DateTime));
            dataTable.Columns.Add("acceptterms", typeof(bool));
            dataTable.Columns.Add("accepttermsdate", typeof(DateTime));
            dataTable.Columns.Add("accepttermsrqud", typeof(bool));
            dataTable.Columns.Add("declineterms", typeof(bool));
            dataTable.Columns.Add("companyid", typeof(int));
            dataTable.Columns.Add("position", typeof(string));
            dataTable.Columns.Add("havecompany", typeof(bool));
            string memberid = "";
            string companyname = "";
            try
            {
                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {

                    memberid = new helper().get5RandomDidit();
                    lst.Add(memberid);
                    row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = memberid;
                    row[2] = gv.Rows[i][0].ToString().ToTitleCase();
                    row[3] = gv.Rows[i][1].ToString().ToTitleCase();
                    row[4] = gv.Rows[i][6].ToString();
                    row[5] = 0;
                    row[6] = 0;
                    row[7] = gv.Rows[i][7].ToString();
                    row[8] = gv.Rows[i][4].ToString().PhoneNumber();
                    row[9] = gv.Rows[i][3].ToString();
                    row[10] = new companyservices().getalterntivephone(companyid, ref companyname); //alternative
                    row[11] = 0;
                    row[12] = "";
                    row[13] = "";
                    row[14] = DateTime.Now;
                    row[15] = "";
                    row[16] = 1013;
                    row[17] = 0;
                    row[18] = 0;
                    row[19] = new memberservices().getmembermainid(gv.Rows[i][5].ToString());//5/manager
                    row[20] = gv.Rows[i][3].ToString();
                    row[21] = _cr.EncryptStringAES(gv.Rows[i][4].ToString());
                    row[22] = "";
                    row[23] = 1007;
                    row[24] = DateTime.Now;
                    row[25] = createdby;
                    row[26] = false;
                    row[27] = false;
                    row[28] = "default";
                    row[29] = "";//skills
                    row[30] = true;
                    row[31] = getmembertype(gv.Rows[i][8].ToString());//mmebertype
                    row[32] = "";
                    row[33] = false;
                    row[34] = false;
                    row[35] = false;
                    row[36] = false;
                    row[37] = companyname;
                    row[38] = false;
                    row[39] = false;
                    row[40] = false;
                    row[41] = false;
                    row[42] = false;
                    row[43] = false;
                    row[44] = false;
                    row[45] = false;
                    row[46] = false;
                    row[47] = "";
                    row[48] = DateTime.Now;
                    row[49] = false;
                    row[50] = DateTime.Now;
                    row[51] = true;
                    row[52] = false;
                    row[53] = companyid;
                    row[54] = gv.Rows[i][2].ToString();
                    row[55] = false;
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception exception)
            {
            }
            return dataTable;
        }


        [HttpPost]
        public ActionResult city(HttpPostedFileBase file, int stateid)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                string newfilename = "cl_" + DateTime.Now.Ticks.ToString() + file.FileName;
                string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                file.SaveAs(filepath);
                DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 1, false);
                dataTable = GetcityForBulkInsert(dataTable, stateid);
                var objBulk = new BulkUploadToSql()
                {
                    InternalStore_dt = dataTable,
                    TableName = "tbl_temp_city",
                    CommitBatchSize = 200,
                    ConnectionString = conctionstring
                };
                objBulk.Commit_datatable();
                TempData["error"] = dataTable.Rows.Count + " Clients Imported succesfully";
                return RedirectToAction("city", "test");
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                return RedirectToAction("city", "test");
            }
        }
        public DataTable GetcityForBulkInsert(DataTable gv, int stateid)
        {
            DataTable dataTable = new DataTable("DummyTable");
            DataRow row = null;
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("state_Id", typeof(int));
            dataTable.Columns.Add("name", typeof(string));
            dataTable.Columns.Add("Isactive", typeof(bool));
            try
            {
                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = stateid;
                    row[2] = gv.Rows[i][0].ToString();
                    row[3] = true;
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception exception)
            {
            }
            return dataTable;
        }

    }

}