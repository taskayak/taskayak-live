﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class jobsController : Controller
    {
        // GET: jobs
        public List<int> _permission;
        public int pageid = 4;
        public jobsController()
        {
            int roleid = System.Web.HttpContext.Current.Request.Cookies["_pid"] == null ? 0 : Convert.ToInt16(System.Web.HttpContext.Current.Request.Cookies["_pid"].Value.ToString());
            roleservices _ser = new roleservices();
            _permission = _ser.get_all_active_permissions_bytypeid(5, roleid);
        }
        public class Events
        {
            public bool allDay
            {
                get;
                set;
            }

            public string token
            {
                get;
                set;
            }

            public string backgroundColor
            {
                get;
                set;
            }

            public string className
            {
                get;
                set;
            }

            public string date
            {
                get;
                set;
            }

            public string end
            {
                get;
                set;
            }

            public string id
            {
                get;
                set;
            }

            public string start
            {
                get;
                set;
            }

            public string title
            {
                get;
                set;
            }

            public string url
            {
                get;
                set;
            }

        }
        public int[] StringToArray(string input, string separator)
        {
            string[] stringList = input.Split(separator.ToCharArray(),
                                              StringSplitOptions.RemoveEmptyEntries);
            int[] list = new int[stringList.Length];

            for (int i = 0; i < stringList.Length; i++)
            {
                list[i] = Convert.ToInt32(stringList[i]);
            }

            return list;
        }
        public ActionResult GetEvents(int id, string start = null, string end = null, string date = "", int type = 1, int state_filter_id = 0, int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, string city = "", int searchtype = 0)
        {
            int[] city_filter_id = null;
            if (!string.IsNullOrEmpty(city))
            {
                city_filter_id = StringToArray(city, ",");
            }
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new jobfilter();
            if (searchtype == 0)
            {
                _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid);
                city_filter_id = string.IsNullOrEmpty(_filtermodel.city) ? null : Array.ConvertAll(_filtermodel.city.Split(','), int.Parse);
                date = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
                state_filter_id = _filtermodel.state_filter_id ?? 0;
                cclient = _filtermodel.cclient ?? 0;
                cTechnician = _filtermodel.cTechnician ?? 0;
                cDispatcher = _filtermodel.cDispatcher ?? 0;
                Status = _filtermodel.Status ?? 0;

            }
            else if (searchtype == 1)
            {
                _filtermodel.city = city_filter_id == null ? null : city;
                _filtermodel.date = date;
                _filtermodel.state_filter_id = state_filter_id;
                _filtermodel.cclient = cclient;
                _filtermodel.cTechnician = cTechnician;
                _filtermodel.cDispatcher = cDispatcher;
                _filtermodel.Status = Status;
                new filterservices().save_filter<jobfilter>(_filtermodel, userid, pageid);
            }
            if (searchtype == 2)
            {
                city_filter_id = null;
                date = "";
                state_filter_id = 0;
                cclient = 0;
                cTechnician = 0;
                cDispatcher = 0;
                Status = 0;
                new filterservices().delete_filter(userid, pageid);
                //clearsearch
            }

            DateTime dateTime;
            jobservices _ser = new jobservices();
            DateTime dateTime1 = (!string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : DateTime.Now);
            List<Events> events = new List<Events>();
            if (string.IsNullOrEmpty(end))
            {
                DateTime currentTimeByTimeZone = DateTime.Now;
                dateTime = currentTimeByTimeZone.AddDays(30);
            }
            else
            {
                dateTime = Convert.ToDateTime(end);
            }
            var model = new jobmodal();
            if (_permission.Contains(51))
            {
                var membertype = new memberservices().getmembertype(userid);
                model = _ser.get_all_active_jobs_bymemberid(id, date, state_filter_id, cclient, cTechnician, cDispatcher, Status, city_filter_id, userid, membertype);
                //ViewBag.hiderate = true;
            }
            else
            {//
                ViewBag.hiderate = false;
                model = _ser.get_all_active_jobs(date, state_filter_id, cclient, cTechnician, cDispatcher, Status, city_filter_id);
            }
            var filterjobs = model._jobs.ToList();
            if (!string.IsNullOrEmpty(date))
            {
                filterjobs = filterjobs.Where(a => a._sdate >= dateTime1 || a._endate <= dateTime).ToList();
            }
            Crypto _crypt = new Crypto();
            foreach (var item in filterjobs)
            {
                events.Add(new Events()
                {
                    id = item.JobID.ToString(),
                    start = item._sdate.ToString("s"),
                    end = item._endate.ToString("s"),
                    title = item.Title,
                    allDay = false,
                    token = _crypt.EncryptStringAES(item.JobID.ToString())
                });
            }
            Events[] array = events.ToArray();
            return base.Json(array, 0);
        }

        [HttpPost]
        public string getunassignjobsforpopup(int stateid = 0, string city = "", string jobid = "", int clientid = 0)
        {
            string ddl = "<option  value='0'>Job ID</option>";
            var deptser = new defaultservices();
            var list = deptser.get_all_job_unassign(stateid, city, jobid, clientid);
            foreach (var item in list)
            {
                ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }


        public ActionResult Index(int? id, int type = 1, int srch = 1, string token = "")
        {
            if (!_permission.Contains(7))
            {
                return View("unauth");
            }
            if (type == 1)
            {
                if (!_permission.Contains(1062))
                {
                    return View("unauth");
                }
            }
            else
            {
                if (!_permission.Contains(1061))
                {
                    return View("unauth");
                }
            }
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var membertype = new memberservices().getmembertype(userid);
            ViewBag.issubcontractor = (membertype == "S" || membertype == "LT") ? true : false;
            var _filtermodel = new jobfilter();
            if (srch == 2)
            {
                new filterservices().delete_filter(userid, pageid);
                //clearsearch
            }
            _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid);
            ViewBag.date = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
            ViewBag.state_filter_id = _filtermodel.state_filter_id ?? 0;
            ViewBag.cclient = _filtermodel.cclient ?? 0;
            ViewBag.city = string.IsNullOrEmpty(_filtermodel.city) ? "" : _filtermodel.city;
            ViewBag.cTechnician = _filtermodel.cTechnician ?? 0;
            ViewBag.cDispatcher = _filtermodel.cDispatcher ?? 0;
            ViewBag.Status = _filtermodel.Status ?? 0;
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            ViewBag.permissions = _permission;
            jobservices _ser = new jobservices();
            job _model = new job();
            string message = "";
            if (!string.IsNullOrEmpty(token))
            {
                Crypto _crypt = new Crypto();
                int jid = Convert.ToInt32(_crypt.DecryptStringAES(token));
                message = _ser.delete_job(jid);
            }
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            _model = new helper().GenerateError<job>(_model, message);
            string view = "admin";
            if (_permission.Contains(51))
            {
                // _model._jobs = _ser.get_all_active_jobs_bymemberid(userid, "")._jobs;
                ViewBag.hiderate = true;
                view = "member";
            }
            else
            {
                ViewBag.hiderate = (membertype == "S" || membertype == "LT") ? true : false;
                // _model._jobs = _ser.get_all_active_jobs("")._jobs;
            }

            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            if (type == 1)
            {
                return View(view, _model);

            }
            else
            {
                ViewBag.userid = userid;
                return View("calender", _model);
            }

        }
        public void update_status(int id, int statusId)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            new jobservices().update_status(statusId, id, userid);
        }
        public JsonResult Fetchadminjobs(int start = 0, int length = 25, int draw = 1, string date = "", int type = 1, int state_filter_id = 0, int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int searchtype = 0)
        {
            string search = Request.Params["search[value]"] == null ? "" : Request.Params["search[value]"];
            string _city_id = Request.Params["city_filter_id"] == null ? "" : Request.Params["city_filter_id"];
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new jobfilter();
            if (searchtype == 0)
            {
                _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid);
                _city_id = string.IsNullOrEmpty(_filtermodel.city) ? "0" : _filtermodel.city;
                date = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
                state_filter_id = _filtermodel.state_filter_id ?? 0;
                cclient = _filtermodel.cclient ?? 0;
                cTechnician = _filtermodel.cTechnician ?? 0;
                cDispatcher = _filtermodel.cDispatcher ?? 0;
                Status = _filtermodel.Status ?? 0;

            }
            else if (searchtype == 1)
            {
                _filtermodel.city = _city_id;
                _filtermodel.date = date;
                _filtermodel.state_filter_id = state_filter_id;
                _filtermodel.cclient = cclient;
                _filtermodel.cTechnician = cTechnician;
                _filtermodel.cDispatcher = cDispatcher;
                _filtermodel.Status = Status;
                new filterservices().save_filter<jobfilter>(_filtermodel, userid, pageid);
            }
            if (searchtype == 2)
            {
                _city_id = "0";
                date = "";
                state_filter_id = 0;
                cclient = 0;
                cTechnician = 0;
                cDispatcher = 0;
                Status = 0;
                new filterservices().delete_filter(userid, pageid);
                //clearsearch
            }

            int[] city_filter_id = Array.ConvertAll(_city_id.Split(','), int.Parse);
            start = (start == 0 ? 1 : start + 1);
            length = (start == 1 ? length : (start - 1) + length);
            jobservices _ser = new jobservices();
            datatableAjax<job_items> tableDate = new datatableAjax<job_items>();
            int total = 0;
            var _model = _ser.get_all_active_jobs_by_index(ref total, start, length, search, date, state_filter_id, cclient, cTechnician, cDispatcher, Status, city_filter_id, notificationUSER: userid);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = (_model.Count() > 0 ? total : 0);
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Fetchmemberjobs(int start = 0, int length = 25, int draw = 1, string date = "", int type = 1, int state_filter_id = 0, int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            string search = Request.Params["search[value]"] == null ? "" : Request.Params["search[value]"];
            string _city_id = Request.Params["city_filter_id"] == null ? "" : Request.Params["city_filter_id"];
            int[] city_filter_id = Array.ConvertAll(_city_id.Split(','), int.Parse);
            start = (start == 0 ? 1 : start + 1);
            length = (start == 1 ? length : (start - 1) + length);
            jobservices _ser = new jobservices();
            var membertype = new memberservices().getmembertype(userid);
            datatableAjax<job_items> tableDate = new datatableAjax<job_items>();
            int total = 0;

            var _model = _ser.get_all_active_jobs_by_index(ref total, start, length, search, date, state_filter_id, cclient, cTechnician, cDispatcher, Status, city_filter_id, userid, notificationUSER: userid, membertype);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = (_model.Count() > 0 ? total : 0);
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Index(string date = "", int type = 1, int state_filter_id = 0, int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int[] city_filter_id = null)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new jobfilter();
            _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid);
            ViewBag.date = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
            ViewBag.state_filter_id = _filtermodel.state_filter_id ?? 0;
            ViewBag.cclient = _filtermodel.cclient ?? 0;
            ViewBag.city = string.IsNullOrEmpty(_filtermodel.city) ? "" : _filtermodel.city;
            ViewBag.cTechnician = _filtermodel.cTechnician ?? 0;
            ViewBag.cDispatcher = _filtermodel.cDispatcher ?? 0;
            ViewBag.Status = _filtermodel.Status ?? 0;
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            ViewBag.permissions = _permission;
            jobservices _ser = new jobservices();
            job _model = new job();
            string message = "";

            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            _model = new helper().GenerateError<job>(_model, message);
            // int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            string view = "admin";
            if (_permission.Contains(51))
            {
                //_model._jobs = _ser.get_all_active_jobs_bymemberid(userid, date, state_filter_id, cclient, cTechnician, cDispatcher, Status, city_filter_id)._jobs;
                ViewBag.hiderate = true;
                view = "member";
            }
            else
            {
                ViewBag.hiderate = false;
                // _model._jobs = _ser.get_all_active_jobs(date, state_filter_id, cclient, cTechnician, cDispatcher, Status, city_filter_id)._jobs;
            }
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            if (type == 1)
            {
                return View(view, _model);

            }
            else
            {
                ViewBag.userid = userid;
                return View("calender", _model);
            }

        }

        public ActionResult view(string token, int type = 1, int viewtype = 1)
        {
            ViewBag.type = type;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            if (!_permission.Contains(56))
            {
                return View("unauth");
            }
            Crypto _crypt = new Crypto();
            int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
            ViewBag.permissions = _permission;
            jobservices _ser = new jobservices();
            job_items1 _model = new job_items1();
            ViewBag.viewtype = viewtype;
            Session["type"] = viewtype;
            _model = _ser.get_all_active_jobs_byid(id, userid);
            if (_permission.Contains(2062))
            {
                ViewBag.stype = new memberservices().getmembertype(userid);
                return View("Tview", _model);
            }
            return View(_model);
        }
        public JsonResult getjob(int id)
        {
            jobservices _ser = new jobservices();
            var _model = _ser.get_all_active_job_details_byid(id);
            return Json(_model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult delete_file(string token, string jtoken)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            Crypto _crypt = new Crypto();
            int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
            int jobid = Convert.ToInt32(_crypt.DecryptStringAES(jtoken));
            ViewBag.permissions = _permission;
            jobservices _ser = new jobservices();
            job_items1 _model = new job_items1();
            var message = _ser.delete_doc(id);
            ViewBag.viewtype = Session["type"] == null ? 1 : Convert.ToInt16(Session["type"]);
            _model = _ser.get_all_active_jobs_byid(jobid, userid);
            _model = new helper().GenerateError<job_items1>(_model, message);
            ViewBag.type = 3;
            if (_permission.Contains(2062))
            {
                return View("Tview", _model);
            }
            return View("view", _model);
        }

        public ActionResult edit(string token, int type = 1)
        {
            if (!_permission.Contains(39))
            {
                return View("unauth");
            }
            Crypto _crypt = new Crypto();
            int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
            ViewBag.permissions = _permission;
            jobservices _ser = new jobservices();
            job _model = new job();
            ViewBag.type = type;
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            _model = _ser.get_all_active_job_details_byid(id);
            return View(_model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult update(int JobID, int type = 1, HttpPostedFileBase file = null, string Comment = "", string file_title = "")
        {
            ViewBag.permissions = _permission;
            ViewBag.type = type;
            jobservices _ser = new jobservices();
            job_items1 _model = new job_items1();
            var mes = "";
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            if (type == 2)
            {
                mes = _ser.add_new_comment(Comment, userid, JobID);
            }
            else if (type == 3)
            {
                string path = Server.MapPath("~/image/jobpicture-" + JobID);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string filenames = new helper().GetRandomalphanumeric(4);
                var extension = Path.GetExtension(file.FileName);
                var imagepath = path + "/" + filenames + extension;
                string tempfilename = "/image/jobpicture-" + JobID + "/" + filenames + extension;
                if (System.IO.File.Exists(imagepath))
                {
                    System.IO.File.Delete(imagepath);
                }
                file.SaveAs(imagepath);
                long b = file.ContentLength;
                long kb = b / 1024;
                mes = _ser.add_new_doc(file_title, userid, JobID, kb.ToString() + " kb", tempfilename);
            }
            ModelState.Clear();
            _model = _ser.get_all_active_jobs_byid(JobID, userid);
            _model = new helper().GenerateError<job_items1>(_model, mes);
            ViewBag.viewtype = Session["type"] == null ? 1 : Convert.ToInt16(Session["type"]);
            if (_permission.Contains(2062))
            {
                return View("Tview", _model);
            }
            return View("view", _model);
        }

        public bool validatejobId(string id)
        {
            return new jobservices().validateJobId(id);
        }

        public bool validatejobIdEdit(int id, string jobid)
        {
            return new jobservices().validateJobId(id, jobid);
        }
        public int getcityidbystate(int stateid, string city)
        {
            if (string.IsNullOrEmpty(city))
            {
                return 0;
            }
            else
            {
                return new defaultservices().savegetCityId(stateid, city);
            }

        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult add(job _model, int type = 1, string techrate = "", string clientrate = "", string add = "", string zip = "")
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new jobfilter();
            _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid);
            ViewBag.date = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
            ViewBag.state_filter_id = _filtermodel.state_filter_id ?? 0;
            ViewBag.cclient = _filtermodel.cclient ?? 0;
            ViewBag.city = string.IsNullOrEmpty(_filtermodel.city) ? "" : _filtermodel.city;
            ViewBag.cTechnician = _filtermodel.cTechnician ?? 0;
            ViewBag.cDispatcher = _filtermodel.cDispatcher ?? 0;
            ViewBag.Status = _filtermodel.Status ?? 0;
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            ViewBag.permissions = _permission;
            jobservices _ser = new jobservices();
            _model.city_id = getcityidbystate(_model.state_id, _model.city);
            var mes = _ser.add_new_job(_model, userid, techrate, clientrate, add, zip);
            _model = new helper().GenerateError<job>(_model, mes);
            string view = "admin";
            if (_permission.Contains(51))
            {
                // _model._jobs = _ser.get_all_active_jobs_bymemberid(userid, "")._jobs;
                ViewBag.hiderate = true;
                view = "member";
            }
            else
            {
                ViewBag.hiderate = false;
                // _model._jobs = _ser.get_all_active_jobs("")._jobs;
            }
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            if (mes.Contains("Error"))
            {
                ViewBag.type = type;
                return View("edit", _model);
            }
            if (type == 1)
            {
                return View(view, _model);

            }
            else
            {
                ViewBag.userid = userid;
                return View("calender", _model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult update_job(job _model, int type = 1)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new jobfilter();
            _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid);
            ViewBag.date = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
            ViewBag.state_filter_id = _filtermodel.state_filter_id ?? 0;
            ViewBag.cclient = _filtermodel.cclient ?? 0;
            ViewBag.city = string.IsNullOrEmpty(_filtermodel.city) ? "" : _filtermodel.city;
            ViewBag.cTechnician = _filtermodel.cTechnician ?? 0;
            ViewBag.cDispatcher = _filtermodel.cDispatcher ?? 0;
            ViewBag.Status = _filtermodel.Status ?? 0;
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            ViewBag.type = type;
            ViewBag.permissions = _permission;
            jobservices _ser = new jobservices();
            _model.city_id = getcityidbystate(_model.state_id, _model.city);
            var mes = _ser.update_job(_model, userid);
            _model = new helper().GenerateError<job>(_model, mes);
            string view = "admin";
            if (_permission.Contains(51))
            {
                ViewBag.hiderate = true;
                view = "member";
            }
            else
            {
                ViewBag.hiderate = false;
            }
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            if (mes.Contains("Error"))
            {
                ViewBag.type = type;
                return View("edit", _model);
            }
            if (type == 1)
            {
                return View(view, _model);
            }
            else
            {
                ViewBag.userid = userid;
                return View("calender", _model);
            }
        }

        [HttpPost]
        public string updateassign(int memberid, int jobid, string rate)
        {
            var name = "";
            jobservices _ser = new jobservices();
            name = _ser.assignjob(jobid, memberid, rate);
            return name;
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult edit(job_items1 _model, int type = 1)
        {
            ViewBag.permissions = _permission;
            jobservices _ser = new jobservices();
            ViewBag.type = type;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());

            var mes = _ser.update_job_byview(_model, userid);
            _model = _ser.get_all_active_jobs_byid(_model.JobID, userid);
            _model = new helper().GenerateError<job_items1>(_model, mes);
            ViewBag.type = Session["type"] == null ? 1 : Convert.ToInt16(Session["type"]);
            if (_permission.Contains(2062))
            {
                return View("Tview", _model);
            }
            return View("view", _model);
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Tedit(job_items1 _model, int type = 1)
        {
            ViewBag.permissions = _permission;
            jobservices _ser = new jobservices();
            ViewBag.type = type;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var mes = _ser.update_job_byviewT(_model, userid);
            _model = _ser.get_all_active_jobs_byid(_model.JobID, userid);
            _model = new helper().GenerateError<job_items1>(_model, mes);
            ViewBag.type = Session["type"] == null ? 1 : Convert.ToInt16(Session["type"]);
            if (_permission.Contains(2062))
            {
                return View("Tview", _model);
            }
            return View("view", _model);
        }
        public PartialViewResult getnotifications(int jobid)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            notificationModal _model = new notificationModal();
            _model.message = new notificationServices().get_all_active_notificationsbyjovbid(jobid, userid);
            _model.mainind = jobid;
            return PartialView("Reply", _model);
        }
        public void update_readstatus(int jobid)
        {
            new notificationServices().update_jobreadstatus(jobid);
        }

        public void update_readstatus2(int jobid)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            new notificationServices().update_jobreadstatus2(jobid, userid);
        }

        [ValidateInput(false)]
        public void Sendmessage(int JobID, string Comment)
        {
            jobservices _ser = new jobservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            _ser.add_new_comment(Comment, userid, JobID);
        }

    }
}