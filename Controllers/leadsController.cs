﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class leadsController : Controller
    {
        public int pageid = 2;
        public List<int> _permission;
        public leadsController()
        {
            int roleid = System.Web.HttpContext.Current.Request.Cookies["_pid"] == null ? 0 : Convert.ToInt16(System.Web.HttpContext.Current.Request.Cookies["_pid"].Value.ToString());
            roleservices _ser = new roleservices();
            _permission = _ser.get_all_active_permissions_bytypeid(12, roleid);
        }
        public ActionResult Index(int? statusId, int? stateid, int[] cityId = null, int[] skills = null, int[] sourceid = null, int post = 1)
        {
            if (!_permission.Contains(8))
            {
                return View("unauth");
            }
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            leadmodal _model = new leadmodal();
            leadservices _services = new leadservices();
            List<int> _ctyiid = new List<int>();
            var _filtermodel = new leadfilter();
            _filtermodel = new filterservices().getfilter<leadfilter>(_filtermodel, userid, pageid);
            ViewBag.statusId = _filtermodel.statusId ?? 0;
            ViewBag.stateid = _filtermodel.stateid ?? 0;
            ViewBag.paymentstatus = string.IsNullOrEmpty(_filtermodel.jobstatus) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.jobstatus.Split(','), int.Parse);
            ViewBag.city = string.IsNullOrEmpty(_filtermodel.city) ? "" : _filtermodel.city;
            ViewBag.jobstatus = string.IsNullOrEmpty(_filtermodel.skiils) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.skiils.Split(','), int.Parse);
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            ViewBag.permissions = _permission;
            // _model = _services.get_all_active_leads();
            string message = "";
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            _model = new helper().GenerateError<leadmodal>(_model, message);
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            _model._skills = get_all_skills();
            return View(_model);
        }


        public void update_status(int leadid, int statusid)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            string memberid = "";
            if (statusid == 4)
            {
                memberid = new memberservices().getLatestMemberId();
            }
            new leadservices().update_lead_status(statusid, leadid, memberid, userid);
        }

        public JsonResult Fetchleads(int? statusId, int? stateid, int start = 0, int length = 25, int draw = 1, int searchtype = 0)
        {
            string search = Request.Params["search[value]"] == null ? "" : Request.Params["search[value]"];
            string _sourceid = Request.Params["sourceid"] == null ? "" : Request.Params["sourceid"];
            string _skills = Request.Params["skills"] == null ? "" : Request.Params["skills"];
            string _city_id = Request.Params["cityId"] == null ? "" : Request.Params["cityId"];
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new leadfilter();
            if (searchtype == 0)
            {
                _filtermodel = new filterservices().getfilter<leadfilter>(_filtermodel, userid, pageid);
                statusId = _filtermodel.statusId;
                stateid = _filtermodel.stateid;
                _sourceid = string.IsNullOrEmpty(_filtermodel.jobstatus) ? "0" : _filtermodel.jobstatus;
                _city_id = string.IsNullOrEmpty(_filtermodel.city) ? "0" : _filtermodel.city;
                _skills = string.IsNullOrEmpty(_filtermodel.skiils) ? "0" : _filtermodel.skiils;
            }
            else if (searchtype == 1)
            {
                _filtermodel.statusId = statusId;
                _filtermodel.stateid = stateid;
                _filtermodel.jobstatus = _sourceid;
                _filtermodel.city = _city_id;
                _filtermodel.skiils = _skills;
                new filterservices().save_filter<leadfilter>(_filtermodel, userid, pageid);
            }
            if (searchtype == 2)
            {
                statusId = null;
                stateid = null;
                _sourceid = "0";
                _city_id = "0";
                _skills = "0";
                new filterservices().delete_filter(userid, pageid);
                //clearsearch
            }
            int[] cityId = Array.ConvertAll(_city_id.Split(','), int.Parse);
            int[] skills = Array.ConvertAll(_skills.Split(','), int.Parse);
            int[] sourceid = Array.ConvertAll(_sourceid.Split(','), int.Parse);
            start = (start == 0 ? 1 : start + 1);
            length = (start == 1 ? length : (start - 1) + length);
            leadservices _services = new leadservices();
            datatableAjax<leadmodals> tableDate = new datatableAjax<leadmodals>();
            int total = 0;
            var _model = _services.get_all_active_leads_by_index(ref total, start, length, search, statusId, stateid, cityId, skills, sourceid);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = (_model.Count() > 0 ? total : 0);
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Index(int? statusId, int? stateid, int[] cityId = null, int[] skills = null, int[] sourceid = null)
        {
            leadmodal _model = new leadmodal();
            leadservices _services = new leadservices();
            List<int> _ctyiid = new List<int>();
            ViewBag.statusId = statusId ?? 0;
            ViewBag.stateid = stateid ?? 0;
            ViewBag.jobstatus = skills == null ? _ctyiid.ToArray() : skills;
            ViewBag.city = cityId == null ? "" : string.Join<int>(",", cityId);
            ViewBag.paymentstatus = sourceid == null ? _ctyiid.ToArray() : sourceid;
            ViewBag.permissions = _permission;
            _model = _services.get_all_active_leads(",", statusId, stateid, cityId, skills, sourceid);
            string message = "";
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            _model = new helper().GenerateError<leadmodal>(_model, message);
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            _model._skills = get_all_skills();
            return View(_model);
        }


        public ActionResult add()
        {
            if (!_permission.Contains(42))
            {
                return View("unauth");
            }
            ViewBag.permissions = _permission;
            leadmodal_item _model = new leadmodal_item();
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            _model._skills = get_all_skills();
            return View(_model);
        }

        public ActionResult edit(string token)
        {
            if (!_permission.Contains(43))
            {
                return View("unauth");
            }
            Crypto _crypt = new Crypto();
            int id =Convert.ToInt32( _crypt.DecryptStringAES(token));
            ViewBag.permissions = _permission;
            leadmodal_item _model = new leadmodal_item();
            leadservices _services = new leadservices();
            _model = _services.get_all_active_leads_byid(id);
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            var message = "";
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            _model = new helper().GenerateError<leadmodal_item>(_model, message);
            _model._skills = get_all_skills();
            return View(_model);
        }

        [HttpPost]
        public ActionResult add(leadmodal_item _item)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            List<int> _ctyiid = new List<int>();
            var _filtermodel = new leadfilter();
            _filtermodel = new filterservices().getfilter<leadfilter>(_filtermodel, userid, pageid);
            ViewBag.statusId = _filtermodel.statusId ?? 0;
            ViewBag.stateid = _filtermodel.stateid ?? 0;
            ViewBag.paymentstatus = string.IsNullOrEmpty(_filtermodel.jobstatus) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.jobstatus.Split(','), int.Parse);
            ViewBag.city = string.IsNullOrEmpty(_filtermodel.city) ? "" : _filtermodel.city;
            ViewBag.jobstatus = string.IsNullOrEmpty(_filtermodel.skiils) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.skiils.Split(','), int.Parse);
            ViewBag.isfilter = _filtermodel.isActiveFilter;

            ViewBag.permissions = _permission;
            leadmodal _model = new leadmodal();
            leadservices _services = new leadservices();
            //clientmodal _model = new clientmodal();
            _item.city_id = getcityidbystate(_item.state_id,_item.city);
            var message = _services.add_new_lead(_item, userid);
            //  _model = _services.get_all_active_leads();
            _model = new helper().GenerateError<leadmodal>(_model, message);
            _model._skills = get_all_skills();
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            return View("Index", _model);
        }
        public int getcityidbystate(int stateid, string city)
        {
            if (string.IsNullOrEmpty(city))
            {
                return 0;
            }
            else
            {
                return new defaultservices().savegetCityId(stateid, city);
            }

        }
        public List<skill_items> get_all_skills()
        {
            var dept = new skillservices();
            return dept.get_all_active_skills()._skill;
        }
        public ActionResult delete(string token)
        {
            if (!_permission.Contains(44))
            {
                return View("unauth");
            }
            Crypto _crypt = new Crypto();
            int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            List<int> _ctyiid = new List<int>();
            var _filtermodel = new leadfilter();
            _filtermodel = new filterservices().getfilter<leadfilter>(_filtermodel, userid, pageid);
            ViewBag.statusId = _filtermodel.statusId ?? 0;
            ViewBag.stateid = _filtermodel.stateid ?? 0;
            ViewBag.paymentstatus = string.IsNullOrEmpty(_filtermodel.jobstatus) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.jobstatus.Split(','), int.Parse);
            ViewBag.city = string.IsNullOrEmpty(_filtermodel.city) ? "" : _filtermodel.city;
            ViewBag.jobstatus = string.IsNullOrEmpty(_filtermodel.skiils) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.skiils.Split(','), int.Parse);
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            leadmodal _model = new leadmodal();
            leadservices _services = new leadservices();
            //clientmodal _model = new clientmodal();
            var message = _services.delete_lead(id);
            // _model = _services.get_all_active_leads();
            _model = new helper().GenerateError<leadmodal>(_model, message);
            _model._skills = get_all_skills();
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            return View("Index", _model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult update(leadmodal_item _item, int type = 1)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            List<int> _ctyiid = new List<int>();
            var _filtermodel = new leadfilter();
            _filtermodel = new filterservices().getfilter<leadfilter>(_filtermodel, userid, pageid);
            ViewBag.statusId = _filtermodel.statusId ?? 0;
            ViewBag.stateid = _filtermodel.stateid ?? 0;
            ViewBag.paymentstatus = string.IsNullOrEmpty(_filtermodel.jobstatus) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.jobstatus.Split(','), int.Parse);
            ViewBag.city = string.IsNullOrEmpty(_filtermodel.city) ? "" : _filtermodel.city;
            ViewBag.jobstatus = string.IsNullOrEmpty(_filtermodel.skiils) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.skiils.Split(','), int.Parse);
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            leadmodal _model = new leadmodal();
            leadservices _services = new leadservices();
            int leadid = _item.leadId;
            var message = "";
            string memberid = "";
            if (_item.statusid == 4)
            {
                memberid = new memberservices().getLatestMemberId();
            }// 
            if (type == 1)
            {
                _item.city_id = getcityidbystate(_item.state_id, _item.city);
                message = _services.update_lead(_item, userid, memberid);
            }
            else
            {
                message = _services.add_new_lead_comment(userid, _item.leadId, _item.comment);
                
            }
            ModelState.Clear();
            TempData["error"] = message;
            if (_item.statusid == 4)
            {
                _model = new helper().GenerateError<leadmodal>(_model, message);
                _model._skills = get_all_skills();
                defaultservices _default = new defaultservices();
                ViewBag.select = _default.get_all_satets();
                return View("Index", _model);
            }
            else
            {
                Crypto _crypt = new Crypto();
                string token = _crypt.EncryptStringAES(leadid.ToString());
                return RedirectToAction("edit", "leads", new { token = token });
            }
        }


        public ActionResult update()
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            List<int> _ctyiid = new List<int>();
            var _filtermodel = new leadfilter();
            _filtermodel = new filterservices().getfilter<leadfilter>(_filtermodel, userid, pageid);
            ViewBag.statusId = _filtermodel.statusId ?? 0;
            ViewBag.stateid = _filtermodel.stateid ?? 0;
            ViewBag.paymentstatus = string.IsNullOrEmpty(_filtermodel.jobstatus) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.jobstatus.Split(','), int.Parse);
            ViewBag.city = string.IsNullOrEmpty(_filtermodel.city) ? "" : _filtermodel.city;
            ViewBag.jobstatus = string.IsNullOrEmpty(_filtermodel.skiils) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.skiils.Split(','), int.Parse);
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            leadmodal _model = new leadmodal();
            leadservices _services = new leadservices();
            //clientmodal _model = new clientmodal();
            var message = "";//= _services.update_lead(_item, userid);
             // _model = _services.get_all_active_leads();
            _model = new helper().GenerateError<leadmodal>(_model, message);
            _model._skills = get_all_skills();
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            return View("Index", _model);
        }
    }
}