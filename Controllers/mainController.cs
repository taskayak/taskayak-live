﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    public class mainController : Controller
    {
        // GET: main
        public ActionResult Index()
        {
            bool redirect = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["isredirect"]);
           if(redirect)
            {
                RedirectToAction("Maintenance");
            }
            return View();
        }
        public ActionResult Maintenance(string ReturnUrl = "")
        {
            return View();
        }
    }
}