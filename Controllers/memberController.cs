﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class memberController : Controller
    {
        public List<int> _permission;
        public int pageid = 3;
        public memberController()
        {
            int roleid = System.Web.HttpContext.Current.Request.Cookies["_pid"] == null ? 0 : Convert.ToInt16(System.Web.HttpContext.Current.Request.Cookies["_pid"].Value.ToString());
            roleservices _ser = new roleservices();
            _permission = _ser.get_all_active_permissions_bytypeid(4, roleid);
        }
        public ActionResult add()
        {
            if (!_permission.Contains(34))
            {
                return View("unauth");
            }
            ViewBag.permissions = _permission;
            membermodal _mdl = new membermodal();
            _mdl._dept = get_all_dept();
            _mdl._roles = get_all_roles();
            _mdl._skills = get_all_skills();
            string memberid = new memberservices().getLatestMemberId();
            int i = 0;
            if (!Int32.TryParse(memberid, out i))
            {
                _mdl.member_id = new helper().get5RandomDidit();
            }
            else
            {
                _mdl.member_id = (i + 1).ToString();
            }
            return View(_mdl);
        }

        public ActionResult index(int? id, int? print, int state_id = 0, int[] city_id = null, int[] amount = null, string background = "", string drug = "", int status = 0, int[] skills = null, string token = "", int d = 0)
        {
            if (!_permission.Contains(6))
            {
                return View("unauth");
            }
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var type = new memberservices().getmembertype(userid);
            ViewBag.issubcontractor = (type == "S" || type == "LT") ? true : false;
            List<int> _ctyiid = new List<int>();
            var _filtermodel = new memberfilter();
            _filtermodel = new filterservices().getfilter<memberfilter>(_filtermodel, userid, pageid);
            ViewBag.state_id = _filtermodel.state_id ?? 0;
            ViewBag.skills = string.IsNullOrEmpty(_filtermodel.skills) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.skills.Split(','), int.Parse);
            ViewBag.amount = string.IsNullOrEmpty(_filtermodel.amount) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.amount.Split(','), int.Parse); ;
            ViewBag.city = string.IsNullOrEmpty(_filtermodel.city_id) ? "" : _filtermodel.city_id;
            ViewBag.drug = _filtermodel.drug;
            ViewBag.membertype = string.IsNullOrEmpty(_filtermodel.membertype) ? "" : _filtermodel.membertype;
            ViewBag.background = _filtermodel.background;
            ViewBag.status = _filtermodel.status ?? 0;
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            ViewBag.permissions = _permission;
            members _mdl = new members();
            helper _helper = new helper();
            memberservices _m = new memberservices();
            string message = "";
            if (!string.IsNullOrEmpty(token))
            {
                Crypto _crypt = new Crypto();
                int _id = Convert.ToInt32(_crypt.DecryptStringAES(token));
                if (_id == userid)
                {
                    message = "Error : You can't delete your account";
                }
                else
                {
                    if (!_permission.Contains(32))
                    {
                        return View("unauth");
                    }
                    else
                    {
                        if (d == 1)
                        {
                            message = _m.remove_memberstatus(_id);
                        }
                        else
                        {
                            message = _m.remove_member(_id);
                        }
                    }
                }
            }
            // _mdl = _m.get_all_active_members();
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            _mdl = new helper().GenerateError<members>(_mdl, message);
            _mdl._skills = get_all_skills();
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            string name = DateTime.Now.ToString("dd_MM_yyyy");
            if (print.HasValue)
            {
                //   var mdl = _m.get_all_active_members(";");
                //DataTable datatable = _helper.ConvertToDatatable<member_item>(mdl._members);
                //this.Getcsv(datatable, "Member_Report_" + name);
            }
            return View(_mdl);
        }

     


        public void update_status(int id, int statusid)
        {
            new memberservices().update_status(id, statusid);
        }


        public JsonResult rolesbycount()
        {
            List<rolecountmodal> _mdl = new List<rolecountmodal>();
            roleservices _ser = new roleservices();
            _mdl = _ser.get_role_bycount();
            return Json(_mdl, JsonRequestBehavior.AllowGet);
        }
        public int getcityidbystate(int stateid, string city)
        {
            if (string.IsNullOrEmpty(city))
            {
                return 0;
            }
            else
            {
                return new defaultservices().savegetCityId(stateid, city);
            }

        }
        public JsonResult Fetchmembers(int start = 0, int length = 25, int draw = 1, int state_id = 0, string background = "", string drug = "", int status = 0, int searchtype = 0, string membertype = "M")
        {
            string search = Request.Params["search[value]"] == null ? "" : Request.Params["search[value]"];
            string _city_id = Request.Params["city_id"] == null ? "" : Request.Params["city_id"];
            string _amount = Request.Params["amount"] == null ? "" : Request.Params["amount"];
            string _skills = Request.Params["skills"] == null ? "" : Request.Params["skills"];
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            bool phoneView = _permission.Contains(5064);
            bool EmailView = _permission.Contains(5065);
            bool rateView = _permission.Contains(5066);
            var _filtermodel = new memberfilter();
            if (searchtype == 0)
            {
                _filtermodel = new filterservices().getfilter<memberfilter>(_filtermodel, userid, pageid);
                status = _filtermodel.status ?? 0;
                state_id = _filtermodel.state_id ?? 0;
                //string  = "", string  = "", int status = 0,
                background = string.IsNullOrEmpty(_filtermodel.background) ? "all" : _filtermodel.background;
                drug = string.IsNullOrEmpty(_filtermodel.drug) ? "all" : _filtermodel.drug;
                _city_id = string.IsNullOrEmpty(_filtermodel.city_id) ? "0" : _filtermodel.city_id;
                _amount = string.IsNullOrEmpty(_filtermodel.amount) ? "0" : _filtermodel.amount;
                _skills = string.IsNullOrEmpty(_filtermodel.skills) ? "0" : _filtermodel.skills;
                membertype = string.IsNullOrEmpty(_filtermodel.membertype) ? "M" : _filtermodel.membertype;
            }
            else if (searchtype == 1)
            {
                _filtermodel.status = status;
                _filtermodel.state_id = state_id;
                _filtermodel.background = background;
                _filtermodel.drug = drug;
                _filtermodel.city_id = _city_id;
                _filtermodel.amount = _amount;
                _filtermodel.skills = _skills;
                _filtermodel.membertype = membertype;
                new filterservices().save_filter<memberfilter>(_filtermodel, userid, pageid);
            }
            if (searchtype == 2)
            {
                status = 0;
                state_id = 0;
                //string  = "", string  = "", int status = 0,
                background = "all";
                drug = "all";
                _city_id = "0";
                _amount = "0";
                _skills = "0";
                membertype = "M";
                new filterservices().delete_filter(userid, pageid);
                //clearsearch
            }
            int[] city_id = Array.ConvertAll(_city_id.Split(','), int.Parse);
            int[] amount = Array.ConvertAll(_amount.Split(','), int.Parse);
            int[] skills = Array.ConvertAll(_skills.Split(','), int.Parse);
            start = (start == 0 ? 1 : start + 1);
            length = (start == 1 ? length : (start - 1) + length);
            memberservices _services = new memberservices();
            datatableAjax<member_item> tableDate = new datatableAjax<member_item>();
            int total = 0;
            var type = new memberservices().getmembertype(userid);
            var _model = _services.get_all_active_members_byIndex(ref total, start, length, search, ",", state_id, city_id, amount, background, drug, status, skills, phoneView, EmailView, rateView, type, userid, membertype);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = (_model.Count() > 0 ? total : 0);
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult index(int? print, int state_id = 0, int[] city_id = null, int[] amount = null, string background = "", string drug = "", int status = 0, int[] skills = null)
        {
            List<int> _ctyiid = new List<int>();
            ViewBag.amount = amount;
            ViewBag.state_id = state_id;
            ViewBag.skills = skills == null ? _ctyiid.ToArray() : skills;
            ViewBag.city = city_id == null ? "" : string.Join<int>(",", city_id);
            ViewBag.drug = drug;
            ViewBag.background = background;
            ViewBag.status = status;
            ViewBag.permissions = _permission;
            members _mdl = new members();
            helper _helper = new helper();
            memberservices _m = new memberservices();
            string message = "";
            //_mdl = _m.get_all_active_members(",", state_id, city_id, amount, background, drug, status, skills);
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            _mdl = new helper().GenerateError<members>(_mdl, message);
            _mdl._skills = get_all_skills();
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            string name = DateTime.Now.ToString("dd_MM_yyyy");
            if (print.HasValue)
            {
                var mdl = _m.get_all_active_members(";");
                DataTable datatable = _helper.ConvertToDatatable<member_item>(mdl._members);
                this.Getcsv(datatable, "Member_Report_" + name);
            }
            return View(_mdl);
        }

        public ActionResult edit(string token, int tab = 1)
        {
            if (!_permission.Contains(31))
            {
                return View("unauth");
            }
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            Crypto _crypt = new Crypto();
            int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
            ViewBag.permissions = _permission;
            ViewBag.tab = tab;
            membermodal _mdl = new membermodal();
            memberservices _m = new memberservices();
            var deptser = new designationservices();
            _mdl = _m.get_all_active_members_byid(id);
            string mes = "";
            if (TempData["error"] != null)
            {
                mes = TempData["error"].ToString();
                _mdl = new helper().GenerateError<membermodal>(_mdl, mes);
            }
            var type = new memberservices().getmembertype(userid);
            ViewBag.issubcontractor = (type == "S" || type == "LT") ? true : false;
            _mdl._roles = get_all_roles();
            _mdl._docs = _m.get_all_doc_bymemberid(id);
            _mdl._skills = get_all_skills();
            _mdl._tools = get_all_tools();
            _mdl._dctype = get_all_doctypes();

            //_mdl._desgnation = deptser.get_designations_by_deptid(_mdl.depid);

            return View(_mdl);
        }
        public List<doctype_item> get_all_doctypes()
        {
            var dept = new documentservices();
            return dept.get_all_active_types();
        }
        [HttpPost]
        public ActionResult edit(membermodal _mdl)
        {
            ViewBag.tab = 1;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            memberservices _m = new memberservices();
            _mdl.city_id = getcityidbystate(_mdl.state_id, _mdl.ecity);
            var mes = _m.update_member(_mdl, userid, false);
            TempData["error"] = mes;
            string token = new Crypto().EncryptStringAES(_mdl.userid.ToString());
            return RedirectToAction("edit", "member", new { token = token, tab = 1 });
        }


        public ActionResult delete_bank(string mtoken, string token)
        {
            Crypto _crypt = new Crypto();
            int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
            ViewBag.tab = 1;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            memberservices _m = new memberservices();
            var mes = _m.delete_bank(id);
            TempData["error"] = mes;
            return RedirectToAction("edit", "member", new { token = mtoken, tab = 2 });
        }
        public ActionResult delete_doc(string mtoken, string token)
        {
            Crypto _crypt = new Crypto();
            int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
            ViewBag.tab = 1;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            memberservices _m = new memberservices();
            var mes = _m.delete_doc(id);
            TempData["error"] = mes;
            return RedirectToAction("edit", "member", new { token = mtoken, tab = 1 });
        }

        [HttpPost]
        public ActionResult edit_bank(int memberid, string bank_name, string branch_name, string account_name, string account_number, string ifsc_code, string pan_number)
        {
            Crypto _crypt = new Crypto();
            string token = _crypt.EncryptStringAES(memberid.ToString());
            ViewBag.tab = 1;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            memberservices _m = new memberservices();
            var mes = _m.update_member_bank(memberid, bank_name, branch_name, account_name, account_number, ifsc_code, pan_number, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "member", new { token = token, tab = 2 });
        }

        [HttpPost]
        public ActionResult bank_update(int memberid, int bankid, string bank_name, string branch_name, string account_name, string account_number, string ifsc_code, string pan_number)
        {
            Crypto _crypt = new Crypto();
            string token = _crypt.EncryptStringAES(memberid.ToString());
            ViewBag.tab = 1;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            memberservices _m = new memberservices();
            var mes = _m.update_bank(bankid, bank_name, branch_name, account_name, account_number, ifsc_code, pan_number, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "member", new { token = token, tab = 2 });
        }

        [HttpPost]
        public ActionResult edit_image(HttpPostedFileBase file, int userid)
        {
            Crypto _crypt = new Crypto();
            string token = _crypt.EncryptStringAES(userid.ToString());
            helper _helper = new helper();
            //int userid = 0;// will be added later
            memberservices _m = new memberservices();
            string path = Server.MapPath("~/image/picture-" + userid);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            ViewBag.tab = 4;
            ViewBag.permissions = _permission;
            string filenames = _helper.GetRandomalphanumeric(3);
            var extension = Path.GetExtension(file.FileName);
            var imagepath = path + "/" + filenames + extension;
            string tempfilename = "~/image/picture-" + userid + "/" + filenames + extension;
            if (System.IO.File.Exists(imagepath))
            {
                System.IO.File.Delete(imagepath);
            }
            file.SaveAs(imagepath);
            //GenerateThumbnails(imagepath, 0.2, imagepath);
            var mes = _m.add_new_member_image(tempfilename, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "member", new { token = token, tab = 4 });
        }


        [HttpPost]
        public ActionResult edit_doc(HttpPostedFileBase file, string token, string doc, int _dctype = 0)
        {
            Crypto _crypt = new Crypto();
            int userid = Convert.ToInt32(_crypt.DecryptStringAES(token.ToString()));
            ViewBag.permissions = _permission;
            //int userid = 0;// will be added later
            memberservices _m = new memberservices();
            string path = Server.MapPath("~/image/doc-" + userid);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            ViewBag.tab = 4;
            string filenames = new helper().GetRandomalphanumeric(4);
            var extension = Path.GetExtension(file.FileName);
            var imagepath = path + "/" + filenames + extension;
            string tempfilename = "/image/doc-" + userid + "/" + filenames + extension;
            if (System.IO.File.Exists(imagepath))
            {
                System.IO.File.Delete(imagepath);
            }
            file.SaveAs(imagepath);
            //GenerateThumbnails(imagepath, 0.2, imagepath);
            var mes = _m.add_new_doc(doc, userid, tempfilename, _dctype);
            TempData["error"] = mes;
            return RedirectToAction("edit", "member", new { token = token, tab = 1 });
        }
        [HttpPost]
        public ActionResult edit_skills(int[] skills, string token)
        {
            ViewBag.tab = 1;
            //  int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            Crypto _crypt = new Crypto();
            int userid = Convert.ToInt32(_crypt.DecryptStringAES(token.ToString()));
            memberservices _m = new memberservices();
            var mes = _m.update_skills_by_member(skills, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "member", new { tab = 2, token = token });
        }
        [HttpPost]
        public ActionResult add_tools(string tooltype1, string toolname1, string token)
        {
            var mes = "";
            Crypto _crypt = new Crypto();
            int userid = Convert.ToInt32(_crypt.DecryptStringAES(token.ToString()));
            mes = new toolservices().add_new_toolbytypename(toolname1, tooltype1, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "member", new { tab = 3, token = token });
        }

        [HttpPost]
        public ActionResult edit_tools(int[] tools, string token)
        {
            ViewBag.tab = 1;
            Crypto _crypt = new Crypto();
            int userid = Convert.ToInt32(_crypt.DecryptStringAES(token.ToString()));
            memberservices _m = new memberservices();
            var mes = _m.update_tools_by_member(tools, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "member", new { tab = 3, token = token });
        }



        private void GenerateThumbnails(string fileName, double scaleFactor = 0.5, string targetPath = "")
        {
            //System.Drawing.Imaging.ImageFormat imageFormat = System.Drawing.Imaging.ImageFormat.Jpeg;
            //Bitmap img = (Bitmap)Bitmap.FromFile(fileName);
            //var newWidth = (int)(img.Width * scaleFactor);
            //var newHeight = (int)(img.Height * scaleFactor);
            //var thumbnailImg = new Bitmap(newWidth, newHeight);
            //var thumbGraph = Graphics.FromImage(thumbnailImg);
            //thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
            //thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
            //thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
            //var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
            //thumbGraph.DrawImage(img, imageRectangle);
            //thumbnailImg.Save(targetPath, img.RawFormat);
        }


        [HttpPost]
        public ActionResult add(membermodal _mdl)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            List<int> _ctyiid = new List<int>();
            var _filtermodel = new memberfilter();
            _filtermodel = new filterservices().getfilter<memberfilter>(_filtermodel, userid, pageid);
            ViewBag.state_id = _filtermodel.state_id ?? 0;
            ViewBag.skills = string.IsNullOrEmpty(_filtermodel.skills) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.skills.Split(','), int.Parse);
            ViewBag.amount = string.IsNullOrEmpty(_filtermodel.amount) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.amount.Split(','), int.Parse); ;
            ViewBag.city = string.IsNullOrEmpty(_filtermodel.city_id) ? "" : _filtermodel.city_id;
            ViewBag.drug = _filtermodel.drug;
            ViewBag.background = _filtermodel.background;
            ViewBag.status = _filtermodel.status ?? 0;
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            ViewBag.permissions = _permission;
            memberservices _m = new memberservices();
            _mdl.city_id = getcityidbystate(_mdl.state_id, _mdl.ecity);
            var mes = _m.add_new_member(_mdl, userid, false, false);
            members _model = new members();
            //  _model = _m.get_all_active_members();
            _model._skills = get_all_skills();
            _model = new helper().GenerateError<members>(_model, mes);
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            var type = new memberservices().getmembertype(userid);
            ViewBag.issubcontractor = (type == "S" || type == "LT") ? true : false;
            return View("index", _model);
        }


        public List<department_items> get_all_dept()
        {
            var deptser = new departmentservices();
            return deptser.get_all_active_departments()._dept;
        }

        public List<role_items> get_all_roles()
        {
            var deptser = new roleservices();
            return deptser.get_all_active_roles()._roles;
        }

        public List<skill_items> get_all_skills()
        {
            var dept = new skillservices();
            return dept.get_all_active_skills()._skill;
        }

        public List<skill_items> get_all_tools()
        {
            var dept = new skillservices();
            return dept.get_all_active_tools()._skill;
        }

        public string get_member_rate(int id)
        {
            var member = new memberservices();
            return member.get_rate(id);
        }


        [HttpPost]
        public string get_all_designation_by_deptid(int deptid)
        {
            string ddl = "<option value='0'>Designation</option>";
            var deptser = new designationservices();
            var list = deptser.get_designations_by_deptid(deptid);
            foreach (var item in list)
            {
                ddl = ddl + "<option value='" + item.id + "'>" + item.name + "</option>";
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public bool validate(int type, string value)
        {
            var _ac = new accountservices();
            bool isexist = false;
            switch (type)
            {
                case 1:
                    isexist = _ac.validate_email(value);
                    break;
                case 2:
                    isexist = _ac.validate_memberid(value);
                    break;
                case 3:
                    isexist = _ac.validate_username(value);
                    break;
            }
            return isexist;
        }

        private void Getcsv(DataTable dt, string FIleName)
        {
            DataColumnCollection columns = dt.Columns;
            if (columns.Contains("memder_p_id"))
            {
                columns.Remove("memder_p_id");
            }
            if (columns.Contains("staus_color"))
            {
                columns.Remove("staus_color");
            }
            string str = string.Concat("attachment; filename=", FIleName, ".csv");
            base.Response.ClearContent();
            base.Response.AddHeader("content-disposition", str);
            base.Response.ContentType = "application/vnd.ms-excel";
            string str1 = "";
            foreach (DataColumn column in dt.Columns)
            {
                base.Response.Write(string.Concat(str1, column.ColumnName));
                str1 = ",";
            }
            base.Response.Write("\n");
            foreach (DataRow row in dt.Rows)
            {
                str1 = "";
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    base.Response.Write(string.Concat(str1, row[i].ToString()));
                    str1 = ",";
                }
                base.Response.Write("\n");
            }
            base.Response.End();
        }

        //public JsonResult Fetchmember(int start = 0, int length = 0, int draw = 1, string searchBy = "", string searchItem = "", int pageindex = 1)
        //{
        //    int num;
        //    if (pageindex != 0)
        //    {
        //        num = pageindex;
        //    }
        //    else
        //    {
        //        num = (start == 0 ? start : start / 10 + 1);
        //    }
        //    start = num;
        //    AdminServices adminService = new AdminServices();
        //    AdminController.TableDate<profile_item> tableDate = new AdminController.TableDate<profile_item>();
        //    List<string[]> strArrays = new List<string[]>();
        //    int num1 = 0;
        //    List<profile_item> allClientsPageWise = adminService.GetAllClientsPageWise(ref num1, searchBy, searchItem, start, length)._profiles;
        //    tableDate.draw = draw;
        //    tableDate.data = allClientsPageWise;
        //    tableDate.recordsFiltered = (allClientsPageWise.Count<profile_item>() > 0 ? allClientsPageWise[0].total : 0);
        //    tableDate.recordsTotal = num1;
        //    return base.Json(tableDate, 0);
        //}
        //[HttpPost]
        //public ActionResult custom_sms(HttpPostedFileBase file, CustomSmsModel model)
        //{
        //    ActionResult action;
        //    utility _utility = new utility();
        //    Comman comman = new Comman();
        //    contactservices contactservice = new contactservices();
        //    bindropdown _bindropdown = new bindropdown();
        //    try
        //    {
        //            string lower = Path.GetExtension(file.FileName).ToLower();
        //            if ((lower == ".txt" ? false : lower != ".csv"))
        //            {
        //                base.TempData["error"] = "Please upload a valid Document with the extenion in .txt, .csv";
        //                action = base.RedirectToAction("custom-sms");
        //            }
        //            else
        //            {
        //                string empty = string.Empty;
        //                string str = string.Empty;
        //                contactservice.AddFileInDb(file.FileName, ref empty, ref str);
        //                int num = model.userid;
        //                string randomalphanumeric = _utility.GetRandomalphanumeric(null);
        //                string str1 = Path.Combine(string.Concat(base.Server.MapPath("~/"), Global.CommanPath.filesavedpath), string.Concat(randomalphanumeric, file.FileName));
        //                file.SaveAs(str1);
        //                string str2 = string.Concat("~", Global.CommanPath.filesavedpath, randomalphanumeric, file.FileName);
        //                DataTable dataTable = ConvertTo.csvToDataTable(str2, true);
        //                model = comman.getContactList(dataTable);
        //                model.filepath = str2;
        //                model.userid = num;
        //                base.ViewData["succes"] = "Review file contents before importing data.";
        //                ((dynamic)base.ViewBag).groups = _bindropdown.BindcustomGroupDropDown("0", model.userid);
        //                ((dynamic)base.ViewBag).SenderNumber = _bindropdown.BindNumberDropDown("0");
        //                model.Totalcontacts = dataTable.Rows.Count;
        //                model.lastadddate = str;
        //                model.Lastaddfile = empty;
        //                action = base.View("custom_sms", model);
        //            }
        //        }
        //        catch (Exception exception1)
        //        {
        //            Exception exception = exception1;
        //            base.TempData["error"] = exception.Message;
        //            comman.LogWrite(Enums.LogType.Error, exception.Message.ToString(), "usercontoller", "upload_contacts", Enums.Priority.Normal, exception.StackTrace);
        //            action = base.RedirectToAction("custom-sms");
        //        }
        //    finally
        //    {
        //    }
        //    return action;
        //}


        public class TableDate<T>
        {
            public List<T> data
            {
                get;
                set;
            }

            public int draw
            {
                get;
                set;
            }

            public int recordsFiltered
            {
                get;
                set;
            }

            public int recordsTotal
            {
                get;
                set;
            }
        }

    }
}