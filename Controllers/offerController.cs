﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;


namespace hrm.Controllers
{

    [AuthorizeVerifiedloggedin]
    public class offerController : Controller
    {
        // GET: jobs
        public List<int> _permission;
        public int pageid = 5;
        public offerController()
        {
            int roleid = System.Web.HttpContext.Current.Request.Cookies["_pid"] == null ? 0 : Convert.ToInt16(System.Web.HttpContext.Current.Request.Cookies["_pid"].Value.ToString());
            roleservices _ser = new roleservices();
            _permission = _ser.get_all_active_permissions_bytypeid(14, roleid);
        }

        public void sendoffer(List<int> data, int tid, int pref, int offerId, bool sms, bool email, string subject)
        {
            helper _helper = new helper();
            string smsText = "";
            string emailText = "";
            // int templateId = tid;
            var template = new templateservices().get_template(tid);
            smsText = sms ? template.sms : "";
            emailText = email ? template.email : "";
            new jobofferservices().sendoffers(data, tid, pref, offerId, sms, email, smsText, emailText, subject);
        }

        public void sendoffer2(List<int> data, int tid, bool termspopup, bool sms, bool email, string subject)
        {
            helper _helper = new helper();
            string smsText = "";
            string emailText = "";
            // int templateId = tid;
            var template = new templateservices().get_template(tid);
            smsText = sms ? template.sms : "";
            emailText = email ? template.email : "";
            new jobofferservices().sendoffers2(data, tid, sms, email, smsText, emailText, subject, termspopup);
        }

        // data: array, 'tid': templates, 'pref': '', 'offerId': offerId,'sms':$("#sms3").prop("checked"),'email':$("#email3").prop("checked"),'subject':subj
        public void sendoffer5(List<int> data, int tid, int jobid, bool sms, bool email, string subject)
        {
            helper _helper = new helper();
            string smsText = "";
            string emailText = "";
            // int templateId = tid;
            var template = new templateservices().get_template(tid);
            smsText = sms ? template.sms : "";
            emailText = email ? template.email : "";
            new jobofferservices().sendoffers4(data, tid, sms, email, smsText, emailText, subject, jobid);
        }


        public class Events
        {
            public bool allDay
            {
                get;
                set;
            }

            public string token
            {
                get;
                set;
            }

            public string backgroundColor
            {
                get;
                set;
            }

            public string className
            {
                get;
                set;
            }

            public string date
            {
                get;
                set;
            }

            public string end
            {
                get;
                set;
            }

            public string id
            {
                get;
                set;
            }

            public string start
            {
                get;
                set;
            }

            public string title
            {
                get;
                set;
            }

            public string url
            {
                get;
                set;
            }

        }
        public int[] StringToArray(string input, string separator)
        {
            string[] stringList = input.Split(separator.ToCharArray(),
                                              StringSplitOptions.RemoveEmptyEntries);
            int[] list = new int[stringList.Length];

            for (int i = 0; i < stringList.Length; i++)
            {
                list[i] = Convert.ToInt32(stringList[i]);
            }

            return list;
        }

        public ActionResult GetEvents(int id, string start = null, string end = null, string date = "", int type = 1, int state_filter_id = 0, int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, string city = "", int searchtype = 0)
        {
            if (!_permission.Contains(3064))
            {
                return View("unauth");
            }
            int[] city_filter_id = null;
            if (!string.IsNullOrEmpty(city))
            {
                city_filter_id = StringToArray(city, ",");
            }
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new jobfilter();
            if (searchtype == 0)
            {
                _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid);
                city_filter_id = string.IsNullOrEmpty(_filtermodel.city) ? null : Array.ConvertAll(_filtermodel.city.Split(','), int.Parse);
                date = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
                state_filter_id = _filtermodel.state_filter_id ?? 0;
                cclient = _filtermodel.cclient ?? 0;
                cTechnician = _filtermodel.cTechnician ?? 0;
                cDispatcher = _filtermodel.cDispatcher ?? 0;
                Status = _filtermodel.Status ?? 0;

            }
            else if (searchtype == 1)
            {
                _filtermodel.city = city_filter_id == null ? null : city;
                _filtermodel.date = date;
                _filtermodel.state_filter_id = state_filter_id;
                _filtermodel.cclient = cclient;
                _filtermodel.cTechnician = cTechnician;
                _filtermodel.cDispatcher = cDispatcher;
                _filtermodel.Status = Status;
                new filterservices().save_filter<jobfilter>(_filtermodel, userid, pageid);
            }
            if (searchtype == 2)
            {
                city_filter_id = null;
                date = "";
                state_filter_id = 0;
                cclient = 0;
                cTechnician = 0;
                cDispatcher = 0;
                Status = 0;
                new filterservices().delete_filter(userid, pageid);
                //clearsearch
            }

            DateTime dateTime;
            jobofferservices _ser = new jobofferservices();
            DateTime dateTime1 = (!string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : DateTime.Now);
            List<Events> events = new List<Events>();
            if (string.IsNullOrEmpty(end))
            {
                DateTime currentTimeByTimeZone = DateTime.Now;
                dateTime = currentTimeByTimeZone.AddDays(30);
            }
            else
            {
                dateTime = Convert.ToDateTime(end);
            }
            var model = new jobmodal();
            if (_permission.Contains(3069))
            {
                model = _ser.get_all_active_jobs_bymemberid(0, date, state_filter_id, cclient, id, cDispatcher, Status, city_filter_id);
                //ViewBag.hiderate = true;
            }
            else
            {//
                ViewBag.hiderate = false;
                model = _ser.get_all_active_jobs(date, state_filter_id, cclient, cTechnician, cDispatcher, Status, city_filter_id);
            }
            var filterjobs = model._jobs.ToList();
            if (!string.IsNullOrEmpty(date))
            {
                filterjobs = filterjobs.Where(a => a._sdate >= dateTime1 || a._endate <= dateTime).ToList();
            }
            Crypto _crypt = new Crypto();
            foreach (var item in filterjobs)
            {
                events.Add(new Events()
                {
                    id = item.JobID.ToString(),
                    start = item._sdate.ToString("s"),
                    end = item._endate.ToString("s"),
                    title = item.Title,
                    allDay = false,
                    token = _crypt.EncryptStringAES(item.JobID.ToString())
                });
            }
            Events[] array = events.ToArray();
            return base.Json(array, 0);
        }

        public ActionResult Index(int? id, int type = 1, int srch = 1, string token = "")
        {
            if (!_permission.Contains(3064))
            {
                return View("unauth");
            }
            if (type == 2)
            {
                if (!_permission.Contains(3072))
                {
                    return View("unauth");
                }
            }
            else
            {
                if (!_permission.Contains(3073))
                {
                    return View("unauth");
                }
            }
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var membertype = new memberservices().getmembertype(userid);
            ViewBag.issubcontractor = (membertype == "S" || membertype == "LT") ? true : false;
            var _filtermodel = new jobfilter();
            if (srch == 2)
            {
                new filterservices().delete_filter(userid, pageid);
                //clearsearch
            }
            _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid);
            ViewBag.date = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
            ViewBag.state_filter_id = _filtermodel.state_filter_id ?? 0;
            ViewBag.cclient = _filtermodel.cclient ?? 0;
            ViewBag.city = string.IsNullOrEmpty(_filtermodel.city) ? "" : _filtermodel.city;
            ViewBag.cTechnician = _filtermodel.cTechnician ?? 0;
            ViewBag.cDispatcher = _filtermodel.cDispatcher ?? 0;
            ViewBag.Status = _filtermodel.Status ?? 0;
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            ViewBag.permissions = _permission;
            jobofferservices _ser = new jobofferservices();
            job _model = new job();
            List<commentmodal> _Comments = new List<commentmodal>();
            commentmodal _item = new commentmodal();
            _item.createdby = "Developer Ghjdshsj One";
            _item.CreatedDate = "02-13-2020 12:10:19 PM";
            _item.Comment = "<p>This is test notification</p>";
            _Comments.Add(_item);
            _model._Comments = _Comments;
            string message = "";
            if (!string.IsNullOrEmpty(token))
            {
                Crypto _crypt = new Crypto();
                int jid = Convert.ToInt32(_crypt.DecryptStringAES(token));
                message = _ser.delete_job(jid);
            }
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            _model = new helper().GenerateError<job>(_model, message);
            string view = "admin";
            if (_permission.Contains(3069))
            {
                // _model._jobs = _ser.get_all_active_jobs_bymemberid(userid, "")._jobs;
                ViewBag.hiderate = true;
                view = "member";
            }
            else
            {
                ViewBag.hiderate = false;
                //  _model._jobs = _ser.get_all_active_jobs("")._jobs;
            }
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            if (type == 1)
            {
                return View(view, _model);

            }
            else
            {
                ViewBag.userid = userid;
                return View("calender", _model);
            }

        }
        [HttpPost]
        public string get_job_Offerforpopup(int stateid = 0, string city = "", string offerid = "", int clientid = 0)
        {
            string ddl = "<option  value='0'>Offer ID</option>";
            var deptser = new defaultservices();
            var list = deptser.get_all_offer_unassign(stateid, city, offerid, clientid);
            foreach (var item in list)
            {
                ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }
        public void update_status(int id, int statusId)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            new jobofferservices().update_status(statusId, id, userid);
        }

        public PartialViewResult getnotifications(int jobid)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            notificationModal _model = new notificationModal();
            _model.message = new notificationServices().get_all_active_notificationsbyofferid(jobid, userid);
            _model.mainind = jobid;
            return PartialView("Replyoffer", _model);
        }
        public PartialViewResult getnotificationsmember(int jobid)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            notificationModal _model = new notificationModal();
            _model.message = new notificationServices().get_all_active_notificationsbyofferid(jobid, userid);
            _model.mainind = jobid;
            return PartialView("Replyoffer2", _model);
        }


        [ValidateInput(false)]
        public void Sendmessage(int JobID, string Comment)
        {
            jobofferservices _ser = new jobofferservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            _ser.add_new_comment(Comment, userid, JobID, true);
        }

        public JsonResult Fetchadminjobs(int start = 0, int length = 25, int draw = 1, string date = "", int type = 1, int state_filter_id = 0, int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int searchtype = 0)
        {
            string search = Request.Params["search[value]"] == null ? "" : Request.Params["search[value]"];
            string _city_id = Request.Params["city_filter_id"] == null ? "" : Request.Params["city_filter_id"];
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new jobfilter();
            if (searchtype == 0)
            {
                _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid);
                _city_id = string.IsNullOrEmpty(_filtermodel.city) ? "0" : _filtermodel.city;
                date = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
                state_filter_id = _filtermodel.state_filter_id ?? 0;
                cclient = _filtermodel.cclient ?? 0;
                cTechnician = _filtermodel.cTechnician ?? 0;
                cDispatcher = _filtermodel.cDispatcher ?? 0;
                Status = _filtermodel.Status ?? 0;

            }
            else if (searchtype == 1)
            {
                _filtermodel.city = _city_id;
                _filtermodel.date = date;
                _filtermodel.state_filter_id = state_filter_id;
                _filtermodel.cclient = cclient;
                _filtermodel.cTechnician = cTechnician;
                _filtermodel.cDispatcher = cDispatcher;
                _filtermodel.Status = Status;
                new filterservices().save_filter<jobfilter>(_filtermodel, userid, pageid);
            }
            if (searchtype == 2)
            {
                _city_id = "0";
                date = "";
                state_filter_id = 0;
                cclient = 0;
                cTechnician = 0;
                cDispatcher = 0;
                Status = 0;
                new filterservices().delete_filter(userid, pageid);
                //clearsearch
            }
            int[] city_filter_id = Array.ConvertAll(_city_id.Split(','), int.Parse);
            start = (start == 0 ? 1 : start + 1);
            length = (start == 1 ? length : (start - 1) + length);
            jobofferservices _ser = new jobofferservices();
            datatableAjax<job_items> tableDate = new datatableAjax<job_items>();
            int total = 0;
            var _model = _ser.get_all_active_jobs_by_index(ref total, userid, start, length, search, date, state_filter_id, cclient, cTechnician, cDispatcher, Status, city_filter_id);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = (_model.Count() > 0 ? total : 0);
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Fetchuserjobs(int start = 0, int length = 25, int draw = 1, string date = "", int type = 1, int state_filter_id = 0, int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int searchtype = 0)
        {
            string search = Request.Params["search[value]"] == null ? "" : Request.Params["search[value]"];
            string _city_id = Request.Params["city_filter_id"] == null ? "" : Request.Params["city_filter_id"];
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new jobfilter();
            var membertype = new memberservices().getmembertype(userid);
            if (searchtype == 0)
            {
                _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid);
                _city_id = string.IsNullOrEmpty(_filtermodel.city) ? "0" : _filtermodel.city;
                date = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
                state_filter_id = _filtermodel.state_filter_id ?? 0;
                cclient = _filtermodel.cclient ?? 0;
                cTechnician = _filtermodel.cTechnician ?? 0;
                cDispatcher = _filtermodel.cDispatcher ?? 0;
                Status = _filtermodel.Status ?? 0;

            }
            else if (searchtype == 1)
            {
                _filtermodel.city = _city_id;
                _filtermodel.date = date;
                _filtermodel.state_filter_id = state_filter_id;
                _filtermodel.cclient = cclient;
                _filtermodel.cTechnician = cTechnician;
                _filtermodel.cDispatcher = cDispatcher;
                _filtermodel.Status = Status;
                new filterservices().save_filter<jobfilter>(_filtermodel, userid, pageid);
            }
            if (searchtype == 2)
            {
                _city_id = "0";
                date = "";
                state_filter_id = 0;
                cclient = 0;
                cTechnician = 0;
                cDispatcher = 0;
                Status = 0;
                new filterservices().delete_filter(userid, pageid);
                //clearsearch
            }
            cTechnician = userid;
            int[] city_filter_id = Array.ConvertAll(_city_id.Split(','), int.Parse);
            start = (start == 0 ? 1 : start + 1);
            length = (start == 1 ? length : (start - 1) + length);
            jobofferservices _ser = new jobofferservices();
            datatableAjax<job_items> tableDate = new datatableAjax<job_items>();
            int total = 0;
          
            var _model = _ser.get_all_active_jobs_by_indexByMember(ref total, userid, start, length, search, date, state_filter_id, cclient, cTechnician, cDispatcher, Status, city_filter_id, membertype:membertype);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = (_model.Count() > 0 ? total : 0);
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Index(string date = "", int type = 1, int state_filter_id = 0, int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int[] city_filter_id = null)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new jobfilter();
            _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid);
            ViewBag.date = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
            ViewBag.state_filter_id = _filtermodel.state_filter_id ?? 0;
            ViewBag.cclient = _filtermodel.cclient ?? 0;
            ViewBag.city = string.IsNullOrEmpty(_filtermodel.city) ? "" : _filtermodel.city;
            ViewBag.cTechnician = _filtermodel.cTechnician ?? 0;
            ViewBag.cDispatcher = _filtermodel.cDispatcher ?? 0;
            ViewBag.Status = _filtermodel.Status ?? 0;
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            ViewBag.permissions = _permission;
            jobofferservices _ser = new jobofferservices();
            job _model = new job();
            string message = "";

            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            _model = new helper().GenerateError<job>(_model, message);
            // int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            string view = "admin";
            if (_permission.Contains(51))
            {
                _model._jobs = _ser.get_all_active_jobs_bymemberid(userid, date, state_filter_id, cclient, cTechnician, cDispatcher, Status, city_filter_id)._jobs;
                ViewBag.hiderate = true;
                view = "member";
            }
            else
            {
                ViewBag.hiderate = false;
                _model._jobs = _ser.get_all_active_jobs(date, state_filter_id, cclient, cTechnician, cDispatcher, Status, city_filter_id)._jobs;
            }
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            if (type == 1)
            {
                return View(view, _model);

            }
            else
            {
                ViewBag.userid = userid;
                return View("calender", _model);
            }

        }
        public void update_readstatus(int jobid)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            new notificationServices().update_jobreadofferstatus(jobid, userid);
        }
        public ActionResult view(string token, int type = 1, int viewtype = 1)
        {
            if (!_permission.Contains(3071))
            {
                return View("unauth");
            }
            Crypto _cryot = new Crypto();
            int id = Convert.ToInt32(_cryot.DecryptStringAES(token));
            ViewBag.permissions = _permission;
            jobofferservices _ser = new jobofferservices();
            job_items1 _model = new job_items1();
            ViewBag.viewtype = viewtype;
            int offerid = 0;
            Session["type"] = viewtype;
            ViewBag.type = type;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var subcontractortype = new memberservices().getmembertype(userid);
            bool isoffered = false;
            _model = _ser.get_all_active_jobs_byid(id, userid, subcontractortype, ref offerid,ref isoffered, userid);
            if (_permission.Contains(3074))
            {
                ViewBag.offerid = offerid;
                ViewBag.isoffered = isoffered;
                var comments = _model._Comments.Where(a => a.isadminpost == true || a.Commentby == userid || a.Commentto == userid).ToList();
                _model._Comments = comments;
                ViewBag.issubcontractor = (subcontractortype == "S" || subcontractortype == "LT") ? true : false;
                return View("Tview", _model);
            }
            else
            {
                var comments = _model._Comments.Where(a => a.Parentid == 0).ToList();
                var chldcomments = _model._Comments.Where(a => a.Parentid != 0).OrderByDescending(a => a.Parentid).ToList();
                _model._Comments = comments;
                _model._childComments = chldcomments;
            }
            return View(_model);
        }
        public JsonResult getjob(int id)
        {
            jobofferservices _ser = new jobofferservices();
            var _model = _ser.get_all_active_job_details_byid(id);
            return Json(_model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult delete_file(string id, string jobid)
        {
            Crypto _crypt = new Crypto();
            int _id = Convert.ToInt32(_crypt.DecryptStringAES(id));
            int _jobid = Convert.ToInt32(_crypt.DecryptStringAES(jobid));
            ViewBag.permissions = _permission;
            jobofferservices _ser = new jobofferservices();
            job_items1 _model = new job_items1();
            var message = _ser.delete_doc(_id);
            int offerid = 0;
            ViewBag.viewtype = Session["type"] == null ? 1 : Convert.ToInt16(Session["type"]);
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var subcontractortype = new memberservices().getmembertype(userid);
            bool isoffered = false;
            _model = _ser.get_all_active_jobs_byid(_jobid, userid, subcontractortype, ref offerid,ref isoffered, userid);
            _model = new helper().GenerateError<job_items1>(_model, message);
            ViewBag.type = 3;
            if (_permission.Contains(3074))
            {
                ViewBag.offerid = offerid;
                ViewBag.isoffered = isoffered;
                var comments = _model._Comments.Where(a => a.isadminpost == true || a.Commentby == userid || a.Commentto == userid).ToList();
                _model._Comments = comments;
              
                ViewBag.issubcontractor = (subcontractortype == "S" || subcontractortype == "LT") ? true : false;

                return View("Tview", _model);
            }
            else
            {
                var comments = _model._Comments.Where(a => a.Parentid == 0).ToList();
                var chldcomments = _model._Comments.Where(a => a.Parentid != 0).OrderByDescending(a => a.Parentid).ToList();
                _model._Comments = comments;
                _model._childComments = chldcomments;
            }
            return View("view", _model);
        }

        public ActionResult edit(string token, int type = 1)
        {
            if (!_permission.Contains(3066))
            {
                return View("unauth");
            }
            Crypto _cryot = new Crypto();
            int id = Convert.ToInt32(_cryot.DecryptStringAES(token));
            ViewBag.permissions = _permission;
            jobofferservices _ser = new jobofferservices();
            job _model = new job();
            ViewBag.type = type;
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            _model = _ser.get_all_active_job_details_byid(id);
            return View(_model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult update(int JobID, int type = 1, HttpPostedFileBase file = null, string Comment = "", string file_title = "", int action = 0, bool isadminpost = false)
        {
            ViewBag.permissions = _permission;
            ViewBag.type = type;
            jobofferservices _ser = new jobofferservices();
            job_items1 _model = new job_items1();
            var mes = "";
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            if (type == 4)
            {///0--assign 1-resubmiut--2-remove
                if (action == 2)
                {
                    mes = _ser.delete_submitedoffer(JobID);
                }
                else if (action == 1)
                {
                    mes = _ser.re_submitedoffer(JobID);
                }
                else if (action == 0)
                {
                    mes = _ser.assign_submitedoffer(JobID);
                }

            }
            if (type == 2)
            {
                mes = _ser.add_new_comment(Comment, userid, JobID, isadminpost);
            }
            else if (type == 3)
            {
                string path = Server.MapPath("~/image/jobofferpicture-" + JobID);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string filenames = new helper().GetRandomalphanumeric(4);
                var extension = Path.GetExtension(file.FileName);
                var imagepath = path + "/" + filenames + extension;
                string tempfilename = "/image/jobofferpicture-" + JobID + "/" + filenames + extension;
                if (System.IO.File.Exists(imagepath))
                {
                    System.IO.File.Delete(imagepath);
                }
                file.SaveAs(imagepath);
                long b = file.ContentLength;
                long kb = b / 1024;
                mes = _ser.add_new_doc(file_title, userid, JobID, kb.ToString() + " kb", tempfilename);
            }
            bool isoffered = false;
            int offerId = 0;
            ModelState.Clear();
            var subcontractortype = new memberservices().getmembertype(userid);
            _model = _ser.get_all_active_jobs_byid(JobID, userid, subcontractortype, ref offerId, ref isoffered, userid);
            _model = new helper().GenerateError<job_items1>(_model, mes);
           ViewBag.viewtype = Session["type"] == null ? 1 : Convert.ToInt16(Session["type"]);
            if (_permission.Contains(3074))
            {
                ViewBag.offerid = offerId;
                ViewBag.isoffered = isoffered;
                var comments = _model._Comments.Where(a => a.isadminpost == true || a.Commentby == userid || a.Commentto == userid).ToList();
                _model._Comments = comments;
                ViewBag.issubcontractor = (subcontractortype == "S" || subcontractortype == "LT") ? true : false;
return View("Tview", _model);
            }
            else
            {
                var comments = _model._Comments.Where(a => a.Parentid == 0).ToList();
                var chldcomments = _model._Comments.Where(a => a.Parentid != 0).OrderByDescending(a => a.Parentid).ToList();
                _model._Comments = comments;
                _model._childComments = chldcomments;
            }
            return View("view", _model);
        }

        public string sendemail()
        { string send = "";
            MailMessage m = new MailMessage("california.massage.sanctuary@gmail.com","3056umesh@gmail.com");
            m.Subject = "test";
            m.AlternateViews.Add(AlternateView.CreateAlternateViewFromString("hello", null, "text/html"));
            m.IsBodyHtml = true;
            m.From = new MailAddress("california.massage.sanctuary@gmail.com");

            m.To.Add(new MailAddress("3056umesh@gmail.com"));
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            NetworkCredential authinfo = new NetworkCredential("california.massage.sanctuary@gmail.com", "cspa13661366");
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = authinfo;
            try
            {
                smtp.Send(m);
            }
            catch(Exception er)
            {
                send = er.Message;
            }
            return send;
        }

        public ActionResult acceptoffer(string  token)
        {
            Crypto _crypt = new Crypto();
            jobofferservices _ser = new jobofferservices();
            int offerid = Convert.ToInt32(_crypt.DecryptStringAES(token));
           string mes = _ser.re_acceptedoffer(offerid);
            TempData["error"] = mes;
            return RedirectToAction("index");
        }

        public ActionResult declineoffer(string token)
        {
            Crypto _crypt = new Crypto();
            jobofferservices _ser = new jobofferservices();
            int offerid = Convert.ToInt32(_crypt.DecryptStringAES(token));
            string mes = _ser.assign_declineoffer(offerid);
            TempData["error"] = mes;
            return RedirectToAction("index");
        }

        public ActionResult update_subjob(int JobID = 0, int offerid = 0, int btn = 0, int type = 1, HttpPostedFileBase file = null, string Comment = "", string file_title = "", int action = 0, string jtoken = "", string token = "")
        {
            Crypto _crypt = new Crypto();
            if (!string.IsNullOrEmpty(jtoken))
            {
                JobID = Convert.ToInt32(_crypt.DecryptStringAES(jtoken));
            }
            if (!string.IsNullOrEmpty(token))
            {
                offerid = Convert.ToInt32(_crypt.DecryptStringAES(token));
            }
            ViewBag.permissions = _permission;
            ViewBag.type = type;
            jobofferservices _ser = new jobofferservices();
            job_items1 _model = new job_items1();
            var mes = "";
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            if (type == 4)
            {///0--assign 1-resubmiut--2-remove
                if (btn == 2)
                {
                    mes = _ser.delete_submitedoffer(offerid);
                }
                else if (btn == 1)
                {
                    mes = _ser.re_submitedoffer(offerid);
                }
                else if (btn == 4)
                {
                    ViewBag.type = 1;
                    mes = _ser.re_acceptedoffer(offerid);
                }
                else if (btn == 5)
                {
                    mes = _ser.assign_declineoffer(offerid);
                    TempData["error"] = mes;
                    return RedirectToAction("index");
                }
                else if (btn == 0)
                {
                    mes = _ser.assign_submitedoffer(offerid);
                    TempData["error"] = mes;
                    return RedirectToAction("index");
                }
            }
            if (type == 2)
            {

                mes = _ser.add_new_comment(Comment, userid, JobID);
            }
            else if (type == 3)
            {
                string path = Server.MapPath("~/image/jobofferpicture-" + JobID);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string filenames = new helper().GetRandomalphanumeric(4);
                var extension = Path.GetExtension(file.FileName);
                var imagepath = path + "/" + filenames + extension;
                string tempfilename = "/image/jobofferpicture-" + JobID + "/" + filenames + extension;
                if (System.IO.File.Exists(imagepath))
                {
                    System.IO.File.Delete(imagepath);
                }
                file.SaveAs(imagepath);
                long b = file.ContentLength;
                long kb = b / 1024;
                mes = _ser.add_new_doc(file_title, userid, JobID, kb.ToString() + " kb", tempfilename);
            }
            bool isoffered = false;
            ModelState.Clear();
            int offerids = 0;
            var subcontractortype = new memberservices().getmembertype(userid);
            _model = _ser.get_all_active_jobs_byid(JobID, userid, subcontractortype,ref offerids, ref isoffered, userid);
            _model = new helper().GenerateError<job_items1>(_model, mes);
            ViewBag.viewtype = Session["type"] == null ? 1 : Convert.ToInt16(Session["type"]);
           if (_permission.Contains(3074))
            {
                ViewBag.isoffered = isoffered;
                ViewBag.offerid = offerids;
                var comments = _model._Comments.Where(a => a.isadminpost == true || a.Commentby == userid || a.Commentto == userid).ToList();
                _model._Comments = comments;
               ViewBag.issubcontractor = (subcontractortype == "S" || subcontractortype == "LT") ? true : false;

                return View("Tview", _model);
            }
            else
            {
                var comments = _model._Comments.Where(a => a.Parentid == 0).ToList();
                var chldcomments = _model._Comments.Where(a => a.Parentid != 0).OrderByDescending(a => a.Parentid).ToList();
                _model._Comments = comments;
                _model._childComments = chldcomments;
            }
            return View("view", _model);
        }


        public bool validatejobId(string id)
        {
            return new jobofferservices().validateJobId(id);
        }

        public string add_comment(string txt, int cmntid, int jobid)
        {
            string res = "";
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            string createdBy = "";
            var cst = new jobofferservices().add_new_comment2(txt, userid, jobid, ref createdBy, false, cmntid);
            res = "<tr><td nowrap style='width:24%;padding:0px;border:0px !important;color: lightgray;padding-left:10px;'>" + createdBy + "</td>";
            res = res + "<td style='width:50%;padding:0px;border:0px !important;color: lightgray;'>" + txt + "</td>";
            res = res + "<td style='width:20%;padding:0px;border:0px !important;color: lightgray;'>" + cst.ToString() + "</td>";
            res = res + "<td nowrap style='width: 20%;padding:0px;border:0px !important;color: lightgray;'></td></tr>";
            HtmlString htm = new HtmlString(res);
            return htm.ToString();
        }


        public string getreply(int mainId)
        {
            string res = "";
            var lst = new jobofferservices().getreply(mainId);
            foreach (var item in lst)
            {
                res = res + "<tr><td nowrap style='width:24%;padding:0px;border:0px !important;color: lightgray;padding-left:10px;'>" + item.createdBy + "</td>";
                res = res + "<td style='width:50%;padding:0px;border:0px !important;color: lightgray;'>" + item.text + "</td>";
                res = res + "<td style='width:20%;padding:0px;border:0px !important;color: lightgray;'>" + item.timedate + "</td>";
                res = res + "<td nowrap style='width: 20%;padding:0px;border:0px !important;color: lightgray;'></td></tr>";

            }
            HtmlString htm = new HtmlString(res);
            return htm.ToString();
        }
        public string getreply1(int mainId)
        {
            string res = "";
            var lst = new jobofferservices().getreply(mainId);
            foreach (var item in lst)
            {
                res = res + "<tr><td nowrap style='width:25%;padding:0px;border:0px !important;color: lightgray;'>" + item.createdBy + "</td>";
                res = res + "<td style='width:51%;padding:0px;border:0px !important;color: lightgray;'>" + item.text + "</td>";
                res = res + "<td style='width:20%;padding:0px;border:0px !important;color: lightgray;'>" + item.timedate + "</td>";
                res = res + "<td nowrap style='width: 20%;padding:0px;border:0px !important;color: lightgray;'></td></tr>";

            }
            HtmlString htm = new HtmlString(res);
            return htm.ToString();
        }

        public bool validatejobIdEdit(int id, string jobid)
        {
            return new jobofferservices().validateJobId(id, jobid);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult add(job _model, int type = 1, string techrate = "", string clientrate = "", string add = "", string zip = "")
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new jobfilter();
            _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid);
            ViewBag.date = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
            ViewBag.state_filter_id = _filtermodel.state_filter_id ?? 0;
            ViewBag.cclient = _filtermodel.cclient ?? 0;
            ViewBag.city = string.IsNullOrEmpty(_filtermodel.city) ? "" : _filtermodel.city;
            ViewBag.cTechnician = _filtermodel.cTechnician ?? 0;
            ViewBag.cDispatcher = _filtermodel.cDispatcher ?? 0;
            ViewBag.Status = _filtermodel.Status ?? 0;
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            ViewBag.permissions = _permission;
            jobofferservices _ser = new jobofferservices();
            //int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            _model.city_id = getcityidbystate(_model.state_id, _model.city);
            var mes = _ser.add_new_job(_model, userid, techrate, clientrate, add, zip);
            _model = new helper().GenerateError<job>(_model, mes);
            string view = "admin";
            if (_permission.Contains(3069))
            {
                // _model._jobs = _ser.get_all_active_jobs_bymemberid(userid, "")._jobs;
                ViewBag.hiderate = true;
                view = "member";
            }
            else
            {
                ViewBag.hiderate = false;
                // _model._jobs = _ser.get_all_active_jobs("")._jobs;
            }
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            if (mes.Contains("Error"))
            {
                ViewBag.type = type;
                return View("edit", _model);
            }
            if (type == 1)
            {
                return View(view, _model);

            }
            else
            {
                ViewBag.userid = userid;
                return View("calender", _model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult update_job(job _model, int type = 1)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new jobfilter();
            _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid);
            ViewBag.date = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
            ViewBag.state_filter_id = _filtermodel.state_filter_id ?? 0;
            ViewBag.cclient = _filtermodel.cclient ?? 0;
            ViewBag.city = string.IsNullOrEmpty(_filtermodel.city) ? "" : _filtermodel.city;
            ViewBag.cTechnician = _filtermodel.cTechnician ?? 0;
            ViewBag.cDispatcher = _filtermodel.cDispatcher ?? 0;
            ViewBag.Status = _filtermodel.Status ?? 0;
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            ViewBag.type = type;
            ViewBag.permissions = _permission;
            jobofferservices _ser = new jobofferservices();
            _model.city_id = getcityidbystate(_model.state_id, _model.city);
            var mes = _ser.update_job(_model, userid);
            _model = new helper().GenerateError<job>(_model, mes);
            string view = "admin";
            if (_permission.Contains(3069))
            {
                // _model._jobs = _ser.get_all_active_jobs_bymemberid(userid, "")._jobs;
                ViewBag.hiderate = true;
                view = "member";
            }
            else
            {
                ViewBag.hiderate = false;
                //  _model._jobs = _ser.get_all_active_jobs("")._jobs;
            }
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.get_all_satets();
            if (mes.Contains("Error"))
            {
                ViewBag.type = type;
                return View("edit", _model);
            }

            if (type == 1)
            {
                return View(view, _model);

            }
            else
            {
                ViewBag.userid = userid;
                return View("calender", _model);
            }
        }

        [HttpPost]
        public string updateassign(int memberid, int jobid, string rate)
        {
            var name = "";
            jobofferservices _ser = new jobofferservices();
            name = _ser.assignjob(jobid, memberid, rate);
            return name;
        }


        //[HttpPost]
        //[ValidateInput(false)]
        //public ActionResult update_job(job _model)
        //{
        //    ViewBag.permissions = _permission;
        //    jobofferservices _ser = new jobofferservices();
        //    int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
        //    var mes = _ser.add_new_job(_model, userid);
        //    _model = new helper().GenerateError<job>(_model, mes);
        //    if (_permission.Contains(51))
        //    {
        //        _model._jobs = _ser.get_all_active_jobs_bymemberid(userid)._jobs;
        //        ViewBag.hiderate = true;
        //    }
        //    else
        //    {
        //        ViewBag.hiderate = false;
        //        _model._jobs = _ser.get_all_active_jobs()._jobs;
        //    }
        //    defaultservices _default = new defaultservices();
        //    ViewBag.select = _default.get_all_satets();
        //    return View("Index", _model);
        //}

        public int getcityidbystate(int stateid, string city)
        {
            if (string.IsNullOrEmpty(city))
            {
                return 0;
            }
            else
            {
                return new defaultservices().savegetCityId(stateid, city);
            }

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult edit(job_items1 _model, int type = 1)
        {
            bool isoffered = false;
            ViewBag.permissions = _permission;
            jobofferservices _ser = new jobofferservices();
            ViewBag.type = type;
            int offerid = 0;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var mes = _ser.update_job_byview(_model, userid);
            var subcontractortype = new memberservices().getmembertype(userid);
            _model = _ser.get_all_active_jobs_byid(_model.JobID, userid, subcontractortype, ref offerid, ref isoffered, userid);
            _model = new helper().GenerateError<job_items1>(_model, mes);
             ViewBag.type = Session["type"] == null ? 1 : Convert.ToInt16(Session["type"]);
            if (_permission.Contains(3074))
            {
                ViewBag.isoffered = isoffered;
                ViewBag.offerid = offerid;
                var comments = _model._Comments.Where(a => a.isadminpost == true || a.Commentby == userid || a.Commentto == userid).ToList();
                _model._Comments = comments;
               ViewBag.issubcontractor = (subcontractortype == "S" || subcontractortype == "LT") ? true : false;

                return View("Tview", _model);
            }
            else
            {
                var comments = _model._Comments.Where(a => a.Parentid == 0).ToList();
                var chldcomments = _model._Comments.Where(a => a.Parentid != 0).OrderByDescending(a => a.Parentid).ToList();
                _model._Comments = comments;
                _model._childComments = chldcomments;
            }
            return View("view", _model);
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Tedit(job_items1 _model, int type = 1)
        {
            bool isoffered = false;
            ViewBag.permissions = _permission;
            jobofferservices _ser = new jobofferservices();
            ViewBag.type = type;
            int offerid = 0;
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var subcontractortype = new memberservices().getmembertype(userid); 
            var mes = _ser.update_job_byviewT(_model, userid);
            _model = _ser.get_all_active_jobs_byid(_model.JobID, userid, subcontractortype, ref offerid, ref isoffered, userid);
            _model = new helper().GenerateError<job_items1>(_model, mes);
            ViewBag.type = Session["type"] == null ? 1 : Convert.ToInt16(Session["type"]);
            if (_permission.Contains(3074))
            {
                ViewBag.isoffered = isoffered;
                ViewBag.offerid = offerid;
                var comments = _model._Comments.Where(a => a.isadminpost == true || a.Commentby == userid || a.Commentto == userid).ToList();
                _model._Comments = comments;
                ViewBag.issubcontractor = (subcontractortype == "S" || subcontractortype == "LT") ? true : false;

                return View("Tview", _model);
            }
            else
            {
                var comments = _model._Comments.Where(a => a.Parentid == 0).ToList();
                var chldcomments = _model._Comments.Where(a => a.Parentid != 0).OrderByDescending(a => a.Parentid).ToList();
                _model._Comments = comments;
                _model._childComments = chldcomments;
            }
            return View("view", _model);
        }
    }
}