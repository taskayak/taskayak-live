﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class templateController : Controller
    {
        public List<int> _permission;
        public templateController()
        {
            int roleid = System.Web.HttpContext.Current.Request.Cookies["_pid"] == null ? 0 : Convert.ToInt16(System.Web.HttpContext.Current.Request.Cookies["_pid"].Value.ToString());
            roleservices _ser = new roleservices();
            _permission = _ser.get_all_active_permissions_bytypeid(1, roleid);
        }
        // GET: template
        public ActionResult Index()
        {
            if (!_permission.Contains(4064))
            {
                return View("unauth");
            }
            templateservices _ser = new templateservices();
            templateModal _temp = _ser.get_all_active_template(null);
            return View(_temp);
        }

        public ActionResult add()
        {
            if (!_permission.Contains(4064))
            {
                return View("unauth");
            }
            templateservices _ser = new templateservices();
            templateitem _temp = new templateitem();
            return View(_temp);
        }

        public ActionResult edit(int id)
        {
            if (!_permission.Contains(4064))
            {
                return View("unauth");
            }
            templateservices _ser = new templateservices();
            templateitem _temp = _ser.get_template(id);
            return View(_temp);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult add(templateitem _temp)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            templateservices _ser = new templateservices();
            var message = _ser.add_new_template(_temp, userid);
            ModelState.Clear();
            templateModal _model = _ser.get_all_active_template(null);
            ViewBag.permissions = _permission;
            _model = new helper().GenerateError<templateModal>(_model, message);
            return View("Index", _model);
           
        }

        public ActionResult delete(int id)
        {
            if (!_permission.Contains(4064))
            {
                return View("unauth");
            }
            ViewBag.permissions = _permission;
            var _ser = new templateservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            var error_text = _ser.delete_template(id);
            templateModal _model = _ser.get_all_active_template(null);
            _model = new helper().GenerateError<templateModal>(_model, error_text);
            return View("Index", _model);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult edit(templateitem _temp)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            templateservices _ser = new templateservices();
            var message = _ser.update_template(_temp, userid);
            ModelState.Clear();
            templateModal _model = _ser.get_all_active_template(null);
            ViewBag.permissions = _permission;
            _model = new helper().GenerateError<templateModal>(_model, message);
            return View("Index", _model);

        }

        public ActionResult setdefault(int tempid,bool templateddl)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            templateservices _ser = new templateservices();
            var message = _ser.update_default(tempid, templateddl);
            ModelState.Clear();
            templateModal _model = _ser.get_all_active_template(null);
            ViewBag.permissions = _permission;
            _model = new helper().GenerateError<templateModal>(_model, message);
            return View("Index", _model);

        }
    }
}