﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    public class testController : Controller
    {
        public List<skill_items> get_all_skills()
        {
            var dept = new skillservices();
            return dept.get_all_active_skills()._skill;
        }

        [HttpPost]
        public ActionResult password_update(string curpass,string newpass)
        {
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            accountservices _accs = new accountservices();
            var message = _accs.validatepassword(curpass, newpass, userid);
            TempData["error"] = message;
            return RedirectToAction("edit", "test", new { tab = 1 });
        }

        [AuthorizeVerifiedloggedin]
        public ActionResult edit(int tab = 1)
        {
            int id = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.tab = tab;
            ViewBag.isuncomplete = Convert.ToBoolean(Request.Cookies["_status"].Value.ToString());
            membermodal _mdl = new membermodal();
            memberservices _m = new memberservices();
            _mdl = _m.get_all_active_members_byid(id);
            string mes = "";
            if (TempData["error"] != null)
            {
                mes = TempData["error"].ToString();
                _mdl = new helper().GenerateError<membermodal>(_mdl, mes);
            }
            _mdl._docs = _m.get_all_doc_bymemberid(id);
            _mdl._skills = get_all_skills();
            _mdl._tools = get_all_tools();
            _mdl._dctype = get_all_doctypes();
            return View(_mdl);
        }
        public List<skill_items> get_all_tools()
        {
            var dept = new skillservices();
            return dept.get_all_active_tools()._skill;
        }
        public List<doctype_item> get_all_doctypes()
        {
            var dept = new documentservices();
            return dept.get_all_active_types();
        }
        // GET: test
        public ActionResult city()
        {
           // var hs = new hubspot().CreateHubSpotCompany();
            //Square _s = new Square();
            //_s.ExecutePayment(Convert.ToDouble("10"), "");
         // new leadservices().get_leads_from_datafetch();

            return View();
        }
        public ActionResult index()
        {
            return View();
        }
        public bool Sendmail(string to, string body)
        {
            bool str = true;
            try
            {
                string smtpEndpoint = "email-smtp.us-east-1.amazonaws.com";
                int port = 587;
                string senderName = "Taskayak";
                string senderAddress = "noreply@taskayak.com";
                string toAddress = to;
                string smtpUsername = "AKIAX27HMUUW47OFXK6R";
                string smtpPassword = "BMViwJZMzP93r6QFU7/fECr/vOeJCNF5lNCfUuQweyL+";
                string subject = "Taskayak";
                AlternateView htmlBody = AlternateView.
                            CreateAlternateViewFromString(body, null, "text/html");

                MailMessage message = new MailMessage();

                // Add sender and recipient email addresses to the message
                message.From = new MailAddress(senderAddress, senderName);
                message.To.Add(new MailAddress(toAddress));
                // Add the subject line, text body, and HTML body to the message
                message.Subject = subject;
                message.AlternateViews.Add(htmlBody);

                // Add optional headers for configuration set and message tags to the message
                //message.Headers.Add("X-SES-CONFIGURATION-SET", configurationSet);

                using (var client = new System.Net.Mail.SmtpClient(smtpEndpoint, port))
                {
                    // Create a Credentials object for connecting to the SMTP server
                    client.Credentials =
                        new NetworkCredential(smtpUsername, smtpPassword);

                    client.EnableSsl = true;

                    // Send the message
                    try
                    {

                        client.Send(message);

                    }
                    // Show an error message if the message can't be sent
                    catch (Exception ex)
                    {
                   //     new Comman().LogWrite(Enums.LogType.Error, ex.Message.ToString(), "Utilis", "Sendmail", Enums.Priority.Critical, ex.StackTrace);
                    }
                }

            }
            catch (Exception exception1)
            {
            }
            return str;
        }


    }
}