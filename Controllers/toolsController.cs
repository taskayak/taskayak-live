﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class toolsController : Controller
    {
        public List<int> _permission;
        public toolsController()
        {
            int roleid = System.Web.HttpContext.Current.Request.Cookies["_pid"] == null ? 0 : Convert.ToInt16(System.Web.HttpContext.Current.Request.Cookies["_pid"].Value.ToString());
            roleservices _ser = new roleservices();
            _permission = _ser.get_all_active_permissions_bytypeid(6, roleid);
        }
        public ActionResult Index()
        {
            //if (!_permission.Contains(3))
            //{
            //    return View("unauth");
            //}
            tools _model = new tools();
            var dept = new toolservices();
            _model.tool_type = dept.get_all_active_types();
            _model.tool_items = dept.get_all_active_tools();
            string message = "";
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            ViewBag.permissions = _permission;
            _model = new helper().GenerateError<tools>(_model, message);
            return View(_model);
        }

        public ActionResult delete(int id)
        {
            //if (!_permission.Contains(20))
            //{
            //    return View("unauth");
            //}
            var dept = new toolservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            var error_text = dept.delete_tools(id, userid);
            tools _model = new tools();
            _model.tool_type = dept.get_all_active_types();
            _model.tool_items = dept.get_all_active_tools();
            _model = new helper().GenerateError<tools>(_model, error_text);
            return View("index", _model);
        }

        [HttpPost]
        public ActionResult Index(tools _model)
        {
            var dept = new toolservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            var error_text = dept.add_new_tool(_model.name,_model.typeid ,userid);
            if (!error_text.ToLower().Contains("error"))
            {
                ModelState.Clear();
            }
            tools _model1 = new tools();
            _model1.tool_type = dept.get_all_active_types();
            _model1.tool_items = dept.get_all_active_tools();
            _model1 = new helper().GenerateError<tools>(_model1, error_text);
            return View(_model1);
        }
        public ActionResult update()
        {
            tools _model = new tools();
            var dept = new toolservices();
            _model.tool_type = dept.get_all_active_types();
            _model.tool_items = dept.get_all_active_tools();
            string message = "";
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            ViewBag.permissions = _permission;
            _model = new helper().GenerateError<tools>(_model, message);
            return View("index", _model);
        }

        [HttpPost]
        public ActionResult update(tools _model)
        {
            var dept = new toolservices();
            int userid = Convert.ToInt16(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            var error_text = dept.update_tool(_model.name, _model.typeid, _model.toolid);
            if (!error_text.ToLower().Contains("error"))
            {
                ModelState.Clear();
            }
            tools _model1 = new tools();
            _model1.tool_type = dept.get_all_active_types();
            _model1.tool_items = dept.get_all_active_tools();
            _model1 = new helper().GenerateError<tools>(_model1, error_text);
            return View("index",_model1);
        }
    }

}