﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace hrm.Database
{
    public class AuthorizeVerifiedloggedinAttribute : FilterAttribute, IAuthorizationFilter
    {
        protected void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary()
            {
                { "action", "login" },
                { "controller", "account" },
                { "ReturnUrl", filterContext.HttpContext.Request.Url.ToString() }
            });
        }

        protected void HandleUncompletedProfileRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary()
            {
                { "action", "edit" },
                  { "controller", "account" },
                { "ReturnUrl", filterContext.HttpContext.Request.Url.ToString() }
            });
        }
        protected void HandleMaintenanceRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary()
            {
                { "action", "Maintenance" },
                  { "controller", "main" },
                { "ReturnUrl", filterContext.HttpContext.Request.Url.ToString() }
            });
        }
        void System.Web.Mvc.IAuthorizationFilter.OnAuthorization(AuthorizationContext filterContext)
        {
            bool flag = false;
            bool redirect = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["isredirect"]);
            if (redirect)
            {
                if (filterContext.HttpContext.Request.Cookies["_xid"] == null)
                {
                    this.HandleUnauthorizedRequest(filterContext);
                }
                else
                {
                    HttpCookie cultureCookie = filterContext.RequestContext.HttpContext.Request.Cookies["_xid"];
                  int id= Convert.ToInt32(cultureCookie.Value);
                    if (id != 3)
                    {
                        this.HandleMaintenanceRequest(filterContext);
                    }
                }
            
            }
            else
            {
                if (filterContext.HttpContext.Request.Cookies["_xid"] != null)
                {
                    flag = true;
                }
                if (!flag)
                {
                    this.HandleUnauthorizedRequest(filterContext);
                }
            }

        }
    }
}