﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace hrm.Database
{
    public class Squareitems
    {
        public string idempotency_key { get; set; }
        public bool ask_for_shipping_address { get; set; }
        // public order order { get; set; }
        public string note { get; set; }
        public string redirect_url { get; set; }
        public _order order { get; set; }
    }
    public class _order
    {
        public string idempotency_key { get; set; }
        public service_charges order { get; set; }
    }

    public class service_charges
    {
      
        public string location_id { get; set; }
        public string reference_id { get; set; }
        public string uid { get; set; }
        public string name { get; set; }
        public Money amount_money { get; set; }
        public Money applied_money { get; set; }
        public Money total_money { get; set; }
        public Money total_tax_money { get; set; }
        public string calculation_phase { get; set; }
        public bool taxable { get; set; }
        public string type { get; set; }
    }
    public class Money
    {
        public long amount { get; set; }
        public string currency { get; set; }
    }

    public class result
    {
        public paymentitems payment { get; set; }

    }

    public class paymentitems
    {
        public string status { get; set; }
    }

    public class Square
    {
        string basePath = ConfigurationManager.AppSettings["squareurl"].ToString();
        string AccessToken = ConfigurationManager.AppSettings["SquareToken"].ToString();
        string AppId = ConfigurationManager.AppSettings["SquareAppId"].ToString();
        string locationid = ConfigurationManager.AppSettings["LocationId"].ToString();
        private static string NewIdempotencyKey()
        {
            return Guid.NewGuid().ToString();
        }

        public string ExecutePayment(double amount, string source_id)
        {
            string res = "";
            try
            {
                var idempotencykey= NewIdempotencyKey(); 
                long cents = Convert.ToInt64(amount * 100);
                long zerocents = Convert.ToInt64(0 * 100);
                Money _money = new Money();
                _money.amount = cents;
                _money.currency = "USD";
                Money _taxmoney = new Money();
                _taxmoney.amount = zerocents;
                _taxmoney.currency = "USD";
                service_charges _scharges = new service_charges();
                _scharges.name = "test";
                _scharges.amount_money = _money;
                _scharges.applied_money = _money;
                _scharges.total_tax_money = _taxmoney;
                _scharges.calculation_phase = "TOTAL_PHASE";
                _scharges.taxable = false;
                _scharges.type = "CUSTOM";
                _scharges.uid="dsfsdfsd";
                _scharges.total_money= _money;
                _order _torder = new _order();
                _torder.idempotency_key = idempotencykey;
                _scharges.reference_id = idempotencykey;
                _scharges.location_id = locationid;
                _torder.order = _scharges;
              //  _order.total_money = _money;
                Squareitems items = new Squareitems();
                items.note = "this is testing ";
                items.ask_for_shipping_address = false;
                items.idempotency_key = idempotencykey;
                items.redirect_url = "https://www.google.com?v=1";
                items.order = _torder;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(basePath);
                //httpWebRequest.Accept = "application/json";
                httpWebRequest.Headers.Add("Authorization", "Bearer " + AccessToken);
                httpWebRequest.Headers.Add("Square-Version", "2019-12-17");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(items);
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    res = streamReader.ReadToEnd();
                }
                //res = new JavaScriptSerializer().Deserialize<result>(res).payment.status;
            }
            catch (WebException ex)
            {
                res = ex.Message;
            }
            return res;
        }
    }
}