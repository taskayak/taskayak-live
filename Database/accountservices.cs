﻿using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class accountservices
    {
        public int validateLogin(loginmodal _login, ref string message, ref int roleid,ref bool isresetneeded,ref bool _status,ref bool accepttermsrqud)
        {
            sqlhelper sqlHelper = new sqlhelper();
            SortedList sortedLists = new SortedList();
            Crypto _crypt = new Crypto();
             isresetneeded = false;
            _status = true;
            int id = 0;
            roleid = 0;
            int statusid = 0;
            message = "Error : Password not match";
            try
            {
                string passowrd = "";
                sortedLists.Add("@userName", _login.username);
                var dt = sqlHelper.fillDataTable("tbl_member_getlogin_v2", "", sortedLists);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        passowrd = _crypt.DecryptStringAES(dt.Rows[i]["Password"].ToString());
                        if (passowrd == _login.password)
                        {
                            id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                            statusid = Convert.ToInt32(dt.Rows[i]["member_status_id"].ToString());
                            roleid = Convert.ToInt32(dt.Rows[i]["roleid"].ToString());
                            isresetneeded = Convert.ToBoolean(dt.Rows[i]["resetneeded"].ToString());
                            _status = statusid == 1007 ? Convert.ToBoolean(dt.Rows[i]["isprofilecompleted"].ToString()) : true;
                            accepttermsrqud = Convert.ToBoolean(dt.Rows[i]["accepttermsrqud"].ToString()) ;
                            break;
                        }
                    }
                }
                else
                {
                    message = "Error : Password not match";
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message ="Error :"+ exception.Message;
                id = 0;
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return id;
        }

     

        public bool validate_email(string email)
        {
            sqlhelper sqlHelper = new sqlhelper();
            SortedList sortedLists = new SortedList();
            bool isexist = false;
            try
            {
                sortedLists.Add("@email", email);
                var dt = sqlHelper.fillDataTable("tbl_member_validate_email", "", sortedLists);
                if (dt.Rows.Count > 0)
                {
                    isexist = true;
                }
                dt.Dispose();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return isexist;
        }

        public bool validate_memberid(string memberid)
        {
            sqlhelper sqlHelper = new sqlhelper();
            SortedList sortedLists = new SortedList();
            bool isexist = false;
            try
            {
                sortedLists.Add("@memberid", memberid);
                var dt = sqlHelper.fillDataTable("tbl_member_validate_memberid", "", sortedLists);
                if (dt.Rows.Count > 0)
                {
                    isexist = true;
                }
                dt.Dispose();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return isexist;
        }

        public bool validate_username(string username)
        {
            sqlhelper sqlHelper = new sqlhelper();
            SortedList sortedLists = new SortedList();
            bool isexist = false;
            try
            {
                sortedLists.Add("@username", username);
                var dt = sqlHelper.fillDataTable("tbl_member_validate_username", "", sortedLists);
                if (dt.Rows.Count > 0)
                {
                    isexist = true;
                }
                dt.Dispose();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return isexist;
        }


        public string reset_password(string username, string tempasswors, ref string name, ref string email)
        {
            sqlhelper sqlHelper = new sqlhelper();
            SortedList sortedLists = new SortedList();
            string isexist = "";
            name = "";
            email = "";
            try
            {
                sortedLists.Add("@username", username);
                sortedLists.Add("@temppassword", tempasswors);
                var dt = sqlHelper.fillDataTable("tbl_member_reset_password", "", sortedLists);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    isexist =dt.Rows[i]["message"].ToString();
                    email = dt.Rows[i]["EMail"].ToString();
                    name = dt.Rows[i]["name"].ToString();
                }
                dt.Dispose();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return isexist;
        }

        public string update_password(int memberid, string password)
        {
            sqlhelper sqlHelper = new sqlhelper();
            SortedList sortedLists = new SortedList();
            string isexist = "";
            try
            {
                sortedLists.Add("@memberid", memberid);
                sortedLists.Add("@password", password);
                isexist = sqlHelper.executeNonQueryWMessage("tbl_member_update_password", "", sortedLists).ToString();
                
            }
            catch (Exception ex)
            {
                isexist = "Error:" + ex.Message;
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return isexist;
        }

        public string validatepassword(string c_password, string n_password, int memberid)
        {
            sqlhelper sqlHelper = new sqlhelper();
            SortedList sortedLists = new SortedList();
            Crypto _crypt = new Crypto();
            string message = "Error : Current password is not match";
            try
            {
                string passowrd = "";
                sortedLists.Add("@id", memberid);
                var dt = sqlHelper.fillDataTable("tbl_member_get_passordbyId", "", sortedLists);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        passowrd = _crypt.DecryptStringAES(dt.Rows[i]["Password"].ToString());
                        if (passowrd == c_password)
                        {
                            n_password = _crypt.EncryptStringAES(n_password);
                            sortedLists.Add("@password", n_password);
                            sqlHelper.executeNonQuery("tbl_member_update_passordbyId", "", sortedLists);
                            message = "Password Updated Successfully";
                        }
                    }
                }
                else
                {
                    message = "Error : Current password is not match";
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error :" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
                sortedLists = null;
            }
            return message;
        }

    }
}