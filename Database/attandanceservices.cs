﻿using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class attandanceservices
    {
        public string add_new_pay(attandanceadmin_items client, int userid, string url = "")
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                var startdattime = Convert.ToDateTime(client.startdate + " " + client.starttime);
                var enddattime = Convert.ToDateTime(client.startdate + " " + client.endtime);
                //double hour = .TotalHours;
                TimeSpan span = (enddattime - startdattime);
                double hour = Convert.ToDouble(String.Format("{0}.{1}", span.Hours, span.Minutes > 0 ? (int)Math.Round(Convert.ToDouble(span.Minutes) * 1.66) : span.Minutes));
                client._memberrate = client._memberrate == null ? "$1" : client._memberrate;
                var amount = hour * Convert.ToDouble(client._memberrate.Replace("$", ""));
                amount = amount + (string.IsNullOrEmpty(client.expnse) ? 0 : Convert.ToDouble(client.expnse.Replace("$", "")));
                SortedList _srt = new SortedList();
                _srt.Add("@memberid", client.memberid);
                _srt.Add("@jobid", client.jobid);
                _srt.Add("@date", client.startdate);
                _srt.Add("@starttime", client.starttime);
                _srt.Add("@endtime", client.endtime);
                _srt.Add("@hour", hour);
                _srt.Add("@rate", client._memberrate);
                _srt.Add("@createdby", userid);
                _srt.Add("@Comment", client.comment);
                _srt.Add("@amount", "$" + amount.ToString());
                _srt.Add("@expense", client.expnse);
                if (!string.IsNullOrEmpty(url))
                {
                    _srt.Add("@docurl", url);
                }
                message = sqlHelper.executeNonQueryWMessage("tbl_attandance_add", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string update_pay(attandanceadmin_items client, int userid, string url = "")
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                var startdattime = Convert.ToDateTime(client.startdate + " " + client.starttime);
                var enddattime = Convert.ToDateTime(client.startdate + " " + client.endtime);
                TimeSpan span = (enddattime - startdattime);
                double hour = Convert.ToDouble(String.Format("{0}.{1}", span.Hours, span.Minutes > 0 ? (int)Math.Round(Convert.ToDouble(span.Minutes) * 1.66) : span.Minutes));
                client._memberrate = client._memberrate == null ? "$1" : client._memberrate;
                var amount = hour * Convert.ToDouble(client._memberrate.Replace("$", ""));
                amount = amount + (string.IsNullOrEmpty(client.expnse) ? 0 : Convert.ToDouble(client.expnse.Replace("$", "")));
                SortedList _srt = new SortedList();

                _srt.Add("@memberid", client.memberid);
                _srt.Add("@jobid", client.jobid);
                _srt.Add("@date", client.startdate);
                _srt.Add("@starttime", client.starttime);
                _srt.Add("@endtime", client.endtime);
                _srt.Add("@hour", hour);
                _srt.Add("@createdby", userid);
                _srt.Add("@statusid", client.statusid);
                _srt.Add("@payid", client.payid);
                _srt.Add("@amount", "$" + amount.ToString());
                _srt.Add("@rate", client._memberrate);
                _srt.Add("@expnse", client.expnse);
                message = sqlHelper.executeNonQueryWMessage("tbl_attandance_update", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string update_user_pay(attandanceadmin_items client, int userid, string url = "")
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                var startdattime = Convert.ToDateTime(client.startdate + " " + client.starttime);
                var enddattime = Convert.ToDateTime(client.startdate + " " + client.endtime);
                TimeSpan span = (enddattime - startdattime);
                double hour = Convert.ToDouble(String.Format("{0}.{1}", span.Hours, span.Minutes > 0 ? (int)Math.Round(Convert.ToDouble(span.Minutes) * 1.66) : span.Minutes));
                client._memberrate = client._memberrate == null ? "$1" : client._memberrate;
                var amount = hour * Convert.ToDouble(client._memberrate.Replace("$", ""));
                amount = amount + (string.IsNullOrEmpty(client.expnse) ? 0 : Convert.ToDouble(client.expnse.Replace("$", "")));
                SortedList _srt = new SortedList();
                _srt.Add("@memberid", client.memberid);
                _srt.Add("@jobid", client.jobid);
                _srt.Add("@date", client.startdate);
                _srt.Add("@starttime", client.starttime);
                _srt.Add("@endtime", client.endtime);
                _srt.Add("@hour", hour);
                _srt.Add("@createdby", userid);
                _srt.Add("@payid", client.payid);
                _srt.Add("@amount", "$" + amount.ToString());
                _srt.Add("@rate", client._memberrate);
                _srt.Add("@expense", client.expnse);
                message = sqlHelper.executeNonQueryWMessage("tbl_attandance_update_user", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public attandanceadmin get_all_active_attandance_dashboard(int? memberId, int? stateid, int[] jobstatus, int? paymentstatus, int[] cityId = null, bool isyear = false)
        {
            sqlhelper sqlHelper = new sqlhelper();
            attandanceadmin _mdl = new attandanceadmin();
            List<attandanceadmins> _model = new List<attandanceadmins>();
            SortedList _srt = new SortedList();
            try
            {
                if (memberId.HasValue && memberId.Value != 0)
                {
                    _srt.Add("@id", memberId.Value);
                }
                if (stateid.HasValue && stateid.Value != 0)
                {
                    _srt.Add("@stateid", stateid.Value);
                }
                if (jobstatus != null)
                {
                    var jobs = string.Join<int>(",", jobstatus);
                    _srt.Add("@jobstatus", jobs);
                }
                if (cityId != null)
                {
                    var city = string.Join<int>(",", cityId);
                    _srt.Add("@city", city);
                }
                if (paymentstatus.HasValue && paymentstatus.Value != 0)
                {
                    _srt.Add("@paymentstatus", paymentstatus.Value);
                }
                if (isyear)
                {
                    _srt.Add("@year", DateTime.Now.Year);
                }
                var dt = sqlHelper.fillDataTable("tbl_attandance_getattandance_map", "", _srt);
                string amount = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    attandanceadmins _item = new attandanceadmins();
                    _item.Client = dt.Rows[i]["Clientname"].ToString().ToTitleCase();
                    _item.date = Convert.ToDateTime(dt.Rows[i]["date"]).ToString("yyyy-MM-dd");
                    _item.Hoursworked = dt.Rows[i]["Hoursworked"].ToString();
                    _item.JobID = dt.Rows[i]["JobId"].ToString();
                    _item.Expense = dt.Rows[i]["expense"].ToString().Currency();
                    _item.Jobstatus = dt.Rows[i]["jobstatus"].ToString();
                    _item.MemberID = dt.Rows[i]["member_id"].ToString();
                    _item.Memberrate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.Name = dt.Rows[i]["membername"].ToString().ToTitleCase();
                    amount = dt.Rows[i]["paymentamount"].ToString();
                    _item.Pay = amount == "default" ? (Convert.ToDouble(dt.Rows[i]["Hoursworked"].ToString()) * Convert.ToDouble(dt.Rows[i]["rate"].ToString().Replace("$", ""))).ToString().Currency() : amount.Currency();
                    _item.Paymentstatus = dt.Rows[i]["name"].ToString();
                    _item.attandanceid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.paymentstatusid = Convert.ToInt32(dt.Rows[i]["paymentstatusid"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._attandance = _model;
            return _mdl;
        }
        public attandanceadmin get_all_active_attandance(int? memberId, int? stateid, int[] jobstatus, int? paymentstatus, int[] cityId = null, bool isyear = false)
        {
            sqlhelper sqlHelper = new sqlhelper();
            attandanceadmin _mdl = new attandanceadmin();
            List<attandanceadmins> _model = new List<attandanceadmins>();
            SortedList _srt = new SortedList();
            try
            {
                if (memberId.HasValue && memberId.Value != 0)
                {
                    _srt.Add("@id", memberId.Value);
                }
                if (stateid.HasValue && stateid.Value != 0)
                {
                    _srt.Add("@stateid", stateid.Value);
                }
                if (jobstatus != null)
                {
                    var jobs = string.Join<int>(",", jobstatus);
                    _srt.Add("@jobstatus", jobs);
                }
                if (cityId != null)
                {
                    var city = string.Join<int>(",", cityId);
                    _srt.Add("@city", city);
                }
                if (paymentstatus.HasValue && paymentstatus.Value != 0)
                {
                    _srt.Add("@paymentstatus", paymentstatus.Value);
                }
                if (isyear)
                {
                    _srt.Add("@year", DateTime.Now.Year);
                }
                var dt = sqlHelper.fillDataTable("tbl_attandance_getattandance", "", _srt);
                string amount = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    attandanceadmins _item = new attandanceadmins();
                    _item.Client = dt.Rows[i]["Clientname"].ToString().ToTitleCase();
                    _item.date = Convert.ToDateTime(dt.Rows[i]["date"]).ToString("yyyy-MM-dd");
                    _item.Hoursworked = dt.Rows[i]["Hoursworked"].ToString();
                    _item.JobID = dt.Rows[i]["JobId"].ToString();
                    _item.Expense = dt.Rows[i]["expense"].ToString().Currency();
                    _item.Jobstatus = dt.Rows[i]["jobstatus"].ToString();
                    _item.MemberID = dt.Rows[i]["member_id"].ToString();
                    _item.Memberrate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.Name = dt.Rows[i]["membername"].ToString().ToTitleCase();
                    amount = dt.Rows[i]["paymentamount"].ToString();
                    _item.Pay = amount == "default" ? (Convert.ToDouble(dt.Rows[i]["Hoursworked"].ToString()) * Convert.ToDouble(dt.Rows[i]["rate"].ToString().Replace("$", ""))).ToString().Currency() : amount.Currency();
                    _item.Paymentstatus = dt.Rows[i]["name"].ToString();
                    _item.attandanceid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.paymentstatusid = Convert.ToInt32(dt.Rows[i]["paymentstatusid"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._attandance = _model;
            return _mdl;
        }
        public List<attandanceadmins> get_all_active_attandance_byIndex(int endindex, int startindex, ref int total, int? memberId, int? stateid, int[] jobstatus, int? paymentstatus, int[] cityId = null, string search = "", int userid = 0)
        {
            sqlhelper sqlHelper = new sqlhelper();
            attandanceadmin _mdl = new attandanceadmin();
            List<attandanceadmins> _model = new List<attandanceadmins>();
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            try
            {
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                if (userid != 0)
                {
                    _srt.Add("@id", userid);
                }
                _srt.Add("@startindex", startindex);
                _srt.Add("@endindex", endindex);
                if (memberId.HasValue && memberId.Value != 0)
                {
                    _srt.Add("@id", memberId.Value);
                }
                if (stateid.HasValue && stateid.Value != 0)
                {
                    _srt.Add("@stateid", stateid.Value);
                }
                if (jobstatus != null)
                {
                    var jobs = string.Join<int>(",", jobstatus.Where(a => a != 0));
                    if (!string.IsNullOrEmpty(jobs))
                    {
                        _srt.Add("@jobstatus", jobs);
                    }
                }
                if (cityId != null)
                {
                    var city = string.Join<int>(",", cityId.Where(a => a != 0));
                    if (!string.IsNullOrEmpty(city))
                    {
                        _srt.Add("@city", city);
                    }
                }
                string sp = "tbl_attandance_getattandance_index_withnostatus";
                if (paymentstatus.HasValue && paymentstatus.Value != 0)
                {
                    sp = "tbl_attandance_getattandance_index_withstatus";
                    _srt.Add("@paymentstatus", paymentstatus.Value);
                }
                var dt = sqlHelper.fillDataTable(sp, "", _srt);
                string amount = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    attandanceadmins _item = new attandanceadmins();
                    _item.Client = dt.Rows[i]["Clientname"].ToString().ToTitleCase();
                    _item.date = Convert.ToDateTime(dt.Rows[i]["date"]).ToString("yyyy-MM-dd");
                    _item.Hoursworked = dt.Rows[i]["Hoursworked"].ToString();
                    _item.JobID = dt.Rows[i]["JobId"].ToString();
                    _item.Expense = dt.Rows[i]["expense"].ToString().Currency();
                    _item.Jobstatus = dt.Rows[i]["jobstatus"].ToString();
                    _item.MemberID = dt.Rows[i]["member_id"].ToString();
                    _item.Memberrate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.Name = dt.Rows[i]["membername"].ToString().ToTitleCase();
                    amount = dt.Rows[i]["paymentamount"].ToString();
                    _item.Pay = amount == "default" ? (Convert.ToDouble(dt.Rows[i]["Hoursworked"].ToString()) * Convert.ToDouble(dt.Rows[i]["rate"].ToString().Replace("$", ""))).ToString().Currency() : amount.Currency();
                    _item.Paymentstatus = dt.Rows[i]["name"].ToString();
                    _item.attandanceid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.paymentstatusid = Convert.ToInt32(dt.Rows[i]["paymentstatusid"].ToString());
                    _item.token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //_mdl._attandance = _model;
            return _model;
        }
        public attandanceadmin get_all_active_attandance_byuserid(int id, int[] jobstatus, int? paymentstatus)
        {
            sqlhelper sqlHelper = new sqlhelper();
            attandanceadmin _mdl = new attandanceadmin();
            List<attandanceadmins> _model = new List<attandanceadmins>();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                if (jobstatus != null)
                {
                    var jobs = string.Join<int>(",", jobstatus);
                    _srt.Add("@jobstatus", jobs);
                }
                if (paymentstatus.HasValue && paymentstatus.Value != 0)
                {
                    _srt.Add("@paymentstatus", paymentstatus.Value);
                }
                var dt = sqlHelper.fillDataTable("tbl_attandance_getattandance", "", _srt);
                string amount = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    attandanceadmins _item = new attandanceadmins();
                    _item.Client = dt.Rows[i]["Clientname"].ToString().ToTitleCase();
                    _item.date = Convert.ToDateTime(dt.Rows[i]["date"]).ToString("yyyy-MM-dd");
                    _item.Hoursworked = dt.Rows[i]["Hoursworked"].ToString();
                    _item.JobID = dt.Rows[i]["JobId"].ToString();
                    _item.Expense = dt.Rows[i]["expense"].ToString().Currency();
                    _item.Jobstatus = dt.Rows[i]["jobstatus"].ToString();
                    _item.MemberID = dt.Rows[i]["member_id"].ToString();
                    _item.Memberrate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.Name = dt.Rows[i]["membername"].ToString().ToTitleCase();
                    amount = dt.Rows[i]["paymentamount"].ToString();
                    _item.Pay = amount == "default" ? (Convert.ToDouble(dt.Rows[i]["Hoursworked"].ToString()) * Convert.ToDouble(dt.Rows[i]["rate"].ToString().Replace("$", ""))).ToString().Currency() : amount.Currency();
                    _item.Paymentstatus = dt.Rows[i]["name"].ToString();
                    _item.attandanceid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.paymentstatusid = Convert.ToInt32(dt.Rows[i]["paymentstatusid"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._attandance = _model;
            return _mdl;
        }
        public attandanceadmin_items get_all_active_attandance_byid(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            attandanceadmin_items _item = new attandanceadmin_items();
            List<commentmodal> _commnts = new List<commentmodal>();
            List<filemodal> _files = new List<filemodal>();
            string amount = "";
            Crypto _crypt = new Crypto();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                var ds = sqlHelper.fillDataSet("tbl_attandance_getpaybyid", "", _srt);
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                var dt2 = ds.Tables[2];
                ds.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _item.memberid = Convert.ToInt32(dt.Rows[i]["Memberid"].ToString());
                    _item.dispatcherid = Convert.ToInt32(dt.Rows[i]["dispatcherid"].ToString());
                    _item.dispatchername = dt.Rows[i]["dispatchername"].ToString();
                    _item.jobid = Convert.ToInt32(dt.Rows[i]["Jobid"].ToString());
                    _item.startdate = Convert.ToDateTime(dt.Rows[i]["date"].ToString()).ToString("yyyy-MM-dd");
                    _item.statusid = Convert.ToInt32(dt.Rows[i]["Paymentstatus"].ToString());
                    _item.starttime = dt.Rows[i]["StartTime"].ToString();
                    _item.endtime = dt.Rows[i]["Endtime"].ToString();
                    _item.membername = dt.Rows[i]["membername"].ToString().ToTitleCase();
                    _item.payid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item._token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    _item._jobtoken = _crypt.EncryptStringAES(dt.Rows[i]["Jobid"].ToString());
                    _item._distoken = _crypt.EncryptStringAES(dt.Rows[i]["dispatcherid"].ToString());
                    _item._memberrate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.createddate = Convert.ToDateTime(dt.Rows[i]["CreatedDate"].ToString()).ToString("yyyy-MM-dd");
                    _item.expnse = dt.Rows[i]["expnse"].ToString();
                    _item.jobtitle = dt.Rows[i]["Ttile"].ToString();
                    _item.jobstatus = dt.Rows[i]["Statusname"].ToString();
                    _item.hourworked = dt.Rows[i]["Hoursworked"].ToString();
                    amount = dt.Rows[i]["paymentamount"].ToString();
                    _item.payamount = amount == "default" ? (Convert.ToDouble(dt.Rows[i]["Hoursworked"].ToString()) * Convert.ToDouble(dt.Rows[i]["rate"].ToString().Replace("$", ""))).ToString().Currency() : amount.Currency();
                }
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    commentmodal _mdl = new commentmodal();
                    _mdl.Comment = dt1.Rows[i]["Comment"].ToString();
                    _mdl.createdby = dt1.Rows[i]["createdby"].ToString().ToTitleCase();
                    _mdl.CreatedDate = dt1.Rows[i]["CreatedDate"].ToString();
                    _commnts.Add(_mdl);
                }
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    filemodal _mdl = new filemodal();
                    _mdl.Filetile = dt2.Rows[i]["Doctitle"].ToString().ToTitleCase();
                    _mdl.url = dt2.Rows[i]["docurl"].ToString();
                    _mdl.createdby = dt2.Rows[i]["createdby"].ToString().ToTitleCase();
                    _mdl.Createddate = dt2.Rows[i]["Createddate"].ToString();
                    _mdl.fileid = Convert.ToInt32(dt2.Rows[i]["id"].ToString());
                    _mdl.filetoken = _crypt.EncryptStringAES(dt2.Rows[i]["id"].ToString());
                    _files.Add(_mdl);
                }
                dt.Dispose();
                dt1.Dispose();
                dt2.Dispose();
                _item._Comments = _commnts;
                _item._files = _files;
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _item.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl._attandance = _model;
            return _item;
        }
        public string add_new_doc(string Filetile, int userid, int jobid, string Size, string url)
        {
            string message = "Document Added Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            var cst = DateTime.UtcNow.AddHours(-6);

            try
            {
                SortedList _srt = new SortedList();

                _srt.Add("@createddate", cst);
                _srt.Add("@Filetile", Filetile);
                _srt.Add("@Jobid", jobid);
                _srt.Add("@Size", Size);
                _srt.Add("@url", url);
                _srt.Add("@createdby", userid);
                sqlHelper.executeNonQuery("tbl_attandance_files_add", "", _srt);

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string remove_doc(int id)
        {
            string message = "Document Deleted Successfully";
            sqlhelper sqlHelper = new sqlhelper();
         
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                sqlHelper.executeNonQuery("tbl_attandance_files_remove", "", _srt);

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public void update_status(int payid, int statusid)
        {
            string message = "Document Deleted Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@payid", payid);
                _srt.Add("@statusid", statusid);
                sqlHelper.executeNonQuery("tbl_attandance_updatestaus", "", _srt);

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //return message;
        }
        public string remove_pay(int id)
        {
            string message = "Pay Deleted Successfully";
            sqlhelper sqlHelper = new sqlhelper();

            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                sqlHelper.executeNonQuery("tbl_attandance_remove", "", _srt);

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string update_pay(int id, string amount, int userid)
        {
            string message = "Pay Amount Updated Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            var cst = DateTime.UtcNow.AddHours(-6);
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                _srt.Add("@createddate", cst);
                _srt.Add("@amount", amount);
                _srt.Add("@createdby", userid);
                sqlHelper.executeNonQuery("tbl_attandance_amount_update", "", _srt);

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string add_new_comment(string comment, int userid, int jobid)
        {
            string message = "Comment Added Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            var cst = DateTime.UtcNow.AddHours(-6);
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@createddate", cst);
                _srt.Add("@JobId", jobid);
                _srt.Add("@comment", comment);
                _srt.Add("@createdby", userid);
                sqlHelper.executeNonQuery("tbl_attandance_comment_add", "", _srt);

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

    }
}