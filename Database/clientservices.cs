﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using hrm.Models;

namespace hrm.Database
{
    public class clientservices
    {
        public void addclientsignupnotification(int newclientid)
        {
            sqlhelper sqlHelper = new sqlhelper();
            SortedList _list = new SortedList();
            _list.Add("@newclientid", newclientid);
            try
            {
                sqlHelper.executeNonQuery("AddclientsignupNotification", "", _list);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // return memberid;
        }
        public string get_rate(int memberId)
        {
            string message = "$1";
            sqlhelper sqlHelper = new sqlhelper();

            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", memberId);
                message = sqlHelper.executeNonQueryWMessage("tbl_clients_getrate", "", _srt).ToString().Currency();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string add_new_client(clientmodal_item client, int userid)
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@Clientname", client.name);
                _srt.Add("@primarycontact", client.primarycontactname);
                _srt.Add("@rate", string.IsNullOrEmpty(client.rate) ? "$0" : client.rate);
                _srt.Add("@Address", client.address);
                _srt.Add("@Cityid", client.city_id);
                _srt.Add("@Stateid", client.state_id);
                _srt.Add("@Pincode", client.zipcode);
                _srt.Add("@Email", client.email);
                _srt.Add("@Phone", client.phone);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_clientaddnewclient", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public int add_new_client(clientmodal_item client)
        {
            int message = 0;
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@Clientname", client.name);
                _srt.Add("@primarycontact", client.primarycontactname);
                _srt.Add("@rate", string.IsNullOrEmpty(client.rate) ? "$0" : client.rate);
                _srt.Add("@Address", client.address);
                _srt.Add("@Cityid", client.city_id);
                _srt.Add("@Stateid", client.state_id);
                _srt.Add("@Pincode", client.zipcode);
                _srt.Add("@Email", client.email);
                _srt.Add("@Phone", client.phone);
                _srt.Add("@createdby", 0);
                message = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("tbl_clientaddnewclient_v2", "", _srt).ToString());
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
               // message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }


        public string update_client(clientmodal_item client)
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@Clientname", client.name);
                _srt.Add("@primary", client.primarycontactname);
                _srt.Add("@rate", string.IsNullOrEmpty(client.rate) ? "$0" : client.rate);
                _srt.Add("@Address", client.address);
                _srt.Add("@Cityid", client.city_id);
                _srt.Add("@Stateid", client.state_id);
                _srt.Add("@Pincode", client.zipcode);
                _srt.Add("@Email", client.email);
                _srt.Add("@Phone", client.phone);
                _srt.Add("@clientid", client.clientid);
                message = sqlHelper.executeNonQueryWMessage("tbl_client_edit_newclient", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public clientmodal get_all_active_clients()
        {
            sqlhelper sqlHelper = new sqlhelper();
            clientmodal _mdl = new clientmodal();
            List<clientmodal_item> _model = new List<clientmodal_item>();
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_client_getallclients", "", null);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    clientmodal_item _item = new clientmodal_item();
                    _item.name = dt.Rows[i]["Clientname"].ToString().ToTitleCase();
                    _item.rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.phone = dt.Rows[i]["Phone"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.clientid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl.clients = _model;
            return _mdl;
        }

        public List<clientmodal_item> get_all_active_clients_bypaging(ref int total,int startindex=0,int endindex=25,string search="")
        {
            sqlhelper sqlHelper = new sqlhelper();
            clientmodal _mdl = new clientmodal();
            List<clientmodal_item> _model = new List<clientmodal_item>();
            Crypto _crypt = new Crypto();
            SortedList _srtlist = new SortedList();
            _srtlist.Add("@startindex", startindex);
            _srtlist.Add("@endIndex", endindex);
            if(!string.IsNullOrEmpty(search))
            {
                _srtlist.Add("@search", search);
            }
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_client_getallclients_by_paging", "", _srtlist);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    clientmodal_item _item = new clientmodal_item();
                    _item.name = dt.Rows[i]["Clientname"].ToString().ToTitleCase();
                    _item.primarycontactname = dt.Rows[i]["primarycontact"].ToString().ToTitleCase();
                    _item.rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.phone = dt.Rows[i]["Phone"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.clientid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _item.token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
           // _mdl.clients = _model;
            return _model;
        }

        public clientmodal_item get_all_active_clients_byid(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            clientmodal_item _item = new clientmodal_item();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@clientid", id);
                var dt = sqlHelper.fillDataTable("tbl_client_getallclient_by_id", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                   // clientmodal_item _item = new clientmodal_item();
                    _item.name = dt.Rows[i]["Clientname"].ToString().ToTitleCase();
                    _item.primarycontactname = dt.Rows[i]["primarycontact"].ToString().ToTitleCase();
                    _item.rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.phone = dt.Rows[i]["Phone"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.city = dt.Rows[i]["city"].ToString();
                    _item.state = dt.Rows[i]["state"].ToString();
                    _item.address = dt.Rows[i]["Address"].ToString();
                    _item.city_id = Convert.ToInt32(dt.Rows[i]["Cityid"].ToString());
                    _item.state_id = Convert.ToInt32(dt.Rows[i]["Stateid"].ToString());
                    _item.zipcode = dt.Rows[i]["Pincode"].ToString();
                    _item.clientid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    // _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _item.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //_mdl.clients = _model;
            return _item;
        }

        public string delete_client(int clientid)
        {
            string message = "Client Deleted Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@clientid", clientid);
                sqlHelper.executeNonQuery("tbl_clients_delete_client", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return message;
        }
    }
}