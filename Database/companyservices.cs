﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using hrm.Models;

namespace hrm.Database
{
    public class companyservices
    {
        public string add_new_company(companymodal_item client, int userid)
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@Clientname", client.name);
                _srt.Add("@primarycontact", client.primarycontactname);
                _srt.Add("@rate", string.IsNullOrEmpty(client.rate) ? "$0" : client.rate);
                _srt.Add("@Address", client.address);
                _srt.Add("@website", client.website);
                _srt.Add("@Cityid", client.city_id);
                _srt.Add("@Stateid", client.state_id);
                _srt.Add("@Pincode", client.zipcode);
                _srt.Add("@Email", client.email);
                _srt.Add("@Phone", client.phone);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_company_addcompany", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string add_new_companywithmember(companymodal_item client, int userid)
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@company", client.name);
                _srt.Add("@primaryname", client.primarycontactname);
                _srt.Add("@hourlyrate", string.IsNullOrEmpty(client.rate) ? "$0" : client.rate);
                _srt.Add("@address", client.address);
                _srt.Add("@cityid", client.city_id);
                _srt.Add("@stateid", client.state_id);
                _srt.Add("@website", client.website);
                _srt.Add("@zip", client.zipcode);
                _srt.Add("@email", client.email);
                _srt.Add("@phone", client.phone);
                _srt.Add("@userId", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_company_addnew", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string delete_company(int clientid)
        {
            string message = "Company deleted successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@clientid", clientid);
                sqlHelper.executeNonQuery("tbl_company_delete_company", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return message;
        }
        public string delete_member(int member)
        {
            string message = "User deactivate successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@memberid", member);
                sqlHelper.executeNonQuery("tbl_member_delete_company", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return message;
        }
        public string updatnotification(int memberid)
        {
            string message = "Member deactivate successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@memberid", memberid);
                sqlHelper.executeNonQuery("tbl_genralmessage_update", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return message;
        }
        public companymodal_item get_all_active_company_byid(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            companymodal_item _item = new companymodal_item();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@clientid", id);
                var dt = sqlHelper.fillDataTable("tbl_company_getallcompany_by_id", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    // clientmodal_item _item = new clientmodal_item();
                    _item.website = dt.Rows[i]["website"].ToString();
                    _item.name = dt.Rows[i]["Clientname"].ToString().ToTitleCase();
                    _item.primarycontactname = dt.Rows[i]["primarycontact"].ToString().ToTitleCase();
                    _item.rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.phone = dt.Rows[i]["Phone"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.city = dt.Rows[i]["city"].ToString();
                    _item.state = dt.Rows[i]["state"].ToString();
                    _item.address = dt.Rows[i]["Address"].ToString();
                    _item.city_id = Convert.ToInt32(dt.Rows[i]["Cityid"].ToString());
                    _item.state_id = Convert.ToInt32(dt.Rows[i]["Stateid"].ToString());
                    _item.zipcode = dt.Rows[i]["Pincode"].ToString();
                    _item.clientid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    // _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _item.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //_mdl.clients = _model;
            return _item;
        }
        public string update_company(companymodal_item client)
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@Clientname", client.name);
                _srt.Add("@website", client.website);
                _srt.Add("@primary", client.primarycontactname);
                _srt.Add("@rate", string.IsNullOrEmpty(client.rate) ? "$0" : client.rate);
                _srt.Add("@Address", client.address);
                _srt.Add("@Cityid", client.city_id);
                _srt.Add("@Stateid", client.state_id);
                _srt.Add("@Pincode", client.zipcode);
                _srt.Add("@Email", client.email);
                _srt.Add("@Phone", client.phone);
                _srt.Add("@clientid", client.clientid);
                message = sqlHelper.executeNonQueryWMessage("tbl_company_update_company", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public List<companymodal_item> get_all_active_company_bypaging(ref int total, int startindex = 0, int endindex = 25, string search = "")
        {
            sqlhelper sqlHelper = new sqlhelper();
            clientmodal _mdl = new clientmodal();
            List<companymodal_item> _model = new List<companymodal_item>();
            Crypto _crypt = new Crypto();
            SortedList _srtlist = new SortedList();
            _srtlist.Add("@startindex", startindex);
            _srtlist.Add("@endIndex", endindex);
            if (!string.IsNullOrEmpty(search))
            {
                _srtlist.Add("@search", search);
            }
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_company_getallcompany_by_paging", "", _srtlist);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    companymodal_item _item = new companymodal_item();
                    _item.name = dt.Rows[i]["Clientname"].ToString().ToTitleCase();
                    _item.primarycontactname = dt.Rows[i]["primarycontact"].ToString().ToTitleCase();
                    _item.rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.phone = dt.Rows[i]["Phone"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.clientid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _item.token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl.clients = _model;
            return _model;
        }
        public string getalterntivephone(int clientid, ref string companyname)
        {
            sqlhelper sqlHelper = new sqlhelper();
            string _model = "";
            SortedList _srt = new SortedList();
            _srt.Add("@id", clientid);
            companyname = "";
            Crypto _crypt = new Crypto();
            try
            {
                var dt = sqlHelper.fillDataTable("getalternativebyclientid", "", _srt);
                if (dt.Rows.Count > 0)
                {
                    _model = dt.Rows[0]["Phone"].ToString();
                    companyname = dt.Rows[0]["company"].ToString();
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

            return _model;

        }
    }
}