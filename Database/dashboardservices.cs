﻿using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class dashboardservices
    {
        public dashboardmodal get_all_active_dashboard()
        {
            sqlhelper sqlHelper = new sqlhelper();
            dashboardmodal _mdl = new dashboardmodal();
            List<dashboardjobs> _js = new List<dashboardjobs>();
            List<ratejobs> _pay = new List<ratejobs>();
            try
            {
                var ds = sqlHelper.fillDataSet("get_dashboard", "", null);
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                var dt2 = ds.Tables[2];
                var dt3 = ds.Tables[3];
                double monthpay = 0;
                double rate = 0;
                double hrsworked =0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _mdl.member = Convert.ToInt32(dt.Rows[i]["m"].ToString());
                    _mdl.jobs = Convert.ToInt32(dt.Rows[i]["j"].ToString());
                    _mdl.leads = Convert.ToInt32(dt.Rows[i]["l"].ToString());
                }
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    rate = Convert.ToDouble(dt1.Rows[i]["amount"].ToString().Replace("$",""));
                    hrsworked = Convert.ToDouble(dt1.Rows[i]["Hoursworked"].ToString());
                    monthpay = monthpay + rate;
                }
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    dashboardjobs _itm = new dashboardjobs();
                    _itm.Ttile = dt2.Rows[i]["Ttile"].ToString();
                    _itm.Technician = dt2.Rows[i]["Technician"].ToString().ToTitleCase();
                    _itm.startdate = Convert.ToDateTime(dt2.Rows[i]["startdate"].ToString()).ToString("dd-MM-yyyy");
                    _itm.enddate =Convert.ToDateTime(dt2.Rows[i]["enddate"].ToString()).ToString("dd-MM-yyyy");
                    _itm.Jobid = dt2.Rows[i]["Jobid"].ToString();
                    _js.Add(_itm);
                }

                for (int i = 0; i < dt3.Rows.Count; i++)
                {
                    ratejobs _itm = new ratejobs();
                    _itm.member = dt3.Rows[i]["member"].ToString();
                    _itm.Hoursworked = dt3.Rows[i]["Hoursworked"].ToString();
                    _itm.date = Convert.ToDateTime(dt3.Rows[i]["date"].ToString()).ToString("dd-MM-yyyy");
                    _itm.name = dt3.Rows[i]["name"].ToString();
                    _itm.rate = dt3.Rows[i]["rate"].ToString().Currency();
                    _pay.Add(_itm);
                }
                _mdl.monthuypay = monthpay.ToString().Currency();
                dt.Dispose();
                ds.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._jobs = _js;
            _mdl._pay = _pay;
            return _mdl;
        }
    }
}