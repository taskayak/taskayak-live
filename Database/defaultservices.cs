﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Data;

namespace hrm.Database
{
    public class defaultservices
    {
        public Dictionary<int, string> get_all_satets()
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("tbl_state_getallactivestate_without_select2", "", null);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }


        public int savegetCityId(int stateid,string city)
        {
            sqlhelper sqlHelper = new sqlhelper();
            int cityId = 0;
            SortedList sortedLists = new SortedList();
            sortedLists.Add("@city", city);
            sortedLists.Add("@stateid", stateid);
            try
            {
                cityId = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("GetcityIdbyname","", sortedLists).ToString());
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return cityId;
        }
        public Dictionary<int, string> get_all_skills()
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("tbl_skill_getallactiveskill_without_select2", "", null);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }
        public Dictionary<int, string> get_all_active_member(string type = "M",bool issubcontractor=false,int userid=0)
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@type", type);
                sortedLists.Add("@issubcontractor", issubcontractor);
                sortedLists.Add("@userid", userid);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_member_getallactivemember_without_select2", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString().ToTitleCase());
                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_active_member_byid(int id,string type)
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@Id", id);
                sortedLists.Add("@type", type);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_member_getallactivemember_without_select2", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_active_manager_byid(int id, string type)
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@Id", id);
                sortedLists.Add("@type", type);
                DataTable dataTable = sqlHelper.fillDataTable("Getmanagerbyuser", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["userId"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_active_member_bycityid(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@Id", id);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_member_getallactivemember_bycityid", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString().ToTitleCase());
                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_active_memberid(int cid=0)
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            sortedLists.Add("@id", cid);
            try
            {
               
                DataTable dataTable = sqlHelper.fillDataTable("tbl_member_getallmemberid", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["member_id"].ToString().ToTitleCase());
                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_membertype_projectmanager()
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {

                DataTable dataTable = sqlHelper.fillDataTable("tbl_member_Getprojectmanager", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["name"].ToString().ToTitleCase());
                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }


        public Dictionary<int, string> get_all_active_memberbycompanyid(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@id", id);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_member_getmemberBycompanyId", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["member_id"].ToString().ToTitleCase());
                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_active_member_bystateid(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@Id", id);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_member_getallactivemember_bystateid", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString().ToTitleCase());
                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }
        public Dictionary<int, string> get_all_city_by_stateid(int stateid)
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@id", stateid);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_state_getallactivecity_without_select2", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_job()
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("tbl_job_activejobs_without_select2", "", null);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_assigned_jobs(int id, bool forpay)
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@id", id);
                sortedLists.Add("@forpay", forpay);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_job_activejobs_without_select2", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }
        public Dictionary<int, string> get_all_client()
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("tbl_clients_getallactiveclient_without_select2", "", null);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_manager_byid(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@id", id);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_projectmanager_getallactiveprojectmanager_without_select2", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_manager_byuserid(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@id", id);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_projectmanager_select2", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }



        public Dictionary<int, string> get_all_template(ref int templateid)
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                DataTable dataTable = sqlHelper.fillDataTable("tbl_template_getalltemplateBySelect", "", null);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                    if (Convert.ToBoolean(dataTable.Rows[i]["isdefault"].ToString()) == true)
                    {
                        templateid = Convert.ToInt32(dataTable.Rows[i]["id"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }


        public Dictionary<int, string> get_all_job_OffersByStateId( int stateId)
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@id", stateId);
                DataTable dataTable = sqlHelper.fillDataTable("tbl_job_offerGetByState", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());
                  
                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }
        //
        //

        public Dictionary<int, string> get_all_offer_unassign(int stateid = 0, string city = "", string jobid = "", int clientid = 0)
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                city = city == "null" ? "" : city;
                if (!string.IsNullOrEmpty(city) && city != "0")
                {
                    sortedLists.Add("@cityid", city);
                }
                if (stateid != 0)
                {
                    sortedLists.Add("@stateid", stateid);
                }
                if (clientid != 0)
                {
                    sortedLists.Add("@clientid", clientid);
                }
                if (!string.IsNullOrEmpty(jobid)&& jobid!="0")
                {
                    sortedLists.Add("@offerid", jobid);
                }
                DataTable dataTable = sqlHelper.fillDataTable("tbl_job_offerGetByfilter", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());

                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_job_unassign(int stateid = 0, string city = "", string jobid = "", int clientid = 0)
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                city = city == "null" ? "" : city;
                if (!string.IsNullOrEmpty(city)&& city!="0")
                {
                    sortedLists.Add("@id", city);
                }
                if (stateid!=0)
                {
                    sortedLists.Add("@stateid", stateid);
                }
                if (clientid != 0)
                {
                    sortedLists.Add("@clientid", clientid);
                }
                if (!string.IsNullOrEmpty(jobid))
                {
                    sortedLists.Add("@jobid", jobid);
                }
                DataTable dataTable = sqlHelper.fillDataTable("tbl_job_Getunassign", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());

                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }

        public Dictionary<int, string> get_all_job_OffersBycity(string city)
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> select2Models = new Dictionary<int, string>();
            string empty = string.Empty;
            SortedList sortedLists = new SortedList();
            try
            {
                if (!string.IsNullOrEmpty(city))
                {
                    sortedLists.Add("@id", city);
                }
                DataTable dataTable = sqlHelper.fillDataTable("tbl_job_offerGetBycity", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    select2Models.Add(Convert.ToInt32(dataTable.Rows[i]["id"].ToString()), dataTable.Rows[i]["Name"].ToString());

                }
            }
            catch (Exception ex)
            {
                //Exception exception = exception1;
                //throw new ArgumentException(string.Concat("Error  : ", exception.StackTrace, " ", exception.Message), "index", exception);
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return select2Models;
        }


    }
}