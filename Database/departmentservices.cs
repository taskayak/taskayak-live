﻿using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class departmentservices
    {
        public department get_all_active_departments()
        {
            sqlhelper sqlHelper = new sqlhelper();
            department _mdl = new department();
            List<department_items> _model = new List<department_items>();
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_department_getalldepts", "", null);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    department_items _item = new department_items();
                    _item.name = dt.Rows[i]["name"].ToString();
                    _item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._dept = _model;
            return _mdl;
        }

        public string add_new_dept(string deptname, int userid)
        {
            string message = "Department Added Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@deptName", deptname);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_department_adddept", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string update_dept(string deptname, int deptid, int userid)
        {
            string message = "Department Updated Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@deptName", deptname);
                _srt.Add("@createdby", userid);
                _srt.Add("@deptid", deptid);
                message = sqlHelper.executeNonQueryWMessage("tbl_department_updatedept", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string delete_dept(int deptid, int userid)
        {
            string message = "Department Added Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@deptid", deptid);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_department_removedept", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
    }
}