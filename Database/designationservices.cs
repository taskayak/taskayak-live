﻿using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class designationservices
    {
        public designation get_all_active_designations()
        {
            sqlhelper sqlHelper = new sqlhelper();
            designation _mdl = new designation();
            List<designation_items> _model = new List<designation_items>();
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_designation_getalldesignation", "", null);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    designation_items _item = new designation_items();
                    _item.name = dt.Rows[i]["name"].ToString();
                    _item._dep_name = dt.Rows[i]["depname"].ToString();
                    _item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.deptid = Convert.ToInt32(dt.Rows[i]["deptid"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._desgnation = _model;
            return _mdl;
        }

        public string add_new_designations(string designation, int userid, int deptid)
        {
            string message = "Designation Added Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@designation", designation);
                _srt.Add("@deptid", deptid);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_designation_adddesignation", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string update_designations(string designation, int designationid, int deptid, int userid)
        {
            string message = "Designation Updated Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@designation", designation);
                _srt.Add("@createdby", userid);
                _srt.Add("@designationid", designationid);
                _srt.Add("@deptid", deptid);
                message = sqlHelper.executeNonQueryWMessage("tbl_designation_updatedesignation", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string delete_designations(int designationid, int userid)
        {
            string message = "Designation Added Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@designationid", designationid);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_designation_removedesignation", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public List<designation_items> get_designations_by_deptid(int depid)
        {
            sqlhelper sqlHelper = new sqlhelper();
            List<designation_items> _model = new List<designation_items>();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@depid", depid);
                var dt = sqlHelper.fillDataTable("tbl_designation_getdesignation_by_deptid", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    designation_items _item = new designation_items();
                    _item.name = dt.Rows[i]["name"].ToString();
                    _item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

            return _model;
        }
    }
}