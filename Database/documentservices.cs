﻿using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class documentservices
    {
        public List<doctype_item> get_all_active_types()
        {
            sqlhelper sqlHelper = new sqlhelper();
            List<doctype_item> _mdl = new List<doctype_item>();
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_doctype_getalltype", "", null);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    doctype_item _item = new doctype_item();
                    _item.name = dt.Rows[i]["name"].ToString();
                    _item.typeid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.mainorder = Convert.ToInt32(dt.Rows[i]["mainorder"].ToString()); //
                    _mdl.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //  _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl._dept = _model;
            return _mdl;
        }

        public int getmaxorder()
        {
            int order = 1;
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                order =Convert.ToInt32( sqlHelper.executeNonQueryWMessage("tbl_doctypetype_getmaxdept", "", _srt).ToString());
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return order;
        }

        public string add_new_type(string name, int userid,int order)
        {
            string message = "Document Type Added Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@deptName", name);
                _srt.Add("@createdby", userid);
                _srt.Add("@mainorder", order);
                message = sqlHelper.executeNonQueryWMessage("tbl_doctypetype_addtype_v2", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string update_type(string deptname, int deptid, int userid,int mainorder)
        {
            string message = "Document Type Updated Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@deptName", deptname);
                _srt.Add("@createdby", userid);
                _srt.Add("@deptid", deptid);
                _srt.Add("@mainorder", mainorder);
                message = sqlHelper.executeNonQueryWMessage("tbl_doctype_updatetype_v2", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string delete_type(int deptid, int userid)
        {
            string message = "Document type Deleted Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@deptid", deptid);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_doctype_removetype", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
    }
}