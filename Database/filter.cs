﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class filter
    {
        public bool isActiveFilter { get; set; }
    }

    public class payfilter: filter
    {
        public int? paymentstatus { get; set; }
        public string cityId { get; set; }
        public string jobstatus { get; set; }
        public int? memberId { get; set; }
        public int? stateid { get; set; }
    }

    public class leadfilter : filter
    {
        public int? statusId { get; set; }
        public string city { get; set; }
        public string jobstatus { get; set; }
        public string skiils { get; set; }
        public int? memberId { get; set; }
        public int? stateid { get; set; }
    }


    public class memberfilter : filter
    {
        public string membertype { get; set; }
        public string amount { get; set; }
        public int? state_id { get; set; }
        public string skills { get; set; }
        public string city_id { get; set; }
        public string drug { get; set; }
        public string background { get; set; }
        public int? status { get; set; }
    }


    public class jobfilter : filter
    {
        public int? state_filter_id { get; set; }
        public string city { get; set; }
        public string date { get; set; }
        public int? cclient { get; set; }
        public int? cTechnician { get; set; }
        public int? cDispatcher { get; set; }
        public int? Status { get; set; }
    }

}