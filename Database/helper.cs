﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Web;

namespace hrm.Database
{
    public class helper
    {
        public T GenerateError<T>(T model, string message)
        {
            message = message.ToLower().Contains("succesfully") ? message.ToLower().Replace("succesfully", "successfully") : message;
            if (!string.IsNullOrEmpty(message))
            {
                if (message.ToLower().Contains("error"))
                {
                    typeof(T).GetProperty("alertclass").SetValue(model, "danger");
                }
                else
                {
                    typeof(T).GetProperty("alertclass").SetValue(model, "success");
                }
                typeof(T).GetProperty("error_text").SetValue(model, message);
            }
            return model;
        }
        public DataTable ConvertToDatatable<T>(List<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable dataTable = new DataTable();
            for (int i = 0; i < properties.Count; i++)
            {
                PropertyDescriptor item = properties[i];
                if ((!item.PropertyType.IsGenericType ? true : item.PropertyType.GetGenericTypeDefinition() != typeof(Nullable<>)))
                {
                    dataTable.Columns.Add(item.Name, item.PropertyType);
                }
                else
                {
                    dataTable.Columns.Add(item.Name, item.PropertyType.GetGenericArguments()[0]);
                }
            }

            object[] value = new object[properties.Count];
            foreach (T datum in data)
            {
                for (int j = 0; j < (int)value.Length; j++)
                {
                    value[j] = properties[j].GetValue(datum);
                }
                dataTable.Rows.Add(value);
            }
            return dataTable;
        }
        public DataTable ConvertToDataTable(string filePath, int numberOfColumns, bool isfirstrowheader = true)
        {
            DataTable dataTable = new DataTable();
            for (int i = 0; i < numberOfColumns; i++)
            {
                int num = i + 1;
                dataTable.Columns.Add(new DataColumn(string.Concat("Column", num.ToString())));
            }
            string[] strArrays = File.ReadAllLines(filePath);
            for (int j = isfirstrowheader ? 1 : 0; j < (int)strArrays.Length; j++)
            {
                string str = strArrays[j];
                string[] strArrays1 = str.Split(new char[] { ',' });
                if (!string.IsNullOrEmpty(strArrays1[0]))
                {
                    DataRow dataRow = dataTable.NewRow();
                    for (int k = 0; k < numberOfColumns; k++)
                    {
                        dataRow[k] = strArrays1[k];
                    }
                    dataTable.Rows.Add(dataRow);
                }
            }
            return dataTable;
        }
        public DataTable ConvertToDataTable(string filePath, int numberOfColumns, List<string> columnName, Dictionary<int, string> _defaultvalues, bool isfirstrowheader = true)
        {
            DataTable dataTable = new DataTable();
            foreach (var item in columnName)
            {
                dataTable.Columns.Add(new DataColumn(item));
            }
            string[] strArrays = File.ReadAllLines(filePath);
            for (int j = isfirstrowheader ? 1 : 0; j < (int)strArrays.Length; j++)
            {
                string str = strArrays[j];
                string[] strArrays1 = str.Split(new char[] { ',' });
                DataRow dataRow = dataTable.NewRow();
                for (int k = 0; k < numberOfColumns; k++)
                {
                    if (k < strArrays1.Length)
                    {
                        dataRow[k] = strArrays1[k];
                    }
                    else
                    {
                        dataRow[k] = _defaultvalues[k];
                    }
                }
                dataTable.Rows.Add(dataRow);
            }
            return dataTable;
        }
        public string GetRandomalphanumeric(int? charcount)
        {
            string str;
            int? nullable;
            string str1 = "umesh";
            try
            {
                int num = 2;
                if (charcount.HasValue)
                {
                    int? nullable1 = charcount;
                    if (nullable1.HasValue)
                    {
                        nullable = new int?(nullable1.GetValueOrDefault() / 2);
                    }
                    else
                    {
                        nullable = null;
                    }
                    num = Convert.ToInt32(nullable);
                }
                string str2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
                char[] chrArray = new char[num];
                Random random = new Random();
                for (int i = 0; i < (int)chrArray.Length; i++)
                {
                    chrArray[i] = str2[random.Next(str2.Length)];
                }
                string str3 = "0123456789";
                char[] chrArray1 = new char[num];
                Random random1 = new Random();
                for (int j = 0; j < (int)chrArray.Length; j++)
                {
                    chrArray1[j] = str3[random1.Next(str3.Length)];
                }
                str1 = string.Concat(new string(chrArray), new string(chrArray1));
                str = str1;
            }
            catch (Exception exception1)
            {
                str = str1;
            }
            return str;
        }
        public string sendemail(string to, string body, string subject, string toName, string filepath = "", string fromemail = "noreply@crewtok.com", string from = "Noreply")
        {
            string str = "sent";
            try
            {
                MailMessage mailMessage = new MailMessage();
                if (string.IsNullOrEmpty(toName))
                {
                    mailMessage.To.Add(new MailAddress(to));
                }
                else
                {
                    mailMessage.To.Add(new MailAddress(to, toName));
                }
                if (!string.IsNullOrEmpty(filepath))
                {
                    mailMessage.Attachments.Add(new Attachment(filepath));
                }
                mailMessage.From = new MailAddress(fromemail, from);
                mailMessage.Subject = subject;
                mailMessage.IsBodyHtml = true;
                mailMessage.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(body, null, "text/html"));
                (new SmtpClient("in-v3.mailjet.com", Convert.ToInt32(587))
                {
                    Credentials = new NetworkCredential("82a3a6c4572a1ac231f8b0d94e289c1b", "fd7d33915f71db9b3e3d354e685cc115")
                }).Send(mailMessage);
            }
            catch (Exception exception1)
            {
            }
            return str;
        }
        public string get5RandomDidit()
        {
            Random r = new Random();
            int randNum = r.Next(11111, 99999);
            return randNum.ToString("D5");
        }


        public string SendemailAWS(string _to, string _text, string subject,string _toname,string senderName = "Convo Technologies")
        {
            string str = "email sent";
            try
            {
                string smtpEndpoint = "email-smtp.us-east-1.amazonaws.com";
                int port = 587;
                string senderAddress = "contact@convotechnology.com";
                string smtpUsername = "AKIAX27HMUUW47OFXK6R";
                string smtpPassword = "BMViwJZMzP93r6QFU7/fECr/vOeJCNF5lNCfUuQweyL+";
                AlternateView htmlBody = AlternateView.
                            CreateAlternateViewFromString(_text, null, "text/html");
                MailMessage message = new MailMessage();
                message.From = new MailAddress(senderAddress, senderName);
                if(!string.IsNullOrEmpty(_toname))
                {
                    message.To.Add(new MailAddress(_to, _toname));
                }else
                {
                    message.To.Add(new MailAddress(_to));
                }
                message.Subject = subject;
                message.AlternateViews.Add(htmlBody);
                using (var client = new System.Net.Mail.SmtpClient(smtpEndpoint, port))
                {
                    // Create a Credentials object for connecting to the SMTP server
                    client.Credentials =
                        new NetworkCredential(smtpUsername, smtpPassword);
                    client.EnableSsl = true;
                    try
                    {

                        client.Send(message);

                    }
                    // Show an error message if the message can't be sent
                    catch (Exception ex)
                    {
                        //     new Comman().LogWrite(Enums.LogType.Error, ex.Message.ToString(), "Utilis", "Sendmail", Enums.Priority.Critical, ex.StackTrace);
                    }
                }
            }
            catch (SmtpException smtpException)
            {
                str = string.Concat("Error : ", smtpException.Message);
            }
            catch (WebException webException1)
            {
                WebException webException = webException1;
                using (WebResponse response = webException.Response)
                {
                    HttpWebResponse httpWebResponse = (HttpWebResponse)response;
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(responseStream))
                        {
                            str = string.Concat("Error : ", streamReader.ReadToEnd());
                        }
                    }
                }
            }
            return str;
        }


    }
}