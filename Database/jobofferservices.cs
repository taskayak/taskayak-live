﻿using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class jobofferservices
    {
        public string add_new_job(job _mdl, int userid, string tecrate, string clientrate, string add, string zip)
        {
            string message = "Offer Added Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            string _sdate = _mdl.sdate + " " + _mdl.stime;
            string _edate = _mdl.sdate + " " + _mdl.etime;
            TimeSpan span = (Convert.ToDateTime(_edate) - Convert.ToDateTime(_sdate));
            var estimhour = String.Format("{0}.{1}", span.Hours, span.Minutes > 0 ? (int)Math.Round(Convert.ToDouble(span.Minutes) * 1.66) : span.Minutes);
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@tecrate", tecrate);
                _srt.Add("@clientrate", clientrate);
                _srt.Add("@address", add);
                _srt.Add("@zip", zip);
                _srt.Add("@JobId", _mdl.JobId);
                _srt.Add("@Ttile", _mdl.title);
                _srt.Add("@Client", _mdl.client_id);
                _srt.Add("@Technician", _mdl.Technician);
                _srt.Add("@Dispatcher", _mdl.Dispatcher);
                _srt.Add("@Project_manager", _mdl.mgr_id);//client manager
                _srt.Add("@Project_manager1", _mdl.mgr_id1);
                _srt.Add("@startdate", _sdate);
                _srt.Add("@expense", _mdl.Expense);
                _srt.Add("@enddate", _edate);
                _srt.Add("@est_Hours", estimhour);
                _srt.Add("@Status_id", _mdl.Status);
                _srt.Add("@City_id", _mdl.city_id);
                _srt.Add("@State_id", _mdl.state_id);
                _srt.Add("@Description", _mdl.description);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_job_offer_addnewjob", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }


        public string sendoffers(List<int> data, int tid, int pref, int jobofferId, bool sms, bool email, string smstext, string emailtext, string subject)
        {
            Crypto _crypt = new Crypto();
            string message = "Offer Added Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                var emailtemp = emailtext;
                var smstemp = smstext;
                var job = get_mailmerge_job_details_byid(jobofferId);
                string acceptlink = "taskayak.com/offer/acceptoffer?token=";
                string declinelink = "taskayak.com/offer/declineoffer?token=";
                SortedList _srt = new SortedList();
                string encryptid = "";
                int offersId = 0;
                foreach (var item in data)
                {
                    acceptlink = "taskayak.com/offer/acceptoffer?token=";
                    declinelink = "taskayak.com/offer/declineoffer?token=";
                    emailtemp = emailtext;
                    smstemp = smstext;
                    var mdl = get_all_active_members_byid(item);
                    smstemp = sms ? smstemp.MailMergeBytext(job.datetime, job.sdate, job.stime, job.etime, job.JobId, job.title, job.Pay_rate, job.description, mdl.FirstName, mdl.lastname, mdl.FirstName + " " + mdl.lastname, mdl._mstate, mdl._mcity, job.city, job.state, job.address, acceptlink, mdl.hourlyrate, mdl.password, mdl.uname, mdl.email, mdl.member_id, declinelink) : "";
                    emailtemp = email ? emailtemp.MailMergeBytext(job.datetime, job.sdate, job.stime, job.etime, job.JobId, job.title, job.Pay_rate, job.description, mdl.FirstName, mdl.lastname, mdl.FirstName + " " + mdl.lastname, mdl._mstate, mdl._mcity, job.city, job.state, job.address, acceptlink, mdl.hourlyrate, mdl.password, mdl.uname, mdl.email, mdl.member_id, declinelink) : "";
                    _srt.Clear();
                    _srt.Add("@memberid", item);
                    _srt.Add("@pref", pref == 1 ? "Auto" : "Manual");
                    _srt.Add("@offerId", jobofferId);
                    _srt.Add("@sms", smstemp);
                    _srt.Add("@email", emailtemp);
                    _srt.Add("@subject", subject);
                    offersId = Convert.ToInt16(sqlHelper.executeNonQueryWMessage("tbl_job_offer_add_v2", "", _srt).ToString());
                    encryptid = _crypt.EncryptStringAES(offersId.ToString());
                    acceptlink = acceptlink + encryptid;
                    declinelink = declinelink + encryptid;
                    smstemp = sms ? smstext.MailMergeBytext(job.datetime, job.sdate, job.stime, job.etime, job.JobId, job.title, job.Pay_rate, job.description, mdl.FirstName, mdl.lastname, mdl.FirstName + " " + mdl.lastname, mdl._mstate, mdl._mcity, job.city, job.state, job.address, acceptlink, mdl.hourlyrate, mdl.password, mdl.uname, mdl.email, mdl.member_id, declinelink) : "";
                    emailtemp = email ? emailtext.MailMergeBytext(job.datetime, job.sdate, job.stime, job.etime, job.JobId, job.title, job.Pay_rate, job.description, mdl.FirstName, mdl.lastname, mdl.FirstName + " " + mdl.lastname, mdl._mstate, mdl._mcity, job.city, job.state, job.address, acceptlink, mdl.hourlyrate, mdl.password, mdl.uname, mdl.email, mdl.member_id, declinelink) : "";
                    _srt.Clear();
                    _srt.Add("@offerId", offersId);
                    _srt.Add("@sms", smstemp);
                    _srt.Add("@email", emailtemp);
                    sqlHelper.executeNonQuery("tbl_job_offer_updatelink", "", _srt);

                }
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string sendoffers2(List<int> data, int tid, bool sms, bool email, string smstext, string emailtext, string subject,bool termspopup)
        {
            string message = "Offer Added Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            var emailtemp = emailtext;
            var smstemp = smstext;
            try
            {
                //var job = get_mailmerge_job_details_byid(offerId);
                //string link = "https://taskayak.com/offer/view?id=" + offerId;
                SortedList _srt = new SortedList();
                foreach (var item in data)
                {
                    emailtemp = emailtext;
                    smstemp = smstext;
                    var mdl = get_all_active_members_byid(item);
                    smstemp = sms ? smstemp.MailMergeBytext("", "", "", "", "", "", "", "", mdl.FirstName, mdl.lastname, mdl.FirstName + " " + mdl.lastname, mdl._mstate, mdl._mcity, "", "", "", "", mdl.hourlyrate, mdl.password, mdl.uname, mdl.email, mdl.member_id) : "";
                    emailtemp = email ? emailtemp.MailMergeBytext("", "", "", "", "", "", "", "", mdl.FirstName, mdl.lastname, mdl.FirstName + " " + mdl.lastname, mdl._mstate, mdl._mcity, "", "", "", "", mdl.hourlyrate, mdl.password, mdl.uname, mdl.email, mdl.member_id) : "";
                    _srt.Clear();
                    _srt.Add("@memberid", item);
                    _srt.Add("@pref", "Auto");
                    _srt.Add("@offerId", 0);
                    _srt.Add("@sms", smstemp);
                    _srt.Add("@email", emailtemp);
                    _srt.Add("@subject", subject);
                    _srt.Add("@termspopup", termspopup);
                    sqlHelper.executeNonQuery("tbl_job_offer_add", "", _srt);
                }
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string sendoffers4(List<int> data, int tid, bool sms, bool email, string smstext, string emailtext, string subject, int jobid)
        {
            string message = "Offer Added Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            var emailtemp = emailtext;
            var smstemp = smstext;
            try
            {
                //var job = get_mailmerge_job_details_byid(offerId);
                //string link = "https://taskayak.com/offer/view?id=" + offerId;
                SortedList _srt = new SortedList();
                foreach (var item in data)
                {
                    emailtemp = emailtext;
                    smstemp = smstext;
                    var mdl = get_all_active_members_byid(item);
                    smstemp = sms ? smstemp.MailMergeBytext("", "", "", "", "", "", "", "", mdl.FirstName, mdl.lastname, mdl.FirstName + " " + mdl.lastname, mdl._mstate, mdl._mcity, "", "", "", "", mdl.hourlyrate, mdl.password, mdl.uname, mdl.email, mdl.member_id) : "";
                    emailtemp = email ? emailtemp.MailMergeBytext("", "", "", "", "", "", "", "", mdl.FirstName, mdl.lastname, mdl.FirstName + " " + mdl.lastname, mdl._mstate, mdl._mcity, "", "", "", "", mdl.hourlyrate, mdl.password, mdl.uname, mdl.email, mdl.member_id) : "";
                    _srt.Clear();
                    _srt.Add("@memberid", item);
                    _srt.Add("@pref", "Auto");
                    _srt.Add("@offerId", 0);
                    _srt.Add("@jobid", jobid);
                    _srt.Add("@sms", smstemp);
                    _srt.Add("@email", emailtemp);
                    _srt.Add("@subject", subject);
                    _srt.Add("@termspopup", false);
                    sqlHelper.executeNonQuery("tbl_job_offer_add", "", _srt);
                }
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }


        public void addmembersignupnotification(int newmemberid)
        {
            sqlhelper sqlHelper = new sqlhelper();
            SortedList _list = new SortedList();
            _list.Add("@memberid", newmemberid);
            try
            {
                sqlHelper.executeNonQuery("AddtechsignupNotification", "", _list);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // return memberid;
        }

        public string sendoffers4(List<string> data, int tid, bool sms, bool email, string smstext, string emailtext, string subject, bool termspopup)
        {
            string message = "Job Offer Added Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            var emailtemp = emailtext;
            var smstemp = smstext;
            try
            {
                //var job = get_mailmerge_job_details_byid(offerId);
                //string link = "https://taskayak.com/offer/view?id=" + offerId;
                SortedList _srt = new SortedList();
                foreach (var item in data)
                {
                    emailtemp = emailtext;
                    smstemp = smstext;
                    var mdl = get_all_active_members_bymemberid(item);
                    smstemp = sms ? smstemp.MailMergeBytext("", "", "", "", "", "", "", "", mdl.FirstName, mdl.lastname, mdl.FirstName + " " + mdl.lastname, mdl._mstate, mdl._mcity, "", "", "", "", mdl.hourlyrate, mdl.password, mdl.uname, mdl.email, mdl.member_id) : "";
                    emailtemp = email ? emailtemp.MailMergeBytext("", "", "", "", "", "", "", "", mdl.FirstName, mdl.lastname, mdl.FirstName + " " + mdl.lastname, mdl._mstate, mdl._mcity, "", "", "", "", mdl.hourlyrate, mdl.password, mdl.uname, mdl.email, mdl.member_id) : "";
                    _srt.Clear();
                    _srt.Add("@memberid", mdl.userid);
                    _srt.Add("@pref", "Auto");
                    _srt.Add("@offerId", 0);
                    _srt.Add("@sms", smstemp);
                    _srt.Add("@email", emailtemp);
                    _srt.Add("@subject", subject);
                    _srt.Add("@termspopup", termspopup);
                    sqlHelper.executeNonQuery("tbl_job_offer_add", "", _srt);
                    addmembersignupnotification(mdl.userid);
                }
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string sendoffers3(List<int> data, int tid, bool sms, bool email, string smstext, string emailtext, string subject, ref string bdy)
        {
            string message = "Job Offer Added Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                //var job = get_mailmerge_job_details_byid(offerId);
                //string link = "https://taskayak.com/offer/view?id=" + offerId;
                SortedList _srt = new SortedList();
                foreach (var item in data)
                {
                    var mdl = get_all_active_members_byid(item);
                    smstext = sms ? smstext.MailMergeBytext("", "", "", "", "", "", "", "", mdl.FirstName, mdl.lastname, mdl.FirstName + " " + mdl.lastname, mdl._mstate, mdl._mcity, "", "", "", "", mdl.hourlyrate, mdl.password, mdl.uname, mdl.email, mdl.member_id) : "";
                    emailtext = emailtext.MailMergeBytext("", "", "", "", "", "", "", "", mdl.FirstName, mdl.lastname, mdl.FirstName + " " + mdl.lastname, mdl._mstate, mdl._mcity, "", "", "", "", mdl.hourlyrate, mdl.password, mdl.uname, mdl.email, mdl.member_id);
                    bdy = emailtext;
                    _srt.Clear();
                    _srt.Add("@memberid", item);
                    _srt.Add("@pref", "Auto");
                    _srt.Add("@offerId", 0);
                    _srt.Add("@sms", smstext);
                    _srt.Add("@email", "");
                    _srt.Add("@subject", subject);
                    sqlHelper.executeNonQuery("tbl_job_offer_add", "", _srt);
                }
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public membermodal get_all_active_members_byid(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            membermodal _item = new membermodal();
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            _srt.Add("@id", id);
            try
            {
                var ds = sqlHelper.fillDataSet("tbl_job_offer_tbl_member_getmemberbyid", "", _srt);
                var dt = ds.Tables[0];
                ds.Dispose();
                if (dt.Rows.Count > 0)
                {
                    _item.zip = dt.Rows[0]["Zip_code"].ToString();
                    _item.address = dt.Rows[0]["address"].ToString();
                    _item.FirstName = dt.Rows[0]["f_name"].ToString().ToTitleCase();
                    _item.lastname = dt.Rows[0]["l_name"].ToString().ToTitleCase();
                    _item._mcity = dt.Rows[0]["city"].ToString();
                    _item._mstate = dt.Rows[0]["state"].ToString();
                    _item.member_id = dt.Rows[0]["member_id"].ToString();
                    _item.password = _crypt.DecryptStringAES(dt.Rows[0]["Password"].ToString());
                    _item.uname = dt.Rows[0]["Username"].ToString();
                    _item.email = dt.Rows[0]["EMail"].ToString();
                    _item.hourlyrate = dt.Rows[0]["rate"].ToString().Currency();
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _item.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl._members = _model;
            return _item;
        }

        public membermodal get_all_active_members_bymemberid(string id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            membermodal _item = new membermodal();
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            _srt.Add("@id", id);
            try
            {
                var ds = sqlHelper.fillDataSet("tbl_job_offer_tbl_member_getmemberbymemberid", "", _srt);
                var dt = ds.Tables[0];
                ds.Dispose();
                if (dt.Rows.Count > 0)
                {
                    _item.zip = dt.Rows[0]["Zip_code"].ToString();
                    _item.address = dt.Rows[0]["address"].ToString();
                    _item.FirstName = dt.Rows[0]["f_name"].ToString().ToTitleCase();
                    _item.lastname = dt.Rows[0]["l_name"].ToString().ToTitleCase();
                    _item._mcity = dt.Rows[0]["city"].ToString();
                    _item._mstate = dt.Rows[0]["state"].ToString();
                    _item.member_id = dt.Rows[0]["member_id"].ToString();
                    _item.userid =Convert.ToInt32(dt.Rows[0]["id"].ToString());
                    _item.password = _crypt.DecryptStringAES(dt.Rows[0]["Password"].ToString());
                    _item.uname = dt.Rows[0]["Username"].ToString();
                    _item.email = dt.Rows[0]["EMail"].ToString();
                    _item.hourlyrate = dt.Rows[0]["rate"].ToString().Currency();
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _item.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl._members = _model;
            return _item;
        }
        public bool validateJobId(string JobId)
        {
            bool isavailable = true;
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", JobId);
                isavailable = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("validate_jobid", "", _srt).ToString()) > 0 ? false : true;
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return isavailable;
        }
        public bool validateJobId(int Id, string jobid)
        {
            bool isavailable = true;
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", Id);
                _srt.Add("@jobid", jobid);
                isavailable = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("validate_jobid_editjob", "", _srt).ToString()) > 0 ? false : true;
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return isavailable;
        }
        public void update_status(int statusId, int jobid, int userId)
        {
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@statusId", statusId);
                _srt.Add("@jobid", jobid);
                sqlHelper.executeNonQuery("tbl_job_offer_update_status", "", _srt);
                //if (statusId == 5)
                //{
                //    job _mdl = get_all_active_job_details_byid(jobid);
                //    attandanceadmin_items client = new attandanceadmin_items();
                //    client.startdate = _mdl.sdate;
                //    client.starttime = _mdl.stime;
                //    client.endtime = _mdl.etime;
                //    client._memberrate = _mdl.Pay_rate;
                //    client.expnse = _mdl.Expense;
                //    client.memberid = _mdl.Technician;
                //    client.jobid = _mdl._jobid;
                //    client.comment = "Pay Created By Approved Job";
                //    new attandanceservices().add_new_pay(client, userId);
                //}
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //return isavailable;
        }
        public string update_job(job _mdl, int userid)
        {
            string message = "Job Offer Updated Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            string _sdate = _mdl.sdate + " " + _mdl.stime;
            string _edate = _mdl.sdate + " " + _mdl.etime;
            TimeSpan span = (Convert.ToDateTime(_edate) - Convert.ToDateTime(_sdate));
            var estimhour = Convert.ToDouble(String.Format("{0}.{1}", span.Hours, span.Minutes > 0 ? (int)Math.Round(Convert.ToDouble(span.Minutes) * 1.66) : span.Minutes));
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@tecrate", _mdl.Pay_rate);
                _srt.Add("@clientrate", _mdl.Client_rate);
                _srt.Add("@address", _mdl.street);
                _srt.Add("@zip", _mdl.zip);
                _srt.Add("@jobid", _mdl._jobid);
                _srt.Add("@JobtitleId", _mdl.JobId);
                _srt.Add("@Ttile", _mdl.title);
                _srt.Add("@Client", _mdl.client_id);
                _srt.Add("@Technician", _mdl.Technician);
                _srt.Add("@Dispatcher", _mdl.Dispatcher);
                _srt.Add("@Project_manager", _mdl.mgr_id);///client manager
                _srt.Add("@Project_manager1", _mdl.mgr_id1);
                _srt.Add("@startdate", _sdate);
                _srt.Add("@expense", _mdl.Expense);
                _srt.Add("@enddate", _edate);
                _srt.Add("@est_Hours", estimhour);
                _srt.Add("@Status_id", _mdl.Status);
                _srt.Add("@City_id", _mdl.city_id);
                _srt.Add("@State_id", _mdl.state_id);
                _srt.Add("@Description", _mdl.description);
                _srt.Add("@createdby", userid);
                sqlHelper.executeNonQuery("tbl_job_offer_updatenewjobv2", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string update_job_byview(job_items1 _mdl, int userid)
        {
            string message = "Job Updated Successfully";
            sqlhelper sqlHelper = new sqlhelper();

            try
            {
                SortedList _srt = new SortedList();
                //_srt.Add("@fname", _mdl.FirstName);
                _srt.Add("@JobId", _mdl.JobID);
                _srt.Add("@Client_id", _mdl.Client_id);
                _srt.Add("@Client_rate", _mdl.Client_rate);
                _srt.Add("@Technicianid", _mdl.Technicianid);
                _srt.Add("@pay_rate", _mdl.pay_rate);
                _srt.Add("@mgr_id", _mdl.mgr_id);
                _srt.Add("@mgr_id1", _mdl.mgr_id1);
                _srt.Add("@dispatcher", _mdl.Dispatcherid);
                _srt.Add("@expense", _mdl.Expense);
                _srt.Add("@Status_id", _mdl.Status);
                sqlHelper.executeNonQuery("tbl_job_offer_updatenewjob", "", _srt);
                //if (_mdl.Status == 1002)
                //{
                //    attandanceadmin_items client = new attandanceadmin_items();
                //    client.startdate = _mdl.sdate; client.starttime = _mdl.stime;
                //    client.endtime = _mdl.etime;
                //    client._memberrate = _mdl.pay_rate;
                //    client.expnse = _mdl.Expense;
                //    client.memberid = _mdl.Technicianid;
                //    client.jobid = _mdl.JobID;
                //    client.comment = "Pay Created By Approved Job";
                //    new attandanceservices().add_new_pay(client, userid);
                //}

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string update_job_byviewT(job_items1 _mdl, int userid)
        {
            string message = "Job Updated Successfully";
            sqlhelper sqlHelper = new sqlhelper();

            try
            {
                SortedList _srt = new SortedList();
                //_srt.Add("@fname", _mdl.FirstName);
                _srt.Add("@JobId", _mdl.JobID);
                _srt.Add("@dispatcher", _mdl.Dispatcherid);
                _srt.Add("@rate", _mdl.pay_rate);
                _srt.Add("@expense", _mdl.Expense);
                _srt.Add("@Status_id", _mdl.Status);
                sqlHelper.executeNonQuery("tbl_job_updatenewjobByT", "", _srt);

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string add_new_comment(string comment, int userid, int jobid, bool isadminpost = false, int commentId = 0)
        {
            string message = "Comment Added Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            var cst = DateTime.UtcNow.AddHours(-6);
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@isadminpost", isadminpost);
                _srt.Add("@commentid", commentId);
                _srt.Add("@JobId", jobid);
                _srt.Add("@comment", comment);
                _srt.Add("@createdby", userid);
                _srt.Add("@createddate", cst);
                sqlHelper.executeNonQuery("tbl_job_comment_offer_addv2", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public DateTime add_new_comment2(string comment, int userid, int jobid, ref string createdBy, bool isadminpost = false, int commentId = 0)
        {
            sqlhelper sqlHelper = new sqlhelper();
            var cst = DateTime.UtcNow.AddHours(-6);
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@isadminpost", isadminpost);
                _srt.Add("@commentid", commentId);
                _srt.Add("@JobId", jobid);
                _srt.Add("@comment", comment);
                _srt.Add("@createdby", userid);
                _srt.Add("@createddate", cst);
                createdBy = sqlHelper.executeNonQueryWMessage("tbl_job_comment_offer_addv3", "", _srt).ToString();
            }
            catch (Exception exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return cst;
        }

        public string delete_job(int jobid)
        {
            string message = "Job Offer Deleted Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@JobId", jobid);
                sqlHelper.executeNonQuery("tbl_job_offer_remove_v2", "", _srt);
            }
            catch (Exception exception)
            {
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return message;
        }


        public string delete_submitedoffer(int offerid)
        {
            string message = "Job Offer removed Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@JobId", offerid);
                sqlHelper.executeNonQuery("tbl_offer_remove_v2", "", _srt);
            }
            catch (Exception exception)
            {
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return message;
        }
        public string re_submitedoffer(int offerid)
        {
            string message = "Job Offer re-submit Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@JobId", offerid);
                sqlHelper.executeNonQuery("tbl_offer_resubmit", "", _srt);
            }
            catch (Exception exception)
            {
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return message;
        }

        public string re_acceptedoffer(int offerid)
        {
            string message = "Job Offer Accepted Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@JobId", offerid);
                sqlHelper.executeNonQuery("tbl_offer_reaccpeted", "", _srt);
            }
            catch (Exception exception)
            {
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return message;
        }

        public string assign_declineoffer(int offerid)
        {
            string message = "Offer declined Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@JobId", offerid);
                sqlHelper.executeNonQuery("tbl_offer_declinejob", "", _srt);
            }
            catch (Exception exception)
            {
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return message;
        }

        public string assign_submitedoffer(int offerid)
        {
            string message = "Offer assigned Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@JobId", offerid);
                sqlHelper.executeNonQuery("tbl_offer_assignjob", "", _srt);
            }
            catch (Exception exception)
            {
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return message;
        }
        public string add_new_doc(string Filetile, int userid, int jobid, string Size, string url)
        {
            string message = "File Added Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            var cst = DateTime.UtcNow.AddHours(-6);
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@createddate", cst);
                _srt.Add("@Filetile", Filetile);
                _srt.Add("@Jobid", jobid);
                _srt.Add("@Size", Size);
                _srt.Add("@url", url);
                _srt.Add("@createdby", userid);
                sqlHelper.executeNonQuery("tbl_job_offer_files_add", "", _srt);

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string delete_doc(int id)
        {
            string message = "File Deleted Successfully";
            sqlhelper sqlHelper = new sqlhelper();

            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                sqlHelper.executeNonQuery("tbl_job_offerfiles_remove", "", _srt);

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string assignjob(int Jobid, int memberid, string rate)
        {
            string message = "File Deleted Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", Jobid);
                _srt.Add("@memberid", memberid);
                _srt.Add("@payrate", rate);
                message = sqlHelper.executeNonQueryWMessage("assignjobtonewmember", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public List<commentModel> getreply(int mainid)
        {
            List<commentModel> message = new List<commentModel>();
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", mainid);
                var dt = sqlHelper.fillDataTable("tbl_notification_getmessagebyparentid", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    commentModel _item = new commentModel();
                    _item.createdBy = dt.Rows[i]["Createdby"].ToString();
                    _item.text = dt.Rows[i]["comment"].ToString();
                    _item.timedate = dt.Rows[i]["Createddate"].ToString();
                    message.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public jobmodal get_all_active_jobs(string date = "", int state_filter_id = 0, int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int[] city_filter_id = null)
        {
            sqlhelper sqlHelper = new sqlhelper();
            jobmodal _mdl = new jobmodal();
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            List<job_items> _jbs = new List<job_items>();
            try
            {
                if (!string.IsNullOrEmpty(date))
                {
                    var dates = date.Split('-');
                    _srt.Add("@startdate", dates[0]);
                    _srt.Add("@enddate", dates[1]);
                }

                if (state_filter_id != 0)
                {
                    _srt.Add("@stateid", state_filter_id);
                }
                if (cclient != 0)
                {
                    _srt.Add("@client", cclient);
                }
                if (cTechnician != 0)
                {
                    _srt.Add("@Technician", cTechnician);
                }
                if (cDispatcher != 0)
                {
                    _srt.Add("@Dispatcher", cDispatcher);
                }
                string sp = "tbl_job_getalljobswithoutstatus";
                if (Status != 0)
                {
                    sp = "tbl_job_getalljobswithstatus";
                    _srt.Add("@Status", Status);
                }
                if (city_filter_id != null)
                {
                    var city = string.Join<int>(",", city_filter_id.Where(a => a != 0));
                    if (!string.IsNullOrEmpty(city))
                    {
                        _srt.Add("@city", city);
                    }
                }
                var dt = sqlHelper.fillDataTable(sp, "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    job_items _item = new job_items();
                    _item.Job_ID = dt.Rows[i]["JobId"].ToString();
                    _item.cityname = dt.Rows[i]["city"].ToString();
                    _item.statename = dt.Rows[i]["state"].ToString();
                    _item.Title = dt.Rows[i]["Ttile"].ToString();
                    _item.Client = dt.Rows[i]["Client"].ToString().ToTitleCase();
                    _item.Technician = dt.Rows[i]["Technician"].ToString().ToTitleCase();
                    _item.Dispatcher = dt.Rows[i]["Dispatcher"].ToString().ToTitleCase();
                    _item.Project_manager = dt.Rows[i]["Project_manager"].ToString().ToTitleCase();
                    _item.sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("yyyy-MM-dd");
                    _item.stime = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("hh:mm tt");
                    _item.etime = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()).ToString("hh:mm tt");
                    _item.Client_rate = dt.Rows[i]["Client_rate"].ToString().Currency();
                    _item._sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString());
                    _item._endate = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString());
                    _item.Pay_rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.Est_hours = dt.Rows[i]["est_Hours"].ToString();
                    _item.hours = dt.Rows[i]["hours"].ToString();
                    _item.Expense = dt.Rows[i]["Expnese"].ToString().Currency();
                    _item.Status = dt.Rows[i]["Status_id"].ToString();
                    _item.JobID = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    // _item.status_id = Convert.ToInt32(dt.Rows[i]["Status_id"].ToString());
                    //total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _jbs.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._jobs = _jbs;
            return _mdl;
        }
        public List<job_items> get_all_active_jobs_by_index(ref int total, int notificationUSER, int startindex, int endindex, string search = "", string date = "", int state_filter_id = 0, int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int[] city_filter_id = null, int userid = 0)
        {
            sqlhelper sqlHelper = new sqlhelper();
            jobmodal _mdl = new jobmodal();
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            List<job_items> _jbs = new List<job_items>();
            try
            {
                if (!string.IsNullOrEmpty(date))
                {
                    var dates = date.Split('-');
                    _srt.Add("@startdate", dates[0]);
                    _srt.Add("@enddate", dates[1]);
                }
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                _srt.Add("@startindex", startindex);
                _srt.Add("@endindex", endindex);
                if (userid != 0)
                {
                    _srt.Add("@id", userid);
                }
                if (state_filter_id != 0)
                {
                    _srt.Add("@stateid", state_filter_id);
                }
                if (cclient != 0)
                {
                    _srt.Add("@client", cclient);
                }
                if (cTechnician != 0)
                {
                    _srt.Add("@Technician", cTechnician);
                }
                if (cDispatcher != 0)
                {
                    _srt.Add("@Dispatcher", cDispatcher);
                }
                string sp = "tbl_job_getalljobs_offer_index_withoutstatus";
                if (Status != 0)
                {
                    sp = "tbl_job_getalljobs_offer_index_withstatus";
                    _srt.Add("@Status", Status);
                }
                if (city_filter_id != null)
                {
                    var city = string.Join<int>(",", city_filter_id.Where(a => a != 0));
                    if (!string.IsNullOrEmpty(city))
                    {
                        _srt.Add("@city", city);
                    }
                }
                notificationServices _nser = new notificationServices();
                var dt = sqlHelper.fillDataTable(sp, "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    job_items _item = new job_items();
                    _item.Job_ID = dt.Rows[i]["JobId"].ToString();
                    _item.cityname = dt.Rows[i]["city"].ToString();
                    _item.statename = dt.Rows[i]["state"].ToString();
                    _item.Title = dt.Rows[i]["Ttile"].ToString();
                    _item.Client = dt.Rows[i]["Client"].ToString().ToTitleCase();
                    _item.Technician = dt.Rows[i]["Technician"].ToString().ToTitleCase();
                    _item.Dispatcher = dt.Rows[i]["Dispatcher"].ToString().ToTitleCase();
                    _item.Project_manager = dt.Rows[i]["Project_manager"].ToString().ToTitleCase();
                    _item.sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("yyyy-MM-dd");
                    _item.stime = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("hh:mm tt");
                    _item.etime = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()).ToString("hh:mm tt");
                    _item.Client_rate = dt.Rows[i]["Client_rate"].ToString().Currency();
                    _item._sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString());
                    _item._endate = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString());
                    _item.Pay_rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.Est_hours = dt.Rows[i]["est_Hours"].ToString();
                    _item.hours = dt.Rows[i]["hours"].ToString();
                    _item.Expense = dt.Rows[i]["Expnese"].ToString().Currency();
                    _item.Status = dt.Rows[i]["Status"].ToString();
                    _item.JobID = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.view = GetViewCount(_item.JobID);
                    _item.accepted = GetacceptedCount(_item.JobID);
                    _item.token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    _item.status_id = Convert.ToInt32(dt.Rows[i]["Status_id"].ToString());
                    _item.commentCount = _nser.GetNotificationCountByType(_item.JobID, NotificationType.Offer, notificationUSER);
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _jbs.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //_mdl._jobs = _jbs;
            return _jbs;
        }

        public int GetViewCount(int jobid)
        {
            sqlhelper sqlHelper = new sqlhelper();
            int count = 0;
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@jobid", jobid);
                var dt = sqlHelper.fillDataTable("tbl_offer_GetViewCountByJobId", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    count = Convert.ToInt32(dt.Rows[0][0].ToString());
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return count;
        }

        public int GetacceptedCount(int jobid)
        {
            sqlhelper sqlHelper = new sqlhelper();
            int count = 0;
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@jobid", jobid);
                var dt = sqlHelper.fillDataTable("tbl_offer_GetAcceptedCountByJobId", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    count = Convert.ToInt32(dt.Rows[0][0].ToString());
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return count;
        }

        public List<job_items> get_all_active_jobs_by_indexByMember(ref int total, int notificationuser, int startindex, int endindex, string search = "", string date = "", int state_filter_id = 0, int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int[] city_filter_id = null, int userid = 0,string membertype="T")
        {
            sqlhelper sqlHelper = new sqlhelper();
            jobmodal _mdl = new jobmodal();
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            List<job_items> _jbs = new List<job_items>();
            try
            {
                if (!string.IsNullOrEmpty(date))
                {
                    var dates = date.Split('-');
                    _srt.Add("@startdate", dates[0]);
                    _srt.Add("@enddate", dates[1]);
                }
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                _srt.Add("@startindex", startindex);
                _srt.Add("@endindex", endindex);
                if (userid != 0)
                {
                    _srt.Add("@id", userid);
                }
                if (state_filter_id != 0)
                {
                    _srt.Add("@stateid", state_filter_id);
                }
                if (cclient != 0)
                {
                    _srt.Add("@client", cclient);
                }
                if (cTechnician != 0)
                {
                    _srt.Add("@Technician", cTechnician);
                }
                if (cDispatcher != 0)
                {
                    _srt.Add("@Dispatcher", cDispatcher);
                }
                string sp = "tbl_job_getalljobs_offer_index_withoutstatus_fortech";
                if (Status != 0)
                {
                    sp = "tbl_job_getalljobs_offer_index_withstatus_fortech";
                    _srt.Add("@Status", Status);
                }
                if (city_filter_id != null)
                {
                    var city = string.Join<int>(",", city_filter_id.Where(a => a != 0));
                    if (!string.IsNullOrEmpty(city))
                    {
                        _srt.Add("@city", city);
                    }
                }
                notificationServices _nser = new notificationServices();
                var dt = sqlHelper.fillDataTable(sp, "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    job_items _item = new job_items();
                    _item.Job_ID = dt.Rows[i]["JobId"].ToString();
                    _item.cityname = dt.Rows[i]["city"].ToString();
                    _item.statename = dt.Rows[i]["state"].ToString();
                    _item.Title = dt.Rows[i]["Ttile"].ToString();
                    _item.Client = dt.Rows[i]["Client"].ToString().ToTitleCase();
                    _item.Technician = dt.Rows[i]["Technician"].ToString().ToTitleCase();
                    _item.Dispatcher = dt.Rows[i]["Dispatcher"].ToString().ToTitleCase();
                    _item.Project_manager = dt.Rows[i]["Project_manager"].ToString().ToTitleCase();
                    _item.sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("yyyy-MM-dd");
                    _item.stime = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("hh:mm tt");
                    _item.etime = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()).ToString("hh:mm tt");
                    _item.Client_rate = membertype=="ST"?"XXXXXX": dt.Rows[i]["Client_rate"].ToString().Currency();
                    _item._sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString());
                    _item._endate = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString());
                    _item.Pay_rate = membertype == "ST" ? "XXXXXX" : dt.Rows[i]["rate"].ToString().Currency();
                    _item.Est_hours = dt.Rows[i]["est_Hours"].ToString();
                    _item.hours = dt.Rows[i]["hours"].ToString();
                    _item.Expense = dt.Rows[i]["Expnese"].ToString().Currency();
                    _item.status_id = Convert.ToInt32(dt.Rows[i]["Status_id"].ToString());
                    _item.Status = _item.status_id != 1002 ? "Available" : "Dispatched";
                    _item.JobID = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _item.commentCount = _nser.GetNotificationCountByType(_item.JobID, NotificationType.Offer, notificationuser);
                    _jbs.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //_mdl._jobs = _jbs;
            return _jbs;
        }

        public jobmodal get_all_active_jobs_bymemberid(int id, string date, int state_filter_id = 0, int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int[] city_filter_id = null)
        {
            sqlhelper sqlHelper = new sqlhelper();
            jobmodal _mdl = new jobmodal();
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            List<job_items> _jbs = new List<job_items>();
            try
            {
                if (!string.IsNullOrEmpty(date))
                {
                    var dates = date.Split('-');
                    _srt.Add("@startdate", dates[0]);
                    _srt.Add("@enddate", dates[1]);
                }
                if (state_filter_id != 0)
                {
                    _srt.Add("@stateid", state_filter_id);
                }
                if (cclient != 0)
                {
                    _srt.Add("@client", cclient);
                }
                if (cTechnician != 0)
                {
                    _srt.Add("@Technician", cTechnician);
                }
                if (cDispatcher != 0)
                {
                    _srt.Add("@Dispatcher", cDispatcher);
                }
                string sp = "tbl_job_offer_getalljobswithoutstatus";
                if (Status != 0)
                {
                    sp = "tbl_job_offer_getalljobswithstatus";
                    _srt.Add("@Status", Status);
                }
                if (city_filter_id != null)
                {
                    var city = string.Join<int>(",", city_filter_id);
                    _srt.Add("@city", city);
                }
                if (id != 0)
                {
                    _srt.Add("@id", id);
                }

                var dt = sqlHelper.fillDataTable(sp, "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    job_items _item = new job_items();
                    _item.Job_ID = dt.Rows[i]["JobId"].ToString();
                    _item.cityname = dt.Rows[i]["city"].ToString();
                    _item.statename = dt.Rows[i]["state"].ToString();
                    _item.Title = dt.Rows[i]["Ttile"].ToString();
                    _item.Client = dt.Rows[i]["Client"].ToString().ToTitleCase();
                    _item.Technician = dt.Rows[i]["Technician"].ToString().ToTitleCase();
                    _item.Dispatcher = dt.Rows[i]["Dispatcher"].ToString().ToTitleCase();
                    _item.Project_manager = dt.Rows[i]["Project_manager"].ToString().ToTitleCase();
                    _item.sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("yyyy-MM-dd hh:mm tt");
                    _item.endate = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()).ToString("yyyy-MM-dd hh:mm tt");
                    _item.Client_rate = dt.Rows[i]["Client_rate"].ToString().Currency();
                    _item.Pay_rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.Est_hours = dt.Rows[i]["est_Hours"].ToString();
                    _item.hours = dt.Rows[i]["hours"].ToString();
                    _item.Expense = dt.Rows[i]["Expnese"].ToString().Currency();
                    _item.Status = dt.Rows[i]["Status_id"].ToString();
                    _item.JobID = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item._sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString());
                    _item._endate = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString());

                    _jbs.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._jobs = _jbs;
            return _mdl;
        }
        public job_items1 get_all_active_jobs_byid(int id, int ViewuserId,string subcontractortype, ref int offerId,ref bool isoffered ,int techId = 0)
        {
            sqlhelper sqlHelper = new sqlhelper();
            job_items1 _item = new job_items1();
            //SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            //List<job_items> _jbs = new List<job_items>();
            List<commentmodal> _commnts = new List<commentmodal>();
            List<filemodal> _files = new List<filemodal>();
            List<offermodal> _offers = new List<offermodal>();
            notificationServices _nser = new notificationServices();
            int acceptcount = 0;
            try
            {//offercount
                SortedList _srt = new SortedList();
                _srt.Add("@jobid", id);
                _srt.Add("@userid", ViewuserId);
                var ds = sqlHelper.fillDataSet("tbl_job_offer_getalljobsbyid_v2", "", _srt);
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                var dt2 = ds.Tables[2];
                var dt3 = ds.Tables[3];
                var dt4 = ds.Tables[4];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _item.Job_ID = dt.Rows[0]["JobId"].ToString();
                    _item.Title = dt.Rows[0]["Ttile"].ToString();
                    _item.Client = dt.Rows[0]["Client"].ToString();
                    _item.address = dt.Rows[0]["street"].ToString().GetAddress(dt.Rows[0]["city"].ToString(), dt.Rows[0]["state"].ToString(), dt.Rows[0]["zipcode"].ToString());
                    _item.token = _crypt.EncryptStringAES(dt.Rows[0]["id"].ToString());
                    _item.distoken = _crypt.EncryptStringAES(dt.Rows[0]["Dispatcherid"].ToString());
                    _item.Client_id = Convert.ToInt32(dt.Rows[0]["Client"].ToString());
                    _item.mgr_id = Convert.ToInt32(dt.Rows[0]["Project_manager"].ToString());
                    _item.mgr_id1 = Convert.ToInt32(dt.Rows[0]["Project_manager1"].ToString());
                    _item.Technician = dt.Rows[0]["Technician"].ToString();
                    _item.Dispatcher = dt.Rows[0]["Dispatcher"].ToString();
                    _item.Project_manager = dt.Rows[0]["Project_manager"].ToString();
                    _item.sdate = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("yyyy-MM-dd");
                    _item.stime = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("hh:mm tt");
                    _item.etime = Convert.ToDateTime(dt.Rows[0]["enddate"].ToString()).ToString("hh:mm tt");
                    _item.Client_rate = subcontractortype=="ST"?"XXXXXX": dt.Rows[0]["Client_rate"].ToString().Currency();
                    _item.Expense = dt.Rows[0]["expense"].ToString().Currency();
                    _item.Est_hours = dt.Rows[0]["est_Hours"].ToString();
                    _item.Status = Convert.ToInt32(dt.Rows[0]["Status_id"].ToString());
                    _item.JobID = Convert.ToInt32(dt.Rows[0]["id"].ToString());
                    _item.description = dt.Rows[0]["description"].ToString();
                    _item.pay_rate = subcontractortype == "ST" ? "XXXXXX" : dt.Rows[0]["pay_rate"].ToString().Currency();
                    _item.Technicianid = Convert.ToInt32(dt.Rows[0]["Technicianid"].ToString());
                    _item.Dispatcherid = Convert.ToInt32(dt.Rows[0]["Dispatcherid"].ToString());
                }
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    commentmodal _mdl = new commentmodal();
                    _mdl.Comment = dt1.Rows[i]["Comment"].ToString();
                    _mdl.createdby = dt1.Rows[i]["createdby"].ToString();
                    _mdl.CreatedDate = dt1.Rows[i]["CreatedDate"].ToString();
                    _mdl.Commentby = Convert.ToInt32(dt1.Rows[i]["commentby"].ToString());
                    _mdl.Commentto = Convert.ToInt32(dt1.Rows[i]["CommentTo"].ToString());
                    _mdl.isadminpost = Convert.ToBoolean(dt1.Rows[i]["IsAdminPost"].ToString());
                    _mdl.Commentid = Convert.ToInt32(dt1.Rows[i]["commentid"].ToString());
                    _mdl.Parentid = Convert.ToInt32(dt1.Rows[i]["parentid"].ToString());
                    _mdl.isread = dt1.Rows[i]["readusers"].ToString().Contains(ViewuserId.ToString());
                    if (_mdl.Commentby == ViewuserId)
                    {
                        _mdl.isread = true;
                    }
                    _commnts.Add(_mdl);
                }
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    filemodal _mdl = new filemodal();
                    _mdl.Filetile = dt2.Rows[i]["Filetile"].ToString();
                    _mdl.fileid = Convert.ToInt32(dt2.Rows[i]["id"].ToString());
                    _mdl.Size = dt2.Rows[i]["Size"].ToString();
                    _mdl.url = dt2.Rows[i]["url"].ToString();
                    _mdl.createdby = dt2.Rows[i]["createdby"].ToString();
                    _mdl.Createddate = dt2.Rows[i]["Createddate"].ToString();
                    _mdl.filetoken = _crypt.EncryptStringAES(dt2.Rows[i]["id"].ToString());
                    _files.Add(_mdl);
                }
                for (int i = 0; i < dt3.Rows.Count; i++)
                {
                    offermodal _mdl = new offermodal();
                    _mdl.id = Convert.ToInt32(dt3.Rows[i]["Id"].ToString());
                    _mdl.jobid = Convert.ToInt32(dt3.Rows[i]["JobId"].ToString());
                    _mdl.offertoken = _crypt.EncryptStringAES(dt3.Rows[i]["Id"].ToString());
                    _mdl.jobtoken = _crypt.EncryptStringAES(dt3.Rows[i]["JobId"].ToString());
                    _mdl.jobindexid = dt3.Rows[i]["jobIndexId"].ToString();
                    _mdl.rate = dt3.Rows[i]["pay_rate"].ToString().Currency();
                    _mdl.state = dt3.Rows[i]["state"].ToString();
                    _mdl.status = dt3.Rows[i]["statusName"].ToString();
                    _mdl.tech = dt3.Rows[i]["Technician"].ToString().ToTitleCase();
                    _mdl.city = dt3.Rows[i]["city"].ToString();
                    _mdl.createddate = Convert.ToDateTime(dt3.Rows[i]["createddate"]).ToString("yyyy-MM-dd hh:mm tt");
                    _offers.Add(_mdl);
                    if (techId == Convert.ToInt16(Convert.ToInt32(dt3.Rows[i]["techId"].ToString())))
                    {
                        offerId = _mdl.id;
                    }
                    if (_mdl.status.ToLower() == "accepted")
                    {
                        acceptcount = acceptcount + 1;
                    }
                }
                isoffered = Convert.ToInt32(dt4.Rows[0]["offercount"].ToString()) > 0 ? true : false;
                _item._Comments = _commnts;
                _item._files = _files;
                _item._offers = _offers;
                _item.acceptedCount = acceptcount;
                _item.msgcount = _nser.GetNotificationCountByType(_item.JobID, NotificationType.Offer, ViewuserId);
                dt.Dispose();
                ds.Dispose();
                dt1.Dispose();
                dt2.Dispose();
                dt3.Dispose();
                dt4.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _item.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl._jobs = _jbs;
            return _item;
        }
        public job get_all_active_job_details_byid(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            job _item = new job();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@jobid", id);
                Crypto _crypt = new Crypto();
                var dt = sqlHelper.fillDataTable("tbl_job_offer_getfulljobsbyid_old", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _item.JobId = dt.Rows[0]["JobId"].ToString();
                    _item.title = dt.Rows[0]["Ttile"].ToString();
                    _item.client_id = Convert.ToInt32(dt.Rows[0]["Client"].ToString());
                    _item.mgr_id = Convert.ToInt32(dt.Rows[0]["Project_manager"].ToString());
                    _item.sdate = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("yyyy-MM-dd");
                    _item.edate = Convert.ToDateTime(dt.Rows[0]["enddate"].ToString()).ToString("yyyy-MM-dd");
                    _item.stime = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("hh:mm tt");
                    _item.etime = Convert.ToDateTime(dt.Rows[0]["enddate"].ToString()).ToString("hh:mm tt");
                    _item.Est_hours = Convert.ToDouble(dt.Rows[0]["est_Hours"].ToString());
                    _item.Status = Convert.ToInt32(dt.Rows[0]["Status_id"].ToString());
                    _item._jobid = id;
                    _item.description = dt.Rows[0]["Description"].ToString();
                    _item.Technician = Convert.ToInt32(dt.Rows[0]["Technician"].ToString());
                    _item.Dispatcher = Convert.ToInt32(dt.Rows[0]["Dispatcher"].ToString());
                    _item.Expense = dt.Rows[0]["Expnese"].ToString().Currency();
                    _item.city_id = Convert.ToInt32(dt.Rows[0]["City_id"].ToString());
                    _item.city = dt.Rows[0]["City"].ToString();
                    _item.state_id = Convert.ToInt32(dt.Rows[0]["State_id"].ToString());
                    _item.Client_rate = dt.Rows[0]["Client_rate"].ToString().Currency();
                    _item.Pay_rate = dt.Rows[0]["payrate"].ToString().Currency();
                    _item.street = dt.Rows[0]["address"].ToString();
                    _item.zip = dt.Rows[0]["zipcode"].ToString();
                    _item.token = _crypt.EncryptStringAES(id.ToString());
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _item.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl._jobs = _jbs;
            return _item;
        }


        public job get_mailmerge_job_details_byid(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            job _item = new job();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@jobid", id);
                var dt = sqlHelper.fillDataTable("tbl_job_offer_getfulljobsbyid", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _item.JobId = dt.Rows[0]["JobId"].ToString();
                    _item.title = dt.Rows[0]["Ttile"].ToString();
                    _item.sdate = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("yyyy-MM-dd");
                    _item.datetime = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("yyyy-MM-dd hh:mm tt");
                    _item.stime = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("hh:mm tt");
                    _item.etime = Convert.ToDateTime(dt.Rows[0]["enddate"].ToString()).ToString("hh:mm tt");
                    _item.description = dt.Rows[0]["Description"].ToString();
                    _item.Pay_rate = dt.Rows[0]["payrate"].ToString().Currency();
                    _item.street = dt.Rows[0]["address"].ToString();
                    _item.city = dt.Rows[0]["cityname"].ToString();
                    _item.state = dt.Rows[0]["statename"].ToString();
                    _item.zip = dt.Rows[0]["zipcode"].ToString();
                    _item.address = _item.street.GetAddress(_item.city, _item.state, _item.zip);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                _item.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _item;
        }
    }
}