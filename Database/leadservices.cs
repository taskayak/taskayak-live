﻿using hrm.Models;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace hrm.Database
{
    public class leadservices
    {
        public string add_new_lead(leadmodal_item lead, int userid,int dfecthid=0)
        {
            string message = "";
            string skills = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@firstname", lead.firstname);
                _srt.Add("@lastname", lead.lastname);
                _srt.Add("@rate", string.IsNullOrEmpty(lead.rate) ? "$0" : lead.rate);
                _srt.Add("@statusid", lead.statusid);
                _srt.Add("@Cityid", lead.city_id);
                _srt.Add("@Stateid", lead.state_id);
                _srt.Add("@srcid", lead.srcid);
                _srt.Add("@Email", lead.email);
                _srt.Add("@Phone", lead.phone);
                _srt.Add("@createdby", userid);
                _srt.Add("@Company", lead.Company);
                _srt.Add("@Website", lead.Website);
                _srt.Add("@LinkedIn", lead.LinkedIn);
                _srt.Add("@dfecthid", dfecthid);
                if (lead.skill != null)
                {
                    skills = string.Join<int>(",", lead.skill);
                    _srt.Add("@skills", skills);
                }
                message = sqlHelper.executeNonQueryWMessage("tbl_lead_addnewlead", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }


        public string add_new_lead_comment(int userid, int leadid, string comment)
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            var cst = DateTime.UtcNow.AddHours(-6);
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@createdby", userid);
                _srt.Add("@createddate", cst);
                _srt.Add("@leadid", leadid);
                _srt.Add("@comment", comment);
                message = sqlHelper.executeNonQueryWMessage("tbl_lead_addnewcomment", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string delete_lead(int leadid)
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@leadid", leadid);
                message = sqlHelper.executeNonQueryWMessage("tbl_lead_deletelead", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string update_lead(leadmodal_item lead, int userid, string memberid)
        {
            string message = "";
            string skills = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                var rate = string.IsNullOrEmpty(lead.rate) ? "$0" : lead.rate;
                SortedList _srt = new SortedList();
                _srt.Add("@firstname", lead.firstname);
                _srt.Add("@lastname", lead.lastname);
                _srt.Add("@rate", rate);
                _srt.Add("@statusid", lead.statusid);
                _srt.Add("@Cityid", lead.city_id);
                _srt.Add("@Stateid", lead.state_id);
                _srt.Add("@CreatedBy", userid);
                _srt.Add("@srcid", lead.srcid);
                _srt.Add("@Email", lead.email);
                _srt.Add("@Phone", lead.phone);
                _srt.Add("@leadid", lead.leadId);
                _srt.Add("@company", lead.Company);
                _srt.Add("@website", lead.Website);
                _srt.Add("@linkedin", lead.LinkedIn);
                if (lead.skill != null)
                {
                    skills = string.Join<int>(",", lead.skill);
                    _srt.Add("@skills", skills);
                }
                message = sqlHelper.executeNonQueryWMessage("tbl_lead_updatenewlead_v2", "", _srt).ToString();
                if (lead.statusid == 4)
                {
                    int i = 0;
                    if (!Int32.TryParse(memberid, out i))
                    {
                        memberid = new helper().get5RandomDidit();
                    }
                    else
                    {
                        memberid = (i + 1).ToString();
                    }
                    membermodal _mdl = new membermodal();
                    _mdl.FirstName = lead.firstname;
                    _mdl.lastname = lead.lastname;
                    _mdl.hourlyrate = rate;
                    _mdl.city_id = lead.city_id;
                    _mdl.state_id = lead.state_id;
                    _mdl.email = lead.email;
                    _mdl.phone = lead.phone;
                    _mdl.password = lead.phone.Replace("-", "");
                    _mdl.membertype = "M";
                    _mdl.member_id = memberid;
                    _mdl.status = 1;
                    _mdl.background = "Pending";
                    _mdl.drugtested = "Pending";
                    _mdl.image = "default";
                    _mdl.skills = lead.skill;
                    new memberservices().add_new_member(_mdl, userid);
                    message = "Member succesfully converted to user";
                }
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public void update_lead_status(int statusId, int leadid, string memberid, int userid)
        {
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@leadid", leadid);
                _srt.Add("@statusId", statusId);
                sqlHelper.executeNonQuery("tbl_lead_updatestatus", "", _srt);
                if (statusId == 4)
                {
                    leadmodal_item lead = get_all_active_leads_byid(leadid);
                    int i = 0;
                    if (!Int32.TryParse(memberid, out i))
                    {
                        memberid = new helper().get5RandomDidit();
                    }
                    else
                    {
                        memberid = (i + 1).ToString();
                    }
                    membermodal _mdl = new membermodal();
                    _mdl.FirstName = lead.firstname;
                    _mdl.lastname = lead.lastname;
                    _mdl.hourlyrate = lead.rate;
                    _mdl.city_id = lead.city_id;
                    _mdl.state_id = lead.state_id;
                    _mdl.email = lead.email;
                    _mdl.phone = lead.phone;
                    _mdl.password = lead.phone.Replace("-", "");
                    _mdl.membertype = "M";
                    _mdl.member_id = memberid;
                    _mdl.status = 1;
                    _mdl.background = "Pending";
                    _mdl.drugtested = "Pending";
                    _mdl.image = "default";
                    _mdl.skills = lead.skill;
                    new memberservices().add_new_member(_mdl, userid);
                    //message = "Lead Succesfully converted to member";
                }
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //return message;
        }

        public void getstateCityId(string cityname,string stateName,ref int stateid,ref int cityId)
        {
            sqlhelper sqlHelper = new sqlhelper();
            SortedList _srtlist = new SortedList();
            _srtlist.Add("@cityname", cityname);
            _srtlist.Add("@statename", stateName);
            var ds = sqlHelper.fillDataTable("tbl_city_getcityidbyname", "", _srtlist);
            stateid = Convert.ToInt32(ds.Rows[0]["sid"].ToString());
            cityId = Convert.ToInt32(ds.Rows[0]["cityid"].ToString());
        }


        public void get_leads_from_datafetch()
        {
           
            sqlhelper sqlHelper = new sqlhelper();
            var ds = sqlHelper.fillDataTable("tbldatafetch_Gettbldatafetchlead", "", null);
            int cityid = 0;
            int stateid = 0;
            for (int i = 0; i < ds.Rows.Count; i++)
            {
                jsonrateModal _ratemodel = new jsonrateModal();
                var id = Convert.ToInt32(ds.Rows[i]["id"].ToString());
                var text = ds.Rows[i]["Text"].ToString();
                JavaScriptSerializer js = new JavaScriptSerializer();
                jsonmainleadModal _model = js.Deserialize<jsonmainleadModal>(text);
                leadmodal_item lead = new leadmodal_item();
                getstateCityId(_model.data.profile.city, _model.data.profile.state,ref stateid, ref cityid);
                lead.firstname = _model.data.profile.firstname;
                lead.lastname = _model.data.profile.lastname;
                lead.rate = "$0.00";
                lead.statusid = 1;
                lead.city_id = cityid;
                lead.state_id = stateid;
                lead.srcid = 2;
                lead.email = "";
                lead.phone = "";
                lead.Company = _model.data.profile.companyname;
                lead.Website = _model.data.website;
                lead.LinkedIn = _model.data.profile.linkedinUrl;
                int userid = 2009;
                add_new_lead(lead, userid, id);
            }
           
            ds.Dispose();
           
        }

        public leadmodal_item get_all_active_leads_byid(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            leadmodal_item _item = new leadmodal_item();
            List<commentmodal> _subComments = new List<commentmodal>();
            List<filemodal> _subfiles = new List<filemodal>();
            List<int> _skills = new List<int>();
            Crypto _crypt = new Crypto();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@leadid", id);
                var ds = sqlHelper.fillDataSet("tbl_lead_getalllead_by_id", "", _srt);
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                var dt2 = ds.Tables[2];
                var dt3 = ds.Tables[3];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    // clientmodal_item _item = new clientmodal_item();
                    _item.firstname = dt.Rows[i]["Firstname"].ToString().ToTitleCase();
                    _item.lastname = dt.Rows[i]["Lastname"].ToString().ToTitleCase();
                    _item.leadId = Convert.ToInt32(dt.Rows[i]["leadid"].ToString());
                    _item.city_id = Convert.ToInt32(dt.Rows[i]["Cityid"].ToString());
                    _item.city = dt.Rows[i]["City"].ToString();
                    _item.state_id = Convert.ToInt32(dt.Rows[i]["Stateid"].ToString());
                    _item.rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.phone = dt.Rows[i]["phonernumber"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.Company = dt.Rows[i]["Company"].ToString();
                    _item.Website = dt.Rows[i]["Website"].ToString();
                    _item.LinkedIn = dt.Rows[i]["LinkedIn"].ToString();
                    _item.statusid = Convert.ToInt32(dt.Rows[i]["Statusid"].ToString());
                    _item.srcid = Convert.ToInt32(dt.Rows[i]["Sourcid"].ToString());
                    _item._token = _crypt.EncryptStringAES(dt.Rows[i]["leadid"].ToString());
                    if (dt1.Rows.Count > 0)
                    {
                        for (int j = 0; j < dt1.Rows.Count; j++)
                        {
                            _skills.Add(Convert.ToInt32(dt1.Rows[j][0].ToString()));
                        }
                        _item.skill = _skills.ToArray();
                    }
                }
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    commentmodal _mdl = new commentmodal();
                    _mdl.Comment = dt2.Rows[i]["Comment"].ToString();
                    _mdl.createdby = dt2.Rows[i]["createdby"].ToString().ToTitleCase();
                    _mdl.CreatedDate = dt2.Rows[i]["CreatedDate"].ToString();
                    _subComments.Add(_mdl);
                }
                for (int i = 0; i < dt3.Rows.Count; i++)
                {
                    filemodal _mdl = new filemodal();
                    _mdl.Filetile = dt3.Rows[i]["Doctitle"].ToString().ToTitleCase();
                    _mdl.url = dt3.Rows[i]["docurl"].ToString();
                    _mdl.createdby = dt3.Rows[i]["createdby"].ToString().ToTitleCase();
                    _mdl.Createddate = dt3.Rows[i]["Createddate"].ToString();
                    _mdl.fileid = Convert.ToInt32(dt3.Rows[i]["id"].ToString());
                    _mdl.filetoken = _crypt.EncryptStringAES(dt3.Rows[i]["id"].ToString());
                    _subfiles.Add(_mdl);
                }
                dt.Dispose();
                dt1.Dispose();
                dt2.Dispose();
                dt3.Dispose();
                ds.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _item.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //_mdl.clients = _model;
            _item._Comments = _subComments;
            _item._files = _subfiles;
            return _item;
        }
        public List<skill_items> get_all_skills_byuserid(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            List<skill_items> _model = new List<skill_items>();
            SortedList _srt = new SortedList();
            _srt.Add("@id", id);
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_skills_getallskills_bylead", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    skill_items _item = new skill_items();
                    _item.name = dt.Rows[i]["name"].ToString();

                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

            return _model;
        }
        public List<leadmodals> get_all_active_leads_by_index(ref int total, int startIndex, int endIndex, string search = "", int? statusId = null, int? stateid = null, int[] cityId = null, int[] skills = null, int[] sourceid = null)
        {
            sqlhelper sqlHelper = new sqlhelper();
            leadmodal _mdl = new leadmodal();
            List<leadmodals> _model = new List<leadmodals>();
            SortedList _srt = new SortedList();
            try
            {
                string sp = "tbl_lead_getall_leads_bynotlead_byindex";
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                _srt.Add("@startindex", startIndex);
                _srt.Add("@endindex", endIndex);
                if (stateid.HasValue && stateid.Value != 0)
                {
                    _srt.Add("@stateId", stateid);
                }
                if (statusId.HasValue && statusId.Value != 0)
                {
                    _srt.Add("@statusId", statusId);
                    sp = "tbl_lead_getall_leads_byindex";
                }
                if (cityId != null)
                {
                    var city = string.Join<int>(",", cityId.Where(a => a != 0));
                    if (!string.IsNullOrEmpty(city))
                    {
                        _srt.Add("@city", city);
                    }
                }
                if (skills != null)
                {
                    var skill = string.Join<int>(",", skills.Where(a => a != 0));
                    if (!string.IsNullOrEmpty(skill))
                    {
                        _srt.Add("@skills", skill);
                    }
                }
                if (sourceid != null)
                {
                    var source = string.Join<int>(",", sourceid.Where(a => a != 0));
                    if (!string.IsNullOrEmpty(source))
                    {
                        _srt.Add("@sourceid", source);
                    }
                }
                var dt = sqlHelper.fillDataTable(sp, "", _srt);
                Crypto _crypt = new Crypto();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    leadmodals _item = new leadmodals();
                    _item.leadid = Convert.ToInt32(dt.Rows[i]["leadid"].ToString());
                    _item.fullname = dt.Rows[i]["fullname"].ToString().ToTitleCase();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.phone = dt.Rows[i]["phonernumber"].ToString().PhoneNumber();
                    _item.rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.source = dt.Rows[i]["srcname"].ToString();
                    _item.Website = dt.Rows[i]["Website"].ToString();
                    _item.Company = dt.Rows[i]["Company"].ToString();
                    _item.LinkedIn = dt.Rows[i]["LinkedIn"].ToString();
                    _item.status = dt.Rows[i]["Name"].ToString();
                    _item.city = dt.Rows[i]["cityname"].ToString();
                    _item.state = dt.Rows[i]["statename"].ToString();
                    _item.status_id = Convert.ToInt32(dt.Rows[i]["statusId"].ToString());
                    _item.token = _crypt.EncryptStringAES(dt.Rows[i]["leadid"].ToString());
                    var items = get_all_skills_byuserid(_item.leadid);
                    if (items.Count > 0)
                    {
                        _item.skills = string.Join(",", items.Select(k => k.name).ToArray());
                    }
                    else
                    {
                        _item.skills = "--";
                    }
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //_model;
            return _model;
        }
        public leadmodal get_all_active_leads(string delimater = ",", int? statusId = null, int? stateid = null, int[] cityId = null, int[] skills = null, int[] sourceid = null)
        {
            sqlhelper sqlHelper = new sqlhelper();
            leadmodal _mdl = new leadmodal();
            List<leadmodals> _model = new List<leadmodals>();
            SortedList _srt = new SortedList();
            try
            {
                string sp = "tbl_lead_getall_leads_bynotlead";
                if (stateid.HasValue && stateid.Value != 0)
                {
                    _srt.Add("@stateId", stateid);
                }
                if (statusId.HasValue && statusId.Value != 0)
                {
                    _srt.Add("@statusId", statusId);
                    sp = "tbl_lead_getall_leads";
                }
                if (cityId != null)
                {
                    var city = string.Join<int>(",", cityId);
                    _srt.Add("@city", city);
                }
                if (skills != null)
                {
                    var skill = string.Join<int>(",", skills);
                    _srt.Add("@skills", skill);
                }
                if (sourceid != null)
                {
                    var source = string.Join<int>(",", sourceid);
                    _srt.Add("@sourceid", source);
                }
                var dt = sqlHelper.fillDataTable(sp, "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    leadmodals _item = new leadmodals();
                    _item.leadid = Convert.ToInt32(dt.Rows[i]["leadid"].ToString());
                    _item.fullname = dt.Rows[i]["fullname"].ToString().ToTitleCase();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.phone = dt.Rows[i]["phonernumber"].ToString().PhoneNumber();
                    _item.rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.source = dt.Rows[i]["srcname"].ToString();
                    _item.status = dt.Rows[i]["Name"].ToString();
                    _item.city = dt.Rows[i]["cityname"].ToString();
                    _item.state = dt.Rows[i]["statename"].ToString();

                    var items = get_all_skills_byuserid(_item.leadid);
                    if (items.Count > 0)
                    {
                        _item.skills = string.Join(delimater, items.Select(k => k.name).ToArray());
                    }
                    else
                    {
                        _item.skills = "n/a";
                    }
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._leads = _model;
            return _mdl;
        }
    }
}