﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using hrm.Models;

namespace hrm.Database
{
    public class memberservices
    {
        public string getLatestMemberId()
        {
            sqlhelper sqlHelper = new sqlhelper();
            profilemodal _model = new profilemodal();
            string memberid = "";
            try
            {
                memberid = sqlHelper.executeNonQueryWMessage("tbl_member_getlatestmemberid", "", null).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return memberid;
        }


        public void addmembersignupnotification(int newmemberid)
        {
            sqlhelper sqlHelper = new sqlhelper();
            SortedList _list = new SortedList();
            _list.Add("@memberid", newmemberid);
            try
            {
                sqlHelper.executeNonQuery("AddtechsignupNotification", "", _list);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // return memberid;
        }
        public void update_status(int id, int statusid)
        {
            sqlhelper sqlHelper = new sqlhelper();
            profilemodal _model = new profilemodal();
            string memberid = "";
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                _srt.Add("@statusid", statusid);
                sqlHelper.executeNonQuery("tbl_member_update_status", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //return memberid;
        }

        public void acceptterms(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            profilemodal _model = new profilemodal();
            string memberid = "";
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                sqlHelper.executeNonQuery("tbl_member_Accept", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //return memberid;
        }

        public void declineterms(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            profilemodal _model = new profilemodal();
            string memberid = "";
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                sqlHelper.executeNonQuery("tbl_member_decline", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //return memberid;
        }


        public cmplt update_complete(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            cmplt _model = new cmplt();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                var cst = DateTime.UtcNow.AddHours(-6);
                _srt.Add("@date", cst);
                var dt = sqlHelper.fillDataTable("tbl_member_getcomplet_v3", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _model.isddcomple = Convert.ToBoolean(dt.Rows[i]["isbankcomplete"].ToString());
                    _model.ismailcomple = Convert.ToBoolean(dt.Rows[i]["ismailcomplete"].ToString());
                    _model.isrtwcomple = Convert.ToBoolean(dt.Rows[i]["isrtwcomplete"].ToString());
                    _model.isw9compl = Convert.ToBoolean(dt.Rows[i]["isw9complete"].ToString());
                    _model.message = dt.Rows[i]["result"].ToString();

                }
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return _model;
        }


        public profilemodal get_member_profile(int memberid, bool isadmin)
        {
            sqlhelper sqlHelper = new sqlhelper();
            profilemodal _model = new profilemodal();
            SortedList _srt = new SortedList();
            List<jobnotificationmodal> offer = new List<jobnotificationmodal>();
            List<jobnotificationmodal> job = new List<jobnotificationmodal>();
            List<string> _general = new List<string>();
            Crypto _crypt = new Crypto();
            List<string> pay = new List<string>();
            _model.GeneralMessage = _general;
            _srt.Add("@id", memberid);
            try
            {
                var ds = sqlHelper.fillDataSet("tbl_member_getprofile_v2", "", _srt);
                var dt = ds.Tables[0];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _model.image = dt.Rows[i]["image"].ToString();
                    _model.name = dt.Rows[i]["name"].ToString();
                }
                var dt1 = ds.Tables[1];//offer
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    jobnotificationmodal _item = new jobnotificationmodal();
                    _item.jobid = Convert.ToInt32(dt1.Rows[i]["offerid"].ToString());
                    _item.notificationId = Convert.ToInt32(dt1.Rows[i]["id"].ToString());
                    _item.message = dt1.Rows[i]["body"].ToString();
                    _item.name = dt1.Rows[i]["sender"].ToString().ToTitleCase();
                    _item.jobindexid = dt1.Rows[i]["offerindexid"].ToString();
                    _item.time = Convert.ToDateTime(dt1.Rows[i]["Createddate"]).ToString("hh:mm tt");
                    _item.Url = "https://taskayak.com/offer/view?type=2&token=" + _crypt.EncryptStringAES(dt1.Rows[i]["offerid"].ToString());
                    offer.Add(_item);
                }
                var dt2 = ds.Tables[2];//job
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    jobnotificationmodal _item = new jobnotificationmodal();
                    _item.jobid = Convert.ToInt32(dt2.Rows[i]["jobid"].ToString());
                    _item.notificationId = Convert.ToInt32(dt2.Rows[i]["id"].ToString());
                    _item.message = dt2.Rows[i]["body"].ToString();
                    _item.name = dt2.Rows[i]["sender"].ToString().ToTitleCase();
                    _item.jobindexid = dt2.Rows[i]["jobindexid"].ToString();
                    _item.time = Convert.ToDateTime(dt2.Rows[i]["Createddate"]).ToString("hh:mm tt");
                    _item.Url = "https://taskayak.com/jobs/view?type=2&token=" + _crypt.EncryptStringAES(dt2.Rows[i]["jobid"].ToString());

                    job.Add(_item);
                }
                var dt3 = ds.Tables[3];//pay
                for (int i = 0; i < dt3.Rows.Count; i++)
                {
                    _general.Add(dt3.Rows[i]["message"].ToString());
                }
                ds.Dispose();
                dt.Dispose();
                dt1.Dispose();
                dt2.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _model.offerMessage = offer;
            _model.jobMessage = job;
            _model.payMessage = get_paynotification(isadmin ? 0 : memberid);
            return _model;
        }

        public List<paynotificationmodal> get_paynotification(int memberid = 0)
        {
            sqlhelper sqlHelper = new sqlhelper();
            List<paynotificationmodal> _model = new List<paynotificationmodal>();
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            if (memberid != 0)
            {
                _srt.Add("@id", memberid);
            }
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_notification_getalladminpay", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    paynotificationmodal _item = new paynotificationmodal();
                    _item.hrs = dt.Rows[i]["Hoursworked"].ToString();
                    _item.amt = dt.Rows[i]["paymentamount"].ToString().Currency();
                    _item.jobindexid = dt.Rows[i]["jobid"].ToString();
                    _item.name = dt.Rows[i]["member"].ToString().ToTitleCase();
                    _item.payid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.rate = dt.Rows[i]["memberrate"].ToString().Currency(); ;
                    _item.Url = "https://taskayak.com/pay/edit?token=" + _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _model;
        }
        public List<filemodal> get_all_doc_bymemberid(int memberid)
        {
            sqlhelper sqlHelper = new sqlhelper();
            List<filemodal> _model = new List<filemodal>();
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            _srt.Add("@id", memberid);
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_member_doc_get_v2", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    filemodal _item = new filemodal();
                    _item.Filetile = dt.Rows[i]["filename"].ToString();
                    _item.url = dt.Rows[i]["url"].ToString();
                    _item.fileid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.Filetypename = dt.Rows[i]["Filetypename"].ToString();
                    _item.Filetypeid = Convert.ToInt32(dt.Rows[i]["Filetypeid"].ToString());
                    _item.mainorder = Convert.ToInt32(dt.Rows[i]["mainorder"].ToString());
                    _item.filetoken = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

            return _model;
        }

        public List<bank_item> get_all_bank_bymemberid(int memberid)
        {
            sqlhelper sqlHelper = new sqlhelper();
            List<bank_item> _model = new List<bank_item>();
            SortedList _srt = new SortedList();
            _srt.Add("@id", memberid);
            Crypto _crypt = new Crypto();
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_member_bank_getdetails", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    bank_item _item = new bank_item();
                    _item.bankid = Convert.ToInt32(dt.Rows[i]["bankid"].ToString());
                    _item.banktoken = _crypt.EncryptStringAES(dt.Rows[i]["bankid"].ToString());
                    _item.acc_name = dt.Rows[i]["account_name"].ToString();
                    _item.acc_number = dt.Rows[i]["account_number"].ToString();
                    _item.bankname = dt.Rows[i]["name"].ToString();
                    _item.branchname = dt.Rows[i]["branch"].ToString();
                    _item.ifsccode = dt.Rows[i]["ifsc_code"].ToString();
                    _item.pan_number = dt.Rows[i]["pan_number"].ToString();
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

            return _model;
        }


        public string getmembertype(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            string _model = "m";
            SortedList _srt = new SortedList();
            _srt.Add("@id", id);
            Crypto _crypt = new Crypto();
            try
            {
                var dt = sqlHelper.fillDataTable("GetMembertype", "", _srt);
                if (dt.Rows.Count > 0)
                {
                    _model = dt.Rows[0]["type"].ToString();
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

            return _model;

        }

        public int getcompanyid(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            int cmpyid = 0;
            SortedList _srt = new SortedList();
            _srt.Add("@id", id);
            Crypto _crypt = new Crypto();
            try
            {
                var dt = sqlHelper.fillDataTable("getcompanyId", "", _srt);
                if (dt.Rows.Count > 0)
                {
                    cmpyid = Convert.ToInt32(dt.Rows[0]["companyid"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return cmpyid;
        }


        public int getmembermainid(string memberid)
        {
            sqlhelper sqlHelper = new sqlhelper();
            int _model = 0;
            SortedList _srt = new SortedList();
            _srt.Add("@id", memberid);
            Crypto _crypt = new Crypto();
            try
            {
                var dt = sqlHelper.fillDataTable("getmemberidbym_id", "", _srt);
                if (dt.Rows.Count > 0)
                {
                    _model = Convert.ToInt16(dt.Rows[0]["id"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

            return _model;

        }

        public string add_new_member(membermodal _mdl, int userid, bool termsaccepted = false, bool checkbucontracter = false, bool termsrequired = false)
        {
            string message = "User added successfully";
            sqlhelper sqlHelper = new sqlhelper();
            Crypto _crypt = new Crypto();
            string skills = "";
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@fname", _mdl.FirstName);
                _srt.Add("@termsaccepted", termsaccepted);
                _srt.Add("@termsrequired", termsrequired);
                _srt.Add("@checkbucontracter", checkbucontracter);
                _srt.Add("@lname", _mdl.lastname);
                if (_mdl.companyid != 0)
                {
                    _srt.Add("@companyid", _mdl.companyid);
                }
                _srt.Add("@memberid", _mdl.member_id);
                _srt.Add("@username", _mdl.email);
                _srt.Add("@type", _mdl.membertype);
                _srt.Add("@password", _crypt.EncryptStringAES(_mdl.password));
                _srt.Add("@hourlyrate", _mdl.hourlyrate);
                _srt.Add("@tax", _mdl.tax);
                _srt.Add("@statusid", _mdl.status);
                _srt.Add("@deptid", _mdl.depid);
                _srt.Add("@desgntid", _mdl.desid);
                _srt.Add("@position", _mdl.position);
                _srt.Add("@roleid", _mdl.roleid);
                _srt.Add("@managerid", _mdl.Managerid);
                _srt.Add("@dob", _mdl.dob);
                _srt.Add("@gender", _mdl.gender);
                _srt.Add("@bckground", _mdl.background);
                _srt.Add("@drug", _mdl.drugtested);
                _srt.Add("@email", _mdl.email);
                _srt.Add("@phone", _mdl.phone);
                _srt.Add("@alternativephone", _mdl.alertnative_phone);
                _srt.Add("@stateid", _mdl.state_id);
                _srt.Add("@cityid", _mdl.city_id);
                _srt.Add("@zip", _mdl.zip);
                _srt.Add("@address", _mdl.address);
                _srt.Add("@createdby", userid);
                _srt.Add("@company", _mdl.company);
                _srt.Add("@dd", _mdl.drivingdistance);
                if (_mdl.skills != null)
                {
                    skills = string.Join<int>(",", _mdl.skills);
                    _srt.Add("@skills", skills);
                }
                sqlHelper.executeNonQueryWMessage("tbl_member_allnew", "", _srt).ToString();

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public int add_new_membergetid(membermodal _mdl, int userid, bool termsaccepted = false, bool checkbucontracter = false, bool termsrequired = false)
        {
            int message = 0;
            sqlhelper sqlHelper = new sqlhelper();
            Crypto _crypt = new Crypto();
            string skills = "";
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@fname", _mdl.FirstName);
                _srt.Add("@termsaccepted", termsaccepted);
                _srt.Add("@termsrequired", termsrequired);
                _srt.Add("@checkbucontracter", checkbucontracter);
                _srt.Add("@lname", _mdl.lastname);
                if (_mdl.companyid != 0)
                {
                    _srt.Add("@companyid", _mdl.companyid);
                }
                _srt.Add("@memberid", _mdl.member_id);
                _srt.Add("@username", _mdl.email);
                _srt.Add("@type", _mdl.membertype);
                _srt.Add("@password", _crypt.EncryptStringAES(_mdl.password));
                _srt.Add("@hourlyrate", _mdl.hourlyrate);
                _srt.Add("@tax", _mdl.tax);
                _srt.Add("@statusid", _mdl.status);
                _srt.Add("@deptid", _mdl.depid);
                _srt.Add("@desgntid", _mdl.desid);
                _srt.Add("@position", _mdl.position);
                _srt.Add("@roleid", _mdl.roleid);
                _srt.Add("@managerid", _mdl.Managerid);
                _srt.Add("@dob", _mdl.dob);
                _srt.Add("@gender", _mdl.gender);
                _srt.Add("@bckground", _mdl.background);
                _srt.Add("@drug", _mdl.drugtested);
                _srt.Add("@email", _mdl.email);
                _srt.Add("@phone", _mdl.phone);
                _srt.Add("@alternativephone", _mdl.alertnative_phone);
                _srt.Add("@stateid", _mdl.state_id);
                _srt.Add("@cityid", _mdl.city_id);
                _srt.Add("@zip", _mdl.zip);
                _srt.Add("@address", _mdl.address);
                _srt.Add("@createdby", userid);
                _srt.Add("@company", _mdl.company);
                _srt.Add("@dd", _mdl.drivingdistance);
                if (_mdl.skills != null)
                {
                    skills = string.Join<int>(",", _mdl.skills);
                    _srt.Add("@skills", skills);
                }
                message=Convert.ToInt32( sqlHelper.executeNonQueryWMessage("tbl_member_allnew", "", _srt).ToString());

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string add_new_doc(string Filetile, int userid, string url, int _dctype = 0, bool ispopupform = false)
        {
            string message = "Document added successfully";
            sqlhelper sqlHelper = new sqlhelper();

            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@Filetile", Filetile);
                _srt.Add("@ispopupform", ispopupform);
                _srt.Add("@url", url);
                _srt.Add("@createdby", userid);
                _srt.Add("@_dctype", _dctype);
                sqlHelper.executeNonQuery("tbl_member_doc_add_v2", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string delete_doc(int docid)
        {
            string message = "Document deleted successfully";
            sqlhelper sqlHelper = new sqlhelper();

            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@docid", docid);
                sqlHelper.executeNonQuery("tbl_member_doc_delete", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string get_rate(int memberId)
        {
            string message = "$1";
            sqlhelper sqlHelper = new sqlhelper();

            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", memberId);
                message = sqlHelper.executeNonQueryWMessage("tbl_member_getrate", "", _srt).ToString().Currency();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string delete_bank(int bankid)
        {
            string message = "Bank details deleted successfully";
            sqlhelper sqlHelper = new sqlhelper();

            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@bankid", bankid);
                sqlHelper.executeNonQuery("tbl_member_bank_delete", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public getmailing getmailingdetails(int memberid)
        {
            getmailing _mdl = new getmailing();
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", memberid);
                var dt = sqlHelper.fillDataTable("cmprfltbl_member_getmailing", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _mdl.cityid = Convert.ToInt32(dt.Rows[i]["city_id"].ToString());
                    _mdl.stateid = Convert.ToInt32(dt.Rows[i]["state_id"].ToString());
                    _mdl.hrlyrate = dt.Rows[i]["rate"].ToString().Currency();
                    _mdl.travldistance = dt.Rows[i]["traveldistance"].ToString();
                    _mdl.zipcode = dt.Rows[i]["Zip_code"].ToString();
                    _mdl.address = dt.Rows[i]["address"].ToString();
                    _mdl.isddcomple = Convert.ToBoolean(dt.Rows[i]["isbankcomplete"].ToString());
                    _mdl.isrtwcomple = Convert.ToBoolean(dt.Rows[i]["isrtwcomplete"].ToString());
                    _mdl.isw9compl = Convert.ToBoolean(dt.Rows[i]["isw9complete"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _mdl;
        }


        public rtw getrtw(int memberid)
        {
            rtw _mdl = new rtw();
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", memberid);
                var dt = sqlHelper.fillDataTable("cmprfltbl_member_getrtw", "", _srt);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        _mdl.citizen = Convert.ToBoolean(dt.Rows[i]["citizen"].ToString());
                        _mdl.age = Convert.ToBoolean(dt.Rows[i]["age"].ToString());
                        _mdl.auth = Convert.ToBoolean(dt.Rows[i]["auth"].ToString());
                        _mdl.fealony = Convert.ToBoolean(dt.Rows[i]["fealony"].ToString());
                        _mdl.iscomplete = Convert.ToBoolean(dt.Rows[i]["isrtwcomplete"].ToString());
                        _mdl.explainfelony = dt.Rows[i]["explainfelony"].ToString();
                        _mdl.isddcomple = Convert.ToBoolean(dt.Rows[i]["isbankcomplete"].ToString());
                        _mdl.ismailing = Convert.ToBoolean(dt.Rows[i]["ismailcompleted"].ToString());
                        _mdl.isw9compl = Convert.ToBoolean(dt.Rows[i]["isw9complete"].ToString());

                    }
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _mdl;
        }


        public w9 getw9(int memberid)
        {
            w9 _mdl = new w9();
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", memberid);
                var dt = sqlHelper.fillDataTable("cmprfltbl_member_getw9", "", _srt);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //_mdl.fealony = Convert.ToBoolean(dt.Rows[i]["city_id"].ToString());
                        //_mdl.explainfelony = dt.Rows[i]["address"].ToString();
                        //  _mdl.w9id = Convert.ToInt32(dt.Rows[i]["w9id"].ToString());
                        _mdl.entity = Convert.ToInt32(dt.Rows[i]["entity"].ToString());
                        _mdl.EIN1 = dt.Rows[i]["EIN1"].ToString();
                        _mdl.EIN2 = dt.Rows[i]["EIN2"].ToString();
                        _mdl.EIN3 = dt.Rows[i]["EIN3"].ToString();
                        _mdl.EIN4 = dt.Rows[i]["EIN4"].ToString();
                        _mdl.SSN1 = dt.Rows[i]["SSN1"].ToString();
                        _mdl.SSN2 = dt.Rows[i]["SSN2"].ToString();
                        _mdl.SSN3 = dt.Rows[i]["SSN3"].ToString();
                        _mdl.SSN4 = dt.Rows[i]["SSN4"].ToString();
                        _mdl.SSN5 = dt.Rows[i]["SSN5"].ToString();
                        _mdl.Business1 = dt.Rows[i]["Business1"].ToString();
                        _mdl.Business2 = dt.Rows[i]["Business2"].ToString();
                        _mdl.Business3 = dt.Rows[i]["Business3"].ToString();
                        _mdl.Business4 = dt.Rows[i]["Business4"].ToString();
                        _mdl.city = dt.Rows[i]["cityid"].ToString();
                        _mdl.state = dt.Rows[i]["stateid"].ToString();
                        _mdl.Address1 = dt.Rows[i]["Address1"].ToString();
                        _mdl.Address2 = dt.Rows[i]["Address2"].ToString();
                        _mdl.zip = dt.Rows[i]["zip"].ToString();
                        _mdl.isddcomple = Convert.ToBoolean(dt.Rows[i]["isbankcomplete"].ToString());
                        _mdl.ismailing = Convert.ToBoolean(dt.Rows[i]["ismailcompleted"].ToString());
                        _mdl.isrtwcompl = Convert.ToBoolean(dt.Rows[i]["isrtwcomplete"].ToString());


                    }
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _mdl;
        }

        public pp getpp(int memberid)
        {
            pp _mdl = new pp();
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", memberid);
                var dt = sqlHelper.fillDataTable("tbl_member_getpp", "", _srt);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        _mdl.imageurl = dt.Rows[i]["Imageurl"].ToString().Replace("~", "https://taskayak.com").Replace("default", "https://taskayak.com/content/images/default.webp");
                        _mdl.isrtwcomple = Convert.ToBoolean(dt.Rows[i]["isrtwcomplete"].ToString());
                        _mdl.isddcomple = Convert.ToBoolean(dt.Rows[i]["isbankcomplete"].ToString());
                        _mdl.isw9compl = Convert.ToBoolean(dt.Rows[i]["isw9complete"].ToString());
                        _mdl.ismailcomple = Convert.ToBoolean(dt.Rows[i]["ismailcompleted"].ToString());
                    }
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _mdl;

        }

        public dd getdd(int memberid)
        {
            dd _mdl = new dd();
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", memberid);
                var dt = sqlHelper.fillDataTable("cmprfltbl_member_getdd", "", _srt);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        _mdl.bank_authorization = Convert.ToBoolean(dt.Rows[i]["bank_authorization"].ToString());
                        _mdl.Typeofaccount = dt.Rows[i]["Typeofaccount"].ToString();
                        _mdl.bankname = dt.Rows[i]["name"].ToString();
                        _mdl.Accountcitystate = dt.Rows[i]["Accountcitystate"].ToString();
                        _mdl.routingnumber = dt.Rows[i]["routingnumber"].ToString();
                        _mdl.account_number = dt.Rows[i]["account_number"].ToString();
                        _mdl.account_name = dt.Rows[i]["account_name"].ToString();
                        _mdl.isrtwcomple = Convert.ToBoolean(dt.Rows[i]["isrtwcomplete"].ToString());
                        _mdl.ismailing = Convert.ToBoolean(dt.Rows[i]["ismailcompleted"].ToString());
                        _mdl.isw9compl = Convert.ToBoolean(dt.Rows[i]["isw9complete"].ToString());

                    }
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _mdl;
        }


        public void updatw9info(int memberid, w9 _mdl)
        {

            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", memberid);
                _srt.Add("@entity", _mdl.entity);
                _srt.Add("@EIN1", _mdl.EIN1);
                _srt.Add("@EIN2", _mdl.EIN2);
                _srt.Add("@EIN3", _mdl.EIN3);
                _srt.Add("@EIN4", _mdl.EIN4);
                _srt.Add("@SSN1", _mdl.SSN1);
                _srt.Add("@SSN2", _mdl.SSN2);
                _srt.Add("@SSN3", _mdl.SSN3);
                _srt.Add("@SSN4", _mdl.SSN4);
                _srt.Add("@ssn5", _mdl.SSN5);
                _srt.Add("@Business1", _mdl.Business1);
                _srt.Add("@Business2", _mdl.Business2);
                _srt.Add("@Business3", _mdl.Business3);
                _srt.Add("@Business4", _mdl.Business4);
                _srt.Add("@cityid", _mdl.city);
                _srt.Add("@stateid", _mdl.state);
                _srt.Add("@Address1", _mdl.Address1);
                _srt.Add("@Address2", _mdl.Address2);
                _srt.Add("@zip", _mdl.zip);
                sqlHelper.executeNonQuery("cmprfltbl_member_addw9", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            //return _mdl;
        }

        public string updatddinfo(int memberid, dd _mdl)
        {
            string techid = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", memberid);
                //  _srt.Add("@w9id", _mdl.w9id);
                _srt.Add("@bank_authorization", _mdl.bank_authorization);
                _srt.Add("@Typeofaccount", _mdl.Typeofaccount);
                _srt.Add("@bankname", _mdl.bankname);
                _srt.Add("@Accountcitystate", _mdl.Accountcitystate);
                _srt.Add("@routingnumber", _mdl.routingnumber);
                _srt.Add("@account_number", _mdl.account_number);
                _srt.Add("@account_name", _mdl.account_name);
                techid = sqlHelper.executeNonQueryWMessage("cmprfltbl_member_adddd_v2", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return techid;
        }

        public void updatrtwinfo(int memberid, rtw _mdl)
        {

            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", memberid);
                //  _srt.Add("@w9id", _mdl.w9id);
                _srt.Add("@age", _mdl.age);
                _srt.Add("@auth", _mdl.auth);
                _srt.Add("@citizen", _mdl.citizen);
                _srt.Add("@explainfelony", _mdl.explainfelony);
                _srt.Add("@fealony", _mdl.fealony);

                sqlHelper.executeNonQuery("cmprfltbl_member_addrtw", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            //return _mdl;
        }

        public void updatemailinginfo(int memberid, getmailing _mdl)
        {

            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", memberid);
                _srt.Add("@city_id", _mdl.cityid);
                _srt.Add("@state_id", _mdl.stateid);
                _srt.Add("@rate", _mdl.hrlyrate);
                _srt.Add("@traveldistance", _mdl.travldistance);
                _srt.Add("@address", _mdl.address);
                _srt.Add("@Zip_code", _mdl.zipcode);
                sqlHelper.executeNonQuery("cmprfltbl_member_updatemailing", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            //return _mdl;
        }
        public string add_new_member_image(string image, int memberid)
        {
            string message = "Profile image updated successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@image", image);
                _srt.Add("@id", memberid);
                sqlHelper.executeNonQueryWMessage("tbl_member_allimageupdate", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string remove_member(int memberid)
        {
            string message = "User Deleted Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", memberid);
                sqlHelper.executeNonQueryWMessage("tbl_member_remove", "", _srt).ToString();

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string remove_memberstatus(int memberid)
        {
            string message = "User Deleted Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", memberid);
                sqlHelper.executeNonQueryWMessage("tbl_member_remove_status", "", _srt).ToString();

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string update_member(membermodal _mdl, int userid, bool checkcompany)
        {
            string message = "User Updated Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            Crypto _crypt = new Crypto();
            try
            {
                _mdl.roleid = _mdl.membertype == "ST" ? 1012 : _mdl.membertype == "LT" ? 1009 : _mdl.roleid;
                SortedList _srt = new SortedList();
                _srt.Add("@fname", _mdl.FirstName);
                _srt.Add("@lname", _mdl.lastname);
                _srt.Add("@checkcompany", checkcompany);
                _srt.Add("@memberid", _mdl.member_id);
                _srt.Add("@type", _mdl.membertype);
                _srt.Add("@username", _mdl.email);
                _srt.Add("@password", _crypt.EncryptStringAES(_mdl.password));
                _srt.Add("@hourlyrate", _mdl.hourlyrate);
                _srt.Add("@tax", _mdl.tax);
                _srt.Add("@statusid", _mdl.status);
                _srt.Add("@userid", _mdl.userid);
                _srt.Add("@deptid", 0);
                _srt.Add("@desgntid", 0);
                _srt.Add("@roleid", _mdl.roleid);
                _srt.Add("@managerid", _mdl.Managerid);
                _srt.Add("@dob", "");
                _srt.Add("@gender", "M");
                _srt.Add("@bckground", _mdl.background);
                _srt.Add("@drug", _mdl.drugtested);
                _srt.Add("@email", _mdl.email);
                _srt.Add("@phone", _mdl.phone);
                _srt.Add("@alternativephone", _mdl.alertnative_phone);
                _srt.Add("@stateid", _mdl.state_id);
                _srt.Add("@cityid", _mdl.city_id);
                _srt.Add("@zip", _mdl.zip);
                _srt.Add("@address", _mdl.address);
                _srt.Add("@company", _mdl.company);
                _srt.Add("@dd", _mdl.drivingdistance);
                _srt.Add("@createdby", userid);
                sqlHelper.executeNonQueryWMessage("tbl_member_allupdate", "", _srt).ToString();

            }
            catch (Exception exception)
            {
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return message;
        }




        public string update_member_by_member(membermodal _mdl, int userid)
        {
            string message = "Profile Updated Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            Crypto _crypt = new Crypto();
            string skills = "";
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@fname", _mdl.FirstName);
                _srt.Add("@lname", _mdl.lastname);
                _srt.Add("@hourlyrate", _mdl.hourlyrate);
                _srt.Add("@tax", _mdl.tax);
                _srt.Add("@userid", userid);
                _srt.Add("@dob", _mdl.dob);
                _srt.Add("@gender", _mdl.gender);
                _srt.Add("@email", _mdl.email);
                _srt.Add("@phone", _mdl.phone);
                _srt.Add("@alternativephone", _mdl.alertnative_phone);
                _srt.Add("@stateid", _mdl.state_id);
                _srt.Add("@cityid", _mdl.city_id);
                _srt.Add("@zip", _mdl.zip);
                _srt.Add("@address", _mdl.address);
                _srt.Add("@createdby", userid);
                _srt.Add("@company", _mdl.company);
                _srt.Add("@drivingdistance", _mdl.drivingdistance);
                if (_mdl.skills != null)
                {
                    skills = string.Join<int>(",", _mdl.skills);
                    _srt.Add("@skills", skills);
                }
                sqlHelper.executeNonQueryWMessage("tbl_member_allupdate_byuser", "", _srt).ToString();

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string update_member_by_member(int member_id, string membertype, string fname, int clientid, string lname, string email, string phone, string address, string zip, string Position, int manager_id, int userid, int city, int state, int roleid = 0)
        {
            string message = "User added successfully";
            sqlhelper sqlHelper = new sqlhelper();
            Crypto _crypt = new Crypto();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@member_id", member_id);
                _srt.Add("@membertype", membertype);
                _srt.Add("@fname", fname);
                _srt.Add("@clientid", clientid);
                _srt.Add("@lname", lname);
                _srt.Add("@email", email);
                _srt.Add("@phone", phone);
                _srt.Add("@createdby", userid);
                _srt.Add("@address", address);
                _srt.Add("@zip", zip);
                _srt.Add("@Position", Position);
                _srt.Add("@manager_id", manager_id);
                _srt.Add("@city", city);
                _srt.Add("@state", state);
                if (roleid != 0)
                {
                    _srt.Add("@roleid", roleid);
                }
                sqlHelper.executeNonQueryWMessage("tbl_member_allupdate_bycompany", "", _srt).ToString();

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string update_skills_by_member(int[] skills, int userid)
        {
            string message = "Skills updated successfully";
            sqlhelper sqlHelper = new sqlhelper();
            Crypto _crypt = new Crypto();
            string _skills = "";
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@userid", userid);
                _skills = string.Join<int>(",", skills);
                _srt.Add("@skills", _skills);
                sqlHelper.executeNonQueryWMessage("tbl_member_allupdateskills_byuser_v2", "", _srt).ToString();

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string update_tools_by_member(int[] skills, int userid)
        {
            string message = "Tools updated successfully";
            sqlhelper sqlHelper = new sqlhelper();
            Crypto _crypt = new Crypto();
            string _skills = "";
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@userid", userid);
                _skills = string.Join<int>(",", skills);
                _srt.Add("@skills", _skills);
                sqlHelper.executeNonQueryWMessage("tbl_member_allupdatetools_byuser_v2", "", _srt).ToString();

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string update_member_bank(int memberid, string bank_name, string branch_name, string account_name, string account_number, string ifsc_code, string pan_number, int userid)
        {
            string message = "Bank details add successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@name", bank_name);
                _srt.Add("@branch", branch_name);
                _srt.Add("@account_name", account_name);
                _srt.Add("@account_number", account_number);
                _srt.Add("@ifsc_code", ifsc_code);
                _srt.Add("@pan_number", pan_number);
                _srt.Add("@memberid", memberid);
                _srt.Add("@createdby", userid);
                sqlHelper.executeNonQuery("tbl_member_bank_add", "", _srt);

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string update_bank(int bankid, string bank_name, string branch_name, string account_name, string account_number, string ifsc_code, string pan_number, int userid)
        {
            string message = "Bank details updated successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@name", bank_name);
                _srt.Add("@branch", branch_name);
                _srt.Add("@account_name", account_name);
                _srt.Add("@account_number", account_number);
                _srt.Add("@ifsc_code", ifsc_code);
                _srt.Add("@pan_number", pan_number);
                _srt.Add("@bankid", bankid);
                _srt.Add("@createdby", userid);
                sqlHelper.executeNonQuery("tbl_member_bank_update", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public List<int> getMembersByratePayid(int id)
        {
            string minamount = "0"; string maxamount = "200";
            switch (id)
            {
                case 1:
                    minamount = "20";
                    maxamount = "35";
                    break;
                case 2:
                    minamount = "40";
                    maxamount = "50";
                    break;
                case 3:
                    minamount = "55";
                    maxamount = "65";
                    break;
                case 4:
                    minamount = "70";
                    maxamount = "200";
                    break;
            }
            List<int> _l = new List<int>();
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@min", minamount);
                _srt.Add("@max", maxamount);
                var dt = sqlHelper.fillDataTable("tbl_member_getallmembersbyrate", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _l.Add(Convert.ToInt32(dt.Rows[i]["Id"].ToString()));
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return _l;


        }


        public List<skill_items> get_all_skills_byuserid(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            List<skill_items> _model = new List<skill_items>();
            SortedList _srt = new SortedList();
            _srt.Add("@id", id);
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_skills_getallskills_byuid", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    skill_items _item = new skill_items();
                    _item.name = dt.Rows[i]["name"].ToString();

                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

            return _model;
        }
        public members get_all_active_members(string delimater = ",", int state_id = 0, int[] city_id = null, int[] amount = null, string background = "", string drug = "", int status = 0, int[] skills = null)
        {
            sqlhelper sqlHelper = new sqlhelper();
            members _mdl = new members();
            List<member_item> _model = new List<member_item>();
            List<int> id = new List<int>();
            //getMembersByratePayid
            if (amount == null)
            {
                id = getMembersByratePayid(0);
            }
            else
            {
                foreach (var item in amount)
                {
                    id.AddRange(getMembersByratePayid(item));
                }
            }
            try
            {
                SortedList _srt = new SortedList();
                if (state_id != 0)
                {
                    _srt.Add("@state", state_id);
                }
                if (status != 0)
                {
                    _srt.Add("@status", status);
                }
                if (!string.IsNullOrEmpty(background) && background != "all")
                {
                    _srt.Add("@background", background);
                }
                if (!string.IsNullOrEmpty(drug) && drug != "all")
                {
                    _srt.Add("@drug", drug);
                }
                if (city_id != null)
                {
                    var city = string.Join<int>(",", city_id);
                    _srt.Add("@city", city);
                }
                if (skills != null)
                {
                    var skill = string.Join<int>(",", skills);
                    _srt.Add("@skills", skill);
                }
                var dt = sqlHelper.fillDataTable("tbl_member_getallmembers", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    member_item _item = new member_item();
                    _item.memder_p_id = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
                    _item.memberid = dt.Rows[i]["member_id"].ToString();
                    _item.fname = dt.Rows[i]["f_name"].ToString().ToTitleCase();
                    _item.lname = dt.Rows[i]["l_name"].ToString().ToTitleCase();
                    _item.city = dt.Rows[i]["cityname"].ToString();
                    _item.state = dt.Rows[i]["statename"].ToString();
                    _item.phone = dt.Rows[i]["phone"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["EMail"].ToString();
                    _item.hourlyrate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.background = dt.Rows[i]["Background"].ToString();
                    _item.drug = dt.Rows[i]["drug_tested"].ToString();
                    _item.role = dt.Rows[i]["rolename"].ToString();
                    _item.staus = dt.Rows[i]["name"].ToString();
                    _item.skills = "n/a";
                    var items = get_all_skills_byuserid(_item.memder_p_id);
                    if (items.Count > 0)
                    {
                        _item.skills = string.Join(delimater, items.Select(k => k.name).ToArray());
                    }
                    if (id.Contains(_item.memder_p_id))
                    {
                        _model.Add(_item);
                    }
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._members = _model;
            return _mdl;
        }

        public popupmodal GetjobPopupmodal(int jobid)
        {
            popupmodal _model = new popupmodal();
            sqlhelper sqlHelper = new sqlhelper();
            SortedList _srt = new SortedList();
            _srt.Add("@jobid", jobid);
            try
            {
                var dt = sqlHelper.fillDataTable("getjobdetailbyid", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _model.cityid = Convert.ToInt32(dt.Rows[i]["city_id"].ToString());
                    _model.stateid = Convert.ToInt32(dt.Rows[i]["State_id"].ToString());
                    _model.clientid = Convert.ToInt32(dt.Rows[i]["client"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _model;
        }

        public popupmodal GetofferPopupmodal(int jobid)
        {
            popupmodal _model = new popupmodal();
            sqlhelper sqlHelper = new sqlhelper();
            SortedList _srt = new SortedList();
            _srt.Add("@jobid", jobid);
            try
            {
                var dt = sqlHelper.fillDataTable("getofferdetailbyid", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _model.cityid = Convert.ToInt32(dt.Rows[i]["city_id"].ToString());
                    _model.stateid = Convert.ToInt32(dt.Rows[i]["State_id"].ToString());
                   _model.clientid = Convert.ToInt32(dt.Rows[i]["client"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
            }
            finally
            {
                sqlHelper.Dispose();
            }
            return _model;
        }

        public List<member_item> get_all_active_members_byIndex(ref int total, int startIndex, int Endindex, string search = "", string delimater = ",", int state_id = 0, int[] city_id = null, int[] amount = null, string background = "", string drug = "", int status = 0, int[] skills = null, bool phone = true, bool email = true, bool rate = true, string type = "S", int userid = 0, string membertype = "M")
        {
            sqlhelper sqlHelper = new sqlhelper();
            members _mdl = new members();
            List<member_item> _model = new List<member_item>();
            List<int> id = new List<int>();
            Crypto _crypt = new Crypto();
            bool icontactor = type == "S" || type == "LT";
            //getMembersByratePayid
            if (amount.Where(a => a != 0).Count() == 0)
            {
                id = getMembersByratePayid(0);
            }
            else
            {
                foreach (var item in amount.Where(a => a != 0))
                {
                    id.AddRange(getMembersByratePayid(item));
                }
            }
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@startIndex", startIndex);
                _srt.Add("@endIndex", Endindex);
                if (state_id != 0)
                {
                    _srt.Add("@state", state_id);
                }
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                if (!string.IsNullOrEmpty(membertype) && membertype != "M")
                {
                    _srt.Add("@membertype", membertype);
                }
                if (type == "S" || type == "LT")
                {
                    _srt.Add("@companyid", userid);
                }
                string sp = "tbl_member_getallmembers_byindexwotstatus";
                if (status != 0)
                {
                    sp = "tbl_member_getallmembers_byindex";
                    _srt.Add("@status", status);
                }
                if (!string.IsNullOrEmpty(background) && background != "all")
                {
                    _srt.Add("@background", background);
                }
                if (!string.IsNullOrEmpty(drug) && drug != "all")
                {
                    _srt.Add("@drug", drug);
                }
                if (city_id != null)
                {
                    //city_id= city_id.r(str => String.IsNullOrEmpty(str));
                    var city = string.Join<int>(",", city_id.Where(a => a != 0));
                    if (!string.IsNullOrEmpty(city))
                    {
                        _srt.Add("@city", city);
                    }
                }
                if (skills != null)
                {
                    var skill = string.Join<int>(",", skills.Where(a => a != 0));
                    if (!string.IsNullOrEmpty(skill))
                    {
                        _srt.Add("@skills", skill);
                    }
                }

                var dt = sqlHelper.fillDataTable(sp, "", _srt);
                int _all = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    member_item _item = new member_item();
                    _item.acceptrequire = Convert.ToBoolean(dt.Rows[i]["accepttermsrqud"].ToString());
                    _item.decline = Convert.ToBoolean(dt.Rows[i]["decline"].ToString());
                    _item.memder_p_id = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
                    _item.termaccepted = Convert.ToBoolean(dt.Rows[i]["acceptterms"].ToString());
                    _item.memberid = dt.Rows[i]["member_id"].ToString();
                    _item.fname = dt.Rows[i]["f_name"].ToString().ToTitleCase();
                    _item.lname = dt.Rows[i]["l_name"].ToString().ToTitleCase();
                    _item.city = dt.Rows[i]["cityname"].ToString();
                    _item.state = dt.Rows[i]["statename"].ToString();
                    _item.phone = !phone ? "xxx-xxx-xxxx" : dt.Rows[i]["phone"].ToString().PhoneNumber();
                    _item.email = !email ? "xxx" : dt.Rows[i]["EMail"].ToString();
                    _item.hourlyrate = !rate ? "xxx" : dt.Rows[i]["rate"].ToString().Currency();
                    _item.background = dt.Rows[i]["Background"].ToString();
                    _item.drug = dt.Rows[i]["drug_tested"].ToString();
                    _item.role = dt.Rows[i]["rolename"].ToString();
                    _item.staus = dt.Rows[i]["name"].ToString();
                    if (icontactor)
                    {
                        _item.staus = _item.staus != "Inactive" ? "Active" : "Inactive";
                    }
                    _item.skills = "--";
                    _item.staus_id = Convert.ToInt32(dt.Rows[i]["member_status_id"].ToString());
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _item.token = _crypt.EncryptStringAES(dt.Rows[i]["Id"].ToString());
                    var items = get_all_skills_byuserid(_item.memder_p_id);
                    if (items.Count > 0)
                    {
                        _item.skills = string.Join(delimater, items.Select(k => k.name).ToArray());
                    }
                    if (id.Contains(_item.memder_p_id))
                    {
                        _model.Add(_item);
                    }
                    else
                    {
                        _all = _all + 1;
                    }
                }
                total = total - _all;
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //_mdl._members = _model;
            return _model;
        }



        public membermodal get_all_active_members_byid(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            membermodal _item = new membermodal();
            _item.userid = id;
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            List<int> _skills = new List<int>();
            List<int> _skills2 = new List<int>();
            _srt.Add("@id", id);
            try
            {
                var ds = sqlHelper.fillDataSet("tbl_member_getmemberbyid", "", _srt);
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                var dt2 = ds.Tables[2];
                if (dt.Rows.Count > 0)
                {
                    _item.checkbucontracter = Convert.ToBoolean(dt.Rows[0]["havecompany"].ToString());
                    _item.tax = dt.Rows[0]["tax"].ToString().Currency();
                    _item.token = _crypt.EncryptStringAES(id.ToString());
                    _item.membertype = dt.Rows[0]["type"].ToString();
                    _item.companyid = Convert.ToInt32(dt.Rows[0]["companyid"].ToString());
                    _item.status = Convert.ToInt32(dt.Rows[0]["member_status_id"].ToString());
                    _item.depid = Convert.ToInt32(dt.Rows[0]["Department_Id"].ToString());
                    _item.desid = Convert.ToInt32(dt.Rows[0]["Designation_id"].ToString());
                    _item.roleid = Convert.ToInt32(dt.Rows[0]["RoleId"].ToString());
                    _item.Managerid = Convert.ToInt32(dt.Rows[0]["ManagerId"].ToString());
                    _item.city_id = Convert.ToInt32(dt.Rows[0]["city_id"].ToString());
                    _item.ecity = dt.Rows[0]["city"].ToString();
                    _item.state_id = Convert.ToInt32(dt.Rows[0]["state_id"].ToString());
                    _item.zip = dt.Rows[0]["Zip_code"].ToString();
                    _item.address = dt.Rows[0]["address"].ToString();
                    _item.dob = Convert.ToDateTime(dt.Rows[0]["dob"].ToString()).ToString("yyyy-MM-dd");
                    _item.gender = dt.Rows[0]["Gender"].ToString();
                    _item.image = dt.Rows[0]["Imageurl"].ToString();
                    _item.background = dt.Rows[0]["Background"].ToString();
                    _item.drugtested = dt.Rows[0]["drug_tested"].ToString();
                    _item.email = dt.Rows[0]["EMail"].ToString();
                    _item.phone = dt.Rows[0]["Phone"].ToString().PhoneNumber();
                    _item.alertnative_phone = dt.Rows[0]["Alternative_phone"].ToString().PhoneNumber();
                    _item.hourlyrate = dt.Rows[0]["hrlyrate"].ToString().Currency();
                    _item.member_id = dt.Rows[0]["member_id"].ToString();
                    _item.FirstName = dt.Rows[0]["f_name"].ToString().ToTitleCase();
                    _item.lastname = dt.Rows[0]["l_name"].ToString().ToTitleCase();
                    _item.uname = dt.Rows[0]["Username"].ToString();
                    _item.password = _crypt.DecryptStringAES(dt.Rows[0]["Password"].ToString());
                    _item.confirmpassword = _crypt.DecryptStringAES(dt.Rows[0]["Password"].ToString());
                    _item.company = dt.Rows[0]["company"].ToString();
                    _item.drivingdistance = dt.Rows[0]["dd"].ToString();
                    _item.isdd = Convert.ToBoolean(dt.Rows[0]["isbankcomplete"].ToString());
                    _item.isma = Convert.ToBoolean(dt.Rows[0]["ismailcompleted"].ToString());
                    _item.isrtw = Convert.ToBoolean(dt.Rows[0]["isrtwcomplete"].ToString());
                    _item.isw9 = Convert.ToBoolean(dt.Rows[0]["isw9complete"].ToString());
                    _item.termstatus = Convert.ToBoolean(dt.Rows[0]["acceptterms"].ToString());
                    _item.declineterms = Convert.ToBoolean(dt.Rows[0]["declineterms"].ToString());
                    _item.termdate = Convert.ToDateTime(dt.Rows[0]["accepttermsdate"].ToString()).ToString("yyyy-MM-dd");
                    _item.accepttermsrqud = Convert.ToBoolean(dt.Rows[0]["accepttermsrqud"].ToString());

                    if (dt1.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt1.Rows.Count; i++)
                        {
                            _skills.Add(Convert.ToInt32(dt1.Rows[i][0].ToString()));
                        }
                        _item.skills = _skills.ToArray();
                    }

                    if (dt2.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt2.Rows.Count; i++)
                        {
                            _skills2.Add(Convert.ToInt32(dt2.Rows[i][0].ToString()));
                        }
                        _item.tools = _skills2.ToArray();
                    }
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _item.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl._members = _model;
            return _item;
        }


        public membermodal get_members_detail_byid(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            membermodal _item = new membermodal();
            _item.userid = id;
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            _srt.Add("@id", id);
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_member_getmemberdetailbyid", "", _srt);
                if (dt.Rows.Count > 0)
                {
                    _item.Managerid = Convert.ToInt32(dt.Rows[0]["ManagerId"].ToString());
                    _item.member_id = dt.Rows[0]["member_id"].ToString();
                    _item.zip = dt.Rows[0]["Zip_code"].ToString();
                    _item.address = dt.Rows[0]["address"].ToString();
                    _item.email = dt.Rows[0]["EMail"].ToString();
                    _item.position = dt.Rows[0]["position"].ToString();
                    _item.membertype = dt.Rows[0]["type"].ToString();
                    _item.phone = dt.Rows[0]["Phone"].ToString().PhoneNumber();
                    _item.FirstName = dt.Rows[0]["f_name"].ToString().ToTitleCase();
                    _item.lastname = dt.Rows[0]["l_name"].ToString().ToTitleCase();
                    _item.ecity = dt.Rows[0]["city"].ToString();
                    _item.state_id = Convert.ToInt32(dt.Rows[0]["state_id"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _item.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl._members = _model;
            return _item;
        }
    }
}