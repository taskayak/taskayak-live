﻿using hrm.Models;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class prjmanagerservices
    {
        public List<prjmngr_item> get_all_active_prj_byclient(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            prjmngrmodal _mdl = new prjmngrmodal();
            List<prjmngr_item> _model = new List<prjmngr_item>();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                var dt = sqlHelper.fillDataTable("tbl_projectmanager_getallprjs", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    prjmngr_item _item = new prjmngr_item();
                    _item.name = dt.Rows[i]["name"].ToString().ToTitleCase();
                    _item.phone = dt.Rows[i]["Phonenumber"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();

                    _item.prjmanagerid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl.clients = _model;
            return _model;
        }

        public List<prjmngr_item> get_all_active_prj_byclient_byindex(ref int total, int id, int startIndex, int endIndex, string search = "")
        {
            sqlhelper sqlHelper = new sqlhelper();
            prjmngrmodal _mdl = new prjmngrmodal();
            List<prjmngr_item> _model = new List<prjmngr_item>();
            Crypto _crypt = new Crypto();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                _srt.Add("@startIndex", startIndex);
                _srt.Add("@endIndex", endIndex);
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                var dt = sqlHelper.fillDataTable("tbl_projectmanager_getallprjs_by_index", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    prjmngr_item _item = new prjmngr_item();
                    _item.name = dt.Rows[i]["name"].ToString().ToTitleCase();
                    _item.phone = dt.Rows[i]["Phonenumber"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.prjmanagerid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.clientid = id;
                    _item.prj_token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    _item.clt_token = _crypt.EncryptStringAES(id.ToString());
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl.clients = _model;
            return _model;
        }

        public List<companyprjmngr_item> get_all_active_company_byclient_byindex(ref int total, int id, int startIndex, int endIndex, string search = "")
        {
            sqlhelper sqlHelper = new sqlhelper();
            prjmngrmodal _mdl = new prjmngrmodal();
            List<companyprjmngr_item> _model = new List<companyprjmngr_item>();
            Crypto _crypt = new Crypto();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                _srt.Add("@startIndex", startIndex);
                _srt.Add("@endIndex", endIndex);
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                var dt = sqlHelper.fillDataTable("tbl_member_getallcompany_by_index", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    companyprjmngr_item _item = new companyprjmngr_item();
                    _item.name = dt.Rows[i]["name"].ToString().ToTitleCase();
                    _item.phone = dt.Rows[i]["Phonenumber"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.position = dt.Rows[i]["position"].ToString();
                    _item.manager = dt.Rows[i]["manager"].ToString();
                    _item.prjmanagerid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.clientid = id;
                    _item.prj_token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    _item.clt_token = _crypt.EncryptStringAES(id.ToString());
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl.clients = _model;
            return _model;
        }

        public string add_new_manager(prjmngr_item prjectmgr, int userid)
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@name", prjectmgr.name);
                _srt.Add("@clientid", prjectmgr.clientid);
                _srt.Add("@Email", prjectmgr.email);
                _srt.Add("@Phone", prjectmgr.phone);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_manager_add", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string update_manager(prjmngr_item prjectmgr)
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@name", prjectmgr.name);
                _srt.Add("@Email", prjectmgr.email);
                _srt.Add("@Phone", prjectmgr.phone);
                _srt.Add("@managerid", prjectmgr.prjmanagerid);
                message = sqlHelper.executeNonQueryWMessage("tbl_manager_update", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string delete_manager(int managerid)
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@managerid", managerid);
                message = sqlHelper.executeNonQueryWMessage("tbl_manager_remove", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
    }
}