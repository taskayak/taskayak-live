﻿using hrm.Models;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class roleservices
    {

        public List<rolecountmodal> get_role_bycount()
        {
            sqlhelper sqlHelper = new sqlhelper();
            rolemodal _mdl = new rolemodal();
            List<rolecountmodal> _model = new List<rolecountmodal>();
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_role_getrole", "", null);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    rolecountmodal _item = new rolecountmodal();
                    _item.name = dt.Rows[i]["name"].ToString();
                    _item.y = Convert.ToInt32(dt.Rows[i]["count"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return _model;
        }


        public rolemodal get_all_active_roles()
        {
            sqlhelper sqlHelper = new sqlhelper();
            rolemodal _mdl = new rolemodal();
            List<role_items> _model = new List<role_items>();
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_role_getallroles", "", null);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    role_items _item = new role_items();
                    _item.name = dt.Rows[i]["name"].ToString();
                    _item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._roles = _model;
            return _mdl;
        }
        public rolemodal get_all_active_roleswithstatus()
        {
            sqlhelper sqlHelper = new sqlhelper();
            rolemodal _mdl = new rolemodal();
            List<role_items> _model = new List<role_items>();
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_role_getallactiveroles", "", null);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    role_items _item = new role_items();
                    _item.name = dt.Rows[i]["name"].ToString();
                    _item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.status_id = Convert.ToInt32(dt.Rows[i]["statusid"].ToString());
                    _item.status = dt.Rows[i]["status"].ToString();
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._roles = _model;
            return _mdl;
        }
        public Dictionary<int, string> get_all_active_permissions()
        {
            sqlhelper sqlHelper = new sqlhelper();
            Dictionary<int, string> _mdl = new Dictionary<int, string>();
            // List<role_items> _model = new List<role_items>()
            string name = "";
            int id = 0;
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_permissions_getall", "", null);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    // role_items _item = new role_items();
                    name = dt.Rows[i]["name"].ToString();
                    id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _mdl.Add(id, name);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //   _mdl._roles = _model;
            return _mdl;
        }
        public List<permissionitem> get_all_active_permissions_with_type()
        {
            sqlhelper sqlHelper = new sqlhelper();
            List<permissionitem> _mdl = new List<permissionitem>();

            // List<role_items> _model = new List<role_items>()
            string name = "";
            string type = "";
            int id = 0;
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_permissions_getall", "", null);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    permissionitem _itm = new permissionitem();              // role_items _item = new role_items();
                    _itm.name = dt.Rows[i]["name"].ToString();
                    _itm.type = dt.Rows[i]["type"].ToString();
                    _itm.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _mdl.Add(_itm);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //   _mdl._roles = _model;
            return _mdl;
        }
        public string add_new_role(rolemodal client, int userid)
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@rolename", client.role_name);
                _srt.Add("@statusid", client.statusid);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_role_addnew", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string update_role(rolemodal client, int userid)
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@rolename", client.role_name);
                _srt.Add("@statusid", client.statusid);
                _srt.Add("@roleid", client.role_id);
                message = sqlHelper.executeNonQueryWMessage("tbl_role_update", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }


        public string remove_role(int roleid)
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@roleid", roleid);
                message = sqlHelper.executeNonQueryWMessage("tbl_role_remove", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string getmembertype(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            string _model = "m";
            SortedList _srt = new SortedList();
            _srt.Add("@id", id);
            Crypto _crypt = new Crypto();
            try
            {
                var dt = sqlHelper.fillDataTable("GetMembertype", "", _srt);
                if (dt.Rows.Count > 0)
                {
                    _model = dt.Rows[0]["type"].ToString();
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }

            return _model;

        }

        public List<int> get_all_active_permissions_byroleid(int roleid, ref string name)
        {
            sqlhelper sqlHelper = new sqlhelper();
            List<int> _mdl = new List<int>();
            SortedList _srt = new SortedList();
            int id = 0;
            try
            {
                _srt.Add("@roleid", roleid);
                var ds = sqlHelper.fillDataSet("tbl_permissions_get_byroleid", "", _srt);
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    id = Convert.ToInt32(dt.Rows[i]["PermissionId"].ToString());
                    _mdl.Add(id);
                }
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    name = dt1.Rows[i]["rolename"].ToString();
                }
                dt.Dispose();
                dt1.Dispose();
                ds.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //   _mdl._roles = _model;
            return _mdl;
        }

        public List<int> get_all_active_permissions_bytypeid(int typeid, int roleid)
        {
            sqlhelper sqlHelper = new sqlhelper();
            List<int> _mdl = new List<int>();
            SortedList _srt = new SortedList();
            int id = 0;
            try
            {
                _srt.Add("@typeid", typeid);
                _srt.Add("@roleid", roleid);
                var dt = sqlHelper.fillDataTable("tbl_permissions_get_byroleid_typeid", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    id = Convert.ToInt32(dt.Rows[i]["PermissionId"].ToString());
                    _mdl.Add(id);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //   _mdl._roles = _model;
            return _mdl;
        }

        public bool get_all_active_permissions_bypermissionId(int pid, int roleid)
        {
            sqlhelper sqlHelper = new sqlhelper();
            bool _mdl = false;
            SortedList _srt = new SortedList();
            int id = 0;
            try
            {
                _srt.Add("@pid", pid);
                _srt.Add("@roleid", roleid);
                _mdl = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("tbl_notification_tbl_permissions_get_byroleid_typeid", "", _srt).ToString()) > 0 ? true : false;

              //  dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                // _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            //   _mdl._roles = _model;
            return _mdl;
        }
        public string update_role_permissions(int[] permissionids, int roleid)
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@permissionids", string.Join<int>(",", permissionids));
                _srt.Add("@roleid", roleid);
                message = sqlHelper.executeNonQueryWMessage("tbl_role_permission_update", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string delete_role_permissions(int roleid)
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@roleid", roleid);
                message = sqlHelper.executeNonQueryWMessage("tbl_role_permission_update", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

    }
}