﻿using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class skillservices
    {
        public skill get_all_active_skills()
        {
            sqlhelper sqlHelper = new sqlhelper();
            skill _mdl = new skill();
            List<skill_items> _model = new List<skill_items>();
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_skill_master_getallskills", "", null);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    skill_items _item = new skill_items();
                    _item.name = dt.Rows[i]["name"].ToString();
                    _item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._skill = _model;
            return _mdl;
        }


        public skill get_all_active_tools()
        {
            sqlhelper sqlHelper = new sqlhelper();
            skill _mdl = new skill();
            List<skill_items> _model = new List<skill_items>();
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_tools_master_getalltools", "", null);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    skill_items _item = new skill_items();
                    _item.name = dt.Rows[i]["name"].ToString();
                    _item.typename = dt.Rows[i]["type"].ToString();
                    _item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.typeid = Convert.ToInt32(dt.Rows[i]["typeid"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._skill = _model;
            return _mdl;
        }

        public List<skill_items> get_all_skills_byuserid(int id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            List<skill_items> _model = new List<skill_items>();
            SortedList _srt = new SortedList();
            _srt.Add("@id", id);
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_skills_getallskills_byuid", "", _srt);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    skill_items _item = new skill_items();
                    _item.name = dt.Rows[i]["name"].ToString();
                   
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //_mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
           
            return _model;
        }

        public string add_new_skill(string skill, int userid)
        {
            string message = "Skill Added Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@skill", skill);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_skill_master_addskills", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string update_skill(string skill, int skillid, int userid)
        {
            string message = "Skill Updated Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@skill", skill);
                _srt.Add("@createdby", userid);
                _srt.Add("@skillid", skillid);
                message = sqlHelper.executeNonQueryWMessage("tbl_skill_master_updateskill", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string delete_skill(int skillid, int userid)
        {
            string message = "Skill Added Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@skillid", skillid);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_skill_master_removeskill", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
    }

}