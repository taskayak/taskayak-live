﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public static class statichelper
    {
        public static string PhoneNumber(this string value)
        {
            string oldvalue = value;
            if (!string.IsNullOrEmpty(value))
            {
                value = value.Replace("-", "");
                value = value.Replace(".", "");
                value = value.Replace("(", "");
                value = value.Replace(")", "");
                value = new System.Text.RegularExpressions.Regex(@"\D")
                    .Replace(value, string.Empty);
                if (string.IsNullOrEmpty(value))
                {
                    value = oldvalue;
                }
                else
                {
                    value = value.TrimStart('1');
                    if (value.Length == 7)
                        return Convert.ToInt64(value).ToString("###-####");
                    if (value.Length == 10)
                        return Convert.ToInt64(value).ToString("###-###-####");
                    if (value.Length > 10)
                        return Convert.ToInt64(value)
                            .ToString("###-###-#### " + new String('#', (value.Length - 10)));
                }
            }
            else
            {
                value = "###-###-####";
            }

            return value;
        }

        public static string Currency(this string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                value = value.Replace("$", "");
                try
                {
                    var new_rate = Convert.ToDouble(value);
                    value = String.Format(new CultureInfo("en-US"),"{0:C}", new_rate);
                }
                catch
                {

                }
            }
            else
            {
                value = "$0.00";
            }
            return value;
        }

        public static string ToTitleCase(this string str)
        {
            string result = str;
            if (!string.IsNullOrEmpty(str))
            {
                var words = str.Split(' ');
                for (int index = 0; index < words.Length; index++)
                {
                    var s = words[index];
                    if (s.Length > 0)
                    {
                        words[index] = s[0].ToString().ToUpper() + s.Substring(1);
                    }
                }
                result = string.Join(" ", words);
            }
            return result;
        }

        public static string GetAddress(this string street,string city, string state, string zip)
        {
            string address = "";
            if(!string.IsNullOrEmpty(street))
            {
                address = street;
            }
            if (!string.IsNullOrEmpty(city))
            {
                address = address+", " +city;
            }
            if (!string.IsNullOrEmpty(state))
            {
                address = address +", " + state;
            }
            if (!string.IsNullOrEmpty(zip))
            {
                address = address +" "+ zip;
            }
            return address;
        }
     
        public static string MailMergeBytext(this string text, string datetime = "", string date = "", string StartTime = "", string EndTime = "", string JobId = "", string JobTitle = "", string Price = "", string Description = "", string TechfirstName = "", string TechllastName = "", string TechfullName = "", string TechCity = "", string jobCity = "", string jobState = "", string TechState = "", string JobAddress = "", string AcceptLink = "",string techrate="",string techPW="",string techuser="",string techemail="",string techID="",string declinelink="")
        {
            text = text.Replace("{{DeclineLink}}", declinelink).Replace("{{AcceptLink}}", AcceptLink).Replace("{{JobAddress}}", JobAddress).Replace("{{TechState}}", TechState).Replace("{{jobState}}", jobState).Replace("{{jobCity}}", jobCity).Replace("{{TechCity}}", TechCity).Replace("{{TechfullName}}", TechfullName).Replace("{{TechllastName}}", TechllastName).Replace("{{TechfirstName}}", TechfirstName).Replace("{{Description}}", Description).Replace("{{Price}}", Price).Replace("{{techrate}}", techrate).Replace("{{techPW}}", techPW).Replace("{{techID}}", techID).Replace("{{techuser}}", techuser).Replace("{{techemail}}", techemail).Replace("{{JobTitle}}", JobTitle).Replace("{{JobId}}", JobId).Replace("{{date}}", date).Replace("{{StartTime}}", StartTime).Replace("{{EndTime}}", StartTime).Replace("{{datetime}}", datetime);
            return text;
        }
        public static string Validate(string EncodedResponse)
        {
            var client = new System.Net.WebClient();

            string PrivateKey = "6LfFi7sUAAAAAL3IgLuZRVjKdskSdd_PJk5Dk4_W";

            var GoogleReply = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", PrivateKey, EncodedResponse));

            var captchaResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<ReCaptchaClass>(GoogleReply);

            return captchaResponse.Success.ToLower();
        }
      
    }
    public class ReCaptchaClass
    {

        [JsonProperty("success")]
        public string Success
        {
            get { return m_Success; }
            set { m_Success = value; }
        }

        private string m_Success;
        [JsonProperty("error-codes")]
        public List<string> ErrorCodes
        {
            get { return m_ErrorCodes; }
            set { m_ErrorCodes = value; }
        }


        private List<string> m_ErrorCodes;
    }
}