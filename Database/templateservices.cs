﻿using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class templateservices
    {
        public templateModal get_all_active_template(int? id)
        {
            sqlhelper sqlHelper = new sqlhelper();
            templateModal _mdl = new templateModal();
            List<templateitem> _model = new List<templateitem>();
            try
            {
                SortedList _srt = new SortedList();
                if (id.HasValue)
                {
                    _srt.Add("@id", id.Value);
                }
                var ds = sqlHelper.fillDataSet("tbl_template_getalltemplate", "", _srt);
                var dt = ds.Tables[0];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    templateitem _item = new templateitem();
                    _item.email = dt.Rows[i]["email"].ToString();
                    _item.sms = dt.Rows[i]["sms"].ToString();
                    _item.isdefault = Convert.ToBoolean(dt.Rows[i]["isdefault"].ToString());
                    _item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.title = dt.Rows[i]["title"].ToString();
                    _model.Add(_item);
                }
                dt.Dispose();
                ds.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            _mdl._template = _model;
            return _mdl;
        }

        public string add_new_template(templateitem notice, int userid)
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@title", notice.title);
                _srt.Add("@sms", notice.sms);
                _srt.Add("@email", notice.email);
                _srt.Add("@default", notice.isdefault);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_template_addnew", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }


        public string update_template(templateitem template, int userid)
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@title", template.title);
                _srt.Add("@sms", template.sms);
                _srt.Add("@email", template.email);
                _srt.Add("@default", template.isdefault);
                _srt.Add("@id", template.id);
                message = sqlHelper.executeNonQueryWMessage("tbl_template_update", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public templateitem get_template(int id)
        {
            templateitem _item = new templateitem();
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                int i = 0;
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
               var dt= sqlHelper.fillDataTable("tbl_template_getalltemplate", "", _srt);
                if(dt.Rows.Count>0)
                {
                    _item.email = dt.Rows[i]["email"].ToString();
                    _item.sms = dt.Rows[i]["sms"].ToString();
                    _item.isdefault = Convert.ToBoolean(dt.Rows[i]["isdefault"].ToString());
                    _item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.title = dt.Rows[i]["title"].ToString();
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                _item.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return _item;
        }

        public string update_default(int id, bool status)
        {
            string message = "Template Updated Succesfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                _srt.Add("@status", status);
                sqlHelper.executeNonQuery("tbl_template_updatedefault", "", _srt);
            }
            catch (Exception exception)
            {
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }




        public string delete_template(int id)
        {
            string message = "";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@id", id);
                message = sqlHelper.executeNonQueryWMessage("tbl_template_removetemplate", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
    }
}