﻿using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class toolservices
    {
        public List<tooltype_item> get_all_active_types()
        {
            sqlhelper sqlHelper = new sqlhelper();
         
            List<tooltype_item> _mdl = new List<tooltype_item>();
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_tooltype_getalltype_v2", "", null);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    tooltype_item _item = new tooltype_item();
                    _item.name = dt.Rows[i]["name"].ToString();
                    _item.typeid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.mainorder = Convert.ToInt32(dt.Rows[i]["mainorder"].ToString());
                    _mdl.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
              //  _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
          // _mdl._dept = _model;
            return _mdl;
        }

        public List<tools_item> get_all_active_tools()
        {
            sqlhelper sqlHelper = new sqlhelper();
            List<tools_item> _mdl = new List<tools_item>();
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_tool_getalltools", "", null);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    tools_item _item = new tools_item();
                    _item.name = dt.Rows[i]["name"].ToString();
                    _item.toolid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.typeid = Convert.ToInt32(dt.Rows[i]["TypeId"].ToString());
                    _item.typename = dt.Rows[i]["Type"].ToString();
                    _mdl.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //  _mdl.error_text = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl._dept = _model;
            return _mdl;
        }

        public string add_new_type(string name, int userid,int mainorder)
        {
            string message = "Tool Type Added Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@deptName", name);
                _srt.Add("@mainorder", mainorder);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_tbltooltype_addtype", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string add_new_tool(string name,int typeid, int userid)
        {
            string message = "Tool Added Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@deptName", name);
                _srt.Add("@typeid", typeid);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_tbltool_addtool", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string add_new_toolbytypename(string name, string typeid, int userid)
        {
            string message = "Tool added successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@deptName", name);
                _srt.Add("@typename", typeid);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_tbltool_addtoolbyuser", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public string update_tool(string name, int typeid,int toolid)
        {
            string message = "Tool Updated Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@name", name);
                _srt.Add("@typeid", typeid);
                _srt.Add("@toolid", toolid);
                message = sqlHelper.executeNonQueryWMessage("tbl_tbltool_updatetool", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string update_type(string deptname, int deptid, int userid,int mainorder)
        {
            string message = "Tool Type Updated Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@deptName", deptname);
                _srt.Add("@createdby", userid);
                _srt.Add("@deptid", deptid);
                     _srt.Add("@mainorder", mainorder);
                message = sqlHelper.executeNonQueryWMessage("tbl_tooltype_updatetype", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
        public string delete_type(int deptid, int userid)
        {
            string message = "Tools type Deleted Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@deptid", deptid);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_tooltype_removetype", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }

        public int getmaxorder()
        {
            int order = 1;
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                order = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("tbl_toolytype_getmaxdept", "", _srt).ToString());
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                //message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return order;
        }
        public string delete_tools(int deptid, int userid)
        {
            string message = "Tools Deleted Successfully";
            sqlhelper sqlHelper = new sqlhelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@deptid", deptid);
                _srt.Add("@createdby", userid);
                message = sqlHelper.executeNonQueryWMessage("tbl_tools_removetools", "", _srt).ToString();
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = "Error:" + exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
    }
}