﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace hrm.Models
{
    public class clientmodal : ErrorModel
    {
        public List<clientmodal_item> clients { get; set; }
       // public List<prjmngr_item> _prjlist { get; set; }
    }

    public class clientviewmodal : ErrorModel
    {
        public string cl_token { get; set; }
        public int clientid { get; set; }
        public clientmodal_item clients { get; set; }
        public List<prjmngr_item> _prjlist { get; set; }
    }

    public class clientmodal_item : ErrorModel
    {
        [Required(ErrorMessage = "Please enter name.", AllowEmptyStrings = false)]
        public string name { get; set; }
        public string primarycontactname { get; set; }
        public string rate { get; set; }
        public string address { get; set; }
        public int city_id { get; set; }
        public int clientid { get; set; }
        public int state_id { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipcode { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string token { get; set; }
        public string HubSpotId { get; set; }
    }


    public class companyviewmodal : ErrorModel
    {
        public string cl_token { get; set; }
        public int clientid { get; set; }
        public companymodal_item clients { get; set; }
        public List<companyprjmngr_item> _prjlist { get; set; }
    }

    public class compnymodal : ErrorModel
    {
        public List<companymodal_item> clients { get; set; }
        // public List<prjmngr_item> _prjlist { get; set; }
    }
    public class companymodal_item : ErrorModel
    {
        [Required(ErrorMessage = "Please enter name.", AllowEmptyStrings = false)]
        public string name { get; set; }
        public string website { get; set; }
        public string primarycontactname { get; set; }
        public string rate { get; set; }
        public string address { get; set; }
        public int city_id { get; set; }
        public int clientid { get; set; }
        public int state_id { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipcode { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string token { get; set; }
    }
}