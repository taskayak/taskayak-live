﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Models
{
    public class dashboardmodal:ErrorModel
    {
        public int member { get; set; }
        public int jobs { get; set; }
        public string monthuypay { get; set; }
        public int leads { get; set; }
        public List<dashboardjobs> _jobs { get; set; }
        public List<ratejobs> _pay { get; set; }
    }

    public class dashboardjobs
    {
        public string Jobid { get; set; }
        public string Ttile { get; set; }
        public string Technician { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
    }

    public class ratejobs
    {
        public string rate { get; set; }
        public string Hoursworked { get; set; }
        public string member { get; set; }
        public string date { get; set; }
        public string name { get; set; }
    }

}