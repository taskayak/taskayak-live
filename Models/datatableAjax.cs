﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Models
{
    public class search
    {
        public string value { get; set; }
        public string regex { get; set; }
    }
    public class datatableAjax<T>
        {
            public List<T> data
            {
                get;
                set;
            }

            public int draw
            {
                get;
                set;
            }

            public int recordsFiltered
            {
                get;
                set;
            }

            public int recordsTotal
            {
                get;
                set;
            }
        }
}