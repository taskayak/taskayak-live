﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace hrm.Models
{
    public class department : ErrorModel
    {
        public List<department_items> _dept { get; set; }
        [Required(ErrorMessage = "Please enter name.", AllowEmptyStrings = false)]
        public string dep_name { get; set; }
    }

    public class department_items
    {
        public string name { get; set; }
        public int id { get; set; }
    }
}