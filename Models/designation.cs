﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace hrm.Models
{
    
    public class designation : ErrorModel
    {
        public List<designation_items> _desgnation { get; set; }
        [Required(ErrorMessage = "Please enter name.", AllowEmptyStrings = false)]
        public string desgnation_name { get; set; }
        public int deptid { get; set; }
        public List<department_items> _depts { get; set; }
    }

    public class designation_items
    {
        public string name { get; set; }
        public string _dep_name { get; set; }
        public int id { get; set; }
        public int deptid { get; set; }
    }
}