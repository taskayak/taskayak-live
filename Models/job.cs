﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace hrm.Models
{
    public class job : ErrorModel
    {
        public string token { get; set; }
        public int _jobid { get; set; }
        public List<job_items> _jobs { get; set; }
        [Required(ErrorMessage = "Please enter job id", AllowEmptyStrings = false)]

        public string JobId { get; set; }
        [Required(ErrorMessage = "Please enter job title", AllowEmptyStrings = false)]

        public string title { get; set; }
        [Required(ErrorMessage = "Please enter client", AllowEmptyStrings = false)]
        public List<commentmodal> _Comments { get; set; }
        public int client { get; set; }
        public bool clr1 { get; set; }
        public bool clr2 { get; set; }
        public int client_id { get; set; }
        public int mgr_id { get; set; }
        public int mgr_id1 { get; set; }
        public string description { get; set; }
        public int Technician { get; set; }
        public int Dispatcher { get; set; }
        public int city_id { get; set; }
        public int state_id { get; set; }
        [Required(ErrorMessage = "Please enter project manager", AllowEmptyStrings = false)]

        public string Project_manager { get; set; }
        [Required(ErrorMessage = "Please enter job date", AllowEmptyStrings = false)]

        public string sdate { get; set; }
        public string edate { get; set; }

        [Required(ErrorMessage = "Please enter job start time", AllowEmptyStrings = false)]

        public string stime { get; set; }

        [Required(ErrorMessage = "Please enter job end time", AllowEmptyStrings = false)]

        public string etime { get; set; }


        [Required(ErrorMessage = "Please enter client rate", AllowEmptyStrings = false)]


        public string Client_rate { get; set; }
        public string Pay_rate { get; set; }

        public string street { get; set; }
        public string zip { get; set; }
        public double Est_hours { get; set; }
        public double Hours { get; set; }
        public string Expense { get; set; }
        public int Status { get; set; }
        ///mail merge items
        public string datetime { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string address { get; set; }

    }
    public class commentModel
    {
        public string createdBy { get; set; }
        public string text { get; set; }
        public string timedate { get; set; }
    }
    public class jobmodal : ErrorModel
    {
        public List<job_items> _jobs { get; set; }
    }

    public class job_items
    {
        public string Job_ID { get; set; }
        public int commentCount { get; set; }
        public string token { get; set; }
        public int JobID { get; set; }
        public int status_id { get; set; }
        public string Title { get; set; }
        public string Client { get; set; }
        public string cityname { get; set; }
        public string statename { get; set; }
        public string Technician { get; set; }
        public string Dispatcher { get; set; }
        public string Project_manager { get; set; }
        public string sdate { get; set; }
        public string stime { get; set; }
        public string etime { get; set; }
        public string endate { get; set; }

        public DateTime _sdate { get; set; }
        public DateTime _endate { get; set; }
        public string Client_rate { get; set; }
        public string Pay_rate { get; set; }
        public string Est_hours { get; set; }
        public string hours { get; set; }
        public string Expense { get; set; }
        public string Status { get; set; }
        public int view { get; set; }
        public int accepted { get; set; }
    }

    public class job_items1 : ErrorModel
    {
        public string Job_ID { get; set; }
        public int acceptedCount { get; set; }
        public int msgcount { get; set; }
        public int unreadcount { get; set; }
        public string token { get; set; }
        public string distoken { get; set; }
        public string tectoken { get; set; }
        public string address { get; set; }
        public string filetitle { get; set; }
        public int JobID { get; set; }
        public string Title { get; set; }
        public string stime { get; set; }
        public string etime { get; set; }
        public string description { get; set; }
        public string Client { get; set; }
        public int mgr_id1 { get; set; }
        public int Client_id { get; set; }
        public int mgr_id { get; set; }
        public string Technician { get; set; }
        public int Technicianid { get; set; }
        public string Dispatcher { get; set; }
        public int Dispatcherid { get; set; }
        public string Project_manager { get; set; }
        public string sdate { get; set; }
        public string endate { get; set; }
        public string Client_rate { get; set; }
        public string pay_rate { get; set; }
        public string Est_hours { get; set; }
        public int Status { get; set; }
        public string Expense { get; set; }
        public string Comment { get; set; }
        public List<commentmodal> _Comments { get; set; }
        public List<commentmodal> _childComments { get; set; }
        public List<filemodal> _files { get; set; }
        public List<offermodal> _offers { get; set; }
    }

    public class commentmodal
    {
        public string Comment { get; set; }
        public int Commentid { get; set; }
        public int Parentid { get; set; }
        public int createdbyId { get; set; }
        public bool isread { get; set; }
        public string createdby { get; set; }
        public string CreatedDate { get; set; }
        public int Commentby { get; set; }
        public int Commentto { get; set; }
        public bool isadminpost { get; set; }
    }

    public class filemodal
    {
        public string Filetypename { get; set; }
        public int Filetypeid { get; set; }
        public int mainorder { get; set; }
        public string Filetile { get; set; }
        public string url { get; set; }
        public string createdby { get; set; }
        public string Size { get; set; }
        public string Createddate { get; set; }
        public int fileid { get; set; }
        public string filetoken { get; set; }
    }


    public class offermodal
    {
        public string offertoken { get; set; }
        public string jobtoken { get; set; }
        public string createddate { get; set; }
        public string jobindexid { get; set; }
        public int jobid { get; set; }
        public int id { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string tech { get; set; }
        public string rate { get; set; }
        public string status { get; set; }
    }


}