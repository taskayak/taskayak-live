﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace hrm.Models
{
    public class leadmodal:ErrorModel
    {
        public List<leadmodals> _leads { get; set; }
        public List<skill_items> _skills { get; set; }
    }
    public class jsonmainleadModal
    {
        public jsonleadModal data { get; set; }
        public bool success { get; set; }

    }

    public class jsonleadModal
    {
        public string website { get; set; }
        public jsonprofileModal profile { get; set; }
      
    }

    public class jsonprofileModal
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string city { get; set; }
        public string linkedinUrl { get; set; }
        public string companyname { get; set; }
        //public jsonrateModal rates { get; set; }
    }
    public class jsonrateModal
    {
        [JsonProperty("Parts Swap")]
        public int PartsSwap { get; set; }
        [JsonProperty("Install/Setup")]
        public int setup { get; set; }
        [JsonProperty("Diagnose & Repair")]
        public int Diagnose { get; set; }
    }


    public class leadmodals
    {
        public int leadid { get; set; }
        public string token { get; set; }
        public int status_id { get; set; }
        public string fullname { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string rate { get; set; }
        public string source { get; set; }
        public string status { get; set; }
        public string skills { get; set; }
        public string Company { get; set; }
        public string Website { get; set; }
        public string LinkedIn { get; set; }
    }

    public class leadmodal_item : ErrorModel
    {
        public int leadId { get; set; }
        [Required(ErrorMessage = "Please enter name.", AllowEmptyStrings = false)]
        public string firstname { get; set; }
        public string lastname { get; set; }
        [Required(ErrorMessage = "Please enter email", AllowEmptyStrings = false)]
        public string email { get; set; }
        public string city { get; set; }
        public string phone { get; set; }
        [Required(ErrorMessage = "Please enter rate", AllowEmptyStrings = false)]
        public string rate { get; set; }
        public string _token { get; set; }
        public int city_id { get; set; }
        public int[] skill { get; set; }
        public int state_id { get; set; }
        public int srcid { get; set; }
        public int statusid { get; set; }
        public List<skill_items> _skills { get; set; }
        public List<commentmodal> _Comments { get; set; }
        public List<filemodal> _files { get; set; }
        public string comment { get; set; }

        public string Company { get; set; }
        public string Website { get; set; }
        public string LinkedIn { get; set; }
    }

}