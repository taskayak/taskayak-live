﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace hrm.Models
{
    public class loginmodal : ErrorModel
    {
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 2)]
        [Required(ErrorMessage = "Please enter username.", AllowEmptyStrings = false)]
        public string username { get; set; }
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [Required(ErrorMessage = "Please enter password.", AllowEmptyStrings = false)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        public string password { get; set; }
        public bool rememberme { get; set; }
        public string returnUrl
        {
            get;
            set;
        }
    }
}