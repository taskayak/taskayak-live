﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace hrm.Models
{
    public class membermodal:ErrorModel
    {
      
        public string position { get; set; }
        public string ecity { get; set; }
        public int companyid { get; set; }
        public bool checkbucontracter { get; set; }
        public bool termstatus { get; set; }
        public string termdate { get; set; }
        public bool declineterms { get; set; }
        public bool accepttermsrqud { get; set; }
        public List<doctype_item> _dctype { get; set; }
        [Required(ErrorMessage = "Please enter first name", AllowEmptyStrings = false)]

        public string FirstName { get; set; }
        public string token { get; set; }
        public string lastname { get; set; }

        [Required(ErrorMessage = "Please enter user id", AllowEmptyStrings = false)]

        public string member_id { get; set; }
        public string address { get; set; }
        public int state_id { get; set; }
        public int city_id { get; set; }
        public string zip { get; set; }
        [Required(ErrorMessage = "Please enter email", AllowEmptyStrings = false)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string email { get; set; }

        [Required(ErrorMessage = "Please enter phone number", AllowEmptyStrings = false)]

        public string phone { get; set; }
        public string alertnative_phone { get; set; }
        public string hourlyrate { get; set; }
        public int[] skills { get; set; }
        public int[] tools { get; set; }
        public string background { get; set; }
        public string membertype { get; set; }
        public string drugtested { get; set; }
        public string gender { get; set; }
        public string dob { get; set; }
        public int Managerid { get; set; }
        public int depid { get; set; }
        public int desid { get; set; }
        public int roleid { get; set; }
        [Required(ErrorMessage = "Please enter username", AllowEmptyStrings = false)]

        public string uname { get; set; }
        
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        [Required(ErrorMessage = "Please enter password", AllowEmptyStrings = false)]
        public string password { get; set; }

        [Compare("password", ErrorMessage = "The password and confirm password does not match")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]

        public string confirmpassword { get; set; }
        public string tax { get; set; }
        public int userid { get; set; }
        public int status { get; set; }
        public string image { get; set; }
        public List<department_items> _dept { get; set; }
        public List<designation_items> _desgnation { get; set; }
        public List<role_items> _roles { get; set; }
        public List<skill_items> _skills { get; set; }
        public List<skill_items> _tools { get; set; }
        public List<filemodal> _docs { get; set; }
        public List<bank_item> _bank { get; set; }
        public string _mcity { get; set; }
        public string _mstate { get; set; }
        public string company { get; set; }
        public string drivingdistance { get; set; }
        public bool isma { get; set; }
        public bool isrtw { get; set; }
        public bool isw9 { get; set; }
        public bool isdd { get; set; }
    }
    public class members:ErrorModel
    {
        public List<member_item> _members { get; set; }
        public List<skill_items> _skills { get; set; }
    }
    public class resetmodal:ErrorModel
    {
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        [Required(ErrorMessage = "Please enter password", AllowEmptyStrings = false)]
        public string password { get; set; }

        [Compare("password", ErrorMessage = "The password and confirm password does not match")]
        [DataType(DataType.Password)]
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]

        public string confirmpassword { get; set; }
    }
    public class member_item
    {
        public string memberid { get; set; }
        public bool termaccepted { get; set; }
        public bool decline { get; set; }
        public bool acceptrequire { get; set; }
        public string token { get; set; }
        public int staus_id { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string hourlyrate { get; set; }
        public string background { get; set; }
        public string drug { get; set; }
        public string role { get; set; }
        public string staus { get; set; }
        public string staus_color { get; set; }
        public string skills { get; set; }
        public int memder_p_id { get; set; }
    }
    public class bank_item
    {
        public int bankid { get; set; }
        public string banktoken { get; set; }
        public string bankname { get; set; }
        public string branchname { get; set; }
        public string acc_name { get; set; }
        public string acc_number { get; set; }
        public string ifsccode { get; set; }
        public string pan_number { get; set; }
    }

    public class popupmodal
    {
        public int cityid { get; set; }
        public int stateid { get; set; }
       public int clientid { get; set; }
    }


}