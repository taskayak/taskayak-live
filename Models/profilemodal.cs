﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace hrm.Models
{
    public class profilemodal
    {
        public string name { get; set; }
        public string image { get; set; }
        public List<jobnotificationmodal> offerMessage { get; set; }
        public List<jobnotificationmodal> jobMessage { get; set; }
        public List<paynotificationmodal> payMessage { get; set; }
        public List<string> GeneralMessage { get; set; }
    }

    public class jobnotificationmodal
    {
        public int notificationId { get; set; }
        public string Url { get; set; }
        public int jobid { get; set; }
        public string name { get; set; }
        public string jobindexid { get; set; }
        public string message { get; set; }
        public string time { get; set; }
    }

    public class paynotificationmodal
    {
        public int payid { get; set; }
        public string Url { get; set; }
        public string jobindexid { get; set; }
        public string rate { get; set; }
        public string name { get; set; }
        public string hrs { get; set; }
        public string amt { get; set; }

    }


    public class changepassword:ErrorModel
    {
        [Required(ErrorMessage = "Please enter current password", AllowEmptyStrings = false)]
        public string currentpassword { get; set; }
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        [Required(ErrorMessage = "Please enter new password", AllowEmptyStrings = false)]
        public string password { get; set; }
        [Compare("password", ErrorMessage = "The password and confirm password does not match")]
        [DataType(DataType.Password)]
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        public string confirmpassword { get; set; }

    }
}