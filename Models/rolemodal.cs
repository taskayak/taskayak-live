﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace hrm.Models
{
  
    public class rolemodal : ErrorModel
    {
        public List<role_items> _roles { get; set; }
        [Required(ErrorMessage = "Please enter name.", AllowEmptyStrings = false)]
        public string role_name { get; set; }
        public int role_id { get; set; }
        public int statusid { get; set; }
    }
    public class rolecountmodal
    {
        public string name { get; set; }
        public int y { get; set; }
    }
    public class role_items
    {
        public string name { get; set; }
        public string status { get; set; }
        public int id { get; set; }
        public int status_id { get; set; }
    }


    public class permission:ErrorModel
    {
        public List<permissionitem> _permissions { get; set; }
        public List<int> _Userpermissions { get; set; }
        public int roleid { get; set; }
        public string name { get; set; }
    }
    public class permissionitem
    {
        public int id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
    }
}