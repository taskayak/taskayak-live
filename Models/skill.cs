﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace hrm.Models
{
    public class skill : ErrorModel
    {
        public List<skill_items> _skill { get; set; }
        [Required(ErrorMessage = "Please enter name.", AllowEmptyStrings = false)]
        public string skill_name { get; set; }
    }

    public class skill_items
    {
        public string name { get; set; }
        public string typename { get; set; }
        public int id { get; set; }
        public int typeid { get; set; }
    }
}