var $ = jQuery;

$(document).ready(function(){
	if ( $('.textarea-wysihtml5').length > 0 ) {
		$('.textarea-wysihtml5').wysihtml5({
			toolbar: {
				'fa': true
			}
		});
	}

    // var screenWidth=window.screen.width;
    // if ( screenWidth >= 1920){
    //     $('body').addClass("no-nav-animation left-bar-open");
    //     var handler = setTimeout(function(){
    //         $('body').removeClass("no-nav-animation");
    //         clearTimeout(handler);
    //     }, 200);
    // }

});


$('#left-nav .nav-bottom-sec').slimScroll({
	height: '100%',
	size: '4px',
	color: '#999'
});


$('#bar-setting').click(function (e) {
   
	e.preventDefault();
	if ( $(window).width() > 767 ) {
        if ($('body.has-left-bar').hasClass('left-bar-open')) {
            $('body.has-left-bar').removeClass('left-bar-open');
           
        } else {
            $('body.has-left-bar').addClass('left-bar-open');
         
        }
       
	} else {
		$('.left-nav-bar .nav-bottom-sec').slideToggle(1300, function(){
			$('body.has-left-bar').toggleClass('left-bar-open');
		});
	}

});



$('#left-navigation').find('li.has-sub>a').on('click', function(e){
	e.preventDefault();
	var $thisParent = $(this).parent();

	if ( $thisParent.hasClass('sub-open') ) {

		// Hide the Submenu
		$thisParent.removeClass('sub-open').children('ul.sub').slideUp(500);

	} else {

		// Show the Submenu
		$thisParent.addClass('sub-open').children('ul.sub').slideDown(500);

		// Hide Others Submenu
		$thisParent.siblings('.sub-open').removeClass('sub-open').children('ul.sub').slideUp(150);

	}
});


// alertify customize

alertify.warning = alertify.extend("warning");
alertify.info = alertify.extend("info");

// Tooltip init

$(function () {
    $('[data-toggle="tooltip"]').tooltip({
        trigger: 'hover'
    }) 

    $('[data-toggle="tooltip"]').on('click', function () {
    $(this).tooltip('hide')
})
});
// Form
(function(){

	$('.form-group.form-group-default .form-control').on('focus', function(e){
		$(this).closest('.form-group').addClass('focused');
	}).on('blur', function(e){
		var $closest = $(this).closest('.form-group');
		if ($(this).val().length > 0) {
			$closest.addClass('filled');
		} else {
			$closest.removeClass('filled');
		}
		$closest.removeClass('focused');
	});

	$('.form-group.form-group-default select.form-control').on('change', function(){
		$(this).closest('.form-group').addClass('filled');
    });
    $("#popuptable").on("click", ".parentcomment", function () {
        var msgid = $(this).data("id");
        var jobid = $(this).data("jobid");
        var isoffer = $(this).hasClass("offer");
        if (isoffer) {
            updateofferread(msgid);
        } else {
            updateread(msgid);
        }
        $(".childcommentcomment").css("display", "none");
        var _count = parseInt($("#commentbadge_" + jobid).html());
        if ($(this).hasClass("unread")) {
            _count = _count - 1;
        }
        $(this).removeClass("unread");
        if ($(this).next('.childcommentcomment').hasClass("hide")) {

            $(this).next('.childcommentcomment').removeClass("hide").addClass("show").removeClass("unread");
        } else {
            $(this).next('.childcommentcomment').removeClass("show").addClass("hide").removeClass("unread");
        }

        if (_count > 0) {
            $("#commentbadge_" + jobid).html(_count);
        } else {
            $("#outside_" + jobid).html('--').removeClass("glyphicon").removeClass("glyphicon-comment").removeClass("outside").removeClass("notification");
            $("#commentbadge_" + jobid).remove();
        }

    });

})();

//$(".parentcomment").on("click", function () {
//    debugger;
//    $(".childcommentcomment").css("display","none");
//    if ($(this).next('.childcommentcomment').hasClass("hide")) {
//        $(this).next('.childcommentcomment').removeClass("hide").addClass("show");
//    } else {
//        $(this).next('.childcommentcomment').removeClass("show").addClass("hide");
//    }
//});



$(".parentcomment").on("click", function () {
    var msgid = $(this).data("id");
    var jobid = $(this).data("jobid");
    var isoffer = $(this).hasClass("offer");
    if (isoffer) {
        updateofferread(msgid);
    } else {
        updateread(msgid);
    }
    $(".childcommentcomment").css("display", "none");
    var _count = parseInt($("#commentbadge_" + jobid).html());
    if ($(this).hasClass("unread")) {
        _count = _count - 1;
    }
    $(this).removeClass("unread");
    if ($(this).next('.childcommentcomment').hasClass("hide")) {

        $(this).next('.childcommentcomment').removeClass("hide").addClass("show").removeClass("unread");
    } else {
        $(this).next('.childcommentcomment').removeClass("show").addClass("hide").removeClass("unread");
    }
   
    if (_count > 0) {
        $("#commentbadge_" + jobid).html(_count);
    } else {
        $("#outside_" + jobid).html('--').removeClass("glyphicon").removeClass("glyphicon-comment").removeClass("outside").removeClass("notification");
        $("#commentbadge_" + jobid).remove();
    }

});

function updateread(id) {
    var _url = "http://localhost:50912/jobs/update_readstatus2";
    $.get(_url, { 'jobid': id }, function (

    ) { });
}

function updateofferread(id) {
    var _url = "http://localhost:50912/offer/update_readstatus";
    $.get(_url, { 'jobid': id }, function (

    ) { });
}




